import React from "react";
import { Redirect } from "react-router-dom";
// Layout Types
import { DefaultLayout } from "./layouts";
import { useAuth } from "./utils/Authentication";
import Audiences from "./views/Audiences";
import ChangePassword from "./views/ChangePassword";
import Dashboard from "./views/Dashboard";
import EventSummary from "./views/EventSummary";
import ForgotPassword from "./views/ForgotPassword";
import Login from "./views/Login";
import Membership from "./views/Membership";
import OAuthIntegration from "./views/OAuthIntegration";
import Register from "./views/Register";
import ResetPassword from "./views/ResetPassword";
import TeamProfile from "./views/TeamProfile";
import TemplateEditor from "./views/TemplateEditor";
import TemplateInstancePreview from "./views/TemplateInstancePreview";
import TemplatePreview from "./views/TemplatePreview";
import UserProfileLite from "./views/UserProfileLite";
import WebinarAnalytics from "./views/WebinarAnalytics";
import WebinarConfirmation from "./views/WebinarConfirmation";
import WebinarDetails from "./views/WebinarDetails";
import WebinarList from "./views/WebinarList";
import WebinarRegistration from "./views/WebinarRegistration";
import WebinarReplay from "./views/WebinarReplay";
import WebinarThankYou from "./views/WebinarThankYou";
import WebinarViewer from "./views/WebinarViewer";

const BlankIconSidebarLayout = ({ children, title }) => (
  <DefaultLayout noFooter title={title}>
    {children}
  </DefaultLayout>
);

const noSidebarLayout = ({ children, title }) => (
  <DefaultLayout noFooter noSideBar noMainNavBar title={title}>
    {children}
  </DefaultLayout>
);

const emptyLayout = ({ children }) => <div>{children}</div>;

const webinarDetailsLayout = ({ children, title }) => {
  return (
    <DefaultLayout noWebinar noFooter noSideBar noMainNavBar title={title}>
      {children}
    </DefaultLayout>
  );
};

const noNavBarLayout = ({ children, title }) => {
  return <DefaultLayout title={title}>{children}</DefaultLayout>;
};

const noFooterNoNavBarLayout = ({ children, title }) => {
  return (
    <DefaultLayout noFooter={true} title={title}>
      {children}
    </DefaultLayout>
  );
};

interface DashboardTitleProps {}

const DashboardTitle: React.FC<DashboardTitleProps> = () => {
  const { authUser } = useAuth();
  return (
    <span>
      Welcome back <strong>{authUser?.firstName}</strong>
    </span>
  );
};

export default [
  {
    path: "/",
    exact: true,
    layout: BlankIconSidebarLayout,
    component: () => <Redirect to="/dashboard" />,
  },
  {
    path: "/analytics",
    layout: noNavBarLayout,
    exact: true,
    title: "Webinars",
    component: WebinarAnalytics,
    protected: true,
  },
  {
    path: "/audiences",
    title: "Audience",
    layout: noNavBarLayout,
    component: Audiences,
    protected: true,
  },
  {
    path: "/analytics/:id",
    title: "Webinars",
    layout: noFooterNoNavBarLayout,
    component: WebinarAnalytics,
    protected: true,
  },
  {
    path: "/dashboard",
    layout: noNavBarLayout,
    component: Dashboard,
    protected: true,
    title: DashboardTitle,
  },
  {
    path: "/webinars",
    layout: noNavBarLayout,
    exact: true,
    component: WebinarList,
    title: "Webinars",
    protected: true,
  },
  {
    path: "/events/:id",
    title: "Event Summary",
    layout: noNavBarLayout,
    component: EventSummary,
    protected: true,
  },
  {
    path: "/webinars/:id/:route",
    layout: webinarDetailsLayout,
    component: WebinarDetails,
    protected: true,
  },
  {
    path: "/template/preview/global/:templateId",
    layout: noSidebarLayout,
    component: TemplatePreview,
    protected: true,
    exact: true,
  },
  {
    path: "/template/preview/instance/:instanceId",
    layout: noSidebarLayout,
    component: TemplateInstancePreview,
    protected: true,
    exact: true,
  },
  {
    path: "/editor/:templateId",
    layout: emptyLayout,
    component: TemplateEditor,
    protected: true,
    exact: true,
  },
  {
    path: "/team-settings",
    layout: BlankIconSidebarLayout,
    title: "Team Settings",
    component: TeamProfile,
    protected: true,
  },
  {
    path: "/account",
    layout: noNavBarLayout,
    title: "User Settings",
    component: UserProfileLite,
    protected: true,
  },
  {
    path: "/webinar/watch/:id",
    layout: noSidebarLayout,
    component: WebinarViewer,
    protected: false,
  },
  {
    path: "/webinar/replay/:id",
    layout: noSidebarLayout,
    component: WebinarReplay,
    protected: false,
  },
  {
    path: "/webinar/registration/:id",
    layout: noSidebarLayout,
    component: WebinarRegistration,
    protected: false,
  },
  {
    path: "/webinar/thankyou/:id",
    layout: noSidebarLayout,
    component: WebinarThankYou,
    protected: false,
  },
  {
    path: "/webinar/confirmation/:id",
    layout: noSidebarLayout,
    component: WebinarConfirmation,
    protected: false,
  },
  {
    path: "/login",
    layout: noSidebarLayout,
    component: Login,
    protected: false,
  },
  {
    path: "/register",
    layout: noSidebarLayout,
    component: Register,
    protected: false,
  },
  {
    path: "/membership-details",
    layout: noSidebarLayout,
    component: Membership,
    protected: false,
  },
  {
    path: "/forgot-password",
    layout: noSidebarLayout,
    component: ForgotPassword,
    protected: false,
  },
  {
    path: "/reset-password",
    layout: noSidebarLayout,
    component: ResetPassword,
    protected: false,
  },
  {
    path: "/change-password",
    layout: BlankIconSidebarLayout,
    component: ChangePassword,
    protected: true,
  },
  {
    path: "/oauth/:integration",
    layout: noSidebarLayout,
    component: OAuthIntegration,
    protected: false,
  },
];
