import React, { useCallback, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import {
  Card,
  CardBody,
  Col,
  Container,
  Form,
  FormFeedback,
  FormGroup,
  FormInput,
  Row,
} from "shards-react";
import LoaderButton from "../components/common/LoaderButton";
import background from "../images/bg-onboarding.png";
import { useHttpService } from "../utils/HttpService";

const ForgotPassword = () => {
  const [email, setEmail] = useState("");
  const api = useHttpService();
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  const [helperText, setHelperText] = useState("");
  const [error, setError] = useState(false);
  const [requestStatus, setRequestStatus] = useState("");

  useEffect(() => {
    if (email.trim()) {
      setIsButtonDisabled(false);
    } else {
      setIsButtonDisabled(true);
    }
  }, [email]);

  const handleReset = useCallback(
    async (event) => {
      event.preventDefault();
      try {
        setRequestStatus("request");
        setIsButtonDisabled(true);
        await api.doForgotPassword({ email });
        setRequestStatus("success");
        setIsButtonDisabled(false);
        // history.push({
        //   pathname: "/login",
        //   state: { event: { title: "forgot-password", message: "test" } },
        // });
      } catch (error) {
        setRequestStatus("error");
        setIsButtonDisabled(false);
        setError(true);
        setHelperText(error.message);
      }
    },
    [api, email]
  );

  return (
    <Container
      fluid
      className="main-content-container"
      style={{ height: "100vh" }}
    >
      <Row className="h-100">
        <Col lg="5" md="5" className="h-100">
          <Row className="h-100">
            <Col></Col>
            <Col xs={12} className="my-auto">
              <Card className="bg-transparent no-shadow px-5">
                <CardBody>
                  {/* Logo */}
                  <img
                    className="auth-form__logo d-table mx-auto mb-3"
                    src={require("../images/Large_Icon.svg")}
                    alt="Shards Dashboards - Register Template"
                  />

                  {/* Title */}
                  <h3 className=" font-weight-light  text-center mb-4">
                    Reset Password
                  </h3>

                  {/* Form Fields */}
                  {requestStatus === "success" ? (
                    <div className="font-weight-light text-center">
                      Password reset email sent.
                    </div>
                  ) : (
                    <Form>
                      <FormGroup>
                        <label
                          className="text-secondary"
                          htmlFor="exampleInputEmail1"
                        >
                          Email address
                        </label>
                        <FormInput
                          type="email"
                          id="exampleInputEmail1"
                          placeholder="Enter email"
                          autoComplete="email"
                          invalid={error}
                          size="lg"
                          onChange={(e) => setEmail(e.target.value)}
                        />
                        <FormFeedback>{helperText}</FormFeedback>
                        <small className="form-text text-muted text-center">
                          You will receive an email with a unique token.
                        </small>
                      </FormGroup>
                      <LoaderButton
                        block
                        loading={requestStatus === "request"}
                        theme="accent"
                        className="d-table mx-auto"
                        type="submit"
                        disabled={isButtonDisabled}
                        onClick={(e) => handleReset(e)}
                      >
                        Reset Password
                      </LoaderButton>
                    </Form>
                  )}
                  <br />
                  <div className="auth-form__meta d-flex mb-0">
                    <Link to="/login" className="mx-auto">
                      Back
                    </Link>
                  </div>
                </CardBody>
              </Card>
            </Col>
            <Col></Col>
          </Row>
        </Col>
        <Col
          sm={12}
          lg={7}
          className="d-none d-sm-block"
          style={{
            backgroundImage: `url('${background}')`,
            backgroundSize: "contain",
            backgroundPositionY: "bottom",
            backgroundPositionX: "center",
            backgroundRepeat: "no-repeat",
          }}
        ></Col>
      </Row>
    </Container>
  );
};
export default ForgotPassword;
