/* eslint jsx-a11y/anchor-is-valid: 0 */

/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, { useCallback, useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";
// APIs
import { Link, useHistory } from "react-router-dom";
// Stripe
import { Elements, StripeProvider } from "react-stripe-elements";
import StripeScriptLoader from "react-stripe-script-loader";
import {
  Badge,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  FormCheckbox,
  FormGroup,
  FormRadio,
  Row,
} from "shards-react";
import StripeCard from "../components/Stripe";
import background from "../images/bg-onboarding.png";
import { useAuth } from "../utils/Authentication";
import { ROLES, ROUTES, STRIPE } from "../utils/constants";
import { useStripeService } from "../utils/Stripe";

const { REACT_APP_STRIPE_PUBLISHABLE_KEY } = process.env;
const tiers = [
  {
    nickname: STRIPE.FREE,
    role: ROLES.FREE,
    color: "success",
    // discount: "14 days trail",
  },
  {
    nickname: STRIPE.BASIC,
    role: ROLES.BASIC,
    color: "secondary",
    // discount: "20% off",
  },
  {
    nickname: STRIPE.PREMIUM,
    role: ROLES.PREMIUM,
    color: "primary",
    // discount: "20% off",
  },
  {
    nickname: STRIPE.ENTERPRISE,
    role: ROLES.ENTERPRISE,
    color: "info",
    // discount: "20% off",
  },
];

const sleep = (milliseconds: number) => {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
};

const tierNicknames = tiers.map((o) => o.nickname);
const tierSort = (a, b) =>
  tierNicknames.indexOf(a.name) > tierNicknames.indexOf(b.name);
// const getTier = (name) => tiers.find((x) => x.nickname === name);

const Membership = () => {
  const { register } = useAuth();
  const history = useHistory();
  const stripe = useStripeService();

  const user = history.location.state ? history.location.state : ({} as any);
  useEffect(() => {
    if (typeof user.firstName === "undefined") {
      history.push("/register");
    }
  }, [user, history]);

  const [product, setProduct] = useState<any>();
  // const [organization, setOrganization] = useState<any>();
  const [billingInterval, setBillingInterval] = useState<any>("month");

  const [productList, setProductList] = useState<any>();
  const [title, setTitle] = useState<any>("Choose Membership Plan");

  const [error, setError] = useState("");
  const [loading, setLoading] = useState(true);

  const getStripePlans = useCallback(async () => {
    setLoading(true);
    try {
      let plans = await stripe.getPlans();
      plans = plans.sort(tierSort);
      setProduct(plans[0]);
      setProductList(plans);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  }, [stripe]);

  useEffect(() => {
    if (!history.location.state) history.push(ROUTES.REGISTER);
    else getStripePlans();
  }, [history, getStripePlans]);

  const handleSubscription = useCallback(
    async (event, coupon) => {
      if (event.error) {
        return;
      }

      setLoading(true);
      const token = event.token.id;

      try {
        const price = product.prices.filter(
          (x) => x.recurring.interval === billingInterval
        )[0];
        setTitle("Creating Account");
        const { organization, firstName, lastName, email, password } = user;
        const response = await register({
          firstName,
          lastName,
          email,
          password,
          teamName: organization,
          source: token,
          price: price.id,
          coupon,
        });
        console.log(response);

        setTitle("Welcome To The Community");

        // TODO: Welcome Transition
        // await sleep(5000);

        // Do a hard transition to avoid any stale cache issues
        (window as any).location = ROUTES.DASHBOARD;
        // setLoading(false);
      } catch (error) {
        console.log(error);
        setTitle("Oops, could not complete registration. Please try again.");
        await sleep(3000);
        setTitle("Choose Membership Plan");
        setError(error?.response?.data?.message ?? error.message);
        setLoading(false);
        // TODO: Error Handling Component
        // May disable created user
      }
    },
    [user, product, register, billingInterval]
  );

  return (
    <Container
      fluid
      className="membership-details main-content-container p-0"
      style={{ height: "100vh" }}
    >
      <Row noGutters className="h-100">
        <Col lg="5" md="5" className="h-100">
          <Row className="h-100">
            <Col></Col>
            <Col xs={8} className="my-auto">
              <Card className="bg-transparent no-shadow">
                <CardBody>
                  {/* Logo */}
                  <img
                    className="auth-form__logo d-table mx-auto mb-3"
                    src={require("../images/Large_Icon.svg")}
                    alt="Shards Dashboards - Register Template"
                  />

                  {/* Title */}
                  <h3 className="font-weight-light text-center mb-4">
                    {title}
                  </h3>

                  {loading ? (
                    <Row>
                      <Col className="mx-auto text-center">
                        <Spinner
                          as="span"
                          animation="border"
                          role="status"
                          aria-hidden="true"
                        />
                      </Col>
                    </Row>
                  ) : (
                    <>
                      {error && (
                        <div className="text-center mb-4">
                          <span className="text-danger">{error}</span>
                        </div>
                      )}
                      <Row className="mb-4">
                        <Col className="text-center">
                          <span className="mr-2">Monthly</span>
                          <FormCheckbox
                            toggle
                            small
                            className="d-inline"
                            checked={billingInterval !== "month"}
                            onChange={() =>
                              setBillingInterval(() =>
                                billingInterval === "month" ? "year" : "month"
                              )
                            }
                          ></FormCheckbox>
                          <span className="ml-2">Yearly</span>
                        </Col>
                      </Row>
                      <Form>
                        <FormGroup row>
                          {productList &&
                            productList.map((tier) => (
                              // Enterprise card is full width at sm breakpoint
                              <Col
                                lg={12}
                                md={12}
                                sm={12}
                                key={tier.name}
                                className="my-2"
                              >
                                <FormRadio
                                  name="productType"
                                  checked={product.name === tier.name}
                                  onChange={() => setProduct(tier)}
                                >
                                  <Card
                                    className={`product no-shadow ${
                                      product.name === tier.name && "active"
                                    }`}
                                  >
                                    <CardBody className="p-3">
                                      <Row>
                                        <Col lg={6} md={6}>
                                          {tier.name}
                                          <br />
                                          <Badge
                                            pill
                                            theme={`${
                                              product.name === tier.name
                                                ? "info"
                                                : "primary"
                                            }`}
                                          >
                                            {/* {getTier(tier.name)?.discount} */}
                                          </Badge>
                                        </Col>
                                        <Col
                                          lg={6}
                                          md={6}
                                          className="text-right"
                                        >
                                          <h4 className="d-inline-block font-weight-bold">
                                            $
                                            {tier.prices
                                              .filter(
                                                (x) =>
                                                  x.recurring.interval ===
                                                  billingInterval
                                              )[0]
                                              .amount_usd.toLocaleString()}{" "}
                                          </h4>
                                          <small
                                            style={{
                                              textTransform: "capitalize",
                                            }}
                                          >{`/${billingInterval}`}</small>
                                        </Col>
                                      </Row>
                                    </CardBody>
                                  </Card>
                                </FormRadio>
                              </Col>
                            ))}
                        </FormGroup>
                      </Form>
                      <Col lg="12" md="12" sm="12" className="text-center p-0">
                        <StripeScriptLoader
                          uniqueId="myUniqueId"
                          script="https://js.stripe.com/v3/"
                          loader="Loading..."
                        >
                          <StripeProvider
                            apiKey={REACT_APP_STRIPE_PUBLISHABLE_KEY}
                          >
                            <Elements>
                              <StripeCard handleResult={handleSubscription} />
                            </Elements>
                          </StripeProvider>
                        </StripeScriptLoader>
                      </Col>
                      <div className="auth-form__meta d-flex mb-0 mt-4">
                        <Link to="/register" className="mx-auto">
                          Back
                        </Link>
                      </div>
                    </>
                  )}
                </CardBody>
              </Card>
            </Col>
            <Col></Col>
          </Row>
        </Col>
        <Col
          sm={12}
          lg={7}
          className="d-none d-sm-block"
          style={{
            backgroundImage: `url('${background}')`,
            backgroundSize: "contain",
            backgroundPositionY: "bottom",
            backgroundPositionX: "center",
            backgroundRepeat: "no-repeat",
          }}
        ></Col>
      </Row>
    </Container>
  );
};

export default Membership;
