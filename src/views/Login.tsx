/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, { useCallback, useState } from "react";
import {
  Alert,
  Form,
  FormControl,
  FormGroup,
  FormLabel,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import {
  Card,
  CardBody,
  Col,
  Container,
  FormCheckbox,
  Row,
} from "shards-react";
import LoaderButton from "../components/common/LoaderButton";
import background from "../images/bg-onboarding.png";
import { useAuth } from "../utils/Authentication";
import { ROUTES } from "../utils/constants";

const validationErrors = {
  error: "Please enter valid credentials",
};

const LoginForm: React.FC = () => {
  const { login } = useAuth();

  const [email, setEmail] = useState<any>("");
  const [password, setPassword] = useState<any>("");

  const [error, setError] = useState<string>("");

  const [validated, setValidated] = useState(false);
  const [loading, setLoading] = useState(false);
  const [rememberMe, setRememberMe] = useState(false);

  const handleSubmit = useCallback(
    async (event) => {
      const form = event.currentTarget;
      event.preventDefault();
      event.stopPropagation();

      if (form["email"].checkValidity() && form["password"].checkValidity()) {
        try {
          setLoading(true);
          await login(email, password, rememberMe);
          (window as any).location = ROUTES.DASHBOARD;
        } catch (error) {
          setError(error.response?.data?.message ?? error.message);
        } finally {
          setLoading(false);
        }
      }

      setValidated(true);
    },
    [login, email, password, rememberMe]
  );
  return (
    <Card className="bg-transparent no-shadow">
      <CardBody>
        {/* Logo */}
        <h3 className="font-weight-light">Welcome Back</h3>

        {/* Form Fields */}
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
          {error && (
            <Alert
              variant="danger"
              dismissible
              onClose={() => setError("")}
              data-testid="login-error"
            >
              {error}
            </Alert>
          )}
          <FormGroup>
            <FormLabel>Email</FormLabel>
            <FormControl
              type="email"
              name="email"
              placeholder="Enter email"
              autoComplete="email"
              size="lg"
              required
              isInvalid={!email.match(/.+@.+/)}
              onChange={(e) => {
                setEmail(e.target.value);
              }}
            />
          </FormGroup>
          <FormGroup>
            <FormLabel>Password</FormLabel>
            <FormControl
              type="password"
              name="password"
              autoComplete="current-password"
              size="lg"
              required
              onChange={(e) => setPassword(e.target.value)}
            />
            <FormControl.Feedback type="invalid">
              {validationErrors.error}
            </FormControl.Feedback>
          </FormGroup>
          <Row className="mt-2">
            <Col>
              <FormGroup>
                <FormCheckbox
                  checked={rememberMe}
                  onChange={() => setRememberMe(!rememberMe)}
                >
                  <span className="ml-3">Remember me.</span>
                </FormCheckbox>
              </FormGroup>
            </Col>
            <Col className="text-right mt-1">
              <Link to="/forgot-password">Forgot password?</Link>
            </Col>
          </Row>
          <LoaderButton type="submit" loading={loading}>
            Sign In
          </LoaderButton>
        </Form>
      </CardBody>
    </Card>
  );
};

const Login: React.FC = () => {
  return (
    <Container
      fluid
      className="main-content-container p-0"
      style={{ height: "100vh" }}
    >
      <Row noGutters className="h-100">
        <Col lg="5" md="10" xs={12} className="h-100">
          <Row className="h-100">
            <Col></Col>
            <Col xs={12} lg={8} className="my-auto">
              <LoginForm></LoginForm>
            </Col>
            <Col></Col>
          </Row>
          {/* Meta Details */}
          <Row className="position-absolute w-100" style={{ bottom: "5%" }}>
            <Col></Col>
            <Col xs={8} className="text-center">
              <img
                className="auth-form__logo mr-3"
                src={require("../images/Large_Icon.svg")}
                alt="Shards Dashboards - Login Template"
              />
              Don't have an account yet?
              <Link to="/register" className="ml-2">
                Sign Up
              </Link>
            </Col>
            <Col></Col>
          </Row>
        </Col>
        <Col
          sm={12}
          lg={7}
          className="d-none d-sm-block"
          style={{
            backgroundImage: `url('${background}')`,
            backgroundSize: "contain",
            backgroundPositionY: "bottom",
            backgroundPositionX: "center",
            backgroundRepeat: "no-repeat",
          }}
        ></Col>
      </Row>
    </Container>
  );
};

export default Login;
