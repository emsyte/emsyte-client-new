import React, { useState, useCallback, useEffect } from "react";
import { Container, Row, Col, Button, FormInput } from "shards-react";
import { AttendeeObject } from "@emsyte/common/attendee";
import Swal from "sweetalert2";
import { debounce } from "lodash";
import withReactContent from "sweetalert2-react-content";
import { AgGridReact, AgGridColumn } from "ag-grid-react";
import PageTitle from "../components/common/PageTitle";
import AudienceModal from "../components/audience-modal/AudienceModal";
import { useHttpService } from "../utils/HttpService";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";

export type AudienceDataModel = {
  name: string;
  email: string;
  phone: string;
};

const MySwal = withReactContent(Swal);

const Audiences = () => {
  const api = useHttpService();
  const [gridApi, setGridApi] = useState<any>(null);
  const [audiences, setAudiences] = React.useState<AudienceDataModel[]>([]);
  const [search, setSearch] = useState<any>("");

  const handleGetAudiences = useCallback(async () => {
    try {
      const audiencesResponse = await api.doGetAudience();
      setAudiences(
        audiencesResponse.data.map((audience: AttendeeObject) => ({
          name: [audience.firstName, audience.lastName].join(" "),
          email: audience.email,
          phone: audience.phone,
        }))
      );
    } catch (error) {
      console.log(error);
    }
  }, [api]);

  const onSearch = debounce((e) => {
    setSearch(e.toLocaleLowerCase());
  }, 300);

  useEffect(() => {
    const fetchData = async () => await handleGetAudiences();
    fetchData();
  }, [handleGetAudiences]);

  const onGridReady = (params) => {
    setTimeout(() => {
      setGridApi(params.api);
      params.api.sizeColumnsToFit();
    }, 0);
  };

  const comparator = (valueA, valueB, nodeA, nodeB, isInverted) => {
    valueA = valueA.toLowerCase();
    valueB = valueB.toLowerCase();

    if (valueA === valueB) {
      return 0;
    } else {
      return valueA > valueB ? 1 : -1;
    }
  };

  return (
    <Container fluid className="main-content-container px-4">
      <Row noGutters className="mb-4 page-header">
        <Col sm={8} className="d-flex justify-content-end">
          <FormInput
            placeholder="Search..."
            onChange={(e) => onSearch(e.target.value)}
            style={{ height: "40px", marginTop: "12px", marginRight: 15 }}
          />
          <Button
            theme="dark"
            outline
            style={{ height: "40px", marginTop: "12px" }}
            onClick={(e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
              e.preventDefault();
              e.stopPropagation();
              gridApi?.exportDataAsCsv({
                columnKeys: ["name", "email", "phone"],
                fileName: "export.csv",
              });
            }}
          >
            Export as CSV
          </Button>
        </Col>
      </Row>
      <Row noGutters>
        <Col
          className="ag-theme-alpine"
          style={{ width: "100%", height: "772px" }}
        >
          <AgGridReact
            defaultColDef={{
              minWidth: 150,
              cellStyle: { border: "none" },
            }}
            rowData={audiences.filter(
              (audience) =>
                audience?.email.toLocaleLowerCase().indexOf(search) > -1 ||
                audience?.phone.toLocaleLowerCase().indexOf(search) > -1 ||
                audience?.name.toLocaleLowerCase().indexOf(search) > -1
            )}
            onGridReady={onGridReady}
            onGridColumnsChanged={onGridReady}
            pagination={true}
            paginationAutoPageSize={true}
          >
            <AgGridColumn
              field="name"
              headerName="Name"
              sortable
              comparator={comparator}
            ></AgGridColumn>
            <AgGridColumn
              field="email"
              headerName="Email"
              sortable
              comparator={comparator}
            ></AgGridColumn>
            <AgGridColumn
              field="phone"
              headerName="Phone"
              sortable
              comparator={comparator}
            ></AgGridColumn>
            <AgGridColumn
              headerName=""
              cellRendererFramework={(params) => {
                return (
                  <button
                    style={{
                      border: "none",
                      background: "none",
                      outline: "none",
                      color: "#007bff",
                    }}
                    onClick={(
                      e: React.MouseEvent<HTMLButtonElement, MouseEvent>
                    ) => {
                      e.preventDefault();
                      e.stopPropagation();
                      MySwal.fire({
                        title: "Audience Activity",
                        html: (
                          <AudienceModal
                            email={params.data.email}
                            attendee={params.data}
                            fetchAudienceDetails={api.doGetAudienceDetails}
                          />
                        ),
                        showCloseButton: true,
                        showConfirmButton: false,
                      });
                    }}
                  >
                    View Activity
                  </button>
                );
              }}
            ></AgGridColumn>
          </AgGridReact>
        </Col>
      </Row>
    </Container>
  );
};

export default Audiences;
