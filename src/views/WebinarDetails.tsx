import classnames from "classnames";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { Spinner } from "react-bootstrap";
import { Link, useHistory, useParams } from "react-router-dom";
import { Col, Container, Row } from "shards-react";
import LoaderButton from "../components/common/LoaderButton";
import MainFooter from "../components/layout/MainFooter";
import BasicDetail from "../components/webinar/edit/BasicDetail";
import Chats from "../components/webinar/edit/Chats";
import Monetize from "../components/webinar/edit/Monetize";
import Notifications from "../components/webinar/edit/Notifications";
import Preview from "../components/webinar/edit/Preview";
import Templates from "../components/webinar/edit/Templates";
import { ReactComponent as LogoWords } from "../images/justWebinarWords.svg";
import { ReactComponent as Logo } from "../images/Large_Icon.svg";
import { apiWrapper } from "../utils/api_helpers";
import colors from "../utils/colors";
import { useHttpService } from "../utils/HttpService";

const BASIC = "basic";
const TEMPLATES = "templates";
const NOTIFICATIONS = "notifications";
const MONETIZE = "monetize";
const CHATS = "chats";
// const ADVANCED = "advanced";
const PREVIEW = "preview";
const BACK = "back";

const map = {};
map[BASIC] = "Webinar Info";
map[TEMPLATES] = "Templates";
map[NOTIFICATIONS] = "Notifications";
map[MONETIZE] = "Monetization";
map[CHATS] = "Live Chat";
map[PREVIEW] = "Summary";

// This must go together, because are linked by their index
const screens = [
  BASIC,
  TEMPLATES,
  NOTIFICATIONS,
  MONETIZE,
  // ADVANCED,
  CHATS,
  PREVIEW,
];
const steps = [
  "Webinar Info",
  "Templates",
  "Notifications",
  "Monetization",
  // "Advanced",
  "Live Chat",
  "Summary",
];

interface theProps {
  currentScreen: string;
  enableLinks: boolean;
}

const Bar: React.FC<theProps> = ({ currentScreen, enableLinks }) => {
  return (
    <React.Fragment>
      <div className="webinar-creation-navbar">
        <Link
          to="/webinars"
          className="webinar-creation-back"
          style={{ flex: "0 0 33.3%" }}
        >
          <i className="bi bi-chevron-left" />
          <span className="d-none d-sm-inline">Back</span>
        </Link>
        <div style={{ flex: "0 1 33.3%", textAlign: "center" }}>
          <Logo height="28px" width="30px" />
          <LogoWords height="25px" width="110px" />
        </div>
        <div style={{ flex: "0 2 33.3%" }} />
        <div />
      </div>
      <div className="webinar-creation-steps-navbar">
        {steps.map((step, stepIndex) => {
          const screenIndex = screens.indexOf(currentScreen);
          return (
            <React.Fragment key={step}>
              <div
                className={
                  screenIndex < stepIndex
                    ? "step-future"
                    : screenIndex > stepIndex
                    ? "step-past"
                    : "step-present"
                }
              />

              {enableLinks ? (
                <Link
                  to={screens[stepIndex]}
                  className={classnames(
                    screenIndex !== stepIndex && "d-none d-lg-block d-xl-block",
                    "step-link"
                  )}
                >
                  {step}
                </Link>
              ) : (
                <span
                  className={classnames(
                    screenIndex !== stepIndex && "d-none d-lg-block d-xl-block"
                  )}
                >
                  {step}
                </span>
              )}

              {stepIndex < steps.length - 1 && (
                <div
                  className={
                    screenIndex <= stepIndex ? "line dotted" : "line solid"
                  }
                />
              )}
            </React.Fragment>
          );
        })}
      </div>
    </React.Fragment>
  );
};

function getWebinarRoute(encId: string): {
  id: number | null;
} {
  try {
    return JSON.parse(atob(encId));
  } catch {
    return { id: null };
  }
}

const WebinarDetails = () => {
  const { id: encId, route: _route } = useParams<{
    id: string;
    route: string;
  }>();
  const { id } = getWebinarRoute(encId);

  const api = useHttpService();
  const history = useHistory();
  const route = _route ?? "details";
  const saveRef = useRef(() => {});

  const setRoute = (route: string) => history.push(route);

  const [error, setError] = useState(false);

  const [webinar, setWebinar] = useState<any>({
    id,
  });
  const [loading, setLoading] = useState(true);

  const handleGetWebinar = useCallback(
    async (id: number) => {
      try {
        let { data } = await api.doGetWebinar(id);
        setWebinar(data);
      } catch (err) {
        setError(err.apiResponse?.message ?? err.message);
      }
    },
    [api]
  );

  const handleUpdateWebinar = async () => {
    await handleGetWebinar(webinar.id);
  };
  useEffect(() => {
    if (!id) {
      setLoading(false);
      return;
    }
    apiWrapper(() => handleGetWebinar(id), setLoading);
  }, [handleGetWebinar, id]);

  const actualRoute = route.toString().toLowerCase();

  const renderView = () => {
    switch (actualRoute) {
      case BASIC:
        return (
          <BasicDetail
            webinar={webinar}
            onWebinarUpdate={handleUpdateWebinar}
            onNext={() => setRoute(TEMPLATES)}
            saveRef={saveRef}
          />
        );
      case TEMPLATES:
        return (
          <Templates
            webinar={webinar}
            handleUpdateWebinar={handleUpdateWebinar}
            onNext={() => setRoute(NOTIFICATIONS)}
            saveRef={saveRef}
          />
        );
      case NOTIFICATIONS:
        return (
          <div className="main-content-container p-4">
            <div className="d-flex flex-column">
              <div className="col-lg-10 col-md-11 col-sm-12 align-self-center">
                <Notifications
                  webinar={webinar}
                  onWebinarUpdate={handleUpdateWebinar}
                  onNext={() => setRoute(MONETIZE)}
                  saveRef={saveRef}
                />
              </div>
            </div>
          </div>
        );
      case MONETIZE:
        return (
          <Monetize
            webinar={webinar}
            onWebinarUpdate={handleUpdateWebinar}
            saveRef={saveRef}
            onNext={() => setRoute(CHATS)}
          />
        );
      case CHATS:
        return (
          <Chats
            webinar={webinar}
            onNext={() => setRoute(PREVIEW)}
            saveRef={saveRef}
          />
        );
      // case ADVANCED:
      //   return (
      //     <Advanced
      //       webinar={webinar}
      //       onWebinarUpdate={handleUpdateWebinar}
      //       onNext={() => setRoute(PREVIEW)}
      //       saveRef={saveRef}
      //     />
      //   );
      case PREVIEW:
        return <Preview webinar={webinar} />;
      case BACK:
        history.push("/webinars");
        return (
          <BasicDetail
            webinar={webinar}
            onWebinarUpdate={handleUpdateWebinar}
            onNext={() => setRoute(TEMPLATES)}
            saveRef={saveRef}
          />
        );
      default:
        return (
          <BasicDetail
            webinar={webinar}
            onWebinarUpdate={handleUpdateWebinar}
            onNext={() => setRoute(TEMPLATES)}
            saveRef={saveRef}
          />
        );
    }
  };

  const [saving, setSaving] = useState(false);
  const save = async () => {
    setSaving(true);
    try {
      await saveRef.current();
    } finally {
      setSaving(false);
    }
  };

  const publish = async () => {
    setSaving(true);
    try {
      await api.doActivateWebinar(webinar.id);
      history.push("/webinars");
    } finally {
      setSaving(false);
    }
  };

  return (
    <Container
      fluid
      className="main-content-container px-0 webinar-creation-wrapper"
      style={{ backgroundColor: colors.notWhite.toHex() }}
    >
      {loading ? (
        <Row style={{ height: "75vh" }}>
          <Col className="text-center my-auto">
            <Spinner
              as="span"
              animation="border"
              role="status"
              aria-hidden="true"
              className="m-auto"
            />
          </Col>
        </Row>
      ) : error ? (
        <Row style={{ height: "75vh" }}>
          <Col className="text-center my-auto">Failed to load</Col>
        </Row>
      ) : (
        <React.Fragment>
          <Bar
            currentScreen={route.toString().toLowerCase()}
            enableLinks={typeof id === "number"}
          />
          <div className="webinar-creation-viewport">
            {renderView()}
            <MainFooter />
          </div>
          <div className="webinar-creation-actions d-flex justify-content-end">
            <LoaderButton
              loading={saving}
              variant="outline-secondary"
              style={{ marginRight: "0.675rem", fontSize: "1rem" }}
              onClick={async () => {
                await save();
                setRoute(BACK);
              }}
            >
              Save and Close
            </LoaderButton>
            {actualRoute === PREVIEW ? (
              <LoaderButton
                loading={saving}
                variant="success"
                onClick={async () => {
                  await publish();
                }}
                style={{ fontSize: "1rem" }}
              >
                Save and Publish
              </LoaderButton>
            ) : (
              <LoaderButton
                loading={saving}
                onClick={async () => {
                  await save();
                  // setRoute(getNext(actualRoute));
                }}
                style={{ fontSize: "1rem" }}
              >
                Save and Continue
              </LoaderButton>
            )}
          </div>
        </React.Fragment>
      )}
    </Container>
  );
};

export default WebinarDetails;
