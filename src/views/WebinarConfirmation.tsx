import { TemplateType } from "@emsyte/common/template";
import React from "react";
import { useLocation } from "react-router-dom";
import { Container, Row } from "shards-react";
import { useWebinarTemplate } from "../components/templates/hooks";
import TemplateLoading from "../components/templates/TemplateLoading";

export interface DefaultWebinarConfirmationProps {
  webinarId: number;
}

const DefaultWebinarConfirmation: React.FC<DefaultWebinarConfirmationProps> = () => {
  return (
    <Container>
      <Row>Thank you for registering!</Row>
    </Container>
  );
};

const WebinarConfirmation: React.FC = () => {
  const { search } = useLocation();

  const { loading, webinarId } = useWebinarTemplate(
    TemplateType.CONFIRMATION,
    search
  );

  if (loading) {
    return <TemplateLoading></TemplateLoading>;
  }

  // Fallback registration view
  return (
    <DefaultWebinarConfirmation
      webinarId={webinarId}
    ></DefaultWebinarConfirmation>
  );
};

export default WebinarConfirmation;
