import { WebinarType } from "@emsyte/common/webinar";
import _ from "lodash";
import moment from "moment";
import "moment-timezone";
import React, { useEffect, useState } from "react";
import { Button, Card, Jumbotron, Modal, Spinner } from "react-bootstrap";
import { useInfiniteQuery, useQuery } from "react-query";
import { Link } from "react-router-dom";
import { Col, Container, Row } from "shards-react";
import styled from "styled-components";
import DashboardStats, {
  chartColors,
} from "../components/analytics/DashboardStats";
import TopWebinars from "../components/analytics/TopWebinars";
import Day, {
  fetchEventsInfinite,
  INFINITE_EVENTS_QUERY_KEY,
} from "../components/calendar/views/Day";
import RangePicker from "../components/range_picker";
import illustrationCaution from "../images/illustration-caution.svg";
import illustrationCogwheels from "../images/illustration-cogwheels.svg";
import illustrationWebinar from "../images/illustration-webinar.svg";
import { ReactComponent as Logo } from "../images/just-webinar-words-custom-color.svg";
import { useAuth } from "../utils/Authentication";
import { useChart } from "../utils/hooks/chart";
import { useHttpService } from "../utils/HttpService";

const newWebinarUrl = () => {
  const buff = Buffer.from(
    JSON.stringify({ id: null, type: WebinarType.RECORDED })
  );
  return `/webinars/${buff.toString("base64")}/basic`;
};

const containerHeight = 12.5;
const containerWidth = 13.125;

const AlertContainer = styled.div`
  width: ${containerWidth}rem;
  height: ${containerHeight}rem;
  margin-left: ${-containerWidth / 2}rem;
  margin-top: ${-containerHeight / 4}rem;
  position: absolute;
  top: 50%;
  left: 50%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

const ModuleContainer = styled.div`
  height: 23.75rem;
`;

const ModuleTitle = styled.span`
  color: #2e3338;
  text-transform: uppercase;
`;

const ModuleHeader = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 1rem 1.25rem 0 1.25rem;
`;

const ModuleBody = styled.div`
  padding-top: 0;
  padding-left: 1.25rem;
  padding-right: 1.25rem;
  overflow: auto;
`;

const CenteredContainer = styled.div`
  font-size: 1rem;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const NewUserModalContainer = styled.div`
  color: #2e3338;
  letter-spacing: 1.2px;
`;

const NewUserModalHeader = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  width: 100%;
  padding-top: 2.5rem;
`;

const NewUserModalName = styled.h5`
  text-align: center;
  color: #2e3338;
`;

const NewUserModalGreeting = styled.h5`
  text-align: center;
  font-size: 2rem;
`;

const ChartContainer = styled.div`
  height: 25rem;
  width: 100%;
  padding: 0 1.25rem;
`;

const AlertText = styled.span`
  font-size: 1rem;
`;

export interface DashboardChartProps {
  data: Record<string, number[]>;
  color: string;
  showNotEnoughData?: boolean;
  showProcessig?: boolean;
}

const DashboardChart: React.FC<DashboardChartProps> = ({
  data,
  color,
  showNotEnoughData = false,
  showProcessig = false,
}) => {
  const config = {
    type: "line",
    data: {
      labels: new Array(data.length),
      datasets: [
        {
          label: "REGISTRATION PAGE VIEWS",
          fill: false,
          borderColor: color,
          data: data,
        },
      ],
    },
    options: {
      tooltips: {
        enabled: false,
        custom: false,
      },
      legend: false,
      elements: {
        point: {
          radius: 0,
        },
        line: {
          tension: 0.33,
        },
      },
      scales: {
        xAxes: [
          {
            gridLines: false,
            ticks: {
              display: false,
            },
          },
        ],
      },
      responsive: true,
      maintainAspectRatio: false,
    },
  };
  const { chart, canvasRef } = useChart(config);

  useEffect(() => {
    if (!chart) {
      return;
    }

    chart.update();
  }, [chart]);

  useEffect(() => {
    if (!chart) {
      return;
    }

    // @ts-ignore
    if (data.every((number) => number === 0)) {
      chart.data = { datasets: [] };
    } else chart.data = config.data;
    chart.update();
    // eslint-disable-next-line
  }, [data, chart]);

  return (
    <div className="h-100 w-100">
      <canvas ref={canvasRef} />
      {showNotEnoughData && (
        <AlertContainer>
          <img src={illustrationCaution} alt="illustration caution" />
          <AlertText style={{ textAlign: "center" }}>
            There isn’t enough data to show you anything yet.
          </AlertText>
        </AlertContainer>
      )}
      {!showNotEnoughData && showProcessig && (
        <AlertContainer>
          <img src={illustrationCogwheels} alt="illustration cogwheels" />
          <AlertText style={{ textAlign: "center" }}>
            Your webinar data is being processed,check back soon.
          </AlertText>
        </AlertContainer>
      )}
    </div>
  );
};

const REGISTRATION_VISITS_DATAFIELD = "registrationVisits";

const Dashboard = () => {
  const { authUser } = useAuth();
  const firstName = authUser?.firstName;

  const [loading, setLoading] = useState(false);
  const [neverRunEvent, setNeverRunEvent] = useState(false);
  const [neverCreatedEvent, setNeverCreatedEvent] = useState(false);
  const [showFirstLoginModal, setShowFirstLoginModal] = useState(false);
  const [enabledLabel, setEnabledLabel] = useState(
    REGISTRATION_VISITS_DATAFIELD
  );
  const api = useHttpService();
  const [stats, setStats] = useState<any>(null);
  const eventsQuery = useInfiniteQuery(
    INFINITE_EVENTS_QUERY_KEY,
    fetchEventsInfinite(api)
  );

  let futureEventsCount = 0;
  eventsQuery.data?.pages.forEach((page) => {
    futureEventsCount += page.data.length;
  });

  // For checking if any event has ever run yet
  const pastEventsQuery = useQuery("pastEventsCheck", () =>
    api.doGetAllEventsForUser(1, "ASC")
  );

  const [range, setRange] = useState({
    startDate: moment().startOf("week").toDate(),
    endDate: moment().endOf("week").toDate(),
  });

  useEffect(() => {
    setLoading(true);

    api
      .doGetDashboardAnalytics(range)
      .then((data) => {
        setStats(data);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [api, range]);

  useEffect(() => {
    if (!pastEventsQuery.isLoading) {
      if (!_.isEmpty(pastEventsQuery.data?.data)) {
        if (!moment().isAfter(pastEventsQuery.data?.data[0].start)) {
          // event has been created, but didn't run yet
          setNeverRunEvent(true);
        }
      } else {
        setNeverCreatedEvent(true); // user never created any event
        setShowFirstLoginModal(true);
      }
    }
  }, [pastEventsQuery.data, pastEventsQuery.isLoading]);

  let chartData = undefined;
  if (stats && enabledLabel) {
    chartData = stats.series[enabledLabel];
  }

  useQuery("alerts");

  return (
    <>
      <Container fluid className="main-content-container">
        <Row className="mt-4">
          <Col>
            <Button variant="primary" href={newWebinarUrl()}>
              Create a webinar
            </Button>
          </Col>
          <Col className="d-flex">
            <div className="ml-auto mt-auto">
              <RangePicker
                range={range}
                onChange={(range) => setRange(range as any)}
              />
            </div>
          </Col>
        </Row>

        {/* Small Stats Blocks */}
        <Row className="mb-4">
          <Col>
            <Card className="row flex-row mt-4 ml-0 mr-0">
              <DashboardStats
                stats={stats}
                loading={loading}
                enabledLabel={enabledLabel}
                setEnabledLabel={setEnabledLabel}
              />
              <ChartContainer>
                {chartData && enabledLabel && (
                  <DashboardChart
                    data={chartData}
                    color={chartColors[enabledLabel] || "black"}
                    // @ts-ignore
                    showProcessig={
                      !stats.total[REGISTRATION_VISITS_DATAFIELD] &&
                      (chartData as any).every((number) => number === 0)
                    }
                    showNotEnoughData={neverCreatedEvent || neverRunEvent}
                  />
                )}
              </ChartContainer>
            </Card>
          </Col>
        </Row>

        <Row>
          {/* Sessions */}
          <Col lg="8" md="12" sm="12" className="mb-4">
            <ModuleContainer>
              <Card className="h-100 d-flex">
                <ModuleHeader>
                  <ModuleTitle>Upcoming Webinar Sessions</ModuleTitle>
                  <Link to={`/webinars`} className="font-weight-normal">
                    All webinars <i className="bi bi-chevron-right" />
                  </Link>
                </ModuleHeader>
                <ModuleBody className="flex-grow-1">
                  {eventsQuery?.isLoading ? (
                    <CenteredContainer>
                      <Spinner animation="border" />
                    </CenteredContainer>
                  ) : // @ts-ignore
                  futureEventsCount > 0 ? (
                    <Day
                      style={{ height: "100%", overflow: "auto" }}
                      showRegisteredWord={false}
                    />
                  ) : (
                    /* No Scheduled Sessions Message */
                    <CenteredContainer>
                      <img
                        src={illustrationWebinar}
                        alt="Webinar Illustration"
                      />
                      <span>You don’t have any scheduled sessions.</span>
                      <Button variant="primary" href={newWebinarUrl()}>
                        Create Webinar
                      </Button>
                      <span className="font-weight-normal">or</span>
                      <Link to={`/webinars`} className="font-weight-normal">
                        Add a session of an existing webinar
                      </Link>
                    </CenteredContainer>
                  )}
                </ModuleBody>
              </Card>
            </ModuleContainer>
          </Col>

          {/* Users by Device */}
          <Col lg="4" md="6" sm="6" className="mb-4">
            <TopWebinars />
          </Col>
        </Row>

        {/* New User Modal */}
        <Modal
          show={showFirstLoginModal}
          onHide={() => setShowFirstLoginModal(false)}
          centered
        >
          <NewUserModalContainer>
            <Modal.Header closeButton>
              <NewUserModalHeader>
                <NewUserModalName>Hey {firstName},</NewUserModalName>
                <NewUserModalGreeting>
                  Welcome to
                  <Logo fill="#2E3338" height="40px" width="168px" />
                </NewUserModalGreeting>
              </NewUserModalHeader>
            </Modal.Header>
            <Modal.Body>
              <div className="d-flex flex-column justify-content-center">
                <span className="text-center">
                  Create your first webinar without all the fuss
                </span>
                <img
                  src={illustrationWebinar}
                  alt="Webinar Illustration"
                  height="250px"
                  width="250px"
                  className="w-100 text-center"
                />
                <div className="text-center">
                  <Button variant="primary" href={newWebinarUrl()}>
                    Create Webinar
                  </Button>
                </div>
              </div>
            </Modal.Body>
            <Modal.Footer>
              {/* <span className="text-center">
                Want to learn more about creating your first webinar, check out
                the simple Just Webinar{" "}
                <a href="#">
                  Getting Started Guide{" "}
                  <i className="bi bi-box-arrow-up-right" />
                </a>
              </span> */}
            </Modal.Footer>
          </NewUserModalContainer>
        </Modal>
      </Container>
    </>
  );
};

export default Dashboard;
