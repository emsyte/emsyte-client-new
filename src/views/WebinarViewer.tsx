import {
  EventObject,
  ViewerEventObject,
  WebinarObject,
} from "@emsyte/common/webinar";
import {
  faChevronRight,
  faChevronLeft,
  faSignInAlt,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useMemo, useState } from "react";
import { Alert, Button, Modal, Spinner } from "react-bootstrap";
import Countdown from "react-countdown";
import { useQuery } from "react-query";
import { useParams } from "react-router-dom";
import { Col, Container, Row } from "shards-react";
import styled from "styled-components";
import CountdownView from "../components/webinar/viewer/CountdownView";
import { SidebarContainer } from "../components/webinar/viewer/sidebar";
import { PromotionPopup } from "../components/webinar/viewer/sidebar/tab-panel-contents/PromotionPopup";
import { VideoContainer } from "../components/webinar/viewer/video";
import { LiveEventTimeProvider } from "../components/webinar/viewer/video/hooks/time";
import { apiWrapper } from "../utils/api_helpers";
import { useFeatureFlag } from "../utils/FeatureFlags";
import { useViewerAPI } from "../utils/viewerAPI";

const ViewerWrapper = styled.div`
  display: flex;
  height: 100%;
  overflow: hidden;

  @media (max-width: 767.98px) {
    flex-direction: column;
  }
`;

const VideoArea = styled.div`
  flex-grow: 1;
  background-color: #333;
  position: relative;
`;

const SideBarArea = styled.div`
  overflow: hidden;
  width: 400px;
  transition: all 200ms ease;

  height: 100%;

  @media (max-width: 1199.98px) {
    width: 300px;
  }

  @media (max-width: 767.98px) {
    width: 100% !important;

    &.hidden {
      // Height of just the header
      height: 41px;
    }
  }

  &.hidden {
    width: 0px;
  }
`;

const SideBarContent = styled.div`
  width: 400px;
  position: relative;
  height: 100%;

  @media (max-width: 1199.98px) {
    width: 300px;
  }

  @media (max-width: 767.98px) {
    width: 100% !important;
  }
`;

export interface LiveRoomProps {
  webinar: WebinarObject;
  event: EventObject;
}

const LiveRoom: React.FC<LiveRoomProps> = ({ webinar, event }) => {
  const [entered, setEntered] = useState(false);
  const [showSideBar, setShowSideBar] = useState(window.innerWidth > 767.98);

  const liveChatEnabled = useFeatureFlag("live_chat");

  // useEffect(() => {
  //   if (!entered) {
  //     return;
  //   }

  //   const range = document.createRange();

  //   range.selectNode(document.body);
  //   const documentFragment = range.createContextualFragment(
  //     webinar.settings.liveRoomScript
  //   );

  //   document.body.appendChild(documentFragment);

  //   return () => {
  //     document.body.removeChild(documentFragment);
  //   };
  // }, [entered, webinar]);

  if (!entered) {
    return (
      <div className="h-100">
        <Modal.Dialog>
          <Modal.Header>
            <Modal.Title>Event in progress</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            This event is now in progress. Do you want to enter the live room?
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={() => setEntered(true)} data-testid="enter-room">
              <span className="mr-2">Enter Live Room</span>
              <FontAwesomeIcon icon={faSignInAlt}></FontAwesomeIcon>
            </Button>
          </Modal.Footer>
        </Modal.Dialog>
      </div>
    );
  }
  return (
    <LiveEventTimeProvider event={event}>
      <ViewerWrapper>
        <VideoArea>
          <button
            style={{
              justifyContent: "center",
              alignItems: "center",
              position: "absolute",
              top: "2rem",
              right: "0",
              width: "1rem",
              height: "2.5rem",
              color: "#2E3338",
              fontSize: "0.5rem",
              backgroundColor: "#f0f0f0",
              borderRadius: "4px 0 0 4px",
              border: "none",
              zIndex: 100,
            }}
            data-testid="btn-show-chat"
            className="d-none d-sm-flex"
            title={showSideBar ? "Hide Chat" : "Show Chat"}
            aria-label={showSideBar ? "Hide Chat" : "Show Chat"}
            onClick={() => setShowSideBar((prev) => !prev)}
          >
            {showSideBar ? (
              <FontAwesomeIcon icon={faChevronRight} />
            ) : (
              <FontAwesomeIcon icon={faChevronLeft} />
            )}
          </button>
          <VideoContainer webinar={webinar} event={event} />
          <PromotionPopup event={event} />
        </VideoArea>

        {liveChatEnabled && (
          <SideBarArea
            className={showSideBar ? "" : "hidden"}
            data-testid="sidebar"
          >
            <SideBarContent>
              <SidebarContainer
                webinar={webinar}
                event={event}
                showSideBar={showSideBar}
                onSetShowSidebar={setShowSideBar}
              />
            </SideBarContent>
          </SideBarArea>
        )}
      </ViewerWrapper>
    </LiveEventTimeProvider>
  );
};

// Renderer callback with condition
const CountdownRenderer = ({
  props,
  days,
  hours,
  minutes,
  seconds,
  completed,
  webinar,
  event,
}) => {
  if (completed) {
    // Render a completed state
    return <LiveRoom webinar={webinar} event={event} />;
  } else {
    // Render a countdown
    return (
      <CountdownView
        dateTime={{
          date: props.date,
          days,
          hours,
          minutes,
          seconds,
        }}
        webinar={webinar}
      />
    );
  }
};

const WebinarViewer = () => {
  const { id } = useParams<{ id: string }>();
  const api = useViewerAPI();

  const idData = useMemo(() => {
    const buff = new Buffer(id, "base64");
    return JSON.parse(buff.toString("ascii"));
  }, [id]);

  const [keepFetching, setKeepFetching] = useState(true);

  const { data: event, isLoading } = useQuery(
    ["viewerEvent", idData?.eventId],
    () => api.doGetViewerEvent(idData?.eventId),
    {
      refetchInterval: keepFetching && 1000,
    }
  );

  useEffect(() => {
    if (event?.videoUrl && event.videoUrl.length > 0) {
      setKeepFetching(false);
    }
  }, [event]);

  return (
    <Container fluid className="webinar-viewer p-0" style={{ height: "100vh" }}>
      {isLoading ? (
        <Row>
          <Col className="text-center mt-5">
            <h1>Loading...</h1>
            <Spinner
              as="span"
              animation="border"
              role="status"
              aria-hidden="true"
            />
          </Col>
        </Row>
      ) : event ? (
        <Countdown
          date={event.start}
          renderer={(props) =>
            CountdownRenderer({ ...props, webinar: event.webinar, event })
          }
        />
      ) : (
        <Row>
          <Col sm={3}></Col>
          <Col sm={6}>
            <Alert variant="danger" className="mt-5">
              Invalid session link
            </Alert>
          </Col>
          <Col sm={3}></Col>
        </Row>
      )}
    </Container>
  );
};

export default WebinarViewer;
