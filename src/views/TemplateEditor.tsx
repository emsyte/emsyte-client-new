import { TemplateInstanceObjectFull } from "@emsyte/common/template";
import GrapesJS from "grapesjs";
import "grapesjs-lory-slider";
import "grapesjs-preset-webpage";
import emsytePlugin from "@emsyte/grapesjs-plugin";
import "@emsyte/grapesjs-plugin/dist/grapesjs-plugin.min.css";

import "grapesjs-preset-webpage/dist/grapesjs-preset-webpage.min.css";
import "grapesjs/dist/css/grapes.min.css";

import React, { useCallback, useEffect, useRef, useState } from "react";
import { Card, Fade, Spinner } from "react-bootstrap";
// import ReactDOMServer from "react-dom/server";

import { useHistory, useLocation, useParams } from "react-router-dom";
import styled from "styled-components";
import { useNotification } from "../components/common/Notification";
import customStyleManager from "../components/editor/styleManager";
import { useHttpService } from "../utils/HttpService";

import { WebinarObject } from "@emsyte/common/webinar";
import DomToImage from "dom-to-image";

const Wrapper = styled.div`
  position: relative;
  width: 100vw;
  height: 100vh;
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;

  z-index: 1000;

  background-color: #5a6169;

  display: flex;
  justify-content: center;
  align-items: center;

  .card {
    max-width: 200px;
  }
`;

const Canvas = styled.div`
  width: 100vw;
  height: 100vh !important;
`;

interface Handler {
  (): void | Promise<void>;
}

function useRefCallback<T extends Function>(callback: T): T {
  const ref = useRef<Function>();
  useEffect(() => {
    ref.current = callback;
  }, [callback]);

  const run = useCallback(
    (...args) => {
      if (ref.current) {
        return ref.current(...args);
      }
    },
    [ref]
  );

  return run as any as T;
}

function useRefMirror<T>(value: T) {
  const ref = useRef(value);
  useEffect(() => {
    ref.current = value;
  }, [value]);
  return ref;
}

interface EditorProps {
  template: TemplateInstanceObjectFull;
  webinarObject: WebinarObject | null;
  onUpdateTemplate: (
    data: TemplateInstanceObjectFull,
    thumbnail: Blob
  ) => Promise<any>;
  onReady: Handler;
  onExit: Handler;
  onSave: Handler;
  onReset: Handler;
  onSaveGlobalTemplate: (
    thumbnail: string,
    templateName: string,
    createNew: boolean
  ) => Promise<void>;
}

const Editor: React.FC<EditorProps> = ({
  template,
  webinarObject,
  onUpdateTemplate,
  onSaveGlobalTemplate,
  onReady,
  onExit,
  onSave,
  onReset,
}) => {
  const notification = useNotification();
  const api = useHttpService();
  const [editor, setEditor] = useState<any>(null);

  const handleExit = useRefCallback(onExit);
  const handleSave = useRefCallback(onSave);
  const handleReset = useRefCallback(onReset);
  const handleReady = useRefCallback(onReady);
  const handleUpdateTemplate = useRefCallback(onUpdateTemplate);
  const handleSaveGlobalTemplate = useRefCallback(onSaveGlobalTemplate);
  const templateRef = useRefMirror(template);

  useEffect(() => {
    const editor = GrapesJS.init({
      allowScripts: 1,
      container: "#gjs-editor",
      plugins: ["gjs-preset-webpage", "grapesjs-lory-slider", emsytePlugin],
      storageManager: { id: "", type: "custom", autosave: false },
      assetManager: {
        upload: true,
        autoAdd: true,
        multiUpload: false,
        uploadName: "file",

        customFetch: async (_, config) => {
          const file = config.body.get("file");
          const src = await api.doUploadFile("template", file);
          return { data: src };
        },
      },
      canvas: {
        styles: [
          "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css",
          "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
        ],
        scripts: [
          "https://code.jquery.com/jquery-3.5.1.slim.min.js",
          "https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js",
          "https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js",
        ],
      },
      styleManager: {
        clearProperties: true,
      },
      pluginsOpts: {
        "gjs-preset-webpage": {
          customStyleManager,
          countdownOpts: false,
          formsOpts: false,
          blocksBasicOpts: {
            flexGrid: true,
          },
        },
        "grapesjs-lory-slider": {
          sliderBlock: {
            category: "Extra",
          },
        },
        [emsytePlugin]: {
          webinar: { ...webinarObject },
          template: { ...template },
        },
      },
    });

    editor.on("storage:end:load", () => {
      handleReady();
    });

    editor.StorageManager.add("custom", {
      /**
       * Load the data
       * @param  {Array} keys Array containing values to load, eg, ['gjs-components', 'gjs-style', ...]
       * @param  {Function} clb Callback function to call when the load is ended
       * @param  {Function} clbErr Callback function to call in case of errors
       */
      load(keys, clb, clbErr) {
        const template = templateRef.current;

        // const input = document.getElementById(
        //   "templateTitle"
        // ) as HTMLInputElement;

        // if (input) {
        //   input.value = template.name;
        // }

        clb(template);
      },

      /**
       * Store the data
       * @param  {Object} data Data object to store
       * @param  {Function} clb Callback function to call when the load is ended
       * @param  {Function} clbErr Callback function to call in case of errors
       */
      async store(data, clb, clbErr) {
        const el = editor.getWrapper().getEl();

        const image = await DomToImage.toBlob(el, {
          quality: 0.9,
          height: 1000,
          cacheBust: true,
          style: {
            "background-color": el.style.backgroundColor || "white",
          },
        });

        console.log(image, image.type);

        await handleUpdateTemplate(
          {
            ...data,
          },
          image
        );

        clb();
      },
    });

    editor.on("run:update-existing-template", ({ thumbnail, templateName }) => {
      editor.store(() =>
        handleSaveGlobalTemplate(thumbnail, templateName, false).then(() => {
          handleExit();
        })
      );
    });

    editor.on("run:create-new-template", ({ thumbnail, templateName }) => {
      editor.store(() =>
        handleSaveGlobalTemplate(thumbnail, templateName, true).then(() => {
          handleExit();
        })
      );
    });

    editor.on("run:reset", async () => {
      await handleReset();
      editor.load();
    });
    (window as any).editor = editor;
    editor.Commands.add("exit", async (editor) => {
      handleExit();
    });

    editor.on("load", () => {
      // Hacky Way to Get Template Title on Top ~ Do Not Touch
      const categoryBlock = document.getElementsByClassName("gjs-blocks-cs")[0];
      const no_cat = document.getElementsByClassName("gjs-blocks-no-cat")[0];
      categoryBlock.prepend(no_cat);

      // Close Down Categories To Make the UX Better
      editor.BlockManager.getCategories().forEach((c) => c.set("open", false));
    });

    editor.on("storage:end:store", () => {
      handleSave();
    });

    setEditor(editor);
  }, [
    handleExit,
    handleSave,
    handleReset,
    handleReady,
    handleSaveGlobalTemplate,
    handleUpdateTemplate,
    api,
    webinarObject,
    template,
    templateRef,
  ]);

  useEffect(() => {
    if (!editor) {
      return;
    }

    setTimeout(() => {
      editor.load();
    }, 1000);
  }, [editor]);

  useEffect(() => {
    if (!editor) {
      return;
    }

    // // The upload is started
    // _editor.on("asset:upload:start", () => {
    //   setUploading(true);
    // });

    // // The upload is ended (completed or not)
    // _editor.on("asset:upload:end", () => {
    //   setUploading(false);
    // });

    // Error handling
    editor.on("asset:upload:error", (err) => {
      notification.showMessage({
        title: "Failed to upload",
        description: err.message,
        type: "error",
      });
    });

    // Do something on response
    editor.on("asset:upload:response", (response) => {
      notification.showMessage({
        title: "Uploaded images",
        description: response.message,
      });
    });
  }, [editor, notification]);

  return <Canvas id="gjs-editor"></Canvas>;
};

const TemplateEditor = () => {
  const api = useHttpService();
  const notification = useNotification();

  const history = useHistory();
  const location = useLocation();
  const { templateId: instanceId } = useParams<{
    templateId: string;
  }>();

  const searchParams = new URLSearchParams(location.search);
  const webinar = searchParams.get("webinar");

  const [loading, setLoading] = useState(true);

  const [webinarObject, setWebinarObject] = useState<WebinarObject | null>(
    null
  );

  const [template, setTemplate] = useState<TemplateInstanceObjectFull | null>(
    null
  );

  useEffect(() => {
    const main_fa = document.getElementById("link-font-awesome")!;
    document.head.removeChild(main_fa);
    return () => {
      document.head.appendChild(main_fa);
    };
  }, []);

  const getTemplate = useCallback(
    async (instanceId: number) => {
      const webinarId = webinar && JSON.parse(atob(webinar as string)).id;

      setLoading(true);

      try {
        const { data: _webinar } = await api.doGetWebinar(webinarId);
        setWebinarObject(_webinar);

        if (template && template.id === instanceId) {
          return;
        }

        const { data } = await api.doGetTemplateInstance(instanceId);

        setTemplate(data);
      } catch (error) {
        notification.showMessage({
          title: "Failed to load template",
          type: "error",
        });
        setLoading(false);
      }
    },
    [api, notification, webinar, template]
  );

  useEffect(() => {
    if (template?.id === +instanceId) {
      return;
    }
    getTemplate(+instanceId);
  }, [getTemplate, template, instanceId]);

  const handleExit = () => {
    window.onbeforeunload = null;
    history.push(`/webinars/${webinar}/templates`);
  };
  const handleReset = async () => {
    if (!template) {
      return;
    }

    setLoading(true);
    try {
      await api.doResetTemplateInstance(template.id);
      const { data } = await api.doGetTemplateInstance(template.id);
      setTemplate(data);
    } finally {
      setLoading(false);
    }
  };

  const handleSave = () => {
    notification.showMessage({
      title: "Template saved",
      type: "success",
    });
  };

  const handleSaveGlobalTemplate = async (
    thumbnail: string,
    name: string,
    createNew: boolean
  ) => {
    if (!template) {
      return;
    }

    setLoading(true);
    try {
      if (createNew) {
        await api.doCreateTemplateFromInstance({
          instanceId: template.id,
          name: name,
        });
      } else {
        await api.doUpdateTemplateFromInstance(template.parent.id, {
          instanceId: template.id,
          name: name,
        });
      }
    } finally {
      setLoading(false);
    }
  };

  return (
    <Wrapper>
      {template && (
        <Editor
          template={template}
          webinarObject={webinarObject}
          onUpdateTemplate={async (data, blob) => {
            const file = new File([blob], "template-thumbnail.png", {
              type: blob.type,
            });

            const thumbnail = await api.doUploadFile(
              "template-thumbnail",
              file
            );

            await api.doUpdateTemplateInstance(+instanceId, {
              ...data,
              thumbnail,
            });
          }}
          onReady={() => setLoading(false)}
          onExit={handleExit}
          onSave={handleSave}
          onReset={handleReset}
          onSaveGlobalTemplate={handleSaveGlobalTemplate}
        ></Editor>
      )}
      <Fade in={loading} unmountOnExit={true}>
        <Overlay>
          <Card>
            <Card.Body>
              <div style={{ display: "flex", alignItems: "center" }}>
                <span>
                  <Spinner animation="border" />
                </span>
                <span className="ml-2">Loading Template...</span>
              </div>
            </Card.Body>
          </Card>
        </Overlay>
      </Fade>
    </Wrapper>
  );
};

export default TemplateEditor;
