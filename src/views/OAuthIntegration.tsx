import React, { useEffect } from "react";
import { Spinner } from "react-bootstrap";

export interface OAuthIntegrationProps {}

const OAuthIntegration: React.FC<OAuthIntegrationProps> = () => {
  useEffect(() => {
    if (window.opener) {
      window.opener.postMessage(window.location.search);
    }

    // window.close();
  }, []);

  return <Spinner animation="border"></Spinner>;
};

export default OAuthIntegration;
