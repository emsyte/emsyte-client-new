import { TemplateInstanceObjectFull } from "@emsyte/common/template";
import Parser from "html-react-parser";
import React, { useCallback, useEffect, useState } from "react";
import { useParams, withRouter } from "react-router-dom";
import { compose } from "recompose";
import { useHttpService } from "../utils/HttpService";

const Template = ({ match }) => {
  const api = useHttpService();
  const { instanceId } = useParams<{ instanceId: string }>();
  const [template, setTemplate] = useState<TemplateInstanceObjectFull>();

  const getTemplateContent = useCallback(
    async (id) => {
      try {
        const data = await api.doGetTemplateInstance(id);
        setTemplate(data.data);
      } catch (error) {
        console.log(error);
      }
    },
    [api]
  );

  useEffect(() => {
    const fetchData = () => getTemplateContent(instanceId);
    console.log(instanceId);
    if (instanceId) fetchData();
  }, [instanceId, getTemplateContent]);

  return (
    <div>
      {template && (
        <>
          <style dangerouslySetInnerHTML={{ __html: template["css"] }}></style>
          <div id="gjs-editor">{Parser(template["html"])}</div>
        </>
      )}
    </div>
  );
};

const enhance = compose(withRouter)(Template);

export default enhance;
