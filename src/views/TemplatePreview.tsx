import { TemplateObjectFull } from "@emsyte/common/template";
import Parser from "html-react-parser";
import React, { useCallback, useEffect, useState } from "react";
import { useParams, withRouter } from "react-router-dom";
import { compose } from "recompose";
import { useHttpService } from "../utils/HttpService";

const Template = ({ match }) => {
  const api = useHttpService();
  const { templateId } = useParams<{ templateId: string }>();
  const [template, setTemplate] = useState<TemplateObjectFull>();

  const getTemplateContent = useCallback(
    async (id) => {
      try {
        const data = await api.doGetGlobalTemplate(id);
        setTemplate(data.data);
      } catch (error) {
        console.log(error);
      }
    },
    [api]
  );

  useEffect(() => {
    const fetchData = async () => await getTemplateContent(templateId);
    if (templateId) fetchData();
  }, [templateId, getTemplateContent]);

  return (
    <div>
      {template && (
        <>
          <style dangerouslySetInnerHTML={{ __html: template["css"] }}></style>
          <div id="gjs-editor">{Parser(template["html"])}</div>
        </>
      )}
    </div>
  );
};

const enhance = compose(withRouter)(Template);

export default enhance;
