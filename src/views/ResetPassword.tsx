/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, { useCallback, useState } from "react";
import {
  Form,
  FormControl,
  FormGroup,
  FormLabel,
  Alert,
  Button,
} from "react-bootstrap";
import { Link, useLocation } from "react-router-dom";
import { Card, CardBody, Col, Container, Row } from "shards-react";
import background from "../images/bg-onboarding.png";
import LoaderButton from "../components/common/LoaderButton";
import { useHttpService } from "../utils/HttpService";

const validationErrors = {
  error: "Please enter valid credentials",
};

const ResetPassword: React.FC = () => {
  const api = useHttpService();
  const location = useLocation();
  var searchParams = new URLSearchParams(location.search);
  const [confirmPassword, setConfirmPassword] = useState<string>("");
  const [password, setPassword] = useState<string>("");

  const [error, setError] = useState<string>("");
  const [validated, setValidated] = useState(false);
  const [loading, setLoading] = useState(false);
  const [requestStatus, setRequestStatus] = useState("");

  const handleSubmit = useCallback(
    async (event) => {
      const form = event.currentTarget;
      event.preventDefault();
      event.stopPropagation();

      if (
        form["confirmPassword"].checkValidity() &&
        form["password"].checkValidity() &&
        password === confirmPassword
      ) {
        try {
          const token = searchParams.get("token");
          if (!token) return;
          setLoading(true);
          setRequestStatus("request");
          await api.doForgotPasswordConfirm({
            password,
            token,
          });
          setRequestStatus("success");
        } catch (error) {
          setRequestStatus("failure");
          setError(error.response?.data?.message ?? error.message);
        } finally {
          setLoading(false);
        }
      } else {
        console.log("here");
      }

      setValidated(true);
    },
    [password, confirmPassword, searchParams, api]
  );
  return (
    <Card className="bg-transparent no-shadow">
      <CardBody>
        {/* Logo */}
        <h3 className="font-weight-light">Reset password</h3>
        <p>
          {requestStatus === "success" ? (
            <>
              Password successfully updated
              <br />
              <br />
              <Link to="/login" className="mx-auto">
                <Button>Continue to login</Button>
              </Link>
            </>
          ) : (
            "Create a new password."
          )}
        </p>

        {/* Form Fields */}
        {requestStatus !== "success" && (
          <Form noValidate validated={validated} onSubmit={handleSubmit}>
            {error && (
              <Alert variant="danger" dismissible onClose={() => setError("")}>
                {error}
              </Alert>
            )}
            <FormGroup>
              <FormLabel>Password</FormLabel>
              <FormControl
                type="password"
                name="password"
                autoComplete="current-password"
                size="lg"
                required
                isInvalid={password.length < 8 && password.length !== 0}
                onChange={(e) => setPassword(e.target.value)}
              />
              <FormControl.Feedback type="invalid">
                {validationErrors.error}
              </FormControl.Feedback>
            </FormGroup>
            <FormGroup>
              <FormLabel>Confirm Password</FormLabel>
              <FormControl
                type="password"
                name="confirmPassword"
                autoComplete="current-password"
                size="lg"
                required
                isInvalid={confirmPassword !== password}
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
              <FormControl.Feedback type="invalid">
                {confirmPassword &&
                  password &&
                  password !== confirmPassword &&
                  `Passwords don't match`}
              </FormControl.Feedback>
            </FormGroup>
            <LoaderButton type="submit" loading={loading}>
              Reset Password
            </LoaderButton>
          </Form>
        )}
      </CardBody>
    </Card>
  );
};

const ResetPasswordContainer: React.FC = () => {
  return (
    <Container
      fluid
      className="main-content-container"
      style={{ height: "100vh" }}
    >
      <Row className="h-100">
        <Col lg={5} md="10" xs={12} className="h-100">
          <Row className="h-100">
            <Col></Col>
            <Col xs={12} lg={10} className="my-auto">
              <ResetPassword></ResetPassword>
            </Col>
            <Col></Col>
          </Row>
          {/* Meta Details */}
          <Row className="position-absolute w-100" style={{ bottom: "5%" }}>
            <Col></Col>
            <Col xs={10} className="text-center">
              <img
                className="auth-form__logo mr-3"
                src={require("../images/Large_Icon.svg")}
                alt="Shards Dashboards - Login Template"
              />
              Don't have an account yet?
              <Link to="/register" className="ml-2">
                Sign Up
              </Link>
            </Col>
            <Col></Col>
          </Row>
        </Col>
        <Col
          sm={12}
          lg={7}
          className="d-none d-sm-block"
          style={{
            backgroundImage: `url('${background}')`,
            backgroundSize: "contain",
            backgroundPositionY: "bottom",
            backgroundPositionX: "center",
            backgroundRepeat: "no-repeat",
          }}
        ></Col>
      </Row>
    </Container>
  );
};

export default ResetPasswordContainer;
