import {
  EventObject,
  ViewerEventObject,
  WebinarObject,
} from "@emsyte/common/webinar";
import React, { useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { Col, Container, Row } from "shards-react";
import { VideoContainer } from "../components/webinar/viewer/video";
import { apiWrapper } from "../utils/api_helpers";
import { useViewerAPI } from "../utils/viewerAPI";

import styled from "styled-components";
import { PromotionPopup } from "../components/webinar/viewer/sidebar/tab-panel-contents/PromotionPopup";
import { ReplayEventTimeProvider } from "../components/webinar/viewer/video/hooks/time";

const ViewerWrapper = styled.div`
  display: flex;
  height: 100%;
  overflow: hidden;
`;

const VideoArea = styled.div`
  flex-grow: 1;
  background-color: #333;
  position: relative;
`;

export interface LiveRoomProps {
  webinar: WebinarObject;
  event: EventObject;
}

const WebinarReplay = () => {
  const { id } = useParams<{ id: string }>();
  const api = useViewerAPI();
  const [event, setEvent] = useState<ViewerEventObject | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    if (!id) {
      return;
    }
    const buff = new Buffer(id, "base64");
    const data = JSON.parse(buff.toString("ascii"));

    apiWrapper(async () => {
      const event = await api.doGetViewerEvent(data.eventId);

      setEvent(event);
    }, setLoading);
  }, [api, id, setLoading, setEvent]);

  useEffect(() => {
    if (!event) {
      return;
    }
    const range = document.createRange();

    range.selectNode(document.body);
    const documentFragment = range.createContextualFragment(
      event.webinar.settings.replayRoomScript
    );

    document.body.appendChild(documentFragment);

    return () => {
      document.body.removeChild(documentFragment);
    };
  }, [event]);

  return (
    <Container fluid className="webinar-viewer p-0" style={{ height: "100vh" }}>
      {loading ? (
        <Row>
          <Col className="text-center mt-5">
            <h1>Loading...</h1>
            <Spinner
              as="span"
              animation="border"
              role="status"
              aria-hidden="true"
            />
          </Col>
        </Row>
      ) : (
        event?.webinar && (
          <ReplayEventTimeProvider>
            <ViewerWrapper>
              <VideoArea>
                <VideoContainer webinar={event.webinar} event={event} />
                <PromotionPopup event={event} />
              </VideoArea>
            </ViewerWrapper>
          </ReplayEventTimeProvider>
        )
      )}
    </Container>
  );
};

export default WebinarReplay;
