import React from "react";
import styled from "styled-components";
import UserAccountDetails from "../components/user-profile-lite/UserAccountDetails";

const Styles = styled.div`
  .nav {
    background-color: #fff;
    margin-bottom: 0;
  }

  .nav .nav-link .nav-link.active {
    background-color: #ccc;
    color: black;

    &:hover,
    &:focus {
      background-color: #f5f5f5;
    }
  }

  .container {
    margin-top: 1.5em;
  }
`;

const ContentCard = styled.div`
  display: flex;
  align-items: flex-start;
  min-width: 900px;
  justify-content: center;
  margin-top: 2em;
`;

const UserProfileLite = () => {
  return (
    <Styles>
      <ContentCard>
        <UserAccountDetails />
      </ContentCard>
    </Styles>
  );
};

export default UserProfileLite;
