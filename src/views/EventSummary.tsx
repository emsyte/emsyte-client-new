import { ViewerEventObject } from "@emsyte/common/webinar";
import React, { useEffect, useState } from "react";
import { Alert, Row, Spinner } from "react-bootstrap";
import { useParams } from "react-router-dom";
import { Container } from "shards-react";
import { secondsToDuration } from "../utils/duration";
import { formatDuration } from "../utils/formatters";
import { useHttpService } from "../utils/HttpService";
import { Editor } from "../components/webinar/viewer/sidebar/editor/Editor";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar, faEllipsisH } from "@fortawesome/free-solid-svg-icons";
import { ChatBoxHistories } from "../components/webinar/viewer/sidebar/tab-panel-contents/ChatBoxHistories";
import styled from "styled-components";
import { getHostDMRoom, useChat } from "../utils/SocketService/useChat";
import moment from "moment";
import Chart from "../components/event-summary-chart/EventSummaryChart";
import { ChatUser } from "@emsyte/common/message";
import { AuthType } from "@emsyte/common/auth";

interface EventSummaryDashboardProps {
  event: ViewerEventObject;
  analytics: any;
}
// You may override chat colors here
const Panel = styled.div`
  flex: 1;
  display: flex;
  border-radius: 4px;
  flex-direction: row;
  margin-top: 0.75rem;
  max-height: calc(100vh - 22.25rem);
  min-height: 14rem;

  .menuHeader {
    display: flex;
    align-items: center;
    min-height: 2.5rem;
    padding: 0 1rem;
    font-size: 1rem;
    box-shadow: 0px 0px 2px inset rgba(0, 0, 0, 0.18);
  }

  button {
    background: none;
    border: none;
    font-weight: 600;
    border-radius: 0;
    padding: 0 1.5rem;
    height: 100%;

    &.active {
      font-weight: 700;
      border-bottom: 2px solid #007bff;
      border-radius: 0;
    }

    &.has-notification::after {
      position: absolute;
      content: " ";
      height: 4px;
      width: 4px;
      border-radius: 1rem;
      background-color: #d72e3d;
    }
  }

  .leftPanel {
    width: 20rem;
    display: flex;
    flex-direction: column;
    background-color: #f0f0f0;
    box-shadow: 0px 0px 2px inset rgba(0, 0, 0, 0.18);
  }

  .rightPanel {
    flex: 1;
    display: flex;
    flex-direction: column;
    background-color: white;
  }

  .message {
    align-self: flex-end;
  }

  .text {
    background-color: #f0f0f0;
  }

  .own-message .text {
      fontweight: normal;
      background-color: #007bff;
      color: #f7f8f9;
    }
  }

  ul {
    list-style-type: none;
    padding: 0;
    margin: 0;
    & li {
      font-weight: normal;
      position: relative;
      padding: 0.2rem 1.4rem;
      border-radius: 4px;
      cursor: pointer;

      &:hover {
        background-color: rgba(0, 123, 255, 0.1);
      }

      &.selected {
        background-color: rgba(0, 123, 255, 0.3);
      }

      &::before {
        left: 6px;
        top: 35%;
        position: absolute;
        content: " ";
        height: 8px;
        width: 8px;
        border: 1px solid #868e96;
        border-radius: 1rem;
      }

      &.attendee-active::before {
        border: none;
        background-color: #d72e3d;
      }
    }
  }
`;

const EventSummaryDashboard: React.FC<EventSummaryDashboardProps> = ({
  event,
  analytics,
}) => {
  const {
    chats,
    attendees,
    getBarsData,
    isLoadingHistory,
    isAtBeginning,
    barsData,
    sendMessage,
    removeMessage,
    requestHistory,
  } = useChat(event);

  const [selectedTab, setSelectedTab] = useState<"Attendees" | "Session Info">(
    "Attendees"
  );
  const [selectedRoom, setSelectedRoom] = useState<"general" | string>(
    "general"
  );

  const allAttendees = [
    { id: "general", name: "All attendees" },
    ...analytics.attendees.filter(
      (attendee) => attendee.attended || attendees.includes(attendee.id)
    ),
  ];

  const hasStarted = moment().isAfter(event.start);
  const hasEnded = moment().isAfter(event.end);

  return (
    <div style={{ display: "flex", flexDirection: "column", flex: 1 }}>
      <Chart event={event} getBarsData={getBarsData} barsData={barsData} />
      <Panel>
        <div className="leftPanel">
          <div className="menuHeader">
            <button
              className={`
                ${selectedTab === "Attendees" ? "active" : ""}
                ${false ? "has-notification" : ""}
              `}
              onClick={() => setSelectedTab("Attendees")}
            >
              Attendees
            </button>
            <button
              className={`${selectedTab === "Session Info" ? "active" : ""}`}
              onClick={() => setSelectedTab("Session Info")}
            >
              Session Info
            </button>
          </div>
          <div style={{ padding: "1.2rem 0.8rem", overflowY: "auto" }}>
            {selectedTab === "Attendees" ? (
              <AttendeesTab
                attendees={attendees}
                allAttendees={allAttendees}
                selectedRoom={selectedRoom}
                setSelectedRoom={setSelectedRoom}
              />
            ) : (
              <SessionInfoTab
                analytics={analytics}
                attendees={attendees}
                hasStarted={hasStarted}
                hasEnded={hasEnded}
                selectedRoom={selectedRoom}
                setSelectedRoom={setSelectedRoom}
              />
            )}
          </div>
        </div>

        <div className="rightPanel">
          <div
            className="menuHeader"
            style={{
              justifyContent: "space-between",
              backgroundColor: "#f0f0f0",
            }}
          >
            <span style={{ fontWeight: 400 }}>
              {selectedRoom === "general" ? (
                "Discussion"
              ) : (
                <>
                  Direct Message with{" "}
                  <strong>
                    {
                      allAttendees.find(
                        (attendee) => "attendee-" + attendee.id === selectedRoom
                      )?.name
                    }
                  </strong>
                </>
              )}
            </span>
            <span>
              {/* <FontAwesomeIcon className="mr-2" icon={faSearch} /> */}
              <FontAwesomeIcon icon={faEllipsisH} />
            </span>
          </div>
          <ChatBoxHistories
            admin={true}
            chats={chats}
            currentRoom={selectedRoom}
            removeMessage={removeMessage}
            requestHistory={requestHistory}
            isLoadingHistory={isLoadingHistory}
            isAtBeginning={isAtBeginning}
          />
          <Editor sendMessage={sendMessage} sendTo={selectedRoom} />
        </div>
      </Panel>
    </div>
  );
};

const EventSummary = () => {
  const { id: encId } = useParams<{
    id: string;
  }>();
  const { webinarId, eventId } = JSON.parse(atob(encId));
  const api = useHttpService();
  const [loading, setLoading] = useState(true);
  const [event, setEvent] = useState<ViewerEventObject | null>(null);
  const [analytics, setAnalytics] = useState<any>(null);

  useEffect(() => {
    setLoading(true);
    Promise.all([
      api.doGetWebinarEvent(webinarId, eventId).then((response) => {
        setEvent(response.data);
      }),
      api.doGetEventAnalytics(eventId).then(setAnalytics),
    ]).finally(() => {
      setLoading(false);
    });
  }, [webinarId, eventId, api]);

  return (
    <Container
      fluid
      className="main-content-container px-4 pb-4 h-100"
      style={{ display: "flex", flexDirection: "column" }}
    >
      <Row noGutters className="page-header py-4">
        <div
          style={{
            width: "100%",
            display: "flex",
            justifyContent: "space-between",
          }}
        >
          <span
            style={{
              fontSize: "1.625rem",
              fontWeight: 500,
              lineHeight: 1,
              margin: 0,
              padding: 0,
            }}
          >
            {event ? event.webinar.name : "Event Summary"}
          </span>
          <span style={{ fontWeight: 400 }}>
            <FontAwesomeIcon icon={faCalendar} className="mr-2" />
            {event
              ? `Event on ${new Date(event.start).toLocaleDateString("en-us", {
                  year: "numeric",
                  month: "short",
                  day: "numeric",
                  hour: "numeric",
                  minute: "numeric",
                })}`
              : "Event Summary"}
          </span>
        </div>
      </Row>
      {loading ? (
        <Spinner animation="border"></Spinner>
      ) : event && analytics ? (
        <EventSummaryDashboard event={event} analytics={analytics} />
      ) : (
        <Alert variant="danger">Failed to load data</Alert>
      )}
    </Container>
  );
};

interface RegisteredAttendee {
  attended: boolean;
  name: string;
  id: number;
}

function getAttendeeId(id: number) {
  return `${AuthType.ATTENDEE}-${id}`;
}

interface AttendeesTabProps {
  allAttendees: RegisteredAttendee[];
  attendees: ChatUser[];
  selectedRoom: string | number;
  setSelectedRoom: (room: string) => void;
}

const AttendeesTab: React.FC<AttendeesTabProps> = ({
  allAttendees,
  attendees,
  selectedRoom,
  setSelectedRoom,
}) => {
  return (
    <>
      <div style={{ color: "#2E3338" }}>
        {allAttendees.length - 1} attendees
      </div>
      <ul className="attendees">
        {allAttendees.map((attendee) => (
          <li
            key={attendee.id}
            className={`${attendee.id === selectedRoom ? "selected" : ""} ${
              ["general", ...attendees.map((a) => a.userId)].includes(
                getAttendeeId(attendee.id)
              )
                ? "attendee-active"
                : ""
            }`}
            onClick={() =>
              setSelectedRoom(
                (attendee.id as any) === "general"
                  ? "general"
                  : getHostDMRoom(attendee.id)
              )
            }
          >
            {attendee.name}
          </li>
        ))}
      </ul>
    </>
  );
};

interface SessionInfoTabProps {
  analytics: any;
  hasStarted: boolean;
  hasEnded: boolean;
  attendees: ChatUser[];
  selectedRoom: string | number;
  setSelectedRoom: (room: string) => void;
}

const SessionInfoTab: React.FC<SessionInfoTabProps> = ({
  analytics,
  hasStarted,
  hasEnded,
  attendees,
  selectedRoom,
  setSelectedRoom,
}) => {
  return (
    <>
      <div style={{ color: "#2E3338" }}>
        <div>
          Average watch time{" "}
          {hasEnded
            ? formatDuration(
                secondsToDuration(analytics.attendance.avg_watch_time)
              )
            : "-"}
        </div>
        <div>
          {analytics.attendance.registered} registered /{" "}
          {hasStarted ? analytics.attendance.attendees : "-"} attended
        </div>
        <ul className="attendees">
          {analytics.attendees.map((attendee) => (
            <li
              key={attendee.id}
              className={`${attendee.id === selectedRoom ? "selected" : ""} ${
                attendees
                  .map((a) => a.userId)
                  .includes(getAttendeeId(attendee.id))
                  ? "attendee-active"
                  : ""
              }`}
              onClick={() => setSelectedRoom(getHostDMRoom(attendee.id))}
            >
              {attendee.name}
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default EventSummary;
