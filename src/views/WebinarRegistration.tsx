import { TemplateType } from "@emsyte/common/template";
import { EventObject } from "@emsyte/common/webinar";
import * as _ from "lodash";
import moment from "moment-timezone";
import React, { useEffect, useMemo, useState } from "react";
import { Spinner } from "react-bootstrap";
import Countdown from "react-countdown";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  FormGroup,
  FormInput,
  Row,
} from "shards-react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import LoaderButton from "../components/common/LoaderButton";
import { useWebinarTemplate } from "../components/templates/hooks";
import background from "../images/bg-onboarding.png";
import { apiWrapper } from "../utils/api_helpers";
import { useHttpService } from "../utils/HttpService";

const abbrs = {
  EST: "Eastern Standard Time",
  EDT: "Eastern Daylight Time",
  CST: "Central Standard Time",
  CDT: "Central Daylight Time",
  MST: "Mountain Standard Time",
  MDT: "Mountain Daylight Time",
  PST: "Pacific Standard Time",
  PDT: "Pacific Daylight Time",
};

moment.fn.zoneName = function () {
  var abbr = this.zoneAbbr();
  return abbrs[abbr] || abbr;
};

const getTimezone = (date) => {
  const utcOffset = moment(date).format("ZZ");
  const timezone = _.find(moment.tz.zonesForCountry("US"), (timezoneName) => {
    return utcOffset === moment.tz(timezoneName).format("Z").replace(":", "");
  });
  // @ts-ignore
  return `${moment.tz(date, timezone).format("zz")} (US & Canada) GMT ${utcOffset}`;
};

// Renderer callback with condition
const CountdownRenderer = ({
  props,
  days,
  hours,
  minutes,
  seconds,
  completed,
  webinar,
}) => {
  if (completed) {
    // Render a completed state
    return <>test</>;
  } else {
    // Render a countdown
    return (
      <>
        <Row>
          <Col>
            <Row>
              <Col className="bg-light rounded">
                <h4>{days}</h4>
              </Col>
            </Row>
          </Col>
          <Col className="mx-2">
            <Row>
              <Col className="bg-light rounded">
                <h4>{hours}</h4>
              </Col>
            </Row>
          </Col>
          <Col className="mx-2">
            <Row>
              <Col className="bg-light rounded">
                <h4>{minutes}</h4>
              </Col>
            </Row>
          </Col>
          <Col>
            <Row>
              <Col className="bg-light rounded">
                <h4>{seconds}</h4>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row>
          <Col xs="3" className="px-0 text-center">
            <small className="font-weight-bold">DAYS</small>
          </Col>
          <Col xs="3" className="px-0 text-center">
            <small className="font-weight-bold">HOURS</small>
          </Col>
          <Col xs="3" className="px-0 text-center">
            <small className="font-weight-bold">MINUTES</small>
          </Col>
          <Col xs="3" className="px-0 text-center">
            <small className="font-weight-bold">SECONDS</small>
          </Col>
        </Row>
      </>
    );
  }
};

function isFuture(event: EventObject) {
  return moment(event.start).isAfter(moment());
}

function getNextEvent(events: EventObject[]) {
  for (const event of events) {
    if (isFuture(event)) {
      return event;
    }
  }
  return null;
}

interface RegistrationProps {
  webinarId: number;
}

const DefaultWebinarRegistration: React.FC<RegistrationProps> = ({
  webinarId,
}) => {
  const api = useHttpService();

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [error, setError] = useState(false);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");

  const [webinar, setWebinar] = useState<any>(null);
  const [events, setEvents] = useState<EventObject[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [registering, setRegistering] = useState(false);
  const MySwal = withReactContent(Swal);

  const nextEvent = useMemo(() => getNextEvent(events), [events]);

  const handleEventRegistration = async (eventId: number) => {
    await apiWrapper(async () => {
      const response = await api.doCreateAttendeeForEvent(webinar.id, eventId, {
        email,
        firstName,
        lastName,
        timezone: Intl.DateTimeFormat().resolvedOptions().timeZone,
      });

      MySwal.fire({
        icon: "success",
        title: "Successfully registered!",
        html: (
          <p>
            <p>
              You've successfully registered for the webinar. You should receive
              an email with further information.
            </p>
            <Button href={response.viewLink}>Watch Webinar</Button>
          </p>
        ),
      });
      console.log(response);
    }, setRegistering);
  };

  useEffect(() => {
    if (!webinarId) {
      return;
    }

    apiWrapper(async () => {
      const [webinar, events] = await Promise.all([
        api.doGetPublicWebinar(webinarId),
        api.doGetAllPublicWebinarEvents(webinarId),
      ]);

      setWebinar(webinar);
      setEvents(events);
    }, setLoading);
  }, [webinarId, api, setLoading, setWebinar, setEvents]);

  return (
    <Container fluid className="main-content-container p-0">
      {loading ? (
        <Row>
          <Col className="text-center mt-5">
            <h1>Loading...</h1>
            <Spinner
              as="span"
              animation="border"
              role="status"
              aria-hidden="true"
            />
          </Col>
        </Row>
      ) : (
        <Row noGutters className="h-100">
          <Col sm={12} lg={7} className="h-100">
            <Row className="w-100">
              <Col></Col>
              <Col xs={8} className="text-center">
                <img
                  className="auth-form__logo mr-3"
                  src={require("../images/Large_Icon.svg")}
                  alt="Shards Dashboards - Login Template"
                />
                Just Webinar
              </Col>
              <Col></Col>
            </Row>
            <Row className="h-100">
              <Col></Col>
              <Col xs={10} className="my-auto text-center">
                <Card className="bg-transparent no-shadow">
                  <CardBody>
                    {/* Logo */}
                    <h3>{webinar?.name}</h3>
                    <hr style={{ width: "25%" }} />

                    {/* Title */}
                    <h5 className="text-center mb-5">
                      This event will start soon
                    </h5>
                    <Row className="mb-5">
                      <Col>
                        <>
                          <Row className={"mt-1 mb-4"}>
                            <Col>
                              <h6 className="mb-0">
                                {`${moment(nextEvent?.start).format(
                                  "dddd, Do MMMM YYYY"
                                )}`}
                                {` at ${moment(nextEvent?.start).format(
                                  "h:mm a"
                                )}`}
                              </h6>
                              <small className="text-muted">
                                {getTimezone(nextEvent?.start)}
                              </small>
                            </Col>
                          </Row>
                          <Countdown
                            date={nextEvent?.start}
                            renderer={(props) =>
                              CountdownRenderer({ ...props, webinar })
                            }
                          />
                          <LoaderButton
                            loading={registering}
                            pill
                            theme="accent"
                            type="submit"
                            className={"mt-4 mb-2 font-weight-bold"}
                            onClick={() =>
                              handleEventRegistration(nextEvent!.id)
                            }
                          >
                            Click Here For Your Spot
                          </LoaderButton>
                          <h6>OR</h6>
                          <p>Find another Event:</p>
                          <div>
                            {events
                              .filter(isFuture)
                              .slice(1)
                              .map((event) => (
                                <Card className="no-shadow border">
                                  <CardBody>
                                    <div>
                                      {`${moment(event.start).format(
                                        "dddd, Do MMMM YYYY"
                                      )}`}
                                      {` at ${moment(event.start).format(
                                        "h:mm a"
                                      )}`}
                                    </div>
                                    <div>
                                      <LoaderButton
                                        loading={registering}
                                        pill
                                        theme="secondary"
                                        type="submit"
                                        className={"mt-4 mb-2 font-weight-bold"}
                                        onClick={() =>
                                          handleEventRegistration(event.id)
                                        }
                                      >
                                        Register
                                      </LoaderButton>
                                    </div>
                                  </CardBody>
                                </Card>
                              ))}
                          </div>
                        </>
                      </Col>
                      <Col>
                        <Card className="bg-transparent no-shadow border">
                          <CardBody>
                            {/* Form Fields */}
                            <Form>
                              <FormGroup className="text-left">
                                <label
                                  className="text-secondary"
                                  htmlFor="firstName"
                                >
                                  First Name
                                </label>
                                <FormInput
                                  type="text"
                                  id="firstName"
                                  placeholder="Enter First Name"
                                  autoComplete="given-name"
                                  invalid={error}
                                  size="lg"
                                  value={firstName}
                                  onChange={(e) => setFirstName(e.target.value)}
                                />
                              </FormGroup>
                              <FormGroup className="text-left">
                                <label
                                  className="text-secondary"
                                  htmlFor="lastName"
                                >
                                  Last Name
                                </label>
                                <FormInput
                                  type="text"
                                  id="lastName"
                                  placeholder="Enter Last Name"
                                  autoComplete="family-name"
                                  invalid={error}
                                  size="lg"
                                  value={lastName}
                                  onChange={(e) => setLastName(e.target.value)}
                                />
                              </FormGroup>
                              <FormGroup className="text-left">
                                <label
                                  className="text-secondary"
                                  htmlFor="emailAddress"
                                >
                                  Email Address
                                </label>
                                <FormInput
                                  type="text"
                                  id="emailAddress"
                                  placeholder="Enter Email Address"
                                  autoComplete="email"
                                  invalid={error}
                                  size="lg"
                                  value={email}
                                  onChange={(e) => setEmail(e.target.value)}
                                />
                              </FormGroup>
                            </Form>
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                    <Row>
                      <Col xs="6">
                        <Card className="no-shadow border">
                          <CardBody>
                            <p className="m-0">Can I Get Slides Copy</p>
                            <small className="text-muted">
                              Yes, we post all of our presentations online
                            </small>
                          </CardBody>
                        </Card>
                      </Col>
                      <Col xs="6">
                        <Card className="no-shadow border">
                          <CardBody>
                            <p className="m-0">How Do I login</p>
                            <small className="text-muted">
                              Instructions will be set via email
                            </small>
                          </CardBody>
                        </Card>
                      </Col>
                    </Row>
                    {/* 
                                        <Row className={"mt-4"}>
                                            <Col>
                                                <div className="mb-3 mx-auto">
                                                    <img
                                                        className="rounded-circle border"
                                                        src={`https://ui-avatars.com/api/?name=${user.firstName}+${user.lastName}`}
                                                        alt={`${user.firstName} ${user.lastName}`}
                                                        width="50"
                                                    />
                                                </div>
                                                <h5>{user.firstName} {user.lastName}</h5>
                                            </Col>
                                        </Row> 
                                    */}
                  </CardBody>
                </Card>
              </Col>
              <Col></Col>
            </Row>
            {/* Meta Details */}
            <Row className="position-absolute w-100">
              <Col></Col>
              <Col xs={8} className="text-center">
                <Link
                  to="/register"
                  className="ml-2 text-muted font-weight-normal"
                >
                  Terms & Conditions
                </Link>
                <span className="mx-1">|</span>
                <Link
                  to="/register"
                  className="ml-2 text-muted font-weight-normal"
                >
                  Privacy Policy
                </Link>
                <span className="mx-1">|</span>
                <Link
                  to="/register"
                  className="ml-2 text-muted font-weight-normal"
                >
                  Disclaimer
                </Link>
              </Col>
              <Col></Col>
            </Row>
          </Col>
          <Col
            sm={12}
            lg={7}
            className="d-none d-sm-block"
            style={{
              backgroundImage: `url('${background}')`,
              backgroundSize: "contain",
              backgroundPositionY: "bottom",
              backgroundPositionX: "center",
              backgroundRepeat: "no-repeat",
            }}
          ></Col>
        </Row>
      )}
    </Container>
  );
};

const WebinarRegistration: React.FC = () => {
  const { loading, webinarId } = useWebinarTemplate(TemplateType.REGISTRATION);

  if (loading) {
    return (
      <Container fluid className="main-content-container p-0">
        <Row>
          <Col className="text-center mt-5">
            <h1>Loading...</h1>
            <Spinner
              as="span"
              animation="border"
              role="status"
              aria-hidden="true"
            />
          </Col>
        </Row>
      </Container>
    );
  }

  // Fallback registration view
  return (
    <DefaultWebinarRegistration
      webinarId={webinarId}
    ></DefaultWebinarRegistration>
  );
};

export default WebinarRegistration;
