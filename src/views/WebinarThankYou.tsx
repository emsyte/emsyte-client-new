import { TemplateType } from "@emsyte/common/template";
import React from "react";
import { useLocation } from "react-router-dom";
import { Container, Row } from "shards-react";
import { useWebinarTemplate } from "../components/templates/hooks";
import TemplateLoading from "../components/templates/TemplateLoading";

export interface DefaultWebinarThankYouProps {
  webinarId: number;
}

const DefaultWebinarThankYou: React.FC<DefaultWebinarThankYouProps> = () => {
  return (
    <Container>
      <Row>Thank you for viewing the webinar!</Row>
    </Container>
  );
};

const WebinarThankYou: React.FC = () => {
  const { search } = useLocation();

  const { loading, webinarId } = useWebinarTemplate(
    TemplateType.THANK_YOU,
    search
  );

  if (loading) {
    return <TemplateLoading></TemplateLoading>;
  }

  // Fallback registration view
  return (
    <DefaultWebinarThankYou webinarId={webinarId}></DefaultWebinarThankYou>
  );
};

export default WebinarThankYou;
