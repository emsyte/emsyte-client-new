import { TemplateType } from "@emsyte/common/template";
import { WebinarObject } from "@emsyte/common/webinar";
import { AxiosError } from "axios";
import FunnelGraph from "funnel-graph-js";
import "funnel-graph-js/dist/css/main.css";
import "funnel-graph-js/dist/css/theme.css";
import "funnel-graph-js/dist/js/funnel-graph.min.js";
import _ from "lodash";
import moment from "moment";
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import {
  Button,
  Col,
  Form,
  FormCheck,
  FormControl,
  Spinner,
  Tab,
  Tabs,
} from "react-bootstrap";
import { Range } from "react-date-range";
import { useForm } from "react-hook-form";
import ReactPlayer from "react-player";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useHistory, useLocation, useParams } from "react-router-dom";
import { Card, CardBody, Container, Row } from "shards-react";
import styled from "styled-components";
import PageTitle from "../components/common/PageTitle";
import TextInput from "../components/form/TextInput";
import MainFooter from "../components/layout/MainFooter";
import RangePicker from "../components/range_picker";
import Notifications from "../components/webinar/edit/Notifications";
import {
  getCategoryLabel,
  useTemplates,
} from "../components/webinar/edit/Templates";
import DragAndDrop from "../components/webinar/upload/DragAndDrop";
import { useInterval } from "../components/webinar/viewer/video/hooks/utils";
import emptyWebinar from "../images/empty-webinar.png";
import colors from "../utils/colors";
import { secondsToDuration } from "../utils/duration";
import { useFeatureFlag } from "../utils/FeatureFlags";
import { setRemoteFormErrors } from "../utils/form";
import { formatDuration } from "../utils/formatters";
import { useChart } from "../utils/hooks/chart";
import { useHttpService } from "../utils/HttpService";

function displayPercent(value: number) {
  return isNaN(value) || !isFinite(value) ? "-%" : value.toFixed(2) + "%";
}

const PlayerRow = styled(Row)`
  margin-bottom: 1.5rem;

  .col {
    margin-top: 1.5rem;
  }
  .card {
    height: 100%;
  }
`;

const PlayerContainer = styled.div`
  position: relative;
  padding-top: ${100 / (1920 / 1080)}%; // Assume standard aspect ratio

  & > * {
    position: absolute;
    top: 0;
    left: 0;
  }
`;

const EmotionChartContainer = styled.div`
  position: relative;
  padding-top: 100%;
`;

const ChartArea = styled.div`
  display: flex;
`;

const ChartOptions = styled.div`
  padding-right: 3rem;
  display: flex;
  flex-direction: column;
`;

const ChartWrapper = styled.div`
  flex-grow: 1;
  height: 400px;
  position: relative;
`;

const ChartContainer = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;

const LegendItem = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-weight: 400;
  font-size: 1rem;
  margin-bottom: 0.375rem;
`;

const LegendColor = styled.div`
  display: inline-block;
  width: 1rem;
  height: 0.25rem;
  margin-left: 1rem;
`;

const StatisticsColumn = styled.div`
  flex-basis: 25%;
  margin: 1.25rem 1rem;
`;

const StatisticsTitle = styled.span`
  font-size: 16px;
  line-height: 1.5rem;
`;

const StatisticsPercentage = styled.span`
  font-size: 40px;
  line-height: 48px;
`;

const StatisticsTotal = styled.span`
  font-size: 16px;
  line-height: 1.8125rem;
  margin-left: 0.25rem;
`;

const ReactionText = styled.span`
  font-size: 20px;
  line-height: 1.5rem;
  color: ${colors.success.toRGBA()};
`;

const ReactionDescriptionText = styled.span`
  font-weight: 400;
  font-size: 0.875rem;
  line-height: 1rem;
`;

const PreserveAspectRatioImg = styled.img`
  object-fit: scale-down;
`;

const ThumbnailContainer = styled.div`
  width: 15.625rem;
  display: flex;
  flex-direction: column;
`;

const Title = styled.h6`
  font-weight: 500;
  margin-bottom: 0;
`;

const FormControlTitle = styled.p`
  font-size: 1rem;
  margin-top: 1.25rem;
  margin-bottom: 0;
`;

const LinkLikeButton = styled.button`
  color: ${colors.blue.toHex()};
  border: none;
  background-color: transparent;
`;

const TemplateTitle1 = styled.p`
  font-weight: normal;
  font-size: 14px;
  line-height: 24px;
  color: #2e3338;
  opacity: 0.75;
  margin-bottom: 0;
`;

const TemplateTitle2 = styled.h5`
  font-weight: 500;
  line-height: 24px;
  color: ${colors.gray900.toHex()};
`;

const SCRUBBER_COLOR = "#444";

function argmax(data: number[]) {
  let max = data[0];
  let index = 0;
  for (let i = 1; i < data.length; i++) {
    if (data[i] > max) {
      index = i;
      max = data[i];
    }
  }
  return index;
}

export interface EmotionChartProps {
  data: {
    neutral: number;
    happiness: number;
    surprise: number;
    sadness: number;
    anger: number;
  };
}
const EmotionChart: React.FC<EmotionChartProps> = ({ data }) => {
  const labels = ["Neutral", "Sadness", "Anger", "Surprise", "Happiness"];

  const sorted = [
    data.neutral,
    data.sadness,
    data.anger,
    data.surprise,
    data.happiness,
  ];

  const { chart, canvasRef } = useChart({
    type: "radar",
    data: {
      labels,
      datasets: [
        {
          borderColor: colors.primary.toRGBA(),
          data: sorted,
        },
      ],
    },
    options: {
      aspectRatio: 1,
      legend: false,
      elements: {
        point: {
          radius: 0,
        },
        line: {
          tension: 0.33,
        },
      },
      scale: {
        ticks: {
          min: 0,
          max: 1,
        },
      },
    },
  });

  useEffect(() => {
    if (chart) {
      chart.data.datasets[0].data = sorted;
      chart.update();
    }
  }, [sorted, chart]);

  return (
    <div style={{ textAlign: "center" }}>
      <div
        style={{
          position: "absolute",
          top: 0,
          left: 0,
          width: "100%",
          height: "100%",
        }}
      >
        <canvas ref={canvasRef} />
      </div>
      <ReactionText>{labels[argmax(sorted)]} Reaction</ReactionText>
      <br />
      <ReactionDescriptionText>
        Average cumulative emotional reaction to this webinar.
      </ReactionDescriptionText>
    </div>
  );
};

function useRefCallback<T extends Function>(callback: T): T {
  const ref = useRef<Function>();
  useEffect(() => {
    ref.current = callback;
  }, [callback]);

  const run = useCallback(
    (...args) => {
      if (ref.current) {
        return ref.current(...args);
      }
    },
    [ref]
  );

  return run as any as T;
}

export interface WebinarDetailsProps {
  id: number;
  webinar: any;
}

const WebinarDetails: React.FC<WebinarDetailsProps> = (props) => {
  const api = useHttpService();
  const { webinar } = props;
  const notificationSaveRef = useRef(() => {});

  // Basic Information
  const [files, setFiles] = useState<any>(null);
  const [thumbnail, setThumbnail] = useState(webinar.thumbnail ?? "");
  const [ThumbnailRemoved, setThumbnailRemoved] = useState(false);
  const replayEnabled = useFeatureFlag("replay");
  let duration = secondsToDuration(webinar.duration || 0);

  const handleDrop = (files) => {
    let fileList = files;
    for (const file of files) {
      if (!file.name) return;
      fileList.length = 0;
      fileList.push(file);
      setThumbnail(URL.createObjectURL(file));
    }
    setFiles(fileList);
  };

  // Templates
  // eslint-disable-next-line
  const [templates, loading, reload] = useTemplates(webinar.id);
  const templateTypes = [
    TemplateType.REGISTRATION,
    TemplateType.CONFIRMATION,
    TemplateType.THANK_YOU,
  ];

  // Api Interactions
  const queryClient = useQueryClient();

  const { register, errors, handleSubmit, setError } = useForm({
    defaultValues: {
      name: webinar.name || "",
      videoUrl: webinar.videoUrl || "",
      replayExpiration: webinar.replayExpiration || "",
    },
  });

  const editMutation = useMutation(
    (data: Object) => api.doUpdateWebinar("" + props.id, data),
    {
      onSuccess(result) {
        // Tell react-query to refetch the results at it's earliest convenience
        queryClient.invalidateQueries("webinar");
      },
      onError(error: AxiosError) {
        setRemoteFormErrors(error.response?.data.errors, setError);
      },
    }
  );

  const cardClasses = "col-lg-10 col-md-11 col-sm-12 align-self-center";
  const cardStyles = { marginTop: "1.25rem" };

  return (
    <Form
      onSubmit={handleSubmit((settings) => {
        let thumbnail: any = undefined;
        notificationSaveRef.current();
        if (files && files.length) {
          api.doUploadFile("webinar", files[0]).then((res) => {
            thumbnail = res;
            editMutation.mutate({ ...settings, thumbnail });
          });
        } else {
          thumbnail = ThumbnailRemoved ? "" : webinar.thumbnail;
          editMutation.mutate({ ...settings, thumbnail });
        }
      })}
    >
      {/* Hidden button, will be called from outside */}
      <button
        type="submit"
        id="analytics-submit-button"
        style={{ display: "none" }}
      >
        submit
      </button>
      <div className="d-flex flex-column">
        {editMutation.isLoading ? (
          <Spinner animation="border" className="align-self-center" />
        ) : (
          ""
        )}
        <Card className={cardClasses} style={cardStyles}>
          <CardBody className="row">
            <div
              className="col-lg-7 col-md-6 col-sm-12"
              style={{ marginBottom: "1rem" }}
            >
              <Title>BASIC INFORMATION</Title>
              <FormControlTitle>Webinar title (public)</FormControlTitle>
              <TextInput
                errors={errors}
                name="name"
                placeholder={"Webinar Title"}
                ref={register({
                  required: "Required",
                  maxLength: {
                    value: 70,
                    message: "Longer then 70 characters",
                  },
                })}
              />
              <FormControlTitle style={{ color: colors.gray600.toHex() }}>
                Video file / URL
              </FormControlTitle>
              <FormControl
                value={webinar.videoUrl}
                placeholder="Video Url"
                disabled={true}
              />
              <span
                style={{
                  fontSize: "0.8rem",
                  color: colors.gray600.toHex(),
                  fontWeight: 400,
                }}
              >
                Video Duration:{" "}
                {`hours: ${duration.hours} minutes: ${duration.minutes} seconds: ${duration.seconds}`}
              </span>

              {replayEnabled && (
                <>
                  <FormControlTitle>Replay Expiration</FormControlTitle>
                  <TextInput
                    errors={errors}
                    name="replayExpiration"
                    placeholder={"Webinar Title"}
                    type="number"
                    ref={register()}
                  />
                  <span style={{ fontSize: "0.8rem", fontWeight: 400 }}>
                    Number of days participants can re-watch the webinar
                  </span>
                </>
              )}
            </div>
            <div className="col-lg-5 col-md-6 col-sm-12 align-self-center d-flex justify-content-center">
              <ThumbnailContainer>
                <p
                  style={{
                    fontWeight: 400,
                    fontSize: "1rem",
                    marginBottom: "0.625rem",
                  }}
                >
                  Webinar Thumbnail
                </p>
                <PreserveAspectRatioImg
                  alt={"Thumbnail"}
                  src={thumbnail || emptyWebinar}
                  className="rounded"
                  width="250"
                  height="250"
                />
                <div className="d-flex justify-content-between">
                  <DragAndDrop handleDrop={handleDrop}>
                    <button className="btn btn-outline-secondary custom-secondary">
                      Replace Image
                    </button>
                  </DragAndDrop>
                  <LinkLikeButton
                    onClick={() => {
                      setThumbnailRemoved(true);
                      setThumbnail("");
                    }}
                  >
                    Remove
                  </LinkLikeButton>
                </div>
              </ThumbnailContainer>
            </div>
          </CardBody>
        </Card>

        <Card className={cardClasses} style={cardStyles}>
          <CardBody>
            <Title style={{ marginBottom: "0.625rem" }}>TEMPLATES</Title>
            <div className="d-flex justify-content-between align-items-center">
              <span style={{ fontSize: "0.8rem", fontWeight: 400 }}>
                Edit page templates for webinar registration, confirmation, and
                thank you.
              </span>
              <LinkLikeButton
                onClick={() => {
                  const win = window.open(
                    `/webinars/${btoa(
                      JSON.stringify({ id: webinar.id })
                    )}/templates`,
                    "_blank"
                  );
                  win?.focus();
                }}
              >
                Change Templates
              </LinkLikeButton>
            </div>
            <div className="row" style={{ marginTop: "1rem" }}>
              {!loading &&
                templateTypes.map((type) => {
                  let template = templates.find(
                    (template) => template.type === type
                  );

                  return (
                    <div className="col-lg-4" style={{ padding: "0 0.625rem" }}>
                      <Card style={{ padding: "1rem 1.25rem" }}>
                        <TemplateTitle1>
                          {getCategoryLabel(type)} Template
                        </TemplateTitle1>
                        <TemplateTitle2>{template?.name}</TemplateTitle2>
                        <div
                          className="d-flex align-items-center"
                          style={{ marginTop: "2rem" }}
                        >
                          <button
                            className="btn btn-outline-secondary flex-grow-1 custom-secondary"
                            onClick={() => {
                              const win = window.open(
                                `/template/preview/${template?.id}`,
                                "_blank"
                              );
                              win?.focus();
                            }}
                          >
                            Preview
                          </button>
                          <div
                            className="d-flex align-self-stretch"
                            style={{ paddingLeft: "0.5rem" }}
                          >
                            <Button
                              onClick={() => {
                                const win = window.open(
                                  `/editor/${
                                    template?.instances[0]?.id
                                  }?webinar=${btoa(
                                    JSON.stringify({ id: webinar.id })
                                  )}`,
                                  "_blank"
                                );
                                win?.focus();
                              }}
                              variant="outline-primary"
                              style={{ fontSize: "16px" }}
                              className="align-self-stretch"
                            >
                              Edit
                            </Button>
                          </div>
                        </div>
                      </Card>
                    </div>
                  );
                })}
            </div>
          </CardBody>
        </Card>

        <div className={cardClasses + " p-0"} style={cardStyles}>
          <Notifications
            webinar={webinar}
            onWebinarUpdate={() => new Promise<any>(() => {})}
            saveRef={notificationSaveRef}
            onNext={() => {}}
          />
        </div>
      </div>
    </Form>
  );
};

export interface PlayerChartProps {
  currentTime: number;
  data: Record<string, number[]>;
  reactionCount: number;
  onScrub: (currentTime: number) => void;
}

const PlayerChart: React.FC<PlayerChartProps> = ({
  currentTime,
  onScrub,
  data,
  reactionCount,
}) => {
  const dataLength = data.attention.length;
  const [checked, setChecked] = useState<boolean[]>(
    new Array(dataLength).fill(true)
  );

  const handleScrub = useRefCallback(onScrub);

  const config = {
    type: "line",
    data: {
      labels: new Array(dataLength),
      datasets: [
        {
          label: "Attention",
          fill: false,
          borderColor: colors.black.toRGBA(),
          data: data.attention,
        },
        {
          label: "Neutral",
          fill: false,
          borderColor: colors.gray.toRGBA(),
          data: data.neutral,
        },
        {
          label: "Happiness",
          fill: false,
          borderColor: colors.green.toRGBA(),
          data: data.happiness,
        },
        {
          label: "Surprise",
          fill: false,
          borderColor: colors.yellow.toRGBA(),
          data: data.surprise,
        },
        {
          label: "Anger",
          fill: false,
          borderColor: colors.red.toRGBA(),
          data: data.anger,
        },
        {
          label: "Sadness",
          fill: false,
          borderColor: colors.indigo.toRGBA(),
          data: data.sadness,
        },
      ],
    },
    options: {
      tooltips: {
        enabled: false,
        custom: false,
      },
      legend: false,
      elements: {
        point: {
          radius: 0,
        },
        line: {
          tension: 0.33,
        },
      },
      scales: {
        xAxes: [
          {
            gridLines: false,
            ticks: {
              display: false,
            },
          },
        ],
      },
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
        scrub_line: {
          currentTime: 0,
        },
      },
    },
    plugins: [
      {
        id: "scrub_line",
        afterInit: (chartInstance) => {
          chartInstance.canvas.addEventListener("click", (event) => {
            const area = chartInstance.chartArea;
            const progress =
              (event.offsetX - area.left) / (area.right - area.left);
            handleScrub(Math.min(Math.max(progress, 0), 1));
          });
        },
        afterDraw: (chartInstance, _, options) => {
          const area = chartInstance.chartArea;
          const ctx = chartInstance.ctx;
          const width = area.right - area.left;
          const height = chartInstance.canvas.clientHeight;
          const x = options.currentTime * width + area.left;

          (window as any).chartInstance = chartInstance;

          ctx.lineWidth = 1;
          ctx.strokeStyle = SCRUBBER_COLOR;
          ctx.fillStyle = SCRUBBER_COLOR;

          // Scrub line
          ctx.moveTo(x, 0);
          ctx.lineTo(x, height);
          ctx.stroke();

          // Bottom arrow
          ctx.beginPath();
          ctx.moveTo(x, height - 6);
          ctx.lineTo(x - 3, height);
          ctx.lineTo(x + 3, height);
          ctx.fill();

          // Top arrow
          ctx.beginPath();
          ctx.moveTo(x, 6);
          ctx.lineTo(x - 3, 0);
          ctx.lineTo(x + 3, 0);
          ctx.fill();
        },
      },
    ],
  };
  const { chart, canvasRef } = useChart(config);

  const handleUpdateChart = (event) => {
    const index = +event.target.value;
    checked[index] = event.target.checked;
    setChecked(_.clone(checked));
    chart.getDatasetMeta(index).hidden = !event.target.checked;
    chart.update();
  };

  const handleShowAllChart = (event) => {
    config.data.datasets.forEach((set, i) => {
      chart.getDatasetMeta(i).hidden = !event.target.checked;
      checked[i] = event.target.checked;
    });
    setChecked(_.clone(checked));
    chart.update();
  };

  useEffect(() => {
    if (!chart) {
      return;
    }

    chart.options.plugins.scrub_line.currentTime = currentTime;
    chart.update();
  }, [currentTime, chart]);

  useEffect(() => {
    if (!chart) {
      return;
    }

    chart.data = config.data;
    chart.update();
    // eslint-disable-next-line
  }, [data, chart]);

  return (
    <ChartArea>
      <ChartOptions>
        <div style={{ marginBottom: "0.5rem" }}>
          <div style={{ borderBottom: "1px solid " + colors.gray600.toHex() }}>
            <FormCheck
              custom
              label="Show All"
              name="all"
              id="all"
              checked={checked.every((a) => a)}
              onChange={handleShowAllChart}
            ></FormCheck>
          </div>
        </div>
        {config.data.datasets.map((set, i) => (
          <LegendItem key={i}>
            <FormCheck
              custom
              id={`chart-set-${i}`}
              onChange={handleUpdateChart}
              value={i}
              label={set.label}
              type="switch"
              checked={checked[i]}
            />
            <LegendColor style={{ background: set.borderColor }} />
          </LegendItem>
        ))}
        <small className="text-muted mt-auto">
          Total reactions: {reactionCount}
        </small>
      </ChartOptions>
      <ChartWrapper>
        <ChartContainer>
          <canvas ref={canvasRef}></canvas>
        </ChartContainer>
      </ChartWrapper>
    </ChartArea>
  );
};

export interface AttendanceChartProps {
  attendance: number[];
}

const AttendanceChart: React.FC<AttendanceChartProps> = ({ attendance }) => {
  const config = {
    type: "line",
    data: {
      labels: Array.from({ length: attendance.length }).map((_, i) =>
        formatDuration(secondsToDuration(i * 2 * 60))
      ),
      datasets: [
        {
          label: "Attendance",
          fill: false,
          borderColor: colors.black.toRGBA(),
          data: attendance,
        },
      ],
    },
    options: {
      tooltips: {
        enabled: false,
        custom: false,
      },
      legend: false,
      elements: {
        point: {
          radius: 0,
        },
        line: {
          tension: 0.33,
        },
      },

      responsive: true,
      maintainAspectRatio: false,
    },
  };
  const { chart, canvasRef } = useChart(config);

  useEffect(() => {
    if (!chart) {
      return;
    }

    chart.data = config.data;
    chart.update();
    // eslint-disable-next-line
  }, [attendance, chart]);

  return (
    <ChartArea>
      <ChartWrapper>
        <ChartContainer>
          <canvas ref={canvasRef}></canvas>
        </ChartContainer>
      </ChartWrapper>
    </ChartArea>
  );
};

export const ANALYTIC_VIEWS = {
  analytics: "WebinarAnalysis",
  scheduledSessions: "scheduled_sessions",
  details: "details",
};

export interface WebinarAnalyticsProps {}

const WebinarAnalytics: React.FC<WebinarAnalyticsProps> = () => {
  const { id: encId } = useParams<{ id: string }>();
  const history = useHistory();
  const { id } = JSON.parse(atob(encId));

  const queryClient = useQueryClient();

  const api = useHttpService();

  const [webinars, setWebinars] = useState<WebinarObject[]>([]);
  let urlQueries = useLocation().search;
  let initialScreen = ANALYTIC_VIEWS.analytics;
  if (urlQueries.includes(`view=${ANALYTIC_VIEWS.scheduledSessions}`))
    initialScreen = ANALYTIC_VIEWS.scheduledSessions;
  if (urlQueries.includes(`view=${ANALYTIC_VIEWS.details}`))
    initialScreen = ANALYTIC_VIEWS.details;
  const [key, setKey] = useState(initialScreen);

  const [reactPlayer, setReactPlayer] = useState<ReactPlayer | null>();
  const [currentTime, setCurrentTime] = useState(0);

  const [analytics, setAnalytics] = useState<any>(null);
  const [name, setName] = useState<string>("");
  const [editingName, setEditingName] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);

  const [range, setRange] = useState<Range>({
    startDate: moment().startOf("week").toDate(),
    endDate: moment().endOf("week").toDate(),
  });

  // Only for the name now, it will be good to reformat everything with React-Query
  const webinarQuery = useQuery("webinar", () => api.doGetWebinar(id));

  useEffect(() => {
    api.doGetWebinarAnalytics(id, range as any).then(setAnalytics);
  }, [id, range, api]);

  useEffect(() => {
    api
      .doGetAllWebinarsForUser()
      .then((response) => response.data)
      .then(setWebinars);
  }, [api]);

  const webinar = useMemo(() => {
    return webinars.find((webinar) => webinar.id === id);
  }, [webinars, id]);

  useInterval(() => {
    if (!reactPlayer) {
      return;
    }
    const time = reactPlayer.getCurrentTime();
    const length = reactPlayer.getDuration();
    setCurrentTime(time / length);
  }, 1000 / 60);

  const container = "#funnel";

  const drawFunnel = useCallback(
    (funnelGraph) => {
      let theContainer = document.querySelector(container);
      if (theContainer) {
        // with every draw it will create elements, so must remove the old
        let oldSvgContainer = document.querySelector(
          ".svg-funnel-js__container"
        );
        let oldSvgLabels = document.querySelector(".svg-funnel-js__labels");
        if (oldSvgContainer) theContainer.removeChild(oldSvgContainer);
        if (oldSvgLabels) theContainer.removeChild(oldSvgLabels);

        // Main function to draw
        funnelGraph.draw();

        // Getting the generated svg and statisticsContainer
        let svg = document
          .querySelector(".svg-funnel-js__container")
          ?.querySelector("svg");
        let statisticsContainer = document.querySelector(
          "#statistics-container"
        );

        // Must cut the svg in half, 159px is calculated based in the containter height of 400px
        svg?.setAttribute("height", "159px");
        // won't draw for background without this
        svg?.setAttribute("xmlns", "http://www.w3.org/2000/svg");

        // @ts-ignore
        let transformedSvg = svg.outerHTML;
        // Need to do transformations in the svg element to be compatible for background
        transformedSvg = transformedSvg.replaceAll(`"`, `'`);
        transformedSvg = transformedSvg.replaceAll(`#`, `%23`);

        console.log({ transformedSvg, statisticsContainer });

        // Setting the svg for background via CSS

        if (statisticsContainer) {
          // @ts-ignore
          statisticsContainer.style.backgroundImage = `url("data:image/svg+xml;utf8,${transformedSvg}")`;
        } else {
          // It will trigger the effect again until the statisticsContainer is rendered
          setAnalytics(_.clone(analytics));
        }
      }
    },
    [analytics]
  );

  useEffect(() => {
    // The funnel Component
    var funnelGraph = new FunnelGraph({
      container: container,
      data: {
        labels: [
          "VIEWED REGISTRATION",
          "REGISTERED",
          "ATTENDED",
          "OFFER CLICKS",
        ],
        colors: ["#F9CB76", "#4ECDC4"],
        values: [
          _.get(analytics, ["totals", "uniqueRegistrationVisits"], 0),
          _.get(analytics, ["totals", "registered"], 0),
          _.get(analytics, ["totals", "attended"], 0),
          _.get(analytics, ["totals", "offerClicks"], 0),
        ],
      },
    });

    drawFunnel(funnelGraph);
  }, [analytics, drawFunnel]);

  const tabBackgroundColor = colors.notWhite.toHex();

  return (
    <div className="analytics-wrapper">
      <Container
        fluid
        className="webinar-calendar main-content-container bg-white px-4 container2"
      >
        <Row
          noGutters
          className="page-header d-flex justify-content-between"
        >
          {editingName ? (
            <div className="d-flex">
              <FormControl
                name="name"
                placeholder={"Webinar Title"}
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <Button
                onClick={() => {
                  setLoading(true);
                  api
                    .doUpdateWebinar(id, {
                      name,
                    })
                    .then((res) => {
                      // @ts-ignore
                      webinar.name = res.data.name;
                      setEditingName(false);
                      setLoading(false);
                      queryClient.invalidateQueries("webinar");
                    })
                    .finally(() => setLoading(false));
                }}
                variant="light"
                style={{
                  backgroundColor: colors.notWhite.toHex(),
                  margin: "0 0.25rem",
                }}
              >
                Save
              </Button>
              <Button
                onClick={() => setEditingName(false)}
                variant="light"
                style={{
                  backgroundColor: colors.notWhite.toHex(),
                  margin: "0 0.25rem",
                }}
              >
                Cancel
              </Button>
            </div>
          ) : (
            <PageTitle
              title={webinarQuery.data?.data.name}
              className="text-sm-left mb-3"
            >
              <a
                href="/#"
                style={{ marginLeft: "0.5rem" }}
                onClick={(e) => {
                  e.preventDefault();
                  setName(webinarQuery.data?.data.name || "");
                  setEditingName(true);
                }}
              >
                Edit
              </a>
            </PageTitle>
          )}
          {loading && <Spinner animation="border" />}
          {/*<button onClick={drawFunnel}>draw</button>*/}
          <Button
            variant="light"
            style={{
              backgroundColor: "#F7F8F9",
              border: "1px solid " + colors.gainsboro.toHex(),
            }}
            onClick={() => {
              btoa(JSON.stringify({ id: webinar?.id }));
              history.push(
                `/webinars/${btoa(JSON.stringify({ id: webinar?.id }))}/basic`
              );
            }}
          >
            Edit Webinar
          </Button>
        </Row>
        {!webinar || !analytics ? (
          <Spinner animation="border"></Spinner>
        ) : (
          <>
            <Tabs
              defaultActiveKey="WebinarAnalysis"
              id="uncontrolled-tab-example"
              activeKey={key}
              onSelect={(key) => {
                setKey(key || "");
                history.push(
                  `/analytics/${btoa(
                    JSON.stringify({ id: webinar?.id })
                  )}?view=${key}`
                );
              }}
            >
              <Tab
                eventKey={`${ANALYTIC_VIEWS.analytics}`}
                title="Webinar Analysis"
                style={{
                  backgroundColor: tabBackgroundColor,
                  marginLeft: "-1.5rem",
                  marginRight: "-1.5rem",
                  paddingLeft: "1.5rem",
                  paddingRight: "1.5rem",
                }}
              >
                <Row
                  style={{
                    marginLeft: "-1.5rem",
                    marginRight: "-1.5rem",
                    paddingTop: "1rem",
                  }}
                >
                  <Col style={{ paddingLeft: 0, paddingRight: 0 }}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "flex-end",
                        marginLeft: "1.5rem",
                        marginRight: "1.5rem",
                      }}
                    >
                      <RangePicker range={range} onChange={setRange} />
                    </div>

                    <div id="statistics-container">
                      <StatisticsColumn
                        style={{
                          borderRight: "1px solid " + colors.gray600.toHex(),
                        }}
                      >
                        <StatisticsTitle>VIEWED REGISTRATION</StatisticsTitle>
                        <br />
                        <StatisticsPercentage>
                          {analytics.totals.uniqueRegistrationVisits}
                        </StatisticsPercentage>
                      </StatisticsColumn>
                      <StatisticsColumn
                        style={{
                          borderRight: "1px solid " + colors.gray600.toHex(),
                        }}
                      >
                        <StatisticsTitle>REGISTERED</StatisticsTitle>
                        <br />
                        <StatisticsPercentage>
                          {displayPercent(
                            (analytics.totals.registered /
                              analytics.totals.uniqueRegistrationVisits) *
                              100
                          )}
                        </StatisticsPercentage>
                        <StatisticsTotal>
                          ({analytics.totals.registered})
                        </StatisticsTotal>
                      </StatisticsColumn>
                      <StatisticsColumn
                        style={{
                          borderRight: "1px solid " + colors.gray600.toHex(),
                        }}
                      >
                        <StatisticsTitle>ATTENDED</StatisticsTitle>
                        <br />
                        <StatisticsPercentage>
                          {displayPercent(
                            (analytics.totals.attended /
                              analytics.totals.registered) *
                              100
                          )}
                        </StatisticsPercentage>
                        <StatisticsTotal>
                          ({analytics.totals.attended})
                        </StatisticsTotal>
                      </StatisticsColumn>
                      <StatisticsColumn>
                        <StatisticsTitle>OFFER CLICKS</StatisticsTitle>
                        <br />
                        <StatisticsPercentage>
                          {displayPercent(
                            (analytics.totals.offerClicks /
                              analytics.totals.attended) *
                              100
                          )}
                        </StatisticsPercentage>
                        <StatisticsTotal>
                          ({analytics.totals.offerClicks})
                        </StatisticsTotal>
                      </StatisticsColumn>
                    </div>
                  </Col>
                </Row>
                <PlayerRow className="d-block d-lg-flex">
                  <Col style={{ flexGrow: 2 }}>
                    <PlayerContainer>
                      <ReactPlayer
                        ref={setReactPlayer}
                        controls
                        width="100%"
                        height="100%"
                        url={webinar.videoUrl}
                      ></ReactPlayer>
                    </PlayerContainer>
                  </Col>
                  <Col>
                    <Card>
                      <CardBody>
                        <h5>AVERAGE REACTION</h5>
                        {analytics?.average ? (
                          <EmotionChartContainer>
                            <EmotionChart
                              data={analytics.average}
                            ></EmotionChart>
                          </EmotionChartContainer>
                        ) : (
                          <div className="text-center">
                            <h5>No reaction data for this webinar yet</h5>
                            <small className="text-muted">
                              Reaction reports can take up to 24 hours after the
                              event to be processed.
                            </small>
                          </div>
                        )}
                      </CardBody>
                    </Card>
                  </Col>
                </PlayerRow>
                <Card>
                  <CardBody>
                    {analytics?.series ? (
                      <div>
                        <h6 style={{ fontWeight: 500 }}>REACTION TIMELINE</h6>
                        <PlayerChart
                          data={analytics.series}
                          onScrub={(currentTime) => {
                            if (reactPlayer) {
                              reactPlayer.seekTo(currentTime);
                            }
                          }}
                          reactionCount={analytics.totals.reactionCount}
                          currentTime={currentTime}
                        />
                      </div>
                    ) : (
                      <div className="text-center">
                        <h5>No reaction data for this webinar yet</h5>
                        <small className="text-muted">
                          Reaction reports can take up to 24 hours after the
                          event to be processed.
                        </small>
                      </div>
                    )}
                  </CardBody>
                </Card>
                <Card className="mt-4">
                  <CardBody>
                    {analytics?.attendance ? (
                      <div>
                        <h5 className="text-center">ATTENDANCE</h5>

                        <AttendanceChart attendance={analytics.attendance} />
                      </div>
                    ) : (
                      <div className="text-center">
                        <h5>No attendance data for this webinar yet</h5>
                      </div>
                    )}
                  </CardBody>
                </Card>
                <MainFooter />
              </Tab>

              {/* <Tab
                eventKey={`${ANALYTIC_VIEWS.scheduledSessions}`}
                title="Scheduled Sessions"
                style={{
                  backgroundColor: tabBackgroundColor,
                  marginLeft: "-1.5rem",
                  marginRight: "-1.5rem",
                  paddingLeft: "1.5rem",
                  paddingRight: "1.5rem",
                }}
              >
                <MainFooter />
              </Tab> */}

              <Tab
                eventKey={`${ANALYTIC_VIEWS.details}`}
                title="Webinar Details"
                style={{
                  backgroundColor: tabBackgroundColor,
                  marginLeft: "-1.5rem",
                  marginRight: "-1.5rem",
                  paddingLeft: "1.5rem",
                  paddingRight: "1.5rem",
                }}
              >
                {!webinarQuery.isLoading && (
                  <WebinarDetails id={id} webinar={webinarQuery.data?.data} />
                )}
                <MainFooter />
              </Tab>
            </Tabs>
          </>
        )}
        {/* Where Funnel is generated, this must exist */}
        <div
          id="funnel"
          style={{
            width: "800px",
            height: "400px",
            border: "1px solid red",
            position: "absolute",
            visibility: "hidden",
          }}
        />
      </Container>
      {key === ANALYTIC_VIEWS.details ? (
        <div className="actions">
          <button
            className="col-lg-2 col-md-1 btn btn-primary"
            style={{ fontSize: "1rem" }}
            onClick={() => {
              let submitButton = document.getElementById(
                "analytics-submit-button"
              );
              submitButton?.click();
            }}
          >
            Save Changes
          </button>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default WebinarAnalytics;
