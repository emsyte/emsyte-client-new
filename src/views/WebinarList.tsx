import { faCaretDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment-timezone";
import React, { useEffect, useState } from "react";
import { Calendar as BigCalendar, momentLocalizer } from "react-big-calendar";
import { useInfiniteQuery } from "react-query";
import { useHistory, useLocation } from "react-router-dom";
import { Col, Container, Popover, PopoverBody, Row } from "shards-react";
import Agenda from "../components/calendar/views/Agenda";
import AgendaTooltipContent from "../components/calendar/views/AgendaTooltipContent";
import {
  DayWrapper,
  fetchEventsInfinite,
  INFINITE_EVENTS_QUERY_KEY,
} from "../components/calendar/views/Day";
import Toolbar from "../components/calendar/views/Toolbar";
import { useHttpService } from "../utils/HttpService";

const localizer = momentLocalizer(moment);

const WeekHeader = (props) => {
  let { date } = props;
  const isToday = moment(date).isSame(moment(), "day");
  return (
    <Row className={`${isToday ? `mt-0` : `mt-3`} mb-2 pt-3 pb-2`}>
      <Col md={12} className="text-center">
        {isToday && (
          <>
            <FontAwesomeIcon className="text-primary" icon={faCaretDown} />
            <br />
          </>
        )}

        <span className={`font-weight-bold ${isToday && `text-primary`}`}>
          {date.toLocaleString("default", { weekday: "long" })}
        </span>
      </Col>
      <Col md={12} className="text-center text-muted">
        <small
          className="m-0 rbc-line-height"
          style={{ color: isToday ? "#8db9df" : "" }}
        >{`${date.getDate()}.${date
          .getFullYear()
          .toString()
          .substr(-2)}`}</small>
      </Col>
    </Row>
  );
};

const CustomEvent: any = ({ event }) => {
  const [open, setOpen] = useState(false);

  return (
    <div
      style={{ position: "absolute", top: 0, right: 0, left: 0, bottom: 0 }}
      id={`popover-${event.id}`}
      onClick={() => setOpen(!open)}
    >
      <div>
        <Popover
          target={`#popover-${event.id}`}
          placement="bottom"
          open={open}
          toggle={() => setOpen(!open)}
        >
          <PopoverBody className="pl-0 pr-0 pt-0 pb-0">
            <AgendaTooltipContent
              startTime={moment(event.start).toDate()}
              endTime={moment(event.end).toDate()}
              eventId={event.id}
              webinar={event.webinar}
            />
          </PopoverBody>
        </Popover>
      </div>
    </div>
  );
};

const WebinarList = () => {
  const api = useHttpService();
  const location = useLocation();
  const history = useHistory();
  const [events, setEvents] = useState<any>([]);
  const view = (new URLSearchParams(location.search).get("view") ||
    "agenda") as "week" | "agenda";
  const [loading, setLoading] = useState<boolean>(false);

  const eventsQuery = useInfiniteQuery(
    INFINITE_EVENTS_QUERY_KEY,
    fetchEventsInfinite(api)
  );

  const [page, setPage] = useState(1);

  const handleEventParsing = (_events) => {
    _events = _events.map((event) => {
      return {
        ...event,
        start: moment(event.start).toDate(),
        end: moment(event.end).toDate(),
      };
    });
    setEvents(_events);
  };

  const handleViewChange = (view: string) => {
    history.push("?view=" + view);
  };

  useEffect(() => {
    if (view !== "week" || events.length) {
      return;
    }
    setLoading(true);

    api
      .doGetAllEventsForUser()
      .then((data) => {
        handleEventParsing(data.data);
      })
      .finally(() => setLoading(false));
  }, [api, view, events]);

  return (
    <Container fluid className="main-content-container">
      <BigCalendar
        formats={{ timeGutterFormat: "h A" }}
        events={events}
        timeslots={4}
        step={15}
        views={{
          week: true,
          agenda: Agenda,
          day: DayWrapper,
        }}
        showMultiDayTimes
        defaultView={view}
        defaultDate={new Date()}
        components={{
          toolbar: Toolbar({
            handleViewChange,
            loading,
            eventsQuery,
            page,
            setPage,
          }),
          week: {
            header: WeekHeader,
          },
          event: CustomEvent,
        }}
        localizer={localizer}
        selectable={false}
        onSelectSlot={(e) => console.log(e)}
      />
    </Container>
  );
};

export default WebinarList;
