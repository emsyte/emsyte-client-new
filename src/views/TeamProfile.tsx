import React from "react";
import { Redirect, Route, Switch, useRouteMatch } from "react-router-dom";
import styled from "styled-components";
import GeneralTeamProfile from "../components/team-profile/GeneralTeamProfile";
import TeamIntegrations from "../components/team-profile/TeamIntegrations";
import TeamSubscription from "../components/team-profile/TeamSubscription";

const ContentCard = styled.div`
  display: flex;
  align-items: flex-start;
  min-width: 900px;
  justify-content: center;
`;

const TeamProfile = () => {
  const { path } = useRouteMatch();

  return (
    <Switch>
      <Route path={`${path}/subscription`}>
        <TeamSubscription></TeamSubscription>
      </Route>
      <Route path={`${path}/integrations`}>
        <TeamIntegrations></TeamIntegrations>
      </Route>
      <Route path={`${path}/members`}>
        <GeneralTeamProfile />
      </Route>
      <Route path={`${path}`} exact>
        <Redirect to={`${path}/members`}></Redirect>
      </Route>
    </Switch>
  );
};

export default TeamProfile;
