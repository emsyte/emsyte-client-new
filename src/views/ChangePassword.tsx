import React from "react";
import { Link } from "react-router-dom";

import {
  Container,
  Row,
  Col,
  Card,
  CardBody,
  Form,
  FormGroup,
  FormInput,
  Button,
} from "shards-react";

const ChangePassword = () => (
  <Container
    fluid
    className="main-content-container p-0"
    style={{ height: "100vh" }}
  >
    <Row noGutters className="h-100">
      <Col lg="5" md="5" className="h-100">
        <Row className="h-100">
          <Col></Col>
          <Col xs={10} className="my-auto">
            <Card className="bg-transparent no-shadow px-5">
              <CardBody>
                {/* Logo */}
                <img
                  className="auth-form__logo d-table mx-auto mb-3"
                  src={require("../images/Large_Icon.svg")}
                  alt="Shards Dashboards - Change Password Template"
                />

                {/* Title */}
                <h3 className=" font-weight-light  text-center mb-4">
                  Change Password
                </h3>

                {/* Form Fields */}
                <Form>
                  <FormGroup>
                    <label
                      className="text-secondary"
                      htmlFor="exampleInputPassword1"
                    >
                      Password
                    </label>
                    <FormInput
                      type="password"
                      id="exampleInputPassword1"
                      placeholder="Password"
                      autoComplete="new-password"
                      size="lg"
                    />
                  </FormGroup>
                  <FormGroup>
                    <label
                      className="text-secondary"
                      htmlFor="exampleInputPassword2"
                    >
                      Repeat Password
                    </label>
                    <FormInput
                      type="password"
                      id="exampleInputPassword2"
                      placeholder="Repeat Password"
                      autoComplete="new-password"
                      size="lg"
                    />
                  </FormGroup>
                  <Button
                    pill
                    theme="accent"
                    className="d-table mx-auto"
                    type="submit"
                  >
                    Change Password
                  </Button>
                </Form>
              </CardBody>
            </Card>
          </Col>
          <Col></Col>
        </Row>
        <Row className="position-absolute w-100" style={{ bottom: "30%" }}>
          <Col></Col>
          <Col xs={8} className="text-center">
            {/* Meta Details */}
            <div className="auth-form__meta d-flex mb-0">
              <Link to="/login" className="mx-auto">
                Back
              </Link>
            </div>
          </Col>
          <Col></Col>
        </Row>
      </Col>
      <Col lg="7" md="7" className="bg-primary"></Col>
    </Row>
  </Container>
);

export default ChangePassword;
