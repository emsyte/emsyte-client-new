/* eslint jsx-a11y/anchor-is-valid: 0 */

/* eslint jsx-a11y/anchor-is-valid: 0 */
import React, { useState } from "react";
import {
  Button,
  Form,
  FormControl,
  FormGroup,
  FormLabel,
} from "react-bootstrap";
import { Link, withRouter } from "react-router-dom";
import { compose } from "recompose";
import {
  Card,
  CardBody,
  Col,
  Container,
  FormCheckbox,
  Row,
} from "shards-react";
import background from "../images/bg-onboarding.png";
import { ROUTES } from "../utils/constants";

const validationErrors = {
  firstName: "Please include a first name",
  lastName: "Please include a last name",
  email: "Please include a valid email",
  password: "Please include a password. Must be greater than 8 characters",
  organization: "Must not contain any special characters",
};

const Register = ({ history }: any) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [organization, setOrganization] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isTermsChecked, setIsTermsChecked] = useState(false);

  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    if (form.checkValidity() !== false) {
      const passwordValidation =
        password.trim() &&
        password.length >= 8 &&
        organization.trim() &&
        password.length !== 0;
      if (
        email.trim() &&
        passwordValidation &&
        firstName.trim() &&
        lastName.trim() &&
        organization.trim() &&
        isTermsChecked
      ) {
        history.push(ROUTES.MEMBERSHIP, {
          email,
          password,
          firstName,
          lastName,
          organization,
        });
      }
    }
    setValidated(true);
  };

  return (
    <Container
      fluid
      className="main-content-container p-0"
      style={{ height: "100vh" }}
    >
      <Row noGutters className="h-100">
        <Col lg="5" md="5" className="h-100">
          <Row className="h-100">
            <Col></Col>
            <Col xs={10} className="my-auto">
              <Card className="bg-transparent no-shadow">
                <CardBody>
                  {/* Logo */}
                  <img
                    className="auth-form__logo d-table mx-auto mb-3"
                    src={require("../images/Large_Icon.svg")}
                    alt="Community - Register"
                  />

                  {/* Title */}
                  <h3 className=" text-center font-weight-light mb-4">
                    Create New Account
                  </h3>

                  {/* Form Fields */}
                  <Form
                    noValidate
                    validated={validated}
                    onSubmit={handleSubmit}
                  >
                    <Form.Row>
                      <Form.Group as={Col} md="6">
                        <Form.Label>First name</Form.Label>
                        <Form.Control
                          required
                          type="text"
                          placeholder="First name"
                          name="firstName"
                          size="lg"
                          onChange={(e) => setFirstName(e.target.value)}
                        />
                        <FormControl.Feedback type="invalid">
                          {validationErrors.firstName}
                        </FormControl.Feedback>
                      </Form.Group>
                      <Form.Group as={Col} md="6">
                        <Form.Label>Last name</Form.Label>
                        <Form.Control
                          required
                          type="text"
                          placeholder="Last name"
                          name="lastName"
                          size="lg"
                          onChange={(e) => setLastName(e.target.value)}
                        />
                        <FormControl.Feedback type="invalid">
                          {validationErrors.lastName}
                        </FormControl.Feedback>
                      </Form.Group>
                    </Form.Row>
                    <FormGroup>
                      <FormLabel>Organization</FormLabel>
                      <FormControl
                        required
                        type="text"
                        placeholder="Enter Your Organization Name"
                        name="organization"
                        size="lg"
                        onChange={(e) => setOrganization(e.target.value)}
                      />
                      <FormControl.Feedback type="invalid">
                        {validationErrors.email}
                      </FormControl.Feedback>
                    </FormGroup>
                    <FormGroup>
                      <FormLabel>Email Address</FormLabel>
                      <FormControl
                        required
                        type="text"
                        placeholder="Enter Email"
                        name="email"
                        size="lg"
                        onChange={(e) => setEmail(e.target.value)}
                      />
                      <FormControl.Feedback type="invalid">
                        {validationErrors.email}
                      </FormControl.Feedback>
                    </FormGroup>
                    <FormGroup>
                      <FormLabel>Password</FormLabel>
                      <FormControl
                        required
                        type="password"
                        placeholder="Password"
                        name="password"
                        autoComplete="new-password"
                        size="lg"
                        isInvalid={password.length < 8 && password.length > 0}
                        onChange={(e) => setPassword(e.target.value)}
                      />
                      <FormControl.Feedback type="invalid">
                        {validationErrors.password}
                      </FormControl.Feedback>
                    </FormGroup>
                    <FormGroup>
                      <FormCheckbox
                        required
                        onChange={(e) => setIsTermsChecked(!isTermsChecked)}
                        checked={isTermsChecked}
                      >
                        <span className="ml-3">
                          I agree with the{" "}
                          <a
                            href="https://app.termly.io/document/terms-and-conditions/7a965e8e-6637-494b-a432-0b092decef21"
                            rel="nofollow noopener noreferrer"
                            target="_blank"
                          >
                            Terms &amp; Conditions
                          </a>
                          {" and "}
                          <a
                            href="https://app.termly.io/document/privacy-policy/79f3e9f8-7183-4ac2-a44e-1661e4455f08"
                            rel="nofollow noopener noreferrer"
                            target="_blank"
                          >
                            Privacy Policy
                          </a>
                        </span>
                      </FormCheckbox>
                    </FormGroup>
                    <Button
                      block
                      className="d-table mx-auto py-2"
                      type="submit"
                    >
                      Membership Details
                    </Button>
                  </Form>
                </CardBody>
                <div className="auth-form__meta d-flex mb-0">
                  <Link to="/login" className="mx-auto">
                    Back
                  </Link>
                </div>
                {/* Social Icons */}
                {/* <CardFooter>
                  <ul className="auth-form__social-icons d-table mx-auto">
                    <li>
                      <a href="#">
                        <i className="fab fa-facebook-f" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fab fa-twitter" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fab fa-github" />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i className="fab fa-google-plus-g" />
                      </a>
                    </li>
                  </ul>
                </CardFooter> */}
              </Card>
            </Col>
            <Col></Col>
          </Row>
        </Col>

        <Col
          sm={12}
          lg={7}
          className="d-none d-sm-block"
          style={{
            backgroundImage: `url('${background}')`,
            backgroundSize: "contain",
            backgroundPositionY: "bottom",
            backgroundPositionX: "center",
            backgroundRepeat: "no-repeat",
          }}
        ></Col>
      </Row>
    </Container>
  );
};

const enhance = compose(withRouter)(Register);

export default enhance;
