import React, { useRef, useState } from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FormInput, FormTextarea } from "shards-react";

export interface CopyInputProps {
  value: string;
  textarea?: boolean;
}

const CopyInput: React.FC<CopyInputProps> = ({ value, textarea }) => {
  const ref = useRef<HTMLInputElement>();
  const DEFAULT_TEXT = "Copy to clipboard";
  const [text, setText] = useState(DEFAULT_TEXT);

  const handleCopy = () => {
    if (!ref.current) {
      return;
    }

    ref.current.select();
    ref.current.setSelectionRange(0, 9999999); // For mobile devices
    document.execCommand("copy");
    setText("Copied!");
  };

  return (
    <OverlayTrigger
      overlay={<Tooltip id="copy">{text}</Tooltip>}
      onExited={() => setText(DEFAULT_TEXT)}
    >
      {textarea ? (
        <FormTextarea
          readOnly
          className="text-muted"
          style={{ cursor: "copy" }}
          value={value}
          innerRef={ref}
          onClick={handleCopy}
        ></FormTextarea>
      ) : (
        <FormInput
          readOnly
          className="text-muted"
          style={{ cursor: "copy" }}
          value={value}
          innerRef={ref}
          onClick={handleCopy}
        />
      )}
    </OverlayTrigger>
  );
};
export default CopyInput;
