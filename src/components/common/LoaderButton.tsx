import React from "react";
import { Spinner, Button } from "react-bootstrap";

export interface LoaderButtonProps {
  loading: boolean;
  [P: string]: any;
}

const LoaderButton: React.FC<
  LoaderButtonProps & React.ButtonHTMLAttributes<HTMLButtonElement>
> = ({ loading, children, disabled, ...props }) => {
  return (
    <Button {...(props as any)} disabled={loading || disabled}>
      {loading && (
        <Spinner animation="border" size="sm" className="mr-2"></Spinner>
      )}
      {children}
    </Button>
  );
};

export default LoaderButton;
