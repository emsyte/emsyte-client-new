import React, { useContext, useState } from "react";
import { Toast } from "react-bootstrap";
import styled from "styled-components";

type showMessageParams = {
  title: string;
  description?: string;
  type?: "error" | "success";
};

interface NotificationContextValue {
  showMessage: (params: showMessageParams) => void;
}

const ToastWrapper = styled.div`
  position: absolute;
  top: 20px;
  right: 20px;
  z-index: 9999;
  &.error-message {
    .toast-header,
    .close,
    .toast-body {
      background: red;
      color: white;
    }
  }
  &.success-message {
    .toast-header,
    .close,
    .toast-body {
      background: #007bff;
      color: white;
    }
  }
`;

export const NotificationContext = React.createContext<
  NotificationContextValue
>(null as any);

export const useNotification = () => {
  const context = useContext(NotificationContext);
  return context;
};

export const NotificationProvider = ({ children }) => {
  const [show, setShow] = useState<boolean>(false);
  const [title, setTitle] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [type, setType] = useState<string>("");

  const showMessage = (params) => {
    setTitle(params.title);
    setDescription(params.description);
    setType(params.type);
    setShow(true);
  };

  return (
    <NotificationContext.Provider value={{ showMessage }}>
      <div>
        {children}
        <ToastWrapper className={`${type}-message`}>
          <Toast
            onClose={() => setShow(false)}
            show={show}
            delay={3000}
            autohide
            animation
          >
            <Toast.Header>
              <strong className="mr-auto">{title}</strong>
            </Toast.Header>
            <Toast.Body>{description}</Toast.Body>
          </Toast>
        </ToastWrapper>
      </div>
    </NotificationContext.Provider>
  );
};
NotificationProvider.propTypes = {};

export default NotificationProvider;
