import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { Col } from "shards-react";

const PageTitle = ({ title, subtitle = undefined, className, ...attrs }) => {
  const classes = classNames(
    className,
    "text-center",
    "text-md-left",
    "mb-sm-0",
    "ml-0",
    "mr-0",
    "pl-0",
    "pr-0"
  );

  return (
    <Col xs="12" sm="4" className={classes} { ...attrs }>
      {subtitle && <span className="text-uppercase page-subtitle">{subtitle}</span>}
      <div style={{display: 'flex', padding:"1rem", alignItems: 'baseline', backgroundColor: "white"}}>
        <h3 className="page-title">{title}</h3>{attrs.children}
      </div>
    </Col>
  )
};

PageTitle.propTypes = {
  /**
   * The page title.
   */
  title: PropTypes.string,
  /**
   * The page subtitle.
   */
  subtitle: PropTypes.string
};

export default PageTitle;
