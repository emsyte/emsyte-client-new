import { faTags } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { debounce } from "lodash";
import React, { useState } from "react";
import { FormControl, InputGroup, Spinner } from "react-bootstrap";
import { useMutation } from "react-query";
import { CardElement, injectStripe } from "react-stripe-elements";
import { Button } from "shards-react";
import { useStripeService } from "../../utils/Stripe";

// You can customize your Elements to give it the look and feel of your site.
const createOptions = () => {
  return {
    style: {
      base: {
        fontSize: "16px",
        color: "#424770",
        fontFamily: "Open Sans, sans-serif",
        letterSpacing: "0.025em",
        "::placeholder": {
          color: "#aab7c4",
        },
      },
      invalid: {
        color: "#c23d4b",
      },
    },
  };
};

const StripeCard = ({ history, stripe, handleResult, isLoading }: any) => {
  const [error, setError] = useState("");
  const [applyCoupon, setApplyCoupon] = useState(false);
  const [coupon, setCoupon] = useState("");

  const api = useStripeService();

  const validateMut = useMutation(api.validateCoupon);

  const handleValidateCoupon = debounce((coupon) => {
    validateMut.mutate(coupon);
  });

  const handleChange = ({ error }) => (error ? setError(error.message) : null);

  const handleSubmit = (e) => {
    e.preventDefault();
    if (stripe) {
      stripe.createToken().then((e) => handleResult(e, coupon));
    } else {
      console.log("Stripe hasn't loaded yet.");
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit} className="mx-1">
        <fieldset className="border border-primary rounded">
          <legend className="payment-legend text-white bg-primary text-center">
            Payments Processed by Stripe
          </legend>
          <CardElement
            className="border"
            onChange={handleChange}
            {...createOptions()}
          />
          <div className="error h-auto text-danger ml-3" role="alert">
            {error}
          </div>
          {!applyCoupon && (
            <Button
              className="border-0 bg-transparent btn-nohover text-primary"
              onClick={setApplyCoupon}
            >
              Apply Coupon
            </Button>
          )}
          {applyCoupon && (
            <div>
              <InputGroup className="StripeCoupon text-muted">
                <InputGroup.Prepend>
                  <InputGroup.Text>
                    <FontAwesomeIcon icon={faTags} />
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  placeholder="Coupon code"
                  onChange={(e) => {
                    setCoupon(e.target.value);
                    handleValidateCoupon(e.target.value);
                  }}
                />
                {validateMut.isLoading && (
                  <Spinner animation="border"></Spinner>
                )}
              </InputGroup>
              {validateMut.isSuccess &&
                (validateMut.data.valid ? (
                  <span className="text-success">
                    {validateMut.data.message}
                  </span>
                ) : (
                  <span className="text-danger">
                    {validateMut.data.message}
                  </span>
                ))}
            </div>
          )}
        </fieldset>
        <Button
          pill
          theme="accent"
          className="d-table mx-auto mt-4 w-50"
          type="submit"
          onClick={handleSubmit}
        >
          Create Account
          {isLoading && (
            <Spinner
              as="span"
              animation="border"
              size="sm"
              role="status"
              aria-hidden="true"
              className="pull-right"
            />
          )}
        </Button>
      </form>
    </>
  );
};

export default injectStripe(StripeCard);
