import React, { useState } from "react";
import { CardElement, injectStripe } from "react-stripe-elements";
import { Card, Col, Form, Row } from "shards-react";
import VisaCardIcon from "../../images/payment_icons/1.png";
import DiscoverCardIcon from "../../images/payment_icons/14.png";
import MasterCardIcon from "../../images/payment_icons/2.png";
import AmericanExpressIcon from "../../images/payment_icons/22.png";
import LoaderButton from "../common/LoaderButton";

// You can customize your Elements to give it the look and feel of your site.
const createOptions = () => {
  return {
    style: {
      base: {
        fontSize: "16px",
        color: "#424770",
        fontFamily: "Open Sans, sans-serif",
        letterSpacing: "0.025em",
        "::placeholder": {
          color: "#aab7c4",
        },
      },
      invalid: {
        color: "#c23d4b",
      },
    },
  };
};

const cardImages = [
  VisaCardIcon,
  MasterCardIcon,
  DiscoverCardIcon,
  AmericanExpressIcon,
];

const StripeCard = ({ stripe, handleResult }) => {
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  const handleChange = ({ error }) => (error ? setError(error.message) : null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!stripe) {
      return;
    }
    try {
      setLoading(true);
      const token = await stripe.createToken();
      await handleResult(token);
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <Form onSubmit={handleSubmit} className="mx-1 w-100">
        <Card
          className="no-shadow py-3 px-4"
          style={{ border: "1px solid #ccc" }}
          theme="light"
        >
          <Row className="mb-2">
            <Col className="text-left">
              {
                cardImages.map((image, i) => (
                  <img
                    className="mx-1"
                    width={35}
                    height={20}
                    src={image}
                    alt={"Payments Methods"}
                  />
                ))
                /* <img
                            width={75}
                            height={50}
                            src={getPaymentIconByBrand(item.card.brand)}
                            alt={titleCase(item.card.brand)}
                            /> */
              }
            </Col>
          </Row>
          <Row>
            <Col className="text-left">
              <small>
                <b>Card number</b>
                <span className="text-muted ml-1">
                  <i className="fas fa-lock"></i> Secured by Stripe
                </span>
              </small>
            </Col>
          </Row>
          <CardElement
            className="border m-0"
            onChange={handleChange}
            {...createOptions()}
          />
          <div className="error h-auto text-danger ml-3" role="alert">
            {error}
          </div>
          <LoaderButton
            loading={loading}
            block
            variant="primary"
            className="d-table mx-auto ml-1 mt-4 w-100"
            type="submit"
            onClick={handleSubmit}
          >
            Save Changes
          </LoaderButton>
        </Card>
      </Form>
    </>
  );
};

export default injectStripe(StripeCard);
