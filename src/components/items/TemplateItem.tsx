import {
  TemplateInstanceObject,
  TemplateObject,
  TemplateType,
} from "@emsyte/common/template";
import { WebinarObject } from "@emsyte/common/webinar";
import React, { useState } from "react";
import { Alert, Button, Dropdown, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Card, CardBody, CardFooter } from "shards-react";
import styled from "styled-components";
import EmptyImg from "../../images/EmptyImg.svg";
import colors from "../../utils/colors";
import { unknownValue } from "../../utils/conditions";
import { useHttpService } from "../../utils/HttpService";
import LoaderButton from "../common/LoaderButton";
import { useNotification } from "../common/Notification";

const TemplateItemStyle = styled(Card)`
  position: relative;
  overflow: hidden;
  border-color: #dddddd;
  .lb-active {
    position: absolute;
    left: calc(50% - 20px);
    top: calc(25% - 20px);
    font-size: 0.75rem;
    color: white;
    width: 40px;
    height: 40px;
    background-color: #28a745;
    border-radius: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .lb-custom-tag {
    position: absolute;
    left: calc(0px);
    top: calc(1rem);
    font-size: 0.75rem;
    color: white;
    background-color: ${colors.gray800.toRGBA(0.9)};
    border-radius: 0 0.25rem 0.25rem 0;
    align-items: center;
    justify-content: center;
    font-weight: 700;
    padding: 0 0.25rem;
  }
  .lb-active-text {
    position: absolute;
    left: calc(50% - 42px);
    top: 32%;
    font-size: 16px;
    display: flex;
    align-items: center;
    justify-content: center;
    letter-spacing: 1.2px;
  }

  .lb-custom {
    position: absolute;
    right: 8px;
    top: 5px;
    font-size: 14px;
    color: white;
    width: 50px;
  }

  .space-between {
    justify-content: space-between;
    align-items: center;
  }
  .card-footer {
    padding: 20px;
  }
`;

const TemplateContainer = styled.div`
  min-width: 16rem;
  width: 16rem;
  max-width: 16rem;
  min-height: 21.875rem;
  height: 21.875rem;
  max-height: 21.875rem;
`;

const DropdownContainer = styled.div`
  position: absolute;
  right: 10px;
  top: 10px;
`;

const TemplateName = styled.p`
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  letter-spacing: 1.2px;
  color: #2e3338;
  font-size: 1.25rem;
  margin-bottom: 0.5rem;
`;

// @ts-ignore
const CustomDropdown = React.forwardRef(({ children, onClick }, ref) => (
  <div
    style={{
      width: "30px",
      height: "30px",
      border: "1px solid #E5E5E5",
      borderRadius: "6px",
      cursor: "pointer",
      paddingTop: "2px",
    }}
    ref={ref as any}
    onClick={(e) => {
      onClick(e);
    }}
  >
    <i
      className="bi bi-three-dots-vertical"
      style={{ cursor: "pointer", fontSize: "18px", color: "#868E96" }}
    >
      {/* custom icon */}
      {children}
    </i>
  </div>
));

function getWebinarId(webinar: WebinarObject) {
  return btoa(JSON.stringify({ id: webinar.id }));
}

export interface TemplateTypeLabelProps {
  type: TemplateType;
}

const TypeLabel = styled.span`
  background-color: ${colors.gray600.toHex()};
  border-radius: 0.25rem;
  text-transform: uppercase;
  font-size: 0.75rem;
  color: white;
  padding: 0.25rem 0.25rem;
  font-weight: 700;
`;

const TemplateTypeLabel: React.FC<TemplateTypeLabelProps> = ({ type }) => {
  switch (type) {
    case TemplateType.CONFIRMATION:
      return <TypeLabel>Confirmation</TypeLabel>;
    case TemplateType.THANK_YOU:
      return <TypeLabel>Thank you</TypeLabel>;
    case TemplateType.REGISTRATION:
      return <TypeLabel>Registration</TypeLabel>;
    default:
      return <TypeLabel>Unknown type: {unknownValue(type)}</TypeLabel>;
  }
};

interface Template extends TemplateObject {
  instances: TemplateInstanceObject[];
}

interface TemplateItemProps {
  webinar: WebinarObject;
  item: Template;
  category: string;
  handleTemplateActivation: (e: Template) => Promise<void>;
  onDelete: () => void;
  setShowChangeNameModal?: (show: boolean) => void;
  setEditingTemplate?: (template: Template) => void;
}
const TemplateItem: React.FC<TemplateItemProps> = ({
  webinar,
  item,
  category,
  handleTemplateActivation,
  onDelete,
  setShowChangeNameModal = (show: boolean) => {},
  setEditingTemplate = (x) => {},
}) => {
  const [activating, setActivating] = useState(false);
  const [loading, setLoading] = useState(false);
  const [showDelete, setShowDelete] = useState(false);

  const notification = useNotification();
  const api = useHttpService();

  const handleDelete = async () => {
    setLoading(true);
    try {
      await api.doDeleteTemplate(item.id);
      notification.showMessage({
        title: "Successfully deleted template",
        type: "success",
      });
      setShowDelete(false);
      onDelete();
    } catch (error) {
      notification.showMessage({
        title: "Failed to delete template",
        type: "error",
        description: error.message,
      });
    } finally {
      setLoading(false);
    }
  };

  const thumbnail = item.instances.length
    ? item.instances[0].thumbnail
    : item.thumbnail;

  return (
    <div
      className="d-flex justify-content-center mb-4"
      style={{ margin: "2%" }}
    >
      <TemplateContainer>
        <TemplateItemStyle className="text-center w-100">
          <div>
            <div
              className="card-post__image text-left background-contain rounded-0"
              style={{
                ...(thumbnail
                  ? {
                      backgroundImage: `url("${thumbnail}")`,
                      height: "200px",
                    }
                  : {
                      backgroundSize: "contain",
                      backgroundImage: `url(${EmptyImg})`,
                      height: "200px",
                    }),
                // if selected add opacity
                ...(item.instances.length > 0 ? { opacity: 0.75 } : {}),
              }}
            ></div>
            <hr className="mt-0 mb-1" />

            <DropdownContainer>
              <Dropdown>
                <Dropdown.Toggle id="dropdown-basic" as={CustomDropdown} />
                <Dropdown.Menu>
                  <Dropdown.Item
                    href={
                      item.instances.length > 0
                        ? `/template/preview/instance/${item.instances[0].id}`
                        : `/template/preview/global/${item.id}`
                    }
                    target="_blank"
                  >
                    Preview
                  </Dropdown.Item>
                  {!item.base && (
                    <Dropdown.Item
                      onClick={() => {
                        setShowChangeNameModal(true);
                        setEditingTemplate(item);
                      }}
                    >
                      Rename
                    </Dropdown.Item>
                  )}
                  {/* <Dropdown.Item
                    href={``}
                  >
                    Duplicate
                  </Dropdown.Item> */}

                  {!item.base && (
                    <Dropdown.Item onClick={() => setShowDelete(true)}>
                      Delete Template
                    </Dropdown.Item>
                  )}
                </Dropdown.Menu>
              </Dropdown>
            </DropdownContainer>

            {item.instances.length > 0 && (
              <React.Fragment>
                <div className={`lb-active`}>
                  <i className="fas fa-check" />
                </div>
                <span className="text-uppercase lb-active-text">Selected</span>
              </React.Fragment>
            )}

            {!item.base && (
              <React.Fragment>
                <div className={`lb-custom-tag`}>Custom</div>
              </React.Fragment>
            )}
          </div>
          <CardBody className="position-relative py-2">
            <TemplateName>{`${item.name}`}</TemplateName>
            <TemplateTypeLabel type={item.type} />
          </CardBody>
          <CardFooter className="pt-0 mt-3">
            <div className="d-flex space-between">
              {item.instances.length > 0 ? (
                <Link
                  className="btn btn-primary w-100"
                  to={`/editor/${item.instances[0]?.id}?webinar=${getWebinarId(
                    webinar
                  )}`}
                  style={{ fontSize: "1rem" }}
                >
                  Edit Details
                </Link>
              ) : (
                <LoaderButton
                  outline
                  loading={activating}
                  onClick={() => {
                    setActivating(true);
                    handleTemplateActivation(item)
                      .catch((error) =>
                        notification.showMessage({
                          title: "Failed to activate template",
                          type: "error",
                          description: error.message,
                        })
                      )
                      .finally(() => setActivating(false));
                  }}
                  className="w-100"
                  style={{ fontSize: "1rem" }}
                >
                  Select
                </LoaderButton>
              )}
            </div>
          </CardFooter>
        </TemplateItemStyle>
        <Modal show={showDelete} onHide={() => setShowDelete(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Delete global template?</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Are you sure you want to delete this global template?
            <Alert variant="warning">
              Deleting this template will delete all the template instances tied
              to it as well.
            </Alert>
          </Modal.Body>
          <Modal.Footer>
            <LoaderButton
              loading={loading}
              theme="danger"
              onClick={handleDelete}
            >
              Delete
            </LoaderButton>
            <Button variant="secondary">Cancel</Button>
          </Modal.Footer>
        </Modal>
      </TemplateContainer>
    </div>
  );
};

export default TemplateItem;
