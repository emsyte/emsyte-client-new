
import "moment-timezone";
import React from "react";
import { OverlayTrigger, Popover } from "react-bootstrap";
import {
  DateRangePicker,
  defaultInputRanges,
  defaultStaticRanges,
  Range,
} from "react-date-range";
import { Calendar2Event } from 'react-bootstrap-icons';
import styled from "styled-components";

const DateRangeButton = styled.button`

  font-size: 1rem;
  background: #FFFFFF;
  border: 1px solid #DDDDDD;
  border-radius: 0.25rem;
  padding: 0.25rem 0.625rem;
  display: flex;
  align-items: center;
  justify-content: center;

`;

function getRangeLabel(range: Range) {
  for (const _range of [...defaultStaticRanges]) {
    if (_range.isSelected(range)) {
      return _range.label;
    }
  }

  for (const _range of [...defaultInputRanges]) {
    const value = _range.getCurrentValue(range);
    if (value !== "-") {
      return `${value} ${_range.label}`;
    }
  }

  const from = range.startDate!.toLocaleDateString("en-us", {
    month: "short",
    year: "numeric",
    day: "numeric",
  });

  const to = range.endDate!.toLocaleDateString("en-us", {
    month: "short",
    year: "numeric",
    day: "numeric",
  });

  return `${from} to ${to}`;
}

export interface RangePickerProps {
  range: Range;
  onChange: (range: Range) => void;
}

const RangePicker: React.FC<RangePickerProps> = ({ range, onChange }) => {
  return (
    <OverlayTrigger
      placement="bottom"
      trigger="click"
      rootClose
      overlay={
        <Popover id="date-range" style={{ maxWidth: "none" }}>
          <Popover.Content>
            <DateRangePicker
              ranges={[{ ...range, key: "selection" }]}
              onChange={(item) => {
                onChange((item as any).selection);
              }}
              showSelectionPreview
              moveRangeOnFirstSelection={false}
              months={2}
              direction="horizontal"
            />
          </Popover.Content>
        </Popover>
      }
    >
      <DateRangeButton>
        {getRangeLabel(range)}{" "}
        <Calendar2Event style={{marginLeft: '0.625rem'}}/>
      </DateRangeButton>
    </OverlayTrigger>
  );
};

export default RangePicker;
