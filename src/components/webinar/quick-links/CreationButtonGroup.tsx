import React from "react";
import { withRouter, Link } from "react-router-dom";
import { compose } from "recompose";

import { Row, Col, ButtonGroup, Button } from "shards-react";

const CreationButtonGroup = ({ history, hideTitle = false }) => {
  const getUrl = (type: string) => {
    const buff = new Buffer(JSON.stringify({ id: null, type }));
    return `/webinars/${buff.toString("base64")}/basic`;
  };
  return (
    <>
      {hideTitle && <span className="text-muted">Webinar Quick Links</span>}
      <Row>
        <Col>
          <ButtonGroup size="sm">
            <Button
              as={Link}
              href={getUrl("recorded")}
              className="border rounded"
            >
              <span>Create a webinar</span>
            </Button>
            {/* <Button
              as={Link}
              href={getUrl("live")}
              className="border rounded mx-2"
              theme="light"
            >
              <span>Live Seminar</span>
            </Button> */}
          </ButtonGroup>
        </Col>
      </Row>
    </>
  );
};

const enhance = compose(withRouter)(CreationButtonGroup);

export default enhance;
