import React, { useCallback, useEffect, useRef } from "react";
import { useDropzone } from "react-dropzone";

const DragAndDrop = (props) => {
  const callback = useRef<Function | null>();

  useEffect(() => {
    callback.current = props.handleDrop;
  }, [props.handleDrop]);

  const onDrop = useCallback(
    (acceptedFiles) => {
      if (callback.current) {
        callback.current(acceptedFiles);
      }
    },
    [callback]
  );
  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop,
  });

  return (
    <div
      style={{ display: "inline-block", position: "relative" }}
      {...getRootProps()}
    >
      {isDragActive && (
        <div
          style={{
            border: "dashed grey 4px",
            backgroundColor: "rgba(255,255,255,.8)",
            position: "absolute",
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            zIndex: 9999,
          }}
        >
          <div
            style={{
              position: "absolute",
              top: "50%",
              right: 0,
              left: 0,
              textAlign: "center",
              color: "grey",
              fontSize: 36,
            }}
          >
            <div>Drop Files Here</div>
          </div>
        </div>
      )}
      <input {...getInputProps()} />
      {props.children}
    </div>
  );
};

export default DragAndDrop;
