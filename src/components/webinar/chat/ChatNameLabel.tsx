import { LabelType, MessageType } from "@emsyte/common/message";
import React from "react";
import styled from "styled-components";

const ChatNameLabelWrapper = styled.span`
  .name {
    font-weight: 700;
  }
  .time {
    font-weight: 400;
    font-size: 0.7rem;
  }
  .label {
    padding: 0 0.5rem;
    border-radius: 1rem;
    background-color: #007bff;
    color: white;
    font-weight: 700;
    font-size: 0.7rem;
  }
`;

const ChatNameLabel = ({ message }: { message: MessageType }) => {
  let labelComponent: any = null;
  switch (message.label) {
    case LabelType.PRESENTER:
    case LabelType.SUPPORT:
      labelComponent = <span className="mr-2 label">Host</span>;
      break;
    default:
  }

  return (
    <ChatNameLabelWrapper>
      {message.name && <span className="name">{message.name}</span>}{" "}
      {labelComponent}
      <span className="time">
        {new Date(+message.messageId.split("-")[0]).toLocaleTimeString([], {
          hour: "numeric",
          minute: "2-digit",
        })}
      </span>
    </ChatNameLabelWrapper>
  );
};

export default ChatNameLabel;
