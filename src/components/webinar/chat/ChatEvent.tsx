import { MessageType } from "@emsyte/common/message";
import {
  faEllipsisH,
  faQuestionCircle,
  faReply,
  faShare,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames";
import React from "react";
import { Spinner } from "react-bootstrap";
import styled from "styled-components";
import ChatNameLabel from "./ChatNameLabel";

const ChatEventWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  font-size: 0.75rem;
  margin-top: 0.25rem;
  font-weight: normal;
  width: 100%;

  .message {
    flex: 1;
    max-width: 34rem;
    text-align: left;

    .text {
      word-break: break-word;
    }
  }

  .removed {
    opacity: 0.7;
  }

  .header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 0.25rem;
  }

  .text {
    flex: 1;
    padding: 0.5rem;
    background-color: white;
    border-radius: 3px;

    .removed {
      color: rgba(255, 255, 255, 0.75);
    }
  }

  &.own-message {
    justify-content: flex-end;
  }

  &.local-message {
    opacity: 0.5;
  }

  &.own-message .text {
    border-left: 4px solid #007bff;
    font-weight: bold;
  }

  hr {
    margin-top: 0.5rem;
    margin-bottom: 0.5rem;
  }

  .question {
    color: #007bff;
  }
`;

export const ChatBubbleDeleted = ({
  admin,
  message,
}: {
  admin: boolean;
  message: MessageType;
}) => (
  <ChatEventWrapper>
    <div className="message removed">
      <div className="header">
        <ChatNameLabel message={message} />
        {admin && <FontAwesomeIcon icon={faEllipsisH} />}
      </div>
      {admin ? (
        <>
          <div className="text">
            <QuestionIcon message={message} /> {message.content}
          </div>
          {/* @ts-ignore */}
          {message.removed !== "attendee" ? (
            <em>
              You removed this message. Other attendees will not see the message
              or who sent it. Remove this attendee from the webinar if they are
              overtly offensive or bully other attendees.
            </em>
          ) : (
            <em>The user removed this message.</em>
          )}
        </>
      ) : (
        <div className="text">
          <em>This message was removed by a moderator</em>
        </div>
      )}
    </div>
  </ChatEventWrapper>
);

export const ChatEvent = ({
  admin,
  message,
  removeMessage,
}: {
  admin: boolean;
  message: MessageType;
  removeMessage: (messageId: string) => void;
}) => {
  const isLocalMessage = message.messageId.endsWith("-local");

  if (admin && message.replyTo) {
    return (
      <ChatEventWrapper
        className={classNames(
          message.ownMessage && "own-message",
          isLocalMessage && "local-message"
        )}
      >
        <div
          style={{
            display: "flex",
            marginTop: "1.25rem",
            marginRight: "0.5rem",
            alignSelf: "stretch",
            alignItems: "center",
          }}
        >
          <span>
            <FontAwesomeIcon size="lg" icon={faReply} />
            <span className="ml-2">Reply</span>
          </span>
        </div>
        <div className="message">
          <div className="header">
            <ChatNameLabel message={message} />
            {/* {(admin || message.ownMessage) && (
              <FontAwesomeIcon
                onClick={() => removeMessage(message.messageId)}
                icon={faEllipsisH}
              />
            )} */}
          </div>
          <div className="text">{message.content}</div>
        </div>
      </ChatEventWrapper>
    );
  }

  // let reply: ReplyType | null = null;
  // if (message.replyTo) {
  //   reply = message;
  //   message = message.replyTo;
  // }

  if (message.removed) {
    return <ChatBubbleDeleted admin={admin} message={message} />;
  }

  return (
    <ChatEventWrapper
      className={classNames(
        message.ownMessage && "own-message",
        isLocalMessage && "local-message"
      )}
    >
      <div className="message">
        <div className="header">
          <ChatNameLabel message={message} />
          {isLocalMessage && <Spinner animation="border" size="sm"></Spinner>}
          {/* {(admin || message.ownMessage) && (
            <FontAwesomeIcon
              onClick={() => removeMessage(message.messageId)}
              icon={faEllipsisH}
            />
          )} */}
        </div>
        <div className="text">
          <QuestionIcon message={message} /> {message.content}
          {/* {reply && (
            <>
              <hr />
              <div className="header">
                <div className="reply">
                  <FontAwesomeIcon size="lg" icon={faReply} />{" "}
                  <em>Reply from:</em> <ChatNameLabel message={reply} />
                </div>
              </div>
              {reply.content}
            </>
          )} */}
        </div>
      </div>
      {/* {admin && message.question && !message.ownMessage && (
        <div
          style={{
            display: "flex",
            marginTop: "1.25rem",
            marginLeft: "0.5rem",
            alignSelf: "stretch",
            alignItems: "center",
          }}
        >
          <span>
            <FontAwesomeIcon size="lg" icon={faShare} />
            <span className="ml-2" style={{ color: "#007BFF" }}>
              Reply to question
            </span>
          </span>
        </div>
      )} */}
    </ChatEventWrapper>
  );
};

const QuestionIcon = ({ message }: { message: MessageType }) =>
  message.question ? (
    <span className="question">
      <FontAwesomeIcon size="lg" icon={faQuestionCircle} />
    </span>
  ) : null;
