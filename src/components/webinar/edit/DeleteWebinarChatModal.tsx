import React, { useState, useCallback } from "react";
import { Button } from "react-bootstrap";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "shards-react";
import LoaderButton from "../../common/LoaderButton";

const DeleteWebinarChatModal = ({
  deleteSelected,
  setDeletedSelected,
  handleDelete,
  onComplete,
}) => {
  const [loading, setLoading] = useState<boolean>(false);

  const onDelete = useCallback(
    async (event) => {
      event.preventDefault();
      try {
        setLoading(true);
        await handleDelete();
        onComplete();
        setDeletedSelected(null);
        setLoading(false);
      } catch (error) {
        setLoading(false);
        console.log(error);
      }
    },
    [handleDelete, onComplete, setDeletedSelected]
  );

  return (
    <Modal open={!!deleteSelected} toggle={() => setDeletedSelected(null)}>
      <ModalHeader>
        Delete Webinar Message
        <br />
      </ModalHeader>
      <ModalBody>Do you want to delete this webinar message?</ModalBody>
      <ModalFooter>
        <LoaderButton loading={loading} onClick={onDelete} theme="danger">
          Delete
        </LoaderButton>
        <Button onClick={() => setDeletedSelected(null)}>Cancel</Button>
      </ModalFooter>
    </Modal>
  );
};

export default DeleteWebinarChatModal;
