import React, { useState } from "react";
import { Col, Form, Row } from "react-bootstrap";
import { Button } from "shards-react";
import { useHttpService } from "../../../utils/HttpService/index";
import { useNotification } from "../../common/Notification";
import {
  WebinarChatLabel,
  WebinarChatObject,
  WebinarObject,
} from "@emsyte/common/webinar";
import TextInput from "../../form/TextInput";
import { useForm } from "react-hook-form";
import LoaderButton from "../../common/LoaderButton";
import { durationToSeconds, secondsToDuration } from "../../../utils/duration";

interface WebinarChatFormProp {
  webinar: WebinarObject;
  item?: WebinarChatObject;
  onSuccess: () => void;
  onClose?: () => void;
}

const WebinarChatForm = ({
  webinar,
  item,
  onSuccess,
  onClose = () => {},
}: WebinarChatFormProp) => {
  const api = useHttpService();
  const notification = useNotification();
  const [loading, setLoading] = useState(false);
  const duration = secondsToDuration(item?.offset_timestamp ?? 0);
  const { register, errors, handleSubmit, reset } = useForm({
    defaultValues: {
      name: item?.name || "",
      content: item?.content || "",
      label: WebinarChatLabel.ATTENDEE,
      ...duration,
    },
  });

  const onCreateChat = handleSubmit(async (newWebinarChat) => {
    try {
      setLoading(true);
      const data = {
        content: newWebinarChat.content,
        name: newWebinarChat.name,
        label: newWebinarChat.label,
        offset_timestamp: durationToSeconds({
          hours: +newWebinarChat.hours,
          minutes: +newWebinarChat.minutes,
          seconds: +newWebinarChat.seconds,
        }),
      };

      if (item) {
        await api.doUpdateWebinarChatMessage(webinar.id, item.id, data);
      } else {
        await api.doCreateWebinarChatMessage(webinar.id, data);
      }

      onSuccess();
      reset();
      onClose();
    } catch (error) {
      setLoading(false);
      notification.showMessage({
        title: "Oops!",
        description:
          error.response?.data?.message ??
          `Failed to ${item ? "edit" : "create"} webinar chat`,
        type: "error",
      });
    } finally {
      setLoading(false);
    }
  });

  return (
    <Form onSubmit={onCreateChat}>
      <Row>
        <Col lg={item ? 12 : 3}>
          <Form.Group controlId="offset_timestamp">
            <span className="text-muted text-bold">Offset Timestamp</span>
            <Row className="d-flex center">
              <Col>
                <TextInput
                  errors={errors}
                  name="hours"
                  placeholder="hr"
                  type="number"
                  ref={register({
                    required: "Required",
                    min: 0,
                    max: 23,
                  })}
                ></TextInput>
              </Col>
              :
              <Col>
                <TextInput
                  errors={errors}
                  name="minutes"
                  placeholder="min"
                  type="number"
                  ref={register({
                    required: "Required",
                    min: 0,
                    max: 59,
                  })}
                ></TextInput>
              </Col>
              :
              <Col>
                <TextInput
                  errors={errors}
                  name="seconds"
                  placeholder="sec"
                  type="number"
                  ref={register({
                    required: "Required",
                    min: 0,
                    max: 59,
                  })}
                ></TextInput>
              </Col>
            </Row>
          </Form.Group>
        </Col>
        <Col lg={item ? 12 : 2}>
          <Form.Group controlId="label">
            <span className="text-muted text-bold">Label</span>
            <TextInput
              errors={errors}
              name="label"
              as="select"
              ref={register()}
            >
              <option value="attendee">Attendee</option>
              <option value="presenter">Presenter</option>
              <option value="support_team">Support Team</option>
            </TextInput>
          </Form.Group>
        </Col>
        <Col lg={item ? 12 : 2}>
          <Form.Group controlId="name">
            <span className="text-muted text-bold">Name</span>
            <TextInput
              errors={errors}
              name="name"
              placeholder={"Name"}
              ref={register({
                required: "Required",
              })}
            ></TextInput>
          </Form.Group>
        </Col>
        <Col lg={item ? 12 : 4}>
          <Form.Group controlId="content">
            <span className="text-muted text-bold">Content</span>
            <TextInput
              errors={errors}
              name="content"
              placeholder={"Content"}
              ref={register({
                required: "Required",
              })}
            ></TextInput>
          </Form.Group>
        </Col>
        {item ? (
          <Col lg={12}>
            <Row>
              <Col lg={6}>
                <LoaderButton className="w-100" loading={loading} type="submit">
                  Save
                </LoaderButton>
              </Col>
              <Col lg={6}>
                <Button
                  onClick={() => onClose()}
                  theme="secondary"
                  className="w-100"
                >
                  Cancel
                </Button>
              </Col>
            </Row>
          </Col>
        ) : (
          <Col lg={1}>
            <Form.Group className="text-right">
              {/* <span className="text-muted text-bold">Import CSV</span> */}
              <br />
              <LoaderButton loading={loading} type="submit">
                <i className="fas fa-plus"></i>
              </LoaderButton>
            </Form.Group>
          </Col>
        )}
      </Row>
    </Form>
  );
};

export default WebinarChatForm;
