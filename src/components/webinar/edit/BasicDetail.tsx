import { WebinarObject } from "@emsyte/common/webinar";
import "moment-timezone";
import React, { useEffect, useRef, useState } from "react";
import { Card, Form } from "react-bootstrap";
import { FormProvider, useForm } from "react-hook-form";
import { useQuery, useQueryClient } from "react-query";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { apiWrapper } from "../../../utils/api_helpers";
import { durationToSeconds, secondsToDuration } from "../../../utils/duration";
import { useFeatureFlag } from "../../../utils/FeatureFlags";
import { useHttpService } from "../../../utils/HttpService/index";
import BasicInfo from "./BasicInfo";
import Schedule, { LocalRecurrenceObject } from "./Schedule";

const BasicDetailStyle = styled.div`
  padding: 20px 20px;
`;

function createWebinarRoute(webinar, route: string) {
  const buff = new Buffer(
    JSON.stringify({ id: webinar.id ?? null, type: webinar.type })
  );
  return `/webinars/${buff.toString("base64")}/${route}`;
}

const CustomHeader = styled(Card.Header)`
  padding: 1rem 1.25rem;
`;

const Title = styled.h6`
  margin: 0;
  text-transform: uppercase;
  color: #2e3338;
`;

interface BasicDetailProps {
  webinar: WebinarObject;
  onWebinarUpdate: () => Promise<any>;
  onNext: () => void;
  saveRef: React.MutableRefObject<() => void>;
}
const BasicDetail: React.FC<BasicDetailProps> = ({
  webinar,
  onWebinarUpdate,
  onNext,
  saveRef,
}) => {
  const duration = secondsToDuration(webinar.duration);
  const methods = useForm({
    defaultValues: {
      ...webinar,
      duration_hours: duration.hours,
      duration_minutes: duration.minutes,
      duration_seconds: duration.seconds,
    },
  });

  const { handleSubmit } = methods;

  const [files, setFiles] = useState<any>(null);
  const [thumbnail, setThumbnail] = useState(webinar.thumbnail ?? "");
  const [loading, setLoading] = useState(false);
  console.log(loading);
  const api = useHttpService();

  const replayEnabled = useFeatureFlag("replay");

  const formRef = useRef<HTMLFormElement | null>(null);

  const { data: remoteRecurrences } = useQuery(
    [webinar.id, "recurrences"],
    () => api.doGetRecurrences(webinar.id),
    {
      enabled: !!webinar.id,
    }
  );

  const queryClient = useQueryClient();

  useEffect(() => {
    if (!remoteRecurrences) {
      return;
    }

    setRecurrences((recurrences) => [
      ...recurrences.filter((r) => r.isLocal),

      ...remoteRecurrences.data.map((recurrence) => ({
        isLocal: false,
        shouldDelete: false,
        ...recurrence,
      })),
    ]);
  }, [remoteRecurrences]);

  const [recurrences, setRecurrences] = useState<LocalRecurrenceObject[]>([]);

  const history = useHistory();

  const scheduleRecurrences = async (
    webinarId: number,
    recurrences: LocalRecurrenceObject[]
  ) => {
    try {
      // It's possible that someone has deleted a recurrence and added it back at the same time. To avoid a DB collision, we first delete then create.
      await Promise.all(
        recurrences
          .filter((recurrence) => recurrence.shouldDelete)
          .map((recurrence) =>
            api.doDeleteRecurrence(webinarId, recurrence.id as number)
          )
      );

      await Promise.all(
        recurrences
          .filter((recurrence) => recurrence.isLocal)
          .map(({ shouldDelete, isLocal, id, ...recurrence }) =>
            api.doCreateRecurrence(webinarId, recurrence)
          )
      );
    } finally {
      queryClient.invalidateQueries([webinarId, "recurrences"]);
    }
  };

  const onSubmit = handleSubmit(async (newWebinar) => {
    await apiWrapper(async () => {
      const { duration_hours, duration_minutes, duration_seconds, ...rest } =
        newWebinar;
      const duration = durationToSeconds({
        hours: +duration_hours || 0,
        minutes: +duration_minutes || 0,
        seconds: +duration_seconds || 0,
      });
      const { id } = webinar;

      const thumbnail =
        files && files.length
          ? await api.doUploadFile("webinar", files[0])
          : webinar.thumbnail;

      if (id !== null) {
        await api.doUpdateWebinar(webinar.id, {
          ...rest,
          thumbnail,
        });

        await scheduleRecurrences(webinar.id, recurrences);
        await onWebinarUpdate();
      } else {
        const { data } = await api.doCreateWebinar({
          name: newWebinar.name,
          type: newWebinar.type,
          videoUrl: newWebinar.videoUrl,
          duration,
          replayExpiration: replayEnabled ? newWebinar.replayExpiration : 0,
          thumbnail: thumbnail as string,
        });
        await scheduleRecurrences(data.id, recurrences);

        history.push(createWebinarRoute(data, "basic"));
      }
      onNext();
    }, setLoading);
  });

  saveRef.current = onSubmit;

  return (
    <BasicDetailStyle className="col-sm-8 align-self-center justify-content-center">
      <FormProvider {...methods}>
        <Form onSubmit={onSubmit} ref={formRef}>
          <Card>
            <CustomHeader>
              <Title>Basic Information</Title>
            </CustomHeader>
            <div style={{ padding: "0.25rem 1.25rem 1rem 1.25rem" }}>
              <BasicInfo
                webinar={webinar}
                files={files}
                setFiles={setFiles}
                thumbnail={thumbnail}
                setThumbnail={setThumbnail}
              />
            </div>
          </Card>
          <br />
          <Card>
            <CustomHeader>
              <Title>Webinar sessions</Title>
              Create multiple individual or recurring sessions
            </CustomHeader>

            <div style={{ padding: "0.25rem 1.25rem 1rem 1.25rem" }}>
              <Schedule
                recurrences={recurrences}
                setRecurrences={setRecurrences}
              />
            </div>
          </Card>
        </Form>
      </FormProvider>
    </BasicDetailStyle>
  );
};

export default BasicDetail;
