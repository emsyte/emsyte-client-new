import React from "react";
import { Modal, ModalHeader, ModalBody } from "shards-react";
import WebinarChatForm from "./WebinarChatForm";

const EditWebinarChatModal = ({ onComplete, webinar, setSelected, item }) => {
  return (
    <Modal open={!!item} toggle={() => setSelected(null)}>
      <ModalHeader>
        Edit webinar Message
        <br />
      </ModalHeader>
      <ModalBody>
        <WebinarChatForm
          onClose={() => setSelected(null)}
          onSuccess={onComplete}
          item={item}
          webinar={webinar}
        />
      </ModalBody>
    </Modal>
  );
};

export default EditWebinarChatModal;
