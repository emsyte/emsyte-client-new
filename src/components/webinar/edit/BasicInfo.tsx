import "moment-timezone";
import React from "react";
import { Button, Card, Col, Form, Row } from "react-bootstrap";
import { useFormContext } from "react-hook-form";
import emptyWebinar from "../../../images/empty-webinar.png";
import TextInput from "../../form/TextInput";
import WebinarTypeAccordion from "../../form/WebinarTypeAccordion";
import DragAndDrop from "../upload/DragAndDrop";

const BasicInfo = ({ webinar, files, setFiles, thumbnail, setThumbnail }) => {
  const { errors, register } = useFormContext();

  const handleDrop = (files) => {
    let fileList = files;
    for (const file of files) {
      if (!file.name) return;
      fileList.length = 0;
      fileList.push(file);
      setThumbnail(URL.createObjectURL(file));
    }
    setFiles(fileList);
  };

  const handleRemove = () => {
    setThumbnail("");
  };

  return (
    <Row>
      <Col>
        <Row>
          <Col lg={12}>
            <Form.Group controlId="title">
              <span className="text-muted text-bold">
                Webinar Title (public)
              </span>
              <TextInput
                errors={errors}
                name="name"
                placeholder={"Webinar Title"}
                ref={register({
                  required: "Required",
                  maxLength: {
                    value: 70,
                    message: "Longer then 70 characters",
                  },
                })}
              ></TextInput>
              <small className="float-right">
                Public title. Max 70 characters.
              </small>
            </Form.Group>
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            <Form.Group controlId="title">
              <WebinarTypeAccordion />
            </Form.Group>
          </Col>
        </Row>
      </Col>
      <Col className="text-center">
        <DragAndDrop handleDrop={handleDrop}>
          <Card className="no-shadow rounded-lg border-dashed">
            <Card.Body className="text-center">
              <b>Upload Webinar Thumbnail</b>
              <br />
              <i className="fas fa-cloud-upload-alt fa-3x text-white"></i>
              <p className="text-muted">
                <span className="text-bold">
                  Drag your files here to upload
                </span>
                <br />
                or click to browse
              </p>
              <span>Thumbnail Preview</span>
              <br />
              <img
                alt={"Thumbnail"}
                src={thumbnail || emptyWebinar}
                className="rounded"
                width={200}
                height={150}
              />
              <p className="text-muted">
                <span className="text-bold">Drag an image to upload</span>
              </p>
            </Card.Body>
          </Card>
        </DragAndDrop>
        <div>
          <Button
            onClick={handleRemove}
            variant="link"
            className="mt-2"
            size="sm"
          >
            <span>Remove</span>
          </Button>
        </div>
      </Col>
    </Row>
  );
};

export default BasicInfo;
