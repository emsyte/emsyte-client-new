import { NotificationDeliveryMethod } from "@emsyte/common/notification";
import { Editor } from "@tinymce/tinymce-react";
import React, { useCallback, useEffect, useState } from "react";
import {
  Button,
  Col,
  FormControl,
  FormGroup,
  FormLabel,
  Modal,
  Row,
} from "react-bootstrap";
import styled from "styled-components";
import { filterFalsy } from "../../../utils/conditions";
import { useFeatureFlag } from "../../../utils/FeatureFlags";
import LoaderButton from "../../common/LoaderButton";

const TINYMCE_API_KEY = process.env.REACT_APP_TINYMCE_API_KEY;

const AddEmailStyle = styled.div`
  .hour-input {
    display: inline-block;
    width: auto;
    margin: 0px 10px;
  }
  .right-side {
    background: #000;
    color: white;
    padding: 0px;
    height: 100%;
    h3 {
      color: white;
      padding: 15px;
    }
  }
  .shortcode-item {
    cursor: pointer;
    &:hover {
      background: #d1d1d1;
    }
    padding: 15px;
    .shortcode-item-title {
      color: white;
    }
    .shortcode-item-value {
      color: white;
    }
  }
`;

function useShortcodes() {
  const replayEnabled = useFeatureFlag("replay");

  return filterFalsy([
    {
      title: "Attendee First Name",
      value: "{{ attendee.firstName }}",
    },
    {
      title: "Attendee Last Name",
      value: "{{ attendee.lastName }}",
    },
    {
      title: "Webinar Name",
      value: "{{ webinar.name }}",
    },
    {
      title: "Event Start Time",
      value: "{{ event.start }}",
    },
    {
      title: "View Link",
      value: "{{ viewLink }}",
    },
    replayEnabled && {
      title: "Replay Link",
      value: "{{ replayLink }}",
    },
  ]);
}

const AddEmailModal = ({
  notificationEmail,
  toggle,
  template,
  webinar,
  doCreateNotification,
  doEditNotification,
}) => {
  const [subject, setSubject] = useState(notificationEmail?.subject || "");
  const [content, setContent] = useState(notificationEmail?.content || "");
  const [time, setTime] = useState(notificationEmail?.hoursOffset || 0);
  const [loading, setLoading] = useState(false);

  const shortcodes = useShortcodes();

  const onPressShortcode = useCallback((e) => {
    (window as any).tinymce.activeEditor.insertContent(e.value);
    // if (!content) {
    //   setContent(e.value);
    //   return;
    // }
    // const lastIndex = content.lastIndexOf("</");
    // setContent(
    //   content.substring(0, lastIndex) +
    //     ` ${e.value}` +
    //     content.substring(lastIndex, content.length)
    // );
  }, []);

  const handleSetTime = (e) => {
    setTime(e.currentTarget.value.replace(/[^0-9]/g, ""));
  };

  const doSendEmail = async () => {
    try {
      setLoading(true);
      if (notificationEmail?.id) {
        await doEditNotification(notificationEmail?.id, {
          type: template.type,
          deliveryMethod: NotificationDeliveryMethod.EMAIL,
          subject,
          content: content,
          hoursOffset: Number(time) || 0,
        });
      } else {
        await doCreateNotification({
          type: template.type,
          deliveryMethod: NotificationDeliveryMethod.EMAIL,
          subject,
          content: content,
          hoursOffset: Number(time) || 0,
        });
      }
      setSubject("");
      setContent("");
      setTime(undefined);
      toggle(null);
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (notificationEmail) {
      setSubject(notificationEmail?.subject);
      setContent(notificationEmail?.content);
      setTime(notificationEmail?.hoursOffset);
    }
  }, [notificationEmail]);

  return (
    <Modal
      show={!!notificationEmail}
      onHide={() => toggle(null)}
      size="lg"
      enforceFocus={false}
    >
      <Modal.Header closeButton className="pl-3 py-3">
        <span style={{ fontSize: "1.125rem" }}>
          {notificationEmail?.title
            ? `${notificationEmail?.title} Message`
            : "Email Notification"}
        </span>
      </Modal.Header>
      <div className="pt-3 pl-3 pr-5 pb-3">
        <AddEmailStyle>
          <Row>
            <Col xs={12}>
              <FormGroup>
                {`Send this email `}
                {template.timeUnit === "immediately" ? (
                  <FormControl
                    disabled
                    value="Immediately"
                    htmlSize={10}
                    className="hour-input"
                  ></FormControl>
                ) : (
                  <FormControl
                    value={template.timeUnit === "minutes" ? 15 : time}
                    disabled={template.disabledEditTime}
                    htmlSize={3}
                    className="hour-input"
                    onChange={handleSetTime}
                  ></FormControl>
                )}
                {` ${template.addonTimeInput}`}
              </FormGroup>
            </Col>
            <Col xs={12}>
              <FormGroup>
                <FormLabel htmlFor="email-title">Email Title</FormLabel>
                <FormControl
                  id="email-title"
                  onChange={(e) => setSubject(e.currentTarget.value)}
                  value={subject}
                />
                <br />
              </FormGroup>
            </Col>
            <Col xs={12}>
              <FormLabel>Email body</FormLabel>
            </Col>
            <Col xs={8}>
              <Editor
                value={content}
                onEditorChange={setContent}
                apiKey={TINYMCE_API_KEY}
                init={{
                  height: 500,
                  menubar: false,
                  plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table paste code help wordcount",
                  ],
                  toolbar: `undo redo | formatselect | bold italic backcolor | 
                    alignleft aligncenter alignright alignjustify | 
                    bullist numlist outdent indent | removeformat | help`,
                }}
              />
            </Col>
            <Col xs={4}>
              <div className="right-side">
                <h3>Shortcodes</h3>
                {shortcodes.map((e) => (
                  <div
                    onClick={() => onPressShortcode(e)}
                    key={e.value}
                    className="shortcode-item"
                  >
                    <div className="shortcode-item-title">{e.title}</div>
                    <div className="shortcode-item-value">{e.value}</div>
                  </div>
                ))}
              </div>
            </Col>
          </Row>
        </AddEmailStyle>
      </div>
      <div className="pt-4 pl-0 pr-5 pb-4 d-flex justify-content-end">
        <Button
          onClick={() => toggle(null)}
          variant="outline-secondary"
          className="mr-2"
          style={{ fontSize: "1rem" }}
        >
          Cancel
        </Button>
        <LoaderButton
          loading={loading}
          onClick={doSendEmail}
          variant="primary"
          style={{ fontSize: "1rem" }}
        >
          Save
        </LoaderButton>
      </div>
    </Modal>
  );
};

export default AddEmailModal;
