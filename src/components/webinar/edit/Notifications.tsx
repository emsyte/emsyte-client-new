import {
  IntegrationObjectFull,
  IntegrationProvider,
} from "@emsyte/common/integration";
import {
  NotificationGatewayObject,
  WebinarObject,
} from "@emsyte/common/webinar";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { groupBy } from "lodash";
import React, { useEffect, useState } from "react";
import { Button, Card, Col, Form, Modal, Row, Spinner } from "react-bootstrap";
import { useQuery } from "react-query";
import styled from "styled-components";
import { getIntegrationImage } from "../../../data/integrations-data";
import { filterFalsy } from "../../../utils/conditions";
import { NOTIFICATION_TEMPLATES } from "../../../utils/constants";
import { useFeatureFlag } from "../../../utils/FeatureFlags";
import { useHttpService } from "../../../utils/HttpService/index";
import { useNotification } from "../../common/Notification";
import IntegrationCard from "../../user-profile-lite/Integrations/IntegrationCard";
import EditIntegrationModal from "../../user-profile-lite/modals/EditIntegration/EditIntegration";
import NotificationGroup from "./NotificationGroup";

const checkIconSize = 2.5;

const IntegrationWrapper = styled.div`
  position: relative;
  overflow: hidden;

  .check {
    position: absolute;
    right: calc(50% - ${checkIconSize / 2}rem);
    font-size: ${checkIconSize}rem;
    color: #17c671;
    background-color: white;
    border-radius: ${checkIconSize}rem;
  }
`;

const PreserveAspectRatioImg = styled.img`
  object-fit: scale-down;
`;

const AutoresponderTitle = styled.span`
  font-weight: 400;
  padding-left: 1rem;
  padding-right: 1rem;
  padding-top: 1rem;
  display: block;
`;

const RadioLabel = styled.h6`
  margin: 0;
  letter-spacing: 1.2px;
  font-weight: 500;
`;

const Parenthesis = styled.span`
  font-weight: 400;
  letter-spacing: normal;
`;

const CustomAlertWarning = styled.div`
  margin-left: 1rem;
  color: #856404;
  background-color: #fff3cd;
  border: 1px solid #ffeeba;
  font-weight: 400;
  padding: 0.75rem 1.25rem;
  margin-bottom: 1rem;
`;

const SetupText = styled.div`
  font-weight: 500;
  cursor: pointer;
`;

interface IntegrationCardProps {
  integration: IntegrationObjectFull;
  active: boolean;
  settings?: any;
  onActivate: (integration: IntegrationObjectFull, settings: any) => void;
  configureAction?: any;
}

const BaseIntegrationCard: React.FC<IntegrationCardProps> = ({
  integration,
  active,
  children,
  onActivate,
  configureAction = () => {},
}) => {
  return (
    <IntegrationWrapper className={active ? "active" : ""}>
      <Card className="px-3 py-2">
        <div className="d-flex justify-content-center mb-2">
          <PreserveAspectRatioImg
            src={getIntegrationImage(integration.provider)}
            width={168}
            height={50}
            alt={`${integration.title} name`}
            style={active ? { opacity: 0.6 } : {}}
          />
          {active && <FontAwesomeIcon icon={faCheckCircle} className="check" />}
        </div>
        <div className="d-flex justify-content-center">
          {active ? (
            <Button onClick={configureAction}>Configure</Button>
          ) : (
            <Button onClick={() => onActivate(integration, {})}>Select</Button>
          )}
        </div>
      </Card>
      {active && <FontAwesomeIcon icon={faCheckCircle} className="check" />}
    </IntegrationWrapper>
  );
};

const SMTPIntegrationCard: React.FC<IntegrationCardProps> = ({
  integration,
  active,
  onActivate,
}) => {
  const [showModal, setShowModal] = useState(false);

  return (
    <React.Fragment>
      <BaseIntegrationCard
        integration={integration}
        active={active}
        onActivate={onActivate}
        configureAction={() => setShowModal(true)}
      >
        <Button
          variant="outline-success"
          onClick={() => onActivate(integration, {})}
        >
          Select
        </Button>
      </BaseIntegrationCard>
      <EditIntegrationModal
        integration={integration}
        show={showModal}
        onClose={() => setShowModal(false)}
      />
    </React.Fragment>
  );
};

interface MappedFieldProps {
  name: string;
  label: string;
  fields?: { label: string; value: string }[];
  fieldMapping: Record<string, string>;
  onUpdateFieldMapping: (fieldMapping: Record<string, string>) => void;
}

const MappedField: React.FC<MappedFieldProps> = ({
  name,
  label,
  onUpdateFieldMapping,
  fields,
  fieldMapping,
}) => {
  const loading = !fields;
  return (
    <Form.Group controlId={name}>
      <Form.Label>{label}</Form.Label>
      <Form.Control
        value={fieldMapping[name]}
        as="select"
        disabled={loading}
        onChange={(event) =>
          onUpdateFieldMapping({ ...fieldMapping, [name]: event.target.value })
        }
        custom
      >
        {loading ? (
          <option value="">Loading</option>
        ) : (
          <option value="">- Select Field -</option>
        )}
        {fields?.map((field) => (
          <option
            value={field.value}
            key={field.value}
            disabled={Object.values(fieldMapping).includes(field.value)}
          >
            {field.label}
          </option>
        ))}
      </Form.Control>
    </Form.Group>
  );
};

interface AutoResponderFormProps {
  provider: IntegrationProvider;
  fieldMapping: Record<string, string>;
  onUpdateFieldMapping: (fieldMapping: Record<string, string>) => void;
}

const AutoResponderForm: React.FC<AutoResponderFormProps> = ({
  provider,
  fieldMapping,
  onUpdateFieldMapping,
}) => {
  const api = useHttpService();
  const { data } = useQuery(["integration", provider, "fields"], () =>
    api.doGetNotificationGatewayFields(provider)
  );
  const fields = data?.data;

  const replayEnabled = useFeatureFlag("replay");

  return (
    <Form>
      <AutoresponderTitle>
        Assign custom fields to the fields in the email sender.
      </AutoresponderTitle>
      <div className="d-flex flex-column p-3" style={{ maxWidth: "16.5rem" }}>
        <MappedField
          name="webinar_name"
          fields={fields}
          fieldMapping={fieldMapping}
          onUpdateFieldMapping={onUpdateFieldMapping}
          label="Webinar name"
        />
        <MappedField
          name="live_link"
          fields={fields}
          fieldMapping={fieldMapping}
          onUpdateFieldMapping={onUpdateFieldMapping}
          label="Link to live room"
        />
        {replayEnabled && (
          <MappedField
            name="replay_link"
            fields={fields}
            fieldMapping={fieldMapping}
            onUpdateFieldMapping={onUpdateFieldMapping}
            label="Link to replay room"
          />
        )}
        <MappedField
          name="date"
          fields={fields}
          fieldMapping={fieldMapping}
          onUpdateFieldMapping={onUpdateFieldMapping}
          label="Date"
        />
        <MappedField
          name="time"
          fields={fields}
          fieldMapping={fieldMapping}
          onUpdateFieldMapping={onUpdateFieldMapping}
          label="Time"
        />
        <MappedField
          name="timezone"
          fields={fields}
          fieldMapping={fieldMapping}
          onUpdateFieldMapping={onUpdateFieldMapping}
          label="Timezone"
        />
      </div>
    </Form>
  );
};

const AutoResponderIntegrationCard: React.FC<IntegrationCardProps> = ({
  integration,
  settings,
  active,
  onActivate,
}) => {
  const [show, setShow] = useState(false);
  const [fieldMapping, setFieldMapping] = useState(settings?.fields ?? {});

  const replayEnabled = useFeatureFlag("replay");

  const requiredFields = filterFalsy([
    "live_link",
    replayEnabled && "replay_link",
    "date",
    "time",
    "timezone",
  ]);

  const api = useHttpService();

  // Fetch as soon as possible
  useQuery(["integration", integration.provider, "fields"], () =>
    api.doGetNotificationGatewayFields(integration.provider)
  );

  const isInvalid =
    Object.values(fieldMapping).some((v) => v === "") ||
    requiredFields.some((field) => !fieldMapping[field]);

  return (
    <React.Fragment>
      <BaseIntegrationCard
        integration={integration}
        active={active}
        onActivate={onActivate}
        configureAction={() => setShow(true)}
      />
      <Modal show={show} onHide={() => setShow(false)}>
        <Modal.Header className="pl-3 py-3" closeButton>
          <span style={{ fontSize: "1.125rem" }}>
            Configure {integration.title} fields
          </span>
        </Modal.Header>
        <div>
          <AutoResponderForm
            provider={integration.provider}
            fieldMapping={fieldMapping}
            onUpdateFieldMapping={setFieldMapping}
          />
        </div>
        <div className="d-flex justify-content-end px-3 py-4">
          <Button
            variant="outline-secondary"
            onClick={() => setShow(false)}
            style={{ fontSize: "1rem" }}
          >
            Cancel
          </Button>
          <Button
            disabled={isInvalid}
            onClick={() => {
              onActivate(integration, { fields: fieldMapping });
              setShow(false);
            }}
            className="ml-2"
            style={{ fontSize: "1rem" }}
          >
            Save
          </Button>
        </div>
      </Modal>
    </React.Fragment>
  );
};

interface GatewayFormProps {
  onSelectType: (type: "none" | "smtp" | "autoresponder") => void;
  type: "none" | "smtp" | "autoresponder";
  emailGateway?: NotificationGatewayObject;
  onChangeEmailGateway: (emailGateway: NotificationGatewayObject) => void;
}

const serviceTypeIntegrationTypeMap = {};
serviceTypeIntegrationTypeMap["smtp"] = "email";
serviceTypeIntegrationTypeMap["autoresponder"] = "crm";

export const GatewayForm: React.FC<GatewayFormProps> = ({
  onSelectType,
  type,
  emailGateway,
  onChangeEmailGateway,
}) => {
  const api = useHttpService();
  const [showIntegrationModal, setShowIntegrationModal] = useState(false);

  const { data: integrations, isLoading } = useQuery(
    "integrations",
    api.doGetIntegrations
  );

  const visibleIntegrations =
    type === "smtp"
      ? integrations?.data.filter(
          (integration) =>
            integration.type === "email" && integration.active === true
        ) ?? []
      : type === "autoresponder"
      ? integrations?.data.filter(
          (integration) =>
            integration.type === "crm" && integration.active === true
        ) ?? []
      : [];

  return (
    <Form>
      <Form.Group controlId="smtp">
        <Form.Check
          custom
          onChange={(event: any) => onSelectType(event.target.value)}
          type="radio"
          name="emailGateway"
          value="smtp"
          checked={type === "smtp"}
          label={
            <div className="mb-3">
              <RadioLabel>Integrated Email Sender</RadioLabel>
              <span style={{ fontSize: "0.938rem" }}>
                Connect to an email sender and configure email notifications
                below.
              </span>
            </div>
          }
        />
        {type === "smtp" &&
          (isLoading ? (
            <Spinner animation="border" />
          ) : (
            <React.Fragment>
              {visibleIntegrations.length ? (
                visibleIntegrations.map((integration) => (
                  <Row className="ml-3">
                    <Col style={{ maxWidth: 230 }} key={integration.provider}>
                      <SMTPIntegrationCard
                        active={
                          emailGateway?.integration.provider ===
                          integration.provider
                        }
                        integration={integration}
                        onActivate={(integration, settings) =>
                          onChangeEmailGateway({
                            integration: integration as any,
                            settings,
                          })
                        }
                      />
                    </Col>
                  </Row>
                ))
              ) : (
                <CustomAlertWarning role="alert">
                  {/* Can't use react-alert class from bootstrap because shards interference */}
                  You don’t have an email sender configured.{" "}
                  <SetupText onClick={() => setShowIntegrationModal(true)}>
                    Set up your email sender
                  </SetupText>
                </CustomAlertWarning>
              )}
            </React.Fragment>
          ))}
      </Form.Group>
      <Form.Group controlId="autoresponder">
        <Form.Check
          custom
          onChange={(event: any) => onSelectType(event.target.value)}
          type="radio"
          name="emailGateway"
          value="autoresponder"
          checked={type === "autoresponder"}
          label={
            <div className="mb-3">
              <RadioLabel>
                External Email Sender{" "}
                <Parenthesis className="text-muted">(Advanced)</Parenthesis>
              </RadioLabel>
              <span style={{ fontSize: "0.938rem" }}>
                Use an external email sender to manage and deliver email
                notifications for your webinars.
              </span>
            </div>
          }
        />
        {type === "autoresponder" &&
          (isLoading ? (
            <Spinner animation="border" />
          ) : (
            <Row className="ml-3">
              {visibleIntegrations.length ? (
                visibleIntegrations.map((integration) => (
                  <Col key={integration.provider} style={{ maxWidth: 300 }}>
                    <AutoResponderIntegrationCard
                      active={
                        emailGateway?.integration.provider ===
                        integration.provider
                      }
                      integration={integration}
                      onActivate={(integration, settings) =>
                        onChangeEmailGateway({
                          integration: integration as any,
                          settings,
                        })
                      }
                      settings={emailGateway?.settings}
                    />
                  </Col>
                ))
              ) : (
                <CustomAlertWarning role="alert">
                  You don’t have an external email sender configured.{" "}
                  <SetupText onClick={() => setShowIntegrationModal(true)}>
                    Set up your email sender
                  </SetupText>
                </CustomAlertWarning>
              )}
            </Row>
          ))}
      </Form.Group>
      <Form.Group controlId="none">
        <Form.Check
          custom
          onChange={(event: any) => onSelectType(event.target.value)}
          type="radio"
          name="emailGateway"
          value="none"
          checked={type === "none"}
          label={
            <div className="mb-3">
              <RadioLabel>
                No Notifications{" "}
                <Parenthesis className="text-danger">
                  (Not Recommended)
                </Parenthesis>
              </RadioLabel>
              <span style={{ fontSize: "0.938rem" }}>
                Emails won’t be sent to registered attendees. It’s best to
                communicate with your attendees, so only do this if you’re going
                to set it up later.
              </span>
            </div>
          }
        />
      </Form.Group>
      <Modal
        show={showIntegrationModal}
        onHide={() => setShowIntegrationModal(false)}
      >
        <Modal.Header closeButton className="py-3 pl-3">
          <span style={{ fontSize: "1.125rem" }}>
            Setup {type === "smtp" ? "integrated" : "external"} email sender
          </span>
        </Modal.Header>
        <div className="p-3">
          <div className="row">
            {integrations?.data
              .filter(
                (integration) =>
                  integration.type === serviceTypeIntegrationTypeMap[type]
              )
              .map((integration) => (
                <div className="col-md-6">
                  <IntegrationCard integration={integration} />
                </div>
              ))}
          </div>
        </div>
        <div className="pb-4 px-3 d-flex justify-content-end">
          <Button
            onClick={() => setShowIntegrationModal(false)}
            style={{ fontSize: "1rem" }}
            variant="outline-secondary"
          >
            Close
          </Button>
        </div>
      </Modal>
    </Form>
  );
};
interface NotificationsProps {
  webinar: WebinarObject;
  onWebinarUpdate: () => Promise<any>;
  saveRef: React.MutableRefObject<() => void>;
  onNext: () => void;
}

const Notifications: React.FC<NotificationsProps> = ({
  webinar,
  saveRef,
  onNext,
}) => {
  const api = useHttpService();
  const notification = useNotification();
  const [notifications, setEmailNotifications] = useState<any>(null);
  const [loading, setLoading] = useState(false);

  const [emailGatewayType, setEmailGatewayType] = useState<
    "none" | "smtp" | "autoresponder"
  >(
    webinar.emailGateway
      ? webinar.emailGateway.integration.type === "email"
        ? "smtp"
        : "autoresponder"
      : "none"
  );

  const [emailGateway, setEmailGateway] = useState(webinar.emailGateway);

  const handleSave = async () => {
    try {
      if (emailGateway) {
        await api.doSetNotificationGateway(webinar.id, "email", {
          provider: emailGateway.integration.provider,
          settings: emailGateway.settings,
        });
      }

      onNext();
    } catch (error) {
      notification.showMessage({
        title: "Error saving",
        type: "error",
        description: error.response?.data?.message ?? error.message,
      });
    }
  };

  saveRef.current = handleSave;

  const doDeleteItem = async (item) => {
    try {
      if (!notifications) return;
      await api.doDeleteNotification(webinar.id, item?.id);
      notifications[item.type] = notifications?.[item.type]?.filter(
        (e) => e.id !== item.id
      );
      setEmailNotifications({ ...notifications });

      notification.showMessage({
        title: "Delete successfully!",
        description: "Notification has been deleted!",
        type: "success",
      });
      return item;
    } catch (error) {
      notification.showMessage({
        title: "Oops!",
        description:
          error.response?.data?.message ?? "Delete notification failure.",
        type: "error",
      });
      return error;
    }
  };

  const doCreateNotification = async (item) => {
    try {
      if (!notifications) return;
      const data = await api.doCreateNotification(webinar.id, item);
      if (!notifications[item.type]) {
        notifications[item.type] = [];
      }
      notifications[item.type].push(data.data);
      setEmailNotifications({ ...notifications });

      notification.showMessage({
        title: "Create successfully!",
        description: "Notification has been Created!",
        type: "success",
      });
      return data;
    } catch (error) {
      notification.showMessage({
        title: "Oops!",
        description:
          error.response?.data?.message ?? "Create notification failure.",
        type: "error",
      });
      return error;
    }
  };

  const doEditNotification = async (id, item) => {
    try {
      if (!notifications) return;
      const data = await api.doUpdateNotification(webinar.id, id, item);
      if (!notifications[item.type]) {
        notifications[item.type] = [];
      }
      notifications[item.type] = notifications[item.type].map((e) =>
        e.id !== id ? e : data.data
      );
      setEmailNotifications({ ...notifications });
      notification.showMessage({
        title: "Update successfully!",
        description: "Notification has been updated!",
        type: "success",
      });
      return data;
    } catch (error) {
      notification.showMessage({
        title: "Oops!",
        description:
          error.response?.data?.message ?? "Update notification failure.",
        type: "error",
      });
      return error;
    }
  };

  useEffect(() => {
    const getNoti = async () => {
      try {
        setLoading(true);
        const notification = await api.doGetNotifications(webinar.id);
        setEmailNotifications(groupBy(notification.data, "type"));
        setLoading(false);
      } catch (err) {
        setLoading(false);
      }
    };
    getNoti();
  }, [api, webinar.id]);

  return (
    <React.Fragment>
      <Card>
        <div className="mx-4 my-3">
          <span className="m-0 text-uppercase">Notification Service</span>
        </div>

        <div className="mx-4 my-1">
          <GatewayForm
            onSelectType={(type) => {
              setEmailGatewayType(type);
              setEmailGateway(webinar.emailGateway);
            }}
            emailGateway={emailGateway}
            onChangeEmailGateway={setEmailGateway}
            type={emailGatewayType}
          />
        </div>
      </Card>

      {emailGatewayType === "smtp" && (
        <Card className="mt-3 px-4 py-3">
          <span className="text-uppercase d-block">Notifications</span>
          <span
            className="d-block mt-2"
            style={{ fontSize: "0.813rem", fontWeight: 400 }}
          >
            Create and manage notifications sent to attendees
          </span>
          {NOTIFICATION_TEMPLATES.map((e) => (
            <NotificationGroup
              items={notifications?.[e.type]}
              key={e.type}
              webinar={webinar}
              template={e}
              doDeleteItem={doDeleteItem}
              doCreateNotification={doCreateNotification}
              doEditNotification={doEditNotification}
              loading={loading}
              description={e.description}
            />
          ))}
        </Card>
      )}
    </React.Fragment>
  );
};

export default Notifications;
