import { NotificationDeliveryMethod } from "@emsyte/common/notification";
import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  FormControl,
  FormGroup,
  FormLabel,
  Modal,
  Row,
} from "react-bootstrap";
import styled from "styled-components";
import LoaderButton from "../../common/LoaderButton";

const AddSMSStyle = styled.div`
  .hour-input {
    display: inline-block;
    width: auto;
    margin: 0px 10px;
  }
  .right-side {
    background: #000;
    color: white;
    padding: 0px;
    height: 100%;
    h3 {
      color: white;
      padding: 15px;
    }
  }
  .shortcode-item {
    cursor: pointer;
    &:hover {
      background: #d1d1d1;
    }
    padding: 15px;
    .shortcode-item-title {
      color: white;
    }
    .shortcode-item-value {
      color: white;
    }
  }
`;

const AddSMSModal = ({
  notificationSMS,
  toggle,
  template,
  webinar,
  doCreateNotification,
  doEditNotification,
}) => {
  const [content, setContent] = useState(notificationSMS?.content || "");
  const [time, setTime] = useState(notificationSMS?.hoursOffset || 0);
  const [loading, setLoading] = useState(false);

  const handleSetTime = (e) => {
    setTime(e.currentTarget.value.replace(/[^0-9]/g, ""));
  };

  const doSendSMS = async () => {
    try {
      setLoading(true);
      if (notificationSMS?.id) {
        await doEditNotification(notificationSMS?.id, {
          type: template.type,
          deliveryMethod: NotificationDeliveryMethod.SMS,
          content: content,
          subject: "",
          hoursOffset: Number(time) || 0,
        });
      } else {
        await doCreateNotification({
          type: template.type,
          deliveryMethod: NotificationDeliveryMethod.SMS,
          content: content,
          subject: "",
          hoursOffset: Number(time) || 0,
        });
      }

      setContent("");
      setTime(undefined);
      toggle(false);
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  };

  useEffect(() => {
    if (notificationSMS) {
      setTime(0);
      setContent(notificationSMS?.content || "");
    }
  }, [notificationSMS]);
  const charsRemaining = 160 - content.length;
  return (
    <Modal show={!!notificationSMS} onHide={() => toggle(false)} size="lg">
      <Modal.Header className="pl-3 py-3" closeButton>
        <span style={{fontSize: '1.125rem'}}>SMS Notification</span>
      </Modal.Header>
      <div className="pt-3 pl-3 pr-5 pb-3">
        <AddSMSStyle>
          <Row>
            <Col xs={12}>
              <FormGroup>
                {`Send this email `}
                {template.timeUnit === "immediately" ? (
                  <FormControl
                    disabled
                    value="Immediately"
                    htmlSize={10}
                    className="hour-input"
                  ></FormControl>
                ) : (
                  <FormControl
                    value={template.timeUnit === "minutes" ? 15 : time}
                    disabled={template.disabledEditTime}
                    htmlSize={3}
                    className="hour-input"
                    onChange={handleSetTime}
                  ></FormControl>
                )}
                {` ${template.addonTimeInput}`}
              </FormGroup>
            </Col>
            <Col xs={12}>
              <FormGroup>
                <FormLabel htmlFor="sms-message">SMS message</FormLabel>
                <FormControl
                  as="textarea"
                  id="sms-message"
                  onChange={(e) =>
                    setContent(e.currentTarget.value.slice(0, 160))
                  }
                  value={content}
                />
                <span
                  className={
                    charsRemaining <= 0
                      ? "text-danger"
                      : charsRemaining <= 10
                      ? "text-warning"
                      : ""
                  }
                >
                  {charsRemaining} characters left
                </span>
              </FormGroup>
            </Col>
          </Row>
        </AddSMSStyle>
      </div>
      <div className="pt-0 pl-0 pr-5 pb-4 d-flex justify-content-end">
        <Button onClick={() => toggle(false)} variant="outline-secondary" className="mr-2" style={{fontSize: '1rem'}}>
          Cancel
        </Button>
        <LoaderButton loading={loading} onClick={doSendSMS} variant="primary" style={{fontSize: '1rem'}}>
          Save
        </LoaderButton>
      </div>
    </Modal>
  );
};

export default AddSMSModal;
