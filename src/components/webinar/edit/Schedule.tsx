import { Frequency, RecurrenceObject } from "@emsyte/common/webinar";
import moment from "moment";
import "moment-timezone";
import React, { useState } from "react";
import { Badge, Button, Form, Modal } from "react-bootstrap";
import { Calendar, X } from "react-bootstrap-icons";
import { Controller, useForm } from "react-hook-form";
import TimePicker from "react-time-picker";
import { DatePicker } from "shards-react";
import styled from "styled-components";
import { ReactComponent as ArrowElbow } from "../../../images/arrow_elbow.svg";

const CustomTimePicker = styled(TimePicker)`
  width: auto;
  display: inline-block;

  .react-time-picker__wrapper {
    border: none;
  }

  input:focus-visible {
    border: none;
    outline: none;
  }
`;

const IconButton = styled(Button)`
  background: none !important;
  border: none !important;
  box-shadow: none !important;
  padding: 0;
`;

const RecurrenceFormBody = styled.div`
  background: #f5f5f5;
  padding: 16px 20px;
  margin-bottom: 1rem;
`;

export const daysOfWeek = [
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday",
];

const weekdayId = {
  sunday: 0,
  monday: 1,
  tuesday: 2,
  wednesday: 3,
  thursday: 4,
  friday: 5,
  saturday: 6,
};

interface TimeBlock {
  start: Date;
  end: Date;
}

export interface ScheduleRefType {
  scheduleWebinar: (id: string | number) => void;
}

const DatePickerWrapper = styled.span`
  position: relative;
  display: inline-block;
  .icon {
    position: absolute;
    width: auto;
    top: calc(50% - 0.75em);
    height: 1.5em;
    right: 0.5em;
  }

  input {
    width: 200px;
  }
`;

function formatOffset(offset) {
  const sign = Math.sign(offset);
  offset = Math.abs(offset);

  const hours = Math.floor(offset / 60);
  const minutes = offset % 60;
  let s = `GMT${sign < 0 ? "-" : "+"}${hours}hr`;
  if (minutes) {
    s += ` ${minutes}min`;
  }
  return s;
}

interface IconDatePickerProps {}

const IconDatePicker: React.FC<IconDatePickerProps & any> = ({ ...props }) => {
  return (
    <DatePickerWrapper>
      <DatePicker {...props}></DatePicker>
      <Calendar className="icon"></Calendar>
    </DatePickerWrapper>
  );
};

export interface LocalRecurrenceObject
  extends Omit<RecurrenceObject, "webinarId" | "id"> {
  id: number | string;
  isLocal: boolean;
  shouldDelete: boolean;
}
function mod(a: number, n: number) {
  return ((a % n) + n) % n;
}

function formatRecurrence(recurrence: LocalRecurrenceObject) {
  const start = moment.tz(recurrence.start, "utc");
  const day = start.format("MM/DD/YYYY");
  const time = start.format("hh:mma");
  const end = moment.tz(recurrence.end, "utc").format("MM/DD/YYYY");
  const zone = moment.tz(recurrence.timezone);
  if (day === end) {
    return `${day} @ ${time} ${zone.zoneAbbr()}`;
  }
  if (recurrence.frequency === Frequency.DAILY) {
    return `${day} - ${end} @ ${time} ${zone.zoneAbbr()} - Daily`;
  }
  const days = recurrence.days
    .map((day) => daysOfWeek[mod(day - 1, 7)])
    .join(", ");

  return `${day} - ${end} @ ${time} ${zone.zoneAbbr()} - ${days}`;
}

interface ScheduleProps {
  recurrences: LocalRecurrenceObject[];
  setRecurrences: (recurrences: LocalRecurrenceObject[]) => void;
}

const Schedule: React.FC<ScheduleProps> = ({ recurrences, setRecurrences }) => {
  const { register, control, handleSubmit, watch, reset, setValue } = useForm({
    defaultValues: {
      date: new Date(),
      time: moment().startOf("hour").add(1, "hour").format("HH:mm"),
      days: [] as number[],
      end_date: new Date(),
      frequency: Frequency.DAILY,
      recurring: false,
      timezone: moment.tz.guess(),
    },
  });

  const tz = moment.tz(watch("timezone"));
  const recurring = watch("recurring");
  const frequency = watch("frequency");

  const [showSelectTimeZone, setShowSelectTimeZone] = useState(false);

  const onSubmit = handleSubmit((values) => {
    reset();
    setRecurrences([
      ...recurrences,
      {
        days: values.days?.map((day) => +day) ?? [],
        isLocal: true,
        shouldDelete: false,
        id: "local-" + Math.random(),
        end: values.recurring
          ? moment(values.end_date).format("YYYY-MM-DD")
          : moment(values.date).format("YYYY-MM-DD"),
        frequency: (values.frequency as Frequency) ?? Frequency.DAILY,
        start: moment(values.date).format("YYYY-MM-DD") + " " + values.time,
        timezone: values.timezone,
      },
    ]);
  });

  return (
    <Form
      onSubmit={(event) => {
        event.stopPropagation();

        onSubmit(event);
      }}
    >
      <input type="hidden" name="timezone" ref={register()} />
      <RecurrenceFormBody>
        <div>
          <Controller
            name="date"
            control={control}
            render={({ onChange, onBlur, value }) => (
              <IconDatePicker
                onChange={onChange}
                onBlur={onBlur}
                selected={value}
              ></IconDatePicker>
            )}
          ></Controller>

          {" At "}
          <Controller
            name="time"
            control={control}
            render={({ onChange, ...props }) => (
              <CustomTimePicker
                disableClock
                clearIcon={null}
                className="form-control"
                onChange={(value) => {
                  onChange(value);
                }}
                {...props}
              />
            )}
          ></Controller>

          <span className="text-muted ml-2">
            {tz.zoneName()} ({formatOffset(tz.utcOffset())})
          </span>
          <span>
            <Button variant="link" onClick={() => setShowSelectTimeZone(true)}>
              Select timezone
            </Button>
          </span>
        </div>

        <div className="d-flex">
          <Form.Check
            custom
            type="checkbox"
            id="recurring"
            label="Recurring"
            className="mt-2"
            name="recurring"
            ref={register()}
          ></Form.Check>
          {!recurring && (
            <Button className="ml-auto" type="submit">
              Add Session
            </Button>
          )}
        </div>
        {recurring && (
          <div className="d-flex">
            <ArrowElbow></ArrowElbow>
            <div style={{ flex: 1 }}>
              <div className="d-flex">
                <Form.Group controlId="frequency">
                  <Form.Label>Frequency</Form.Label>
                  <div>
                    <Form.Control
                      name="frequency"
                      ref={register()}
                      as="select"
                      style={{ width: 200 }}
                      custom
                    >
                      <option value={Frequency.DAILY}>Daily</option>
                      <option value={Frequency.WEEKLY}>Weekly</option>
                    </Form.Control>
                  </div>
                </Form.Group>
                <Form.Group controlId="end_date" className="ml-2">
                  <Form.Label>Ends On</Form.Label>
                  <div>
                    <Controller
                      name="end_date"
                      control={control}
                      render={({ onChange, onBlur, value }) => (
                        <IconDatePicker
                          onChange={onChange}
                          onBlur={onBlur}
                          selected={value}
                        ></IconDatePicker>
                      )}
                    ></Controller>
                  </div>
                </Form.Group>

                {frequency === Frequency.DAILY && (
                  <div className="ml-auto" style={{ alignSelf: "center" }}>
                    <Button type="submit">Add Session</Button>
                  </div>
                )}
              </div>
              {frequency === Frequency.WEEKLY && (
                <div>
                  Days of the week
                  <div className="d-flex">
                    {daysOfWeek.map((day) => (
                      <div key={day}>
                        <Form.Check
                          custom
                          type="checkbox"
                          id={day}
                          name="days"
                          value={weekdayId[day.toLowerCase()]}
                          ref={register()}
                        ></Form.Check>
                        <Form.Label htmlFor={day}>{day.slice(0, 3)}</Form.Label>
                      </div>
                    ))}
                    <div className="ml-auto">
                      <Button type="submit">Add Session</Button>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        )}
      </RecurrenceFormBody>
      <div>
        {recurrences
          .filter((r) => !r.shouldDelete)
          .map((recurrence) => (
            <Badge pill variant="secondary" key={recurrence.id}>
              {formatRecurrence(recurrence)}
              <IconButton
                className="ml-1 rounded-circle"
                onClick={() =>
                  setRecurrences(
                    recurrence.isLocal
                      ? recurrences.filter((r) => r.id !== recurrence.id)
                      : recurrences.map((r) =>
                          r.id === recurrence.id
                            ? { ...recurrence, shouldDelete: true }
                            : r
                        )
                  )
                }
              >
                <X size={16}></X>
              </IconButton>
            </Badge>
          ))}
      </div>

      <Modal
        show={showSelectTimeZone}
        onHide={() => setShowSelectTimeZone(false)}
      >
        <Modal.Header>
          <Modal.Title>Select Timezone</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Group controlId="timezone">
            <Form.Label>Timezone</Form.Label>
            <Form.Control
              as="select"
              value={watch("timezone")}
              onChange={(event) => setValue("timezone", event.target.value)}
            >
              {moment.tz.names().map((tz) => (
                <option value={tz} key={tz}>
                  {/* {tz.replace(/_/g, " ")}  */}
                  {moment.tz(tz).zoneName()} ({moment.tz(tz).zoneAbbr()})
                </option>
              ))}
            </Form.Control>
          </Form.Group>
        </Modal.Body>
      </Modal>
    </Form>
  );
};

export default Schedule;
