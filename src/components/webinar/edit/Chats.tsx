import React, { useState, useCallback, useEffect } from "react";
import { Card, Col, Row, Spinner, Table } from "react-bootstrap";
import { Button, CardBody } from "shards-react";
import { useHttpService } from "../../../utils/HttpService/index";
import { WebinarChatLabel, WebinarChatObject } from "@emsyte/common/webinar";
import styled from "styled-components";
import WebinarChatForm from "./WebinarChatForm";
import { useNotification } from "../../common/Notification";
import EditWebinarChatModal from "./EditWebinarChatModal";
import DeleteWebinarChatModal from "./DeleteWebinarChatModal";
import TemplateLoading from "../../templates/TemplateLoading";
import { secondsToDuration } from "../../../utils/duration";
import { formatDuration } from "../../../utils/formatters";

function getLabelName(label: WebinarChatLabel) {
  if (label === WebinarChatLabel.ATTENDEE) {
    return "Attendee";
  }
  if (label === WebinarChatLabel.PRESENTER) {
    return "Presenter";
  }
  if (label === WebinarChatLabel.SUPPORT_TEAM) {
    return "Support Team";
  }
  return "Unknown";
}

const ChatStyle = styled.div`
  padding: 40px 20px;
  .table {
    margin-bottom: 0px;
  }
  .center {
    align-items: center;
  }
  .v-time {
    width: 200px;
  }
  .v-action {
    width: 150px;
  }
`;

const Chats = ({ webinar, onNext, saveRef }) => {
  const api = useHttpService();
  const [chats, setChats] = useState<WebinarChatObject[]>([]);
  const notification = useNotification();
  const [loading, setLoading] = useState(true);
  const [importLoading, setImportLoading] = useState(false);
  const [selected, setSelected] = useState<WebinarChatObject | null>(null);
  const [deleteSelected, setDeleteSelected] =
    useState<WebinarChatObject | null>(null);

  saveRef.current = onNext;

  const showEdit = (item: WebinarChatObject) => {
    setSelected(item);
  };

  const handleDelete = async () => {
    try {
      if (!deleteSelected) return;
      await api.doDeleteChatMessage(webinar.id, deleteSelected?.id);
      setChats(chats.filter((e) => e.id !== deleteSelected?.id));
      notification.showMessage({
        title: "Delete Successfully!",
        description: "Delete webinar chat successfully",
        type: "success",
      });
    } catch (error) {
      notification.showMessage({
        title: "Failed to delete!",
        description: error.message,
        type: "error",
      });
    }
  };

  const onRefresh = useCallback(() => {
    setLoading(true);
    api
      .doGetAllWebinarChatMessages(webinar.id)
      .then(({ data }) => {
        setChats(data);
        setLoading(false);
      })
      .catch((error) => {
        notification.showMessage({
          title: "Oops!",
          description:
            error.response?.data?.message ?? "Failed to load webinar chats",
          type: "error",
        });
        setLoading(false);
      });
  }, [api, notification, webinar.id]);
  const doImportCSV = async (event) => {
    try {
      setImportLoading(true);
      const response = await api.doUploadWebinarChatMessage(
        webinar.id,
        event.target.files[0]
      );
      setImportLoading(false);
      onRefresh();
      notification.showMessage({
        title: "Import Successfully!",
        description:
          response?.data?.message ?? "Import Webinar chat Successfully.",
        type: "success",
      });
    } catch (error) {
      setImportLoading(false);
      notification.showMessage({
        title: "Oops!",
        description:
          error.response?.data?.message ?? "Import Webinar chat Failure.",
        type: "error",
      });
    }
  };

  useEffect(() => {
    onRefresh();
  }, [api, onRefresh, webinar.id]);

  return (
    <ChatStyle>
      <Row>
        <Col>
          <h5 className="mb-0">Insert Message</h5>
        </Col>
        <Col>
          <label
            className="float-right mr-2 btn btn-secondary"
            htmlFor="myFile"
          >
            {importLoading && (
              <Spinner animation="border" size="sm" className="mr-2"></Spinner>
            )}
            <i className="fas fa-file-import"></i>
            Import CSV
          </label>
          <input
            onChange={doImportCSV}
            className="d-none"
            type="file"
            id="myFile"
            accept=".csv"
          />
        </Col>
      </Row>
      <br />
      <Card>
        <CardBody>
          <WebinarChatForm onSuccess={onRefresh} webinar={webinar} />
        </CardBody>
        <Table striped responsive="sm">
          <thead>
            <tr>
              <th>Time</th>
              <th>Label</th>
              <th>Name</th>
              <th>Message</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {chats.map((item) => (
              <tr key={item.id}>
                <td className="v-time">
                  {formatDuration(secondsToDuration(item.offset_timestamp))}
                </td>
                <td>{getLabelName(item.label)}</td>
                <td>{item.name}</td>
                <td>{item.content}</td>
                <td className="v-action">
                  <Button onClick={() => showEdit(item)}>
                    <i className="fas fa-edit"></i>
                  </Button>
                  &nbsp;
                  <Button
                    theme="danger"
                    onClick={() => setDeleteSelected(item)}
                  >
                    <i className="fas fa-trash-alt"></i>
                  </Button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        {chats.length === 0 && !loading && (
          <div className="py-5 text-center">Empty chat.</div>
        )}
        {chats.length === 0 && loading && (
          <div className="py-5 text-center">
            <TemplateLoading />
          </div>
        )}
      </Card>
      <EditWebinarChatModal
        webinar={webinar}
        setSelected={setSelected}
        item={selected}
        onComplete={onRefresh}
      />
      <DeleteWebinarChatModal
        deleteSelected={deleteSelected}
        setDeletedSelected={setDeleteSelected}
        handleDelete={handleDelete}
        onComplete={onRefresh}
      />
    </ChatStyle>
  );
};

export default Chats;
