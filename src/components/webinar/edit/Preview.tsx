import { WebinarObject } from "@emsyte/common/webinar";
import { faMoneyBillAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import { Card, CardBody } from "shards-react";
import emptyWebinar from "../../../images/empty-webinar.png";
import { secondsToDuration } from "../../../utils/duration";
import { useFeatureFlag } from "../../../utils/FeatureFlags";
import { formatCurrency } from "../../../utils/formatters";
import CopyInput from "../../common/CopyLink";

const { REACT_APP_EVENTS } = process.env;

function getEmbedCode(webinar: WebinarObject) {
  const webinarId = Buffer.from(JSON.stringify({ id: webinar.id })).toString(
    "base64"
  );
  return `
<button type="button" data-webinarId="${webinarId}" style="border: 2px solid rgba(0, 0, 0, 0.5); background: rgba(41, 182, 246, 0.95); color: rgb(255, 255, 255); font-size: 24px; padding: 18px 80px; box-shadow: none; border-radius: 4px; white-space: normal; font-weight: 700; line-height: 1.3; cursor: pointer; font-family: Roboto, -apple-system, BlinkMacSystemFont, &quot;Segoe UI&quot;, Roboto, &quot;Helvetica Neue&quot;, Arial, sans-serif; word-break: break-word; margin: auto;" id="justwebinar-registration">Register now</button>
<script src="${REACT_APP_EVENTS}/embed.js" id="justwebinar-script"></script>
  `.trim();
}

function getRegistrationUrl(webinarId: number) {
  const data = { id: webinarId };
  const buff = new Buffer(JSON.stringify(data));
  return (
    window.location.protocol +
    "//" +
    window.location.host +
    "/webinar/registration/" +
    buff.toString("base64")
  );
}

interface PreviewProps {
  webinar: WebinarObject;
}
const Preview: React.FC<PreviewProps> = ({ webinar }) => {
  const registrationUrl = getRegistrationUrl(webinar.id);
  const duration = secondsToDuration(webinar.duration);
  const replayEnabled = useFeatureFlag("replay");

  return (
    <Container className="main-content-container mx-4 p-4">
      <h5>Webinar Title</h5>
      <Row>
        <Col>
          <Row>
            <Col>
              <span className="text-muted text-bold mr-2">Duration</span>
              <span className="text-muted text-bold">
                {duration.hours}hr : {duration.minutes}min : {duration.seconds}
                sec
              </span>
            </Col>
            {replayEnabled && (
              <Col>
                <span className="text-muted text-bold mr-2">
                  Replay Expiration
                </span>
                <span className="text-muted text-bold">
                  {webinar.replayExpiration} Days
                </span>
              </Col>
            )}
          </Row>
          <Row className="m-4">
            {/* <Col>
              <span className="text-primary">
                <FontAwesomeIcon icon={faCalendar} className="mr-2" />
                Schedules
              </span>
              <br />
              <span className="text-primary font-weight-light">
                {handleScheduleFormat(webinar)}
              </span>
            </Col> */}
            <Col>
              <span className="text-primary">
                <FontAwesomeIcon icon={faMoneyBillAlt} className="mr-2" />
                Monetization
              </span>
              <br />
              <span className="text-primary font-weight-light">
                {webinar.isPaid ? (
                  <span>
                    {formatCurrency(webinar.registrationFee)} / attendee
                  </span>
                ) : (
                  <span>Free</span>
                )}
              </span>
            </Col>
          </Row>
          <span className="text-muted text-bold">Registration Link</span>
          <Row>
            <Col md={10}>
              <Row className="my-4">
                <Col>
                  <Card theme="light" className="no-shadow">
                    <CardBody className="rounded p-2">
                      <CopyInput value={registrationUrl} />
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
          <span className="text-muted text-bold">Registration Embed Code</span>
          <Row>
            <Col md={10}>
              <Row className="my-4">
                <Col>
                  <Card theme="light" className="no-shadow">
                    <CardBody className="rounded p-2">
                      <CopyInput textarea value={getEmbedCode(webinar)} />
                    </CardBody>
                  </Card>
                </Col>
              </Row>
            </Col>
          </Row>
        </Col>
        <Col>
          <Card className="no-shadow" theme="light">
            <CardBody className="text-center rounded-lg">
              <span>Webinar Preview</span>
              <br />
              <img
                alt={webinar.name}
                src={webinar.thumbnail ? webinar.thumbnail : emptyWebinar}
                className="rounded"
                width={300}
                height={200}
              />
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default Preview;
