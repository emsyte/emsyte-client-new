import { WebinarObject, WebinarSettingsObject } from "@emsyte/common/webinar";
import React, { useState } from "react";
import { Card, Collapse, Form } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { setRemoteFormErrors } from "../../../utils/form";
import { useHttpService } from "../../../utils/HttpService";
import { useNotification } from "../../common/Notification";
import TextInput from "../../form/TextInput";
import styled from "styled-components";
import { useFeatureFlag } from "../../../utils/FeatureFlags";

const Header = styled.div`
  margin: 1rem 1.25rem;
  display: flex;
  justify-content: space-between;
`;

const Body = styled.div`
  margin: 1rem 1.25rem;
`;

interface AdvancedProps {
  webinar: WebinarObject;
  onWebinarUpdate: () => Promise<any>;
  onNext?: () => void;
  saveRef: React.MutableRefObject<() => void>;
}

const Advanced: React.FC<AdvancedProps> = ({
  webinar,
  onWebinarUpdate,
  onNext = () => {},
  saveRef,
}) => {
  const { id: webinarId } = webinar;
  const [updating, setUpdating] = useState(false);
  console.log(updating);
  const [advancedShow, setAdvancedShow] = useState(true);
  const api = useHttpService();
  const notification = useNotification();

  const replayEnabled = useFeatureFlag("replay");

  const { register, handleSubmit, errors, setError } = useForm({
    defaultValues: webinar.settings,
  });

  const onSubmit = async (settings: WebinarSettingsObject) => {
    setUpdating(true);
    try {
      const response = await api.doUpdateWebinar(webinarId.toString(), {
        settings,
      });
      console.log(response.data);
      onWebinarUpdate();
      onNext();
    } catch (error) {
      setRemoteFormErrors(error.response?.data.errors.settings, setError);
      notification.showMessage({
        title: "Failed to save settings",
        description: error.response?.data?.message ?? error.message,
        type: "error",
      });
    } finally {
      setUpdating(false);
    }
  };

  saveRef.current = handleSubmit(onSubmit);

  return (
    <Card style={{ marginTop: "1.25rem" }}>
      <Header>
        <span className="text-uppercase">Advanced Redirects and Tracking</span>
        <i
          onClick={() => setAdvancedShow(!advancedShow)}
          className={advancedShow ? "bi bi-chevron-down" : "bi bi-chevron-up"}
          style={{ cursor: "pointer" }}
        />
      </Header>
      <hr className="m-0" />
      <Collapse in={advancedShow}>
        <Body>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <Form.Group controlId="postRegistrationUrl">
              <Form.Label>Post-registration URL</Form.Label>
              <TextInput
                name="postRegistrationUrl"
                ref={register({ required: "Required" })}
                errors={errors}
              />
              <small>
                Redirect registered attendees to a custom destination
              </small>
            </Form.Group>

            <Form.Group controlId="postViewUrl">
              <Form.Label>Post-view URL</Form.Label>
              <TextInput
                name="postViewUrl"
                ref={register({ required: "Required" })}
                errors={errors}
              />
              <small>
                Redirect attendees to a custom destinations after the webinar
                has concluded
              </small>
            </Form.Group>

            <hr className="mt-0 mb-3" />

            <h6 className="m-0">3rd Party Tracking Scripts</h6>
            <small className="d-block mb-3">
              Connect to 3rd party advertising and tracking services. Insert the
              scripts supplied by the service for the pages you want to track.
            </small>
            <Form.Group controlId="registrationPageScript">
              <Form.Label>Registration page script</Form.Label>
              <TextInput
                as="textarea"
                name="registrationPageScript"
                ref={register()}
                errors={errors}
              />
            </Form.Group>

            <Form.Group controlId="registrationFormScript">
              <Form.Label>Registration form script</Form.Label>
              <TextInput
                as="textarea"
                name="registrationFormScript"
                ref={register()}
                errors={errors}
              />
            </Form.Group>

            <Form.Group controlId="thankyouPageScript">
              <Form.Label>Post-registration thank you page script</Form.Label>
              <TextInput
                as="textarea"
                name="thankyouPageScript"
                ref={register()}
                errors={errors}
              />
            </Form.Group>

            <Form.Group controlId="liveRoomScript">
              <Form.Label>Live room script</Form.Label>
              <TextInput
                as="textarea"
                name="liveRoomScript"
                ref={register()}
                errors={errors}
              />
            </Form.Group>

            {replayEnabled ? (
              <Form.Group controlId="replayRoomScript">
                <Form.Label>Replay room script</Form.Label>
                <TextInput
                  as="textarea"
                  name="replayRoomScript"
                  ref={register()}
                  errors={errors}
                />
              </Form.Group>
            ) : (
              <input
                type="hidden"
                name="replayRoomScript"
                ref={register()}
                value=""
              />
            )}
          </Form>
        </Body>
      </Collapse>
    </Card>
  );
};

export default Advanced;
