import { NotificationDeliveryMethod } from "@emsyte/common/notification";
import React, { useState } from "react";
import { Button, Spinner } from "react-bootstrap";
import styled from "styled-components";
import AddEmailModal from "./AddEmail";
import AddSMSModal from "./AddSMS";
import colors from "../../../utils/colors";

const NotificationItemStyle = styled.div`
  
  margin-top: 1.25rem;
  h5 {
    margin-bottom: 0;
  }
  .noti-row {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .notification-content {
    border: 1px solid #d1d1d1;
    background: white;
    border-radius: 4px;
    margin-top: 10px;
    .inside {
      display: flex;
      padding: 0.75rem 1rem;
      align-items: center;
      justify-content: center;
      border-left: 6px solid #007BFF;
      border-radius: 3px;
    }
    b {
      margin: 0px 20px;
    }
    far {
      line-height: 1.5;
    }
    .action {
      i {
        color: ${colors.gray600.toHex()};
        cursor: pointer;
        font-size: 1.5rem;
      }
    }
    color: #2E3338
  }
  .description {
    flex: 1;
    padding-right: 45px;
    overflow: hidden;
    text-overflow: ellipsis;
  }
  .empty-text {
  }
`;

const DescriptionContainer = styled.div`
  background-color: ${colors.notWhite.toHex()};
  border-radius: 0.25rem;
  border: 1px dashed rgba(134, 142, 150, 0.5);
  font-weight: 400; 
  padding: 1rem;
  margin-top: 1rem;
`

const Title = styled.h5`
  letter-spacing: 1.2px;
  font-weight: 500;
`

const Separator = styled.span`
  display: block;
  margin-left: 0.25rem;
  margin-right: 0.25rem;
  color: ${colors.gray600.toHex()};
  padding-bottom: 0.125rem;
`

const NotificationItem = ({
  webinar,
  item,
  template,
  doDeleteItem,
  setEditItem,
}) => {
  const [loading, setLoading] = useState(false);
  const editItem = () => {
    setEditItem(item);
  };

  const deleteItem = async () => {
    setLoading(true);
    await doDeleteItem(item);
    setLoading(false);
  };

  return (
    <div className="notification-content">
      <div className="inside">
        <div className="noti-row">
            {item.deliveryMethod === NotificationDeliveryMethod.EMAIL ? (
              <i className="bi bi-envelope-fill mr-2"/>
            ) : (
              <i className="bi bi-chat-left mr-2"/>
            )}
          {/* Not sure if delete this */}
          {/*<b>
            {item.hoursOffset
              ? `${item.hoursOffset} ${template?.addonTimeInput}`
              : "Immediately"}
          </b>*/}
        </div>
        <div className="description">
          {item.deliveryMethod === NotificationDeliveryMethod.SMS
            ? item.content
            : item.subject || template?.title}
        </div>
        <div className="noti-row action">
          <Button onClick={editItem} variant="link" style={{fontSize: '1rem'}} className="py-0 px-2">Edit</Button>
          {loading ? (
            <Spinner animation="border" size="sm"/>
          ) : (
            <i onClick={deleteItem} className="bi bi-x my-n3"/>
          )}
        </div>
      </div>
    </div>
  );
};

const NotificationGroup = ({
  webinar,
  items,
  template,
  doDeleteItem,
  doCreateNotification,
  loading,
  doEditNotification,
  description = "Don't have any notification of this type.",
}) => {
  const [notificationEmail, setNotificationEmail] = useState(null);
  const [notificationSMS, setNotificationSMS] = useState(null);

  const disableEmail =
    items &&
    template.limit <=
      items.filter(
        (item) => item.deliveryMethod === NotificationDeliveryMethod.EMAIL
      ).length;

  const smsDisabled =
    items &&
    template.limit >=
      items.filter(
        (item) => item.deliveryMethod === NotificationDeliveryMethod.SMS
      ).length;
  const setEditItem = (item) => {
    if (item?.deliveryMethod === NotificationDeliveryMethod.SMS) {
      setNotificationSMS(item);
    } else {
      setNotificationEmail(item);
    }
  };

  return (
    <>
      <NotificationItemStyle>
        <div className="noti-row">
          <Title>{template.title}</Title>
          <div className="noti-row">
            {template.hasSendEmail && (
              <Button
                onClick={() => setNotificationEmail(template)}
                variant="link"
                disabled={disableEmail}
                className="px-0"
                style={{fontSize: '1rem'}}
              >
                Add email
              </Button>
            )}
            {template.hasSendSMS && (
              <React.Fragment>
                <Separator>|</Separator>
                <Button
                  onClick={() => setNotificationSMS(template)}
                  variant="link"
                  disabled={smsDisabled}
                  className="px-0"
                  style={{fontSize: '1rem'}}
                >
                  Add SMS
                </Button>
              </React.Fragment>
            )}
          </div>
        </div>
        {items?.map((e) => (
          <NotificationItem
            template={template}
            webinar={webinar}
            item={e}
            key={e.id}
            doDeleteItem={doDeleteItem}
            setEditItem={setEditItem}
          />
        ))}
        {!items?.length &&
        (loading ?
          <div className="empty-text">
            Loading...
          </div> :
          <DescriptionContainer>
            {description}
          </DescriptionContainer>
        )
        }
      </NotificationItemStyle>
      <AddEmailModal
        webinar={webinar}
        notificationEmail={notificationEmail}
        toggle={setNotificationEmail}
        template={template}
        doCreateNotification={doCreateNotification}
        doEditNotification={doEditNotification}
      />
      <AddSMSModal
        webinar={webinar}
        notificationSMS={notificationSMS}
        toggle={setNotificationSMS}
        template={template}
        doCreateNotification={doCreateNotification}
        doEditNotification={doEditNotification}
      />
    </>
  );
};

export default NotificationGroup;
