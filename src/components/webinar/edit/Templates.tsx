import {
  TemplateInstanceObject,
  TemplateObject,
  TemplateType,
} from "@emsyte/common/template";
import { WebinarObject } from "@emsyte/common/webinar";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import {
  Button,
  Dropdown,
  FormControl,
  Modal,
  Spinner,
  Tab,
  Tabs,
} from "react-bootstrap";
import styled from "styled-components";
import colors from "../../../utils/colors";
import { useHttpService } from "../../../utils/HttpService";
import TemplateItem from "../../items/TemplateItem";

type Category = TemplateType | "all" | "active";

export function getCategoryLabel(category: Category): string {
  switch (category) {
    case "all":
      return "";
    case "active":
      return "Active";
    case TemplateType.REGISTRATION:
      return "Registration";
    case TemplateType.CONFIRMATION:
      return "Confirmation";
    case TemplateType.THANK_YOU:
      return "Thank You";
    default:
      return "";
  }
}

interface TemplatesProps {
  webinar: WebinarObject;
  handleUpdateWebinar: (webinar: WebinarObject) => Promise<any>;
  onNext: () => void;
  saveRef: React.MutableRefObject<() => void>;
}

const TabContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  background-color: ${colors.notWhite.toHex()};
  margin-left: -50vw;
  margin-right: -50vw;
  padding-left: 50vw;
  padding-right: 50vw;
`;

const ContainerTemplateSize = styled.div`
  display: flex;
  justify-content: flex-end;
  width: 16rem;
`;

const TemplateSelectorDropdown = styled(Dropdown)`
  margin-top: 1rem;
  margin-bottom: 1.5rem;
`;

const InnerDropdownContainer = styled.div`
  display: flex;
  align-items: center;
  color: #495057;
  height: 38px;
`;

const MainTitle = styled.div`
  text-transform: uppercase;
  letter-spacing: 1.2px;
`;

const ModalTitle = styled.p`
  margin: 0;
  font-size: 18px;
`;

interface Template extends TemplateObject {
  instances: TemplateInstanceObject[];
}

export function useTemplates(webinarId: number) {
  const api = useHttpService();
  const [templates, setTemplates] = useState<TemplateObject[]>([]);
  const [instances, setInstances] = useState<TemplateInstanceObject[]>([]);

  const [loading, setLoading] = useState(true);

  const loadTemplates = useCallback(async () => {
    try {
      setLoading(true);
      const [templates, instances] = await Promise.all([
        api.doGetAllTemplates(),
        api.doGetTemplateInstances(webinarId),
      ]);

      setTemplates(templates.data);
      setInstances(instances.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }, [api, webinarId]);

  useEffect(() => {
    loadTemplates();
  }, [loadTemplates]);

  const templateList = useMemo(() => {
    return templates.map<Template>((template) => {
      const _instances = instances.filter(
        (instance) => instance.parentId === template.id
      );
      return { ...template, instances: _instances };
    });
  }, [templates, instances]);
  return [templateList, loading, loadTemplates] as const;
}

// @ts-ignore
const CustomDropdown = React.forwardRef(({ children, onClick }, ref) => (
  <button
    onClick={onClick}
    ref={ref as any}
    style={{
      fontSize: "1rem",
      backgroundColor: "white",
      border: "1px solid " + colors.gray400.toHex(),
      borderRadius: "6px",
      padding: "0 10px",
    }}
  >
    {children}
  </button>
));

const ALL = "All templates";
const DEFAULT = "Default";
const CUSTOM = "Custom";

const initialSelected = {};
initialSelected[TemplateType.REGISTRATION] = undefined;
initialSelected[TemplateType.CONFIRMATION] = undefined;
initialSelected[TemplateType.THANK_YOU] = undefined;

const Templates: React.FC<TemplatesProps> = ({ webinar, saveRef, onNext }) => {
  const api = useHttpService();
  const [category, setCategory] = useState<Category>(TemplateType.REGISTRATION);
  const [templateType, setTemplateType] = useState<string>(ALL);
  const [showChangeNameModal, setShowChangeNameModal] = useState(false);
  const [editingTemplate, setEditingTemplate] = useState<Template>();
  const [editingName, setEditingName] = useState<string>("");

  const [templates, loading, reload] = useTemplates(webinar.id);

  useEffect(() => {
    setEditingName(editingTemplate ? editingTemplate.name : "");
  }, [editingTemplate]);

  const getTemplatesInCategory = useCallback(
    (category: Category) => {
      let result = templates.filter(
        (template) => template.type === category.toLowerCase()
      );
      if (templateType !== ALL) {
        const baseSelected = templateType === DEFAULT;
        result = result.filter((template) => template.base === baseSelected);
      }
      return result;
    },
    [templates, templateType]
  );

  const filteredTemplates = useMemo(
    () => getTemplatesInCategory(category),
    [getTemplatesInCategory, category]
  );

  const handleTemplateActivation = useCallback(
    async (item: Template) => {
      // Remove existing active templates of type
      const toRemove = templates.filter(
        (template) =>
          template.type === item.type && template.instances.length > 0
      );
      await Promise.all(
        toRemove.map((template) =>
          Promise.all(
            template.instances.map((instance) =>
              api.doDeleteTemplateInstance(instance.id)
            )
          )
        )
      );

      await api.doCreateTemplateInstance(webinar.id, {
        parentId: item.id,
      });
      reload();
    },
    [api, webinar, templates, reload]
  );

  const handleCloseEditingModal = () => {
    setShowChangeNameModal(false);
    setEditingName("");
  };

  const saveEdit = () => {
    api
      .doUpdateTemplateFromInstance(editingTemplate?.id || -1, {
        name: editingName,
      })
      .then(reload);
    handleCloseEditingModal();
  };

  saveRef.current = () => {
    onNext();
  };

  const templateTab = (eventKey, title) => {
    return (
      <Tab eventKey={eventKey} title={title}>
        {loading ? (
          <div className="d-flex w-100 justify-content-center">
            <Spinner animation="border" className="mr-2" />
          </div>
        ) : (
          <TabContainer>
            {/* justify the block to the right */}
            <div className="col-xl-3 offset-xl-9 col-lg-4 offset-lg-8 col-md-6 offset-md-6 col-sm-6 offset-sm-6 d-flex justify-content-center">
              <ContainerTemplateSize>
                <TemplateSelectorDropdown>
                  <Dropdown.Toggle id="dropdown-basic" as={CustomDropdown}>
                    <InnerDropdownContainer>
                      <div className="pr-3">
                        Show:{" "}
                        <span className="font-weight-bold">{templateType}</span>
                      </div>
                      <div className="d-flex flex-column">
                        <i
                          className="bi bi-caret-up-fill"
                          style={{
                            fontSize: "0.625rem",
                            marginBottom: "-0.188rem",
                          }}
                        />
                        <i
                          className="bi bi-caret-down-fill"
                          style={{
                            fontSize: "0.625rem",
                            marginTop: "-0.188rem",
                          }}
                        />
                      </div>
                    </InnerDropdownContainer>
                  </Dropdown.Toggle>

                  <Dropdown.Menu>
                    <Dropdown.Item onClick={() => setTemplateType(ALL)}>
                      {ALL}
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => setTemplateType(DEFAULT)}>
                      {DEFAULT}
                    </Dropdown.Item>
                    <Dropdown.Item onClick={() => setTemplateType(CUSTOM)}>
                      {CUSTOM}
                    </Dropdown.Item>
                  </Dropdown.Menu>
                </TemplateSelectorDropdown>
              </ContainerTemplateSize>
            </div>
            {filteredTemplates.map((item) => {
              return (
                <TemplateItem
                  category={category}
                  onDelete={reload}
                  item={item}
                  key={item.id}
                  webinar={webinar}
                  handleTemplateActivation={handleTemplateActivation}
                  setShowChangeNameModal={setShowChangeNameModal}
                  setEditingTemplate={setEditingTemplate}
                />
              );
            })}
          </TabContainer>
        )}
      </Tab>
    );
  };

  return (
    <div className="webinar-schedule main-content-container">
      <div className="d-flex flex-column bg-white">
        <div className="col-lg-10 col-md-11 col-sm-12 align-self-center mx-0 pt-4">
          <MainTitle>Select Page Templates</MainTitle>
          <br />
          <p className="font-weight-normal mb-3">
            Select then edit the content and appearance for registration,
            welcome and thank you pages. You can save your own template copies
            to use for future webinars.
          </p>
          <Tabs
            defaultActiveKey={TemplateType.REGISTRATION}
            id="template-tabs"
            activeKey={category}
            onSelect={(key) => {
              // @ts-ignore
              setCategory(key);
            }}
          >
            {templateTab(TemplateType.REGISTRATION, "Registration Page")}
            {templateTab(TemplateType.CONFIRMATION, "Welcome Page")}
            {templateTab(TemplateType.THANK_YOU, "Thank You Page")}
          </Tabs>
        </div>
      </div>
      <Modal show={showChangeNameModal} onHide={handleCloseEditingModal}>
        <Modal.Header closeButton className="pl-3">
          <ModalTitle>Rename custom template</ModalTitle>
        </Modal.Header>

        <div className="p-3">
          <p className="mb-1 font-weight-normal">Custom template name</p>
          <FormControl
            value={editingName}
            onChange={(event) => setEditingName(event.target.value)}
          />
        </div>

        <div className="p-3 d-flex justify-content-end">
          <Button
            variant="outline-secondary"
            onClick={handleCloseEditingModal}
            style={{ fontSize: "1rem" }}
          >
            Cancel
          </Button>
          <Button
            variant="primary"
            onClick={saveEdit}
            className="ml-1"
            style={{ fontSize: "1rem" }}
          >
            Save
          </Button>
        </div>
      </Modal>
    </div>
  );
};

export default Templates;
