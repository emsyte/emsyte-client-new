import { WebinarObject, WebinarOfferObject } from "@emsyte/common/webinar";
import "moment-timezone";
import React, { useCallback, useEffect, useRef, useState } from "react";
import {
  Col,
  Row,
  Table,
  Form,
  FormControl,
  Button,
  Card,
  Modal,
  Spinner,
} from "react-bootstrap";
import { apiWrapper } from "../../../utils/api_helpers";
import { durationToSeconds, secondsToDuration } from "../../../utils/duration";
import { formatCurrency, formatDuration } from "../../../utils/formatters";
import { useHttpService } from "../../../utils/HttpService";
import LoaderButton from "../../common/LoaderButton";
import ProductOffer from "../viewer/ProductOffer";
import colors from "../../../utils/colors";
import illustrationWebinar from "../../../images/illustration-webinar.svg";
import styled from "styled-components";
import Advanced from "./Advanced";
import TimePicker from "react-time-picker";

const { REACT_APP_EVENTS } = process.env;
function getRedirectUrl() {
  return REACT_APP_EVENTS + "/complete";
}

const CenteredContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
`;

const CustomTimePicker = styled(TimePicker)`
  .react-time-picker__wrapper {
    border: none;
  }

  input:focus-visible {
    border: none;
    outline: none;
  }
`;

const CustomHeader = styled(Card.Header)`
  padding: 1rem 1.25rem;
`;

const CustomModalHeader = styled(Card.Header)`
  padding-left: 1rem;
  padding-top: 1rem;
  padding-bottom: 1rem;
`;

const Title = styled.h6`
  margin: 0;
  text-transform: uppercase;
  color: #2e3338;
`;

const OffersBody = styled.div`
  margin: 0 1.25rem;
`;

const TableContainer = styled.div`
  margin-top: 2rem;
`;

const CustomModalBody = styled(Card.Header)`
  padding: 1rem 1.25rem 1rem 1rem;
`;

const OfferContainer = styled.div`
  background-color: #f5f5f5;
  width: fit-content;
  padding: 0.5rem 1rem 1rem 1rem;
`;

const NEW_OFFER: Omit<WebinarOfferObject, "id"> = {
  buttonText: "Get Offer",
  description:
    "Cat ipsum dolor sit amet, i’m so hungry i’m so hungry but ew not for that and have secret plans play with twist ties, hey! you there, with the hands.",
  name: "Offer title placeholder",
  checkout: "",
  end: 0,
  start: 0,
  price: 0,
  thumbnail: "",
};

interface MonetizeProps {
  webinar: WebinarObject;
  onWebinarUpdate: () => Promise<any>;
  onNext: () => void;
  saveRef: React.MutableRefObject<() => void>;
}

const Monetize: React.FC<MonetizeProps> = ({
  webinar,
  onNext,
  onWebinarUpdate,
  saveRef,
}) => {
  const { id: webinarId } = webinar;

  const api = useHttpService();
  const [isPaid, setIsPaid] = useState<boolean>(false);
  const [registrationFee, setRegistrationFee] = useState<any>();
  const [checkoutLink, setCheckoutLink] = useState<any>("");

  const saveRefForAdvanced = useRef(async () => {});
  const [showAddOfferModal, setShowAddOfferModal] = useState(false);
  const [showOfferPreviewModal, setShowOfferPreviewModal] = useState(false);
  const [offers, setOffers] = useState<WebinarOfferObject[]>([]);
  const [selectedOffer, setSelectedOffer] = useState<
    Omit<WebinarOfferObject, "id"> & { id?: number }
  >(NEW_OFFER);

  const [thumbnail, setThumbnail] = useState<any>();
  const [files, setFiles] = useState<any>([]);
  const [loadingOffer, setLoadingOffer] = useState(false);
  const [loadingOffers, setLoadingOffers] = useState(false);
  const [loadingWebinar, setLoadingWebinar] = useState(false);
  console.log(loadingWebinar);

  const addAnOffer = () => {
    setShowAddOfferModal(true);
    setThumbnail(undefined);
    setFiles([]);
    setSelectedOffer(NEW_OFFER);
  };

  const getOffers = useCallback(async () => {
    try {
      setLoadingOffers(true);
      const { data } = await api.doGetWebinarOffers(webinarId);
      setOffers(data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoadingOffers(false);
    }
  }, [webinarId, api]);

  const saveOffer = useCallback(
    () =>
      apiWrapper(async () => {
        setLoadingOffer(true);

        const thumbnail =
          files && files.length
            ? await api.doUploadFile("offer", files[0])
            : selectedOffer.thumbnail;

        await api.doCreateWebinarOffer("" + webinarId, {
          ...selectedOffer,
          thumbnail,
        } as any);
        await getOffers();
        setShowAddOfferModal(false);
      }, setLoadingOffer),
    [api, webinarId, selectedOffer, files, getOffers]
  );

  const saveWebinarConfiguration = () =>
    apiWrapper(async () => {
      await api.doUpdateWebinar("" + webinarId, {
        isPaid,
        registrationFee,
        checkoutLink,
      });
      await onWebinarUpdate();
    }, setLoadingWebinar);

  saveRef.current = async () => {
    await saveWebinarConfiguration();
    await saveRefForAdvanced.current();
    onNext();
  };

  const convertRegistrationFee = (fee) => fee / 100;

  const handleSelectOffer = (id: number) => {
    addAnOffer();
    const offer = offers.find((x) => x.id === id);
    if (offer) {
      setSelectedOffer(offer);
      setThumbnail(offer.thumbnail);
    }
  };

  const handlePreviewOffer = (id: number) => {
    setShowOfferPreviewModal(true);
    const offer = offers.find((x) => x.id === id);
    if (offer) {
      setSelectedOffer(offer);
      setThumbnail(offer.thumbnail);
    }
  };

  const handleOfferTime = (stringValue, field: "start" | "end") => {
    if (!selectedOffer || !stringValue) {
      return;
    }
    const [hours, minutes, seconds] = stringValue
      .split(":")
      .map((value) => +value);

    const value = durationToSeconds({ hours, minutes, seconds });

    if (isNaN(value) || value < 0) {
      return;
    }

    setSelectedOffer({
      ...selectedOffer,
      [field]: value,
    });
  };

  const handleDrop = (_files) => {
    let fileList = files;
    for (var i = 0; i < _files.length; i++) {
      if (!_files[i].name) return;
      fileList.length = 0;
      fileList.push(_files[i]);
      setThumbnail(URL.createObjectURL(_files[i]));
    }
    setFiles(fileList);
  };

  useEffect(() => {
    if (webinar.id) {
      const fetchData = async () => await getOffers();
      fetchData();
    }
    setIsPaid(webinar?.isPaid);
    setRegistrationFee(webinar?.registrationFee);
    setCheckoutLink(webinar?.checkoutLink || "");
  }, [getOffers, webinar]);

  const redirectLink = getRedirectUrl();

  const handleCopyLink = (text: string) => {
    navigator.clipboard.writeText(text);
  };

  return (
    <div className="main-content-container col-lg-9 align-self-center">
      <Card style={{ marginTop: "2rem" }}>
        <CustomHeader>
          <Title>Webinar Type</Title>
        </CustomHeader>
        <div style={{ padding: "0.25rem 1.25rem 1rem 1.25rem" }}>
          <div className="form-check">
            <input
              className="form-check-input"
              type="radio"
              name="type"
              id="free"
              value="free"
              onClick={() => setIsPaid(false)}
              checked={!isPaid}
            />
            <label
              style={{ fontWeight: 500, letterSpacing: "1.2px" }}
              htmlFor="free"
            >
              Free
            </label>
            <br />
            <span className="font-weight-normal">
              Allow attendees to view and participate in the webinar for free.
              You can provide attendees with in-session offers to monitize free
              webinars.
            </span>
          </div>
          <div className="form-check" style={{ marginTop: "2rem" }}>
            <input
              className="form-check-input"
              type="radio"
              name="type"
              id="paid"
              value="paid"
              onClick={() => setIsPaid(true)}
              checked={isPaid}
            />
            <label
              style={{ fontWeight: 500, letterSpacing: "1.2px" }}
              htmlFor="paid"
            >
              Paid
            </label>
            <br />
            <span className="font-weight-normal">
              Charge attendees a registration fee prior to being able to
              complete their registration. Attendees will be redirected to the
              payment processing service of your choice.
            </span>
          </div>
          {isPaid && (
            <Row className="my-3">
              <div style={{ marginLeft: "2.5rem" }}>
                <i className="bi bi-arrow-return-right fa-2x" />
              </div>
              <Col
                style={{
                  backgroundColor: colors.notWhite.toHex(),
                  marginLeft: "0.75rem",
                  padding: "0.625rem",
                }}
              >
                <Row>
                  <Col lg={8}>
                    <Form.Group controlId="videoUrl" className="m-0">
                      <span className="font-weight-normal d-block">
                        Registration fee (USD)
                      </span>
                      <FormControl
                        placeholder="Total Amount"
                        type="number"
                        min="0.01"
                        step="0.01"
                        max="2500"
                        value={convertRegistrationFee(registrationFee)}
                        onChange={(e: any) =>
                          setRegistrationFee((e.target.value *= 100))
                        }
                        className="mt-1"
                      />
                      <span className="font-weight-normal d-block mt-2">
                        Checkout link
                      </span>
                      <FormControl
                        placeholder="https://paypal.me/<name>/<amount>"
                        value={checkoutLink}
                        onChange={(e) => setCheckoutLink(e.target.value)}
                        className="mt-1"
                      />
                      <small className="d-block">
                        Destination URL for registration payment processing
                      </small>
                      <span className="font-weight-normal d-block mt-2">
                        Redirect link
                      </span>
                      <div className="d-flex mt-1">
                        <FormControl value={redirectLink} disabled={true} />
                        <Button
                          variant="link"
                          className="p-0"
                          style={{ fontSize: "1rem", width: "5rem" }}
                          onClick={() => handleCopyLink(redirectLink)}
                        >
                          Copy link
                        </Button>
                      </div>
                      <small>
                        Copy this link and set it as the redirect destination in
                        your payment processing service
                      </small>
                    </Form.Group>
                  </Col>
                </Row>
              </Col>
            </Row>
          )}
        </div>
      </Card>
      {/* Offer Column */}
      <br />
      <Card>
        <CustomHeader>
          <Title>Webinar Offers</Title>
        </CustomHeader>
        <OffersBody>
          <small className="d-block">
            Trigger offers to attendees at specific times during your webinar.
          </small>
          {!!offers.length && (
            <Button
              variant="primary"
              onClick={addAnOffer}
              className="mt-2"
              style={{ width: "fit-content" }}
            >
              Add an Offer
            </Button>
          )}

          <TableContainer>
            {loadingOffers ? (
              <CenteredContainer>
                <Spinner animation="border" />
              </CenteredContainer>
            ) : offers.length ? (
              <Table borderless style={{ color: "#2E3338" }}>
                <thead style={{ backgroundColor: "#E9ECEF" }}>
                  <tr>
                    <th className="py-0">
                      Offer Display{" "}
                      <i
                        className="bi bi-caret-down-fill"
                        style={{ fontSize: "0.875rem" }}
                      />
                    </th>
                    <th className="py-0">Title</th>
                    <th className="py-0">Price</th>
                    <th className="py-0">Checkout Link</th>
                    <th className="py-0">Actions</th>
                  </tr>
                </thead>
                <tbody style={{ fontWeight: 400 }}>
                  {offers
                    .sort((a, b) => a.start - b.start)
                    .map((item, idx) => (
                      <tr key={idx} className="border-bottom">
                        <td>{formatDuration(secondsToDuration(item.start))}</td>
                        <td>{item.name}</td>
                        <td>{formatCurrency(item.price)}</td>
                        <td
                          style={{
                            overflow: "hidden",
                            whiteSpace: "nowrap",
                            textOverflow: "ellipsis",
                            maxWidth: "12vw",
                          }}
                        >
                          <a
                            href={item.checkout}
                            target="_blank"
                            rel="noopener noreferrer"
                          >
                            {item.checkout}
                          </a>
                        </td>
                        <td>
                          <Button
                            onClick={() => handleSelectOffer(item.id)}
                            variant="link"
                            style={{ fontSize: "1rem", borderRadius: 0 }}
                            className="py-0 pl-0 border-right"
                          >
                            Edit
                          </Button>
                          <Button
                            onClick={() => handlePreviewOffer(item.id)}
                            variant="link"
                            style={{ fontSize: "1rem" }}
                            className="py-0 pr-0"
                          >
                            Preview
                          </Button>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </Table>
            ) : (
              <CenteredContainer>
                <img src={illustrationWebinar} alt="Webinar Illustration" />
                <span>You don’t have any webinar offers yet.</span>
                <Button variant="primary" onClick={addAnOffer} className="mt-2">
                  Add an Offer
                </Button>
              </CenteredContainer>
            )}
          </TableContainer>
        </OffersBody>
      </Card>

      <Modal
        show={showAddOfferModal}
        onHide={() => setShowAddOfferModal(false)}
        size="lg"
      >
        <CustomModalHeader closeButton>Add an offer</CustomModalHeader>
        <CustomModalBody>
          <div className="row m-0">
            <div
              className="d-flex flex-column col-lg-6 col-md-12 col-sm-12"
              style={{ fontWeight: 400 }}
            >
              <div className="d-flex row">
                <div className="col-6">
                  <span>Start offer display</span>
                  <CustomTimePicker
                    format="HH:mm:ss"
                    maxDetail="second"
                    disableClock={true}
                    hourPlaceholder="hh"
                    minutePlaceholder="mm"
                    secondPlaceholder="ss"
                    clearIcon={null}
                    className="form-control"
                    onChange={(value) => {
                      handleOfferTime(value, "start");
                    }}
                    value={`${secondsToDuration(selectedOffer.start).hours}:${
                      secondsToDuration(selectedOffer.start).minutes
                    }:${secondsToDuration(selectedOffer.start).seconds}`}
                  />
                </div>
                <div className="col-6">
                  <span>End offer display</span>
                  <CustomTimePicker
                    format="HH:mm:ss"
                    maxDetail="second"
                    disableClock={true}
                    hourPlaceholder="hh"
                    minutePlaceholder="mm"
                    secondPlaceholder="ss"
                    clearIcon={null}
                    className="form-control"
                    onChange={(value) => {
                      handleOfferTime(value, "end");
                    }}
                    value={`${secondsToDuration(selectedOffer.end).hours}:${
                      secondsToDuration(selectedOffer.end).minutes
                    }:${secondsToDuration(selectedOffer.end).seconds}`}
                  />
                </div>
              </div>
              <div className="mt-3">
                <span className="d-block">Offer title</span>
                <FormControl
                  placeholder="My Product"
                  value={selectedOffer.name ?? ""}
                  onChange={(e) =>
                    setSelectedOffer({
                      ...selectedOffer,
                      name: e.target.value,
                    })
                  }
                />
              </div>
              <div className="mt-3">
                <span className="d-block">Offer description</span>
                <FormControl
                  as="textarea"
                  rows={3}
                  placeholder="Describe your product"
                  value={selectedOffer.description || ""}
                  onChange={(e) =>
                    setSelectedOffer({
                      ...selectedOffer,
                      description: e.target.value,
                    })
                  }
                />
              </div>
              <div className="mt-3">
                <span>Offer price</span>
                <br />
                <FormControl
                  type="number"
                  min="0.01"
                  step="0.01"
                  placeholder="Item Cost"
                  value={selectedOffer.price / 100}
                  onChange={(e: any) =>
                    setSelectedOffer({
                      ...selectedOffer,
                      price: e.target.value * 100,
                    })
                  }
                  style={{ maxWidth: "10rem" }}
                />
              </div>
              <div className="mt-3">
                <span>Button Text</span>
                <br />
                <FormControl
                  placeholder="Buy Now"
                  value={selectedOffer.buttonText || ""}
                  onChange={(e) =>
                    setSelectedOffer({
                      ...selectedOffer,
                      buttonText: e.target.value,
                    })
                  }
                />
              </div>
              <div className="mt-3">
                <span>Offer checkout link</span>
                <br />
                <FormControl
                  placeholder="https://"
                  type="url"
                  value={selectedOffer.checkout || ""}
                  onChange={(e) =>
                    setSelectedOffer({
                      ...selectedOffer,
                      checkout: e.target.value,
                    })
                  }
                />
              </div>
            </div>
            <div className="d-flex justify-content-center col-lg-6 col-md-12 col-sm-12 mt-3">
              <OfferContainer>
                <span style={{ letterSpacing: "1.2px" }}>Offer Preview</span>
                <ProductOffer
                  offer={
                    {
                      ...selectedOffer,
                      thumbnail: thumbnail,
                    } as any
                  }
                  handleDrop={handleDrop}
                />
              </OfferContainer>
            </div>
          </div>
          <div className="d-flex justify-content-end mt-4">
            <Button
              variant="outline-secondary"
              onClick={() => setShowAddOfferModal(false)}
            >
              Cancel
            </Button>
            <LoaderButton
              loading={loadingOffer}
              onClick={() => saveOffer()}
              className="ml-2"
            >
              Add Offer
            </LoaderButton>
          </div>
        </CustomModalBody>
      </Modal>

      <Modal
        show={showOfferPreviewModal}
        onHide={() => setShowOfferPreviewModal(false)}
      >
        <CustomModalHeader closeButton>Offer Preview</CustomModalHeader>
        <div className="d-flex justify-content-center p-3">
          <OfferContainer>
            <ProductOffer
              offer={
                {
                  ...selectedOffer,
                  thumbnail: thumbnail,
                } as any
              }
              handleDrop={() => {}}
              previewOnly={true}
            />
          </OfferContainer>
        </div>
        <Modal.Footer>
          <Button
            onClick={() => setShowOfferPreviewModal(false)}
            variant="secondary"
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>

      <Advanced
        webinar={webinar}
        onWebinarUpdate={onWebinarUpdate}
        saveRef={saveRefForAdvanced}
      />
    </div>
  );
};

export default Monetize;
