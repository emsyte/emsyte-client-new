import { faPaperPlane } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import styled from "styled-components";

const FormWrapper = styled.form`
  padding: 0.5rem;
  border-top: 1px solid rgba(46, 51, 56, 0.15);
  width: 100%;

  .message-input {
    position: relative;
    width: 100%;

    input {
      padding-right: 2rem;
    }
    
    .paper-plane {
      padding: 0 1rem;
      position: absolute;
      right: 0;
      top: 0;
      color: #868E96;
    }
  }

  .footer {
    margin-top: 0.25rem;
    display: flex;
    align-items: center;
    justify-content: space-between;
    font-size: 0.8rem;
    font-weight: 400;

    .checkbox {
      display: flex, 
      align-items: center;
      color: #4d4d4d;
  
      label {
        margin: 0;
      }
      input {
        margin-right: 2px;
      }
    }

    .subtitle {
      color: #868e96;
    }
  }
`;

export const Editor = ({
  sendMessage,
  sendTo,
}: {
  sendMessage(payload: object): void;
  sendTo: string;
}) => {
  const [content, setContent] = useState("");
  const [question, setQuestion] = useState(false);

  const onSubmit = (e) => {
    e.preventDefault();
    try {
      sendMessage({ content, question, sendTo });
      setContent("");
      setQuestion(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <FormWrapper onSubmit={onSubmit}>
      <div className="message-input">
        <input
          className="form-control"
          placeholder="Write a message..."
          name="chat-message"
          onChange={(e) => setContent(e.target.value)}
          value={content}
        />
        <button className="btn btn-link paper-plane" type="submit">
          <FontAwesomeIcon icon={faPaperPlane} />
        </button>
      </div>
      <div className="footer">
        <span className="checkbox">
          <input
            id="send_as_question"
            name="question"
            type="checkbox"
            checked={question}
            onChange={() => setQuestion((prev) => !prev)}
          />
          <label htmlFor="send_as_question">Send as a question</label>
        </span>
        <span className="subtitle">press ENTER to send</span>
      </div>
    </FormWrapper>
  );
};
