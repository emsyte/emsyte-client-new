import { EventObject, WebinarObject } from "@emsyte/common/webinar";
import React from "react";
import { SidebarTabs } from "./SidebarTabs";

interface SidebarContainerProps {
  webinar: WebinarObject;
  event: EventObject;
  showSideBar: boolean;
  onSetShowSidebar: (showSideBar: boolean) => void;
}

export const SidebarContainer: React.FC<SidebarContainerProps> = ({
  ...props
}) => {
  return (
    <div className="webinar-viewer-sidebar w-100 h-100">
      <SidebarTabs {...props} />
    </div>
  );
};
