import { EventObject, WebinarOfferObject } from "@emsyte/common/webinar";
import React, { useEffect, useState } from "react";
import { Badge, Button } from "shards-react";
import styled from "styled-components";
import { useViewerAPI } from "../../../../../utils/viewerAPI";
import ProductOffer from "../../ProductOffer";
import { useEventTime } from "../../video/hooks/time";
import { EmptyPanelContentPromotions } from "./empty-contents";

const PromotionPopupWrapper = styled.div`
  position: absolute;
  right: 0;
  bottom: 20px;
  #overlay {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(0, 0, 0, 0.1);
    z-index: 1001;
    visibility: hidden;
  }
  .viewFilter {
    z-index: 4;
  }
  #toggleFilter {
    display: none;
    visibility: hidden;
  }
  #viewContainerFilter {
    height: auto;
    width: auto;
    max-height: 0px;
    max-width: 0px;
    position: absolute;

    border-radius: 2px;
    z-index: -1;
    margin-left: 15px;
    overflow: hidden;
    transition: max-width 0.5s, max-height 0.5s;
    transform: translate(-0%, -100%);
    position: absolute;
    top: -10px;
    right: 30px;
    .viewEmpty {
      background: #fff;
      height: 100%;
      padding: 15px;
    }

    .viewContentFilter {
      min-height: 50px;
      min-width: 300px;
    }
  }
  #toggleFilter:checked ~ #viewContainerFilter {
    max-width: 360px;
    max-height: 100vh;
    z-index: 5;
  }
  #toggleFilter:checked ~ #overlay {
    z-index: 4;
    visibility: visible;
  }
`;

interface PromotionPopupProp {
  event: EventObject;
}

export const PromotionPopup: React.FC<PromotionPopupProp> = ({ event }) => {
  const [isShowPromotion, setIsShowPromotion] = useState(false);
  const time = useEventTime();
  const api = useViewerAPI();

  const [allOffers, setAllOffers] = useState<WebinarOfferObject[]>([]);

  const offers = allOffers.filter(
    (offer) => offer.start < time && (offer.end === 0 || offer.end > time)
  );

  useEffect(() => {
    api.doGetViewerOffers(event.id).then((offers) => setAllOffers(offers));
  }, [api, event]);

  return (
    <PromotionPopupWrapper className="sidebar__tabs__content__empty text-center px-4 chat-box">
      <div className="viewFilter">
        <input
          id="toggleFilter"
          type="checkbox"
          checked={isShowPromotion}
          onChange={() => null}
        />
        {offers.length !== 0 && (
          <Button
            pill
            onClick={() => setIsShowPromotion(!isShowPromotion)}
            className="buttonFilter"
            theme={isShowPromotion ? "light" : "primary"}
          >
            {isShowPromotion && <i className="fas fa-times"></i>}
            {isShowPromotion ? " Close" : "Promotion "}
            {!isShowPromotion && offers.length > 0 && (
              <Badge theme="light">{offers.length}</Badge>
            )}
          </Button>
        )}
        <div id="viewContainerFilter">
          <div className="viewContentFilter">
            {offers.length === 0 ? (
              <div className="viewEmpty">
                <EmptyPanelContentPromotions />
              </div>
            ) : (
              offers.map((offer) => (
                <ProductOffer
                  key={offer.id}
                  offer={offer}
                  onOfferClick={() => api.doTrackOfferClick(event.id, offer.id)}
                ></ProductOffer>
              ))
            )}
          </div>
        </div>
      </div>
    </PromotionPopupWrapper>
  );
};
