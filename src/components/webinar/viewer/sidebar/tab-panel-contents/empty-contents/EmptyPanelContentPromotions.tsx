import React from "react";

export const EmptyPanelContentPromotions: React.FC = () => {
  return (
    <div className="sidebar__tabs__content__empty text-center px-4">
      <div className="sidebar__tabs__content__empty__title mt-1 mb-2 font-weight-bolder">
        Noting here yet
      </div>
    </div>
  );
};
