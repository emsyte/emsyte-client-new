import React from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCommentAlt} from "@fortawesome/free-solid-svg-icons";

export const EmptyPanelContentChat: React.FC = () => {
    return (
        <div className="sidebar__tabs__content__empty text-center p-4">
            <div className="sidebar__tabs__content__empty__icon">
                <FontAwesomeIcon icon={faCommentAlt}/>
            </div>
            <div className="sidebar__tabs__content__empty__title mt-1 mb-2 font-weight-bolder">
                No messages yet
            </div>
            <div className="sidebar__tabs__content__empty__subtitle">
                Here you can send chat messages. They will be visible to all attendees.
            </div>
        </div>
    );
};