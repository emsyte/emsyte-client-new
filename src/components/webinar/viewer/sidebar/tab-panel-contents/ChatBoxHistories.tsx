import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { EmptyPanelContentChat } from "./empty-contents";
import { ChatEvent } from "../../../chat/ChatEvent";
import LoaderButton from "../../../../common/LoaderButton";

const ChatBoxWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  overflow-y: auto;

  .chatbox {
    padding: 0 0.5rem;
    display: flex;
    flex-direction: column-reverse;
    overflow-y: auto;
  }
`;

interface ChatBoxHistoriesProps {
  admin: boolean;
  chats: {};
  currentRoom: string;
  removeMessage: (messageId: string) => void;
  requestHistory: (roomId: string) => void;
  isLoadingHistory: boolean;
  isAtBeginning: boolean;
}

export const ChatBoxHistories: React.FC<ChatBoxHistoriesProps> = ({
  admin,
  chats,
  currentRoom,
  removeMessage,
  requestHistory,
  isLoadingHistory,
  isAtBeginning,
}) => {
  const messages = chats[currentRoom]?.messages ?? [];

  const [loading, setLoading] = useState<string[]>([]);

  useEffect(() => {
    if (
      !Object.keys(chats).includes(currentRoom) &&
      !loading.includes(currentRoom)
    ) {
      setLoading((l) => l.concat(currentRoom));

      setTimeout(() => {
        setLoading((l) => l.filter((c) => c !== currentRoom));
      }, 1000);

      requestHistory(currentRoom);
    }
  }, [currentRoom, requestHistory, chats, loading]);

  return (
    <ChatBoxWrapper>
      {messages.length > 0 ? (
        <div className="chatbox sidebar__tabs__content__empty text-center">
          {messages.map((message) => (
            <ChatEvent
              admin={admin}
              key={message.messageId}
              message={message}
              removeMessage={removeMessage}
            />
          ))}
          <div style={{ padding: "1rem" }}>
            {isAtBeginning ? (
              <span className="text-muted">No more messages</span>
            ) : (
              <LoaderButton
                loading={isLoadingHistory}
                variant="link"
                onClick={() => requestHistory(currentRoom)}
              >
                Load more
              </LoaderButton>
            )}
          </div>
        </div>
      ) : (
        <EmptyPanelContentChat />
      )}
    </ChatBoxWrapper>
  );
};
