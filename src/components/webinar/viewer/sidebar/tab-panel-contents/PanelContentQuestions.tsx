import React, { ReactText } from "react";
import { EmptyPanelContentQuestions } from "./empty-contents";
import { Editor } from "../editor";

export const PanelContentQuestions: React.FC = () => {
  return (
    <div className="sidebar__tabs__content d-flex flex-column w-100">
      <div className="sidebar__tabs__content__body">
        <EmptyPanelContentQuestions />
      </div>
      <div className="sidebar__tabs__content__footer w-100">
        <Editor sendMessage={() => {}} sendTo={"general"} />
      </div>
    </div>
  );
};
