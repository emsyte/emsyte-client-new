import { AuthAttendeeObject, AuthType } from "@emsyte/common/auth";
// import Toggler from "../../../Toggler";
import { EventObject, WebinarObject } from "@emsyte/common/webinar";
import { faChevronDown, faChevronUp } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classNames from "classnames";
import jwt from "jsonwebtoken";
import React, { useEffect, useState } from "react";
import styled from "styled-components";
import { useAuth } from "../../../../utils/Authentication";
import {
  getHostDMRoom,
  useChat,
} from "../../../../utils/SocketService/useChat";
import { Editor } from "./editor";
import { ChatBoxHistories } from "./tab-panel-contents/ChatBoxHistories";
interface SidebarTabsProps {
  webinar: WebinarObject;
  event: EventObject;
  showSideBar: boolean;
  onSetShowSidebar: (showSideBar: boolean) => void;
}

const SidebarTabsWrapper = styled.div`
  background-color: #f0f0f0;
  display: flex;
  flex-direction: column;

  .theme-selector {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    min-height: 2rem;
    padding: 0 1rem;
    font-size: 0.9rem;
    font-weight: 400;
  }

  .tab-buttons {
    display: flex;
    align-items: center;
    min-height: 2.5rem;
    padding: 0 1rem;
    font-size: 1rem;
    box-shadow: 0px 0px 2px inset rgba(0, 0, 0, 0.18);
  }

  button {
    background: none;
    border: none;
    font-weight: 600;
    border-radius: 0;
    padding: 0 1.5rem;
    height: 100%;

    &.active {
      font-weight: 700;
      border-bottom: 2px solid #007bff;
      border-radius: 0;
    }

    &.has-notification::after {
      position: absolute;
      content: " ";
      height: 4px;
      width: 4px;
      border-radius: 1rem;
      background-color: #d72e3d;
    }
  }
`;

export const SidebarTabs: React.FC<SidebarTabsProps> = ({
  event,
  showSideBar,
  onSetShowSidebar,
}) => {
  const { getToken } = useAuth();
  const {
    sendMessage,
    removeMessage,
    chats,
    requestHistory,
    isLoadingHistory,
    isAtBeginning,
  } = useChat(event);

  const [currentRoom, setCurrentRoom] = useState<"general" | string>("general");

  const [attendeeId, setAttendeeId] = useState<number | null>(null);

  useEffect(() => {
    const attendee = jwt.decode(getToken()) as AuthAttendeeObject;
    if (attendee.type === AuthType.ATTENDEE) {
      setAttendeeId(attendee.id);
    } else {
      setAttendeeId(null);
    }
  }, [getToken, setAttendeeId]);

  const newGeneral = false;
  const newHost = false;

  return (
    <SidebarTabsWrapper className="sidebar__tabs h-100 w-100">
      {/* <div className="theme-selector">
        Night mode{" "}
        <Toggler
          className="ml-2"
          onChange={(v) => console.log("switch theme", v)}
        />
      </div> */}
      <div className="tab-buttons">
        <button
          className={classNames(
            currentRoom === "general" && "active",
            newGeneral && "has-notification"
          )}
          onClick={() => setCurrentRoom("general")}
        >
          Discussion
        </button>

        {attendeeId !== null && (
          <button
            className={classNames(
              currentRoom === getHostDMRoom(attendeeId) && "active",
              newHost && "has-notification"
            )}
            onClick={() =>
              attendeeId && setCurrentRoom(getHostDMRoom(attendeeId))
            }
          >
            Host
          </button>
        )}

        <button
          onClick={() => onSetShowSidebar(!showSideBar)}
          className="ml-auto d-block d-sm-none"
          title={showSideBar ? "Hide Chat" : "Show Chat"}
          aria-label={showSideBar ? "Hide Chat" : "Show Chat"}
          data-testid="btn-show-chat-mobile"
        >
          {showSideBar ? (
            <FontAwesomeIcon icon={faChevronDown} />
          ) : (
            <FontAwesomeIcon icon={faChevronUp} />
          )}
        </button>
      </div>
      <ChatBoxHistories
        admin={false}
        chats={chats}
        currentRoom={currentRoom}
        removeMessage={removeMessage}
        requestHistory={requestHistory}
        isLoadingHistory={isLoadingHistory}
        isAtBeginning={isAtBeginning}
      />
      <Editor sendMessage={sendMessage} sendTo={currentRoom} />
    </SidebarTabsWrapper>
  );
};
