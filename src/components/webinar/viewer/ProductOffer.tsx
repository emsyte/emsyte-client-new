import { WebinarOfferObject } from "@emsyte/common/webinar";
import React from "react";
import { Button, Card } from "react-bootstrap";
import styled from "styled-components";
import illustrationImg from "../../../images/illustration-img.svg";
import colors from "../../../utils/colors";
import DragAndDrop from "../upload/DragAndDrop";

const ProductOfferStyle = styled(Card)`  
  padding: 0.625rem 1rem;
  border-radius: 0.25rem;
  text-align: center;
  color: #2E3338;
  .card-img {
    border-radius: 0.25rem;
    height: 150px;
    width: 250px;
    background: #d1d1d1;
  }
`;

export interface ProductOfferProps {
  offer: WebinarOfferObject;
  onOfferClick?: () => void;
  handleDrop?: (files: any) => void
  previewOnly?: boolean // It means no drop to update the image
}

const ProductOffer: React.FC<ProductOfferProps> = ({ offer, onOfferClick, handleDrop = () => {}, previewOnly = false }) => {
  console.log(offer.thumbnail)
  return (
    <ProductOfferStyle>
      <DragAndDrop handleDrop={handleDrop}>
      {offer.thumbnail ?
        <Card.Img src={offer.thumbnail}/> :
        <div style={{width: '15.5rem', height: '9.25rem', border: '1px dashed ' + colors.gray600.toHex(), borderRadius: '0.25rem'}}>
          <img src={illustrationImg} alt="illustration img" />
          {!previewOnly &&
          <span className="d-block" style={{fontWeight: 400}}>Drag an image to upload or
            <Button variant="link" style={{fontSize: '1rem'}}>browse images</Button>
          </span>}
        </div>
      }
      </DragAndDrop>
      <div>
        <span className="d-block mt-3" style={{letterSpacing: '1.2px', maxWidth: '15.625rem', fontSize: '1.25rem'}}>{offer.name}</span>
        <span className="d-block mt-3" style={{fontWeight: 400, maxWidth: '15.625'}}>{offer.description}</span>
        <Button
          className="mt-3"
          as="a"
          block
          href={offer.checkout}
          target="_blank"
          rel="noopener noreferrer"
          onClick={onOfferClick}
        >
          {offer.buttonText}
        </Button>
      </div>
    </ProductOfferStyle>
  );
};

export default ProductOffer;
