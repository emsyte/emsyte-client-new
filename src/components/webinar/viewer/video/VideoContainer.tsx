import { EventObject, WebinarObject } from "@emsyte/common/webinar";
import React, { useContext } from "react";
import { useAuth } from "../../../../utils/Authentication";
import { useRecordReaction } from "./hooks/reactions";
import { EventTimeContext } from "./hooks/time";
import { useInterval } from "./hooks/utils";
import { VideoPlayer } from "./VideoPlayer";
import { useViewerAPI } from "../../../../utils/viewerAPI";

interface VideoContainerProps {
  webinar: Omit<WebinarObject, "videoUrl">;
  event: EventObject;
  onSetShowSidebar?: (showSideBar: boolean) => void;
  showSideBar?: boolean;
}

export const VideoContainer: React.FC<VideoContainerProps> = ({
  webinar,
  event,
}) => {
  const { time, isLive } = useContext(EventTimeContext);

  const { authUser } = useAuth();
  const api = useViewerAPI();

  // TODO:
  //   - When the video is finished, upload a final video
  //   - Enabled should come from the webinar settings and/or team plan features.
  //     Also, it should be disabled if a user is viewing the webinar
  const enabled = authUser ? false : true;
  useRecordReaction(event.id, time, isLive && enabled);

  // Track event every 2 minutes
  useInterval(() => {
    if (enabled) {
      api.doTrackHeartbeat(event.id, time);
    }
  }, 2 * 60 * 1000);

  return <VideoPlayer webinar={webinar} event={event} />;
};
