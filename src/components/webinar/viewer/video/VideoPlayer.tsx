import React, { useContext } from "react";
import { VideoBody, VideoHeader, VideoReplayBody } from "./components";
import { WebinarObject, EventObject } from "@emsyte/common/webinar";
import styled from "styled-components";
import { EventTimeContext } from "./hooks/time";

const VideoPlayerWrapper = styled.div`
  position: relative;
`;

interface VideoPlayerProps {
  webinar: Omit<WebinarObject, "videoUrl">;
  event: EventObject;
}

export const VideoPlayer: React.FC<VideoPlayerProps> = ({ webinar, event }) => {
  const { isLive } = useContext(EventTimeContext);

  return (
    <VideoPlayerWrapper className="webinar-viewer-video h-100 w-100 d-flex flex-column">
      <VideoHeader webinar={webinar} event={event} />
      {isLive ? (
        <VideoBody webinar={webinar} event={event} />
      ) : (
        <VideoReplayBody webinar={webinar} event={event} />
      )}
    </VideoPlayerWrapper>
  );
};
