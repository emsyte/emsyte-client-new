import { EventObject, WebinarObject } from "@emsyte/common/webinar";
import React, { useContext } from "react";
import ReactPlayer from "react-player";
import { EventTimeContext } from "../hooks/time";

interface VideoReplayBodyProps {
  webinar: Omit<WebinarObject, "videoUrl">;
  event: EventObject;
}

export const VideoReplayBody: React.FC<VideoReplayBodyProps> = ({
  webinar,
  event,
}) => {
  const eventTime = useContext(EventTimeContext);
  return (
    <ReactPlayer
      url={event.videoUrl}
      controls={true}
      onProgress={(state) => {
        if (eventTime.isLive) {
          return;
        }

        eventTime.setTime(state.playedSeconds);
      }}
      width={"100%"}
      height={"100%"}
    ></ReactPlayer>
  );
};
