import React, { useContext } from "react";
import Badge from "react-bootstrap/Badge";
import { WebinarObject, EventObject } from "@emsyte/common/webinar";
import moment from "moment";
import { EventTimeContext } from "../hooks/time";

interface VideoHeaderTitleProps {
  webinar: Omit<WebinarObject, "videoUrl">;
  event: EventObject;
}

export const VideoHeaderTitle: React.FC<VideoHeaderTitleProps> = ({
  webinar,
  event,
}) => {
  const { isLive } = useContext(EventTimeContext);
  return (
    <div className="video__header__left__title">
      <div className="video__header__left__title__main-title d-flex align-items-center">
        <span className="pr-2 text-white font-weight-bold">{webinar.name}</span>
        {isLive && (
          <Badge variant={"danger"} pill className="text-uppercase">
            Live
          </Badge>
        )}
      </div>
      <div className="video__header__left__title__sub-title">
        <p>{`${moment(event.start).format(
          "dddd, Do MMMM YYYY [at] h:mm a"
        )}`}</p>
      </div>
    </div>
  );
};
