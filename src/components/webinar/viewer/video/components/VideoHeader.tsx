import React from "react";
import { VideoHeaderButtons } from "./VideoHeaderButtons";
import { VideoHeaderTitle } from "./VideoHeaderTitle";
import { WebinarObject, EventObject } from "@emsyte/common/webinar";

interface VideoHeaderProps {
  webinar: Omit<WebinarObject, "videoUrl">;
  event: EventObject;
}

export const VideoHeader: React.FC<VideoHeaderProps> = ({ webinar, event }) => {
  return (
    <div className="video__header w-100 d-flex p-2">
      <div className="video__header__left d-flex justify-content-start">
        <VideoHeaderTitle webinar={webinar} event={event} />
      </div>
      <div className="video__header__right d-flex justify-content-end">
        <VideoHeaderButtons />
      </div>
    </div>
  );
};
