import { faCompressAlt, faExpandAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";
import { Button } from "react-bootstrap";

interface VideoHeaderButtonsProps {}

export const VideoHeaderButtons: React.FC<VideoHeaderButtonsProps> = () => {
  // const { isLive } = useContext(EventTimeContext);
  const [isFullscreen, setIsFullscreen] = useState(false);

  const toggleFullScreen = () => {
    const doc = window.document as any;
    const docEl = doc.documentElement as any;

    const requestFullScreen =
      docEl.requestFullscreen ||
      docEl.mozRequestFullScreen ||
      docEl.webkitRequestFullScreen ||
      docEl.msRequestFullscreen;
    const cancelFullScreen =
      doc.exitFullscreen ||
      doc.mozCancelFullScreen ||
      doc.webkitExitFullscreen ||
      doc.msExitFullscreen;

    if (
      !doc.fullscreenElement &&
      !doc.mozFullScreenElement &&
      !doc.webkitFullscreenElement &&
      !doc.msFullscreenElement
    ) {
      requestFullScreen.call(docEl);
      setIsFullscreen(true);
    } else {
      cancelFullScreen.call(doc);
      setIsFullscreen(false);
    }
  };

  return (
    <div className="video__header__right__buttons">
      {/* <Button className="border-0 mr-2" variant={"outline-light"}>
        <FontAwesomeIcon icon={faBell} />
      </Button> */}
      <Button
        data-testid="btn-fullscreen"
        className="border-0 mr-4"
        variant={"outline-light"}
        onClick={toggleFullScreen}
      >
        {isFullscreen ? (
          <FontAwesomeIcon icon={faCompressAlt} />
        ) : (
          <FontAwesomeIcon icon={faExpandAlt} />
        )}
      </Button>
      {/* <Button className="border-0 mr-2" variant={"outline-light"}>
        <FontAwesomeIcon icon={faQuestionCircle} /> Help
      </Button>
      
      {isLive && ( /*TODO: Should I look at this variable?
        <Button
          data-testid="btn-show-chat"
          className="border-0 mr-2 d-none d-md-inline"
          variant={"outline-light"}
        >
        </Button>
      )*/}
    </div>
  );
};
