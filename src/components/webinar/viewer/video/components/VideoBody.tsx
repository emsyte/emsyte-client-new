import {
  EventObject,
  WebinarObject,
  WebinarType,
} from "@emsyte/common/webinar";
import React, { useState } from "react";
import { Fade, Modal } from "react-bootstrap";
import ReactPlayer from "react-player";
import styled from "styled-components";
import { getTimeSinceEventStart } from "../hooks/time";
import { useInterval } from "../hooks/utils";

const Wrapper = styled.div`
  position: relative;
  display: flex;
  width: 100%;
  height: 100%;
  justify-content: center;
  align-items: center;
  overflow: hidden;
`;

const EndOverlay = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  background-color: #333;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const Player = styled.div`
  position: absolute;
  width: 100%;
  top: -200px;
  bottom: -200px;
`;

const Overlay = styled.div`
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
`;

interface VideoBodyProps {
  webinar: Omit<WebinarObject, "videoUrl">;
  event: EventObject;
}

const THRESHOLD = 1;

export const VideoBody: React.FC<VideoBodyProps> = ({ webinar, event }) => {
  const [reactPlayer, setReactPlayer] = useState<ReactPlayer | null>();

  const [ended, setEnded] = useState(false);
  const handleVideoComplete = () => {
    setEnded(true);

    setTimeout(() => {
      (window as any).location = webinar.settings.postViewUrl;
    }, 1000);
  };

  useInterval(() => {
    if (!reactPlayer) {
      return;
    }

    if (webinar.type !== WebinarType.RECORDED) {
      return;
    }

    const syncError =
      getTimeSinceEventStart(event) - reactPlayer?.getCurrentTime();

    if (Math.abs(syncError) > THRESHOLD) {
      console.log("Fixing drift");
      reactPlayer.seekTo(getTimeSinceEventStart(event), "seconds");
    }
  }, 1000);

  const setIFrameTabIndex = (iframeContainer: HTMLDivElement | null) => {
    if (!iframeContainer) {
      return;
    }

    const iframe = iframeContainer.querySelector("iframe");

    if (!iframe) {
      return;
    }
    iframe.tabIndex = -1;
  };

  return (
    <Wrapper>
      <Player ref={setIFrameTabIndex}>
        <ReactPlayer
          url={event.videoUrl}
          onEnded={handleVideoComplete}
          controls={false}
          autoPlay
          playing={true}
          ref={setReactPlayer}
          width={"100%"}
          height={"100%"}
        ></ReactPlayer>
      </Player>
      <Overlay></Overlay>
      <Fade in={ended}>
        <EndOverlay>
          <Modal.Dialog data-testid="end-notice">
            <Modal.Body>
              <h5>The webinar has ended</h5>
            </Modal.Body>
          </Modal.Dialog>
        </EndOverlay>
      </Fade>
    </Wrapper>
  );
};
