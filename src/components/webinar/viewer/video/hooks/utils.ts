import { useEffect, useRef } from "react";

export function useInterval(callback: Function, period: number) {
  const savedCallback = useRef<Function>();

  useEffect(() => {
    savedCallback.current = callback;
  }, [callback]);

  useEffect(() => {
    function tick() {
      if (savedCallback.current) {
        savedCallback.current();
      }
    }

    const id = setInterval(tick, period);
    return () => clearInterval(id);
  }, [savedCallback, period]);
}
