import axios from "axios";
import { useCallback, useEffect, useState } from "react";
import RecordRTC from "recordrtc";
import { useHttpService } from "../../../../../utils/HttpService";
import { useInterval } from "./utils";

// Upload every two minutes
const UPLOAD_INTERVAL = 1000 * 10;

async function getReactionFile(recorder: RecordRTC, keepRecording: boolean) {
  await new Promise((cb) => recorder.stopRecording(() => cb(null)));

  // get recorded blob
  const blob = recorder.getBlob();
  // we need to upload "File" --- not "Blob"
  const fileObject = new File([blob], "video", {
    type: "video/webm",
  });

  if (keepRecording) {
    recorder.startRecording();
  }

  return fileObject;
}

export function useRecordReaction(
  eventId: number,
  time: number,
  enabled: boolean
) {
  const [recorder, setRecorder] = useState<RecordRTC | null>(null);
  const [chunkOffset, setChunkOffset] = useState(0);

  const api = useHttpService();

  const saveReaction = useCallback(
    async (offset: number, keepRecording: boolean) => {
      if (!recorder) {
        return;
      }

      const fileObject = await getReactionFile(recorder, keepRecording);
      const uploadUrl = await api.doGetUploadUrl(eventId, offset);
      await axios.put(uploadUrl, fileObject, {
        headers: { "Content-Type": "video/webm" },
      });
    },
    [api, recorder, eventId]
  );

  useEffect(() => {
    if (!enabled) {
      return;
    }

    navigator.mediaDevices
      .getUserMedia({
        video: true,
        audio: false,
      })
      .then((camera) => {
        const recorder = new RecordRTC(camera, {
          type: "video",
        });
        recorder.startRecording();
        setRecorder(recorder);
      })
      .catch((err) => console.log("getUserMedia error: " + err));
  }, [enabled]);

  useInterval(() => {
    if (!enabled || !recorder) {
      return;
    }

    setChunkOffset(time);
    saveReaction(chunkOffset, true);
  }, UPLOAD_INTERVAL);

  return () => {
    if (!enabled || !recorder) {
      return;
    }

    saveReaction(chunkOffset, false);
  };
}
