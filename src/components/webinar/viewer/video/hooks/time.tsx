import { EventObject } from "@emsyte/common/webinar";
import moment from "moment";
import {
  useState,
  useCallback,
  useContext,
  useRef,
  useEffect,
  useMemo,
} from "react";
import { useInterval } from "./utils";
import React from "react";

type EventTimeContextValue =
  | {
      time: number;
      isLive: true;
    }
  | {
      time: number;
      isLive: false;
      setTime: (time: number) => void;
    };

export const EventTimeContext = React.createContext<EventTimeContextValue>({
  isLive: true,
  time: 0,
});

export function getTimeSinceEventStart(event: EventObject) {
  return moment().diff(moment(event.start), "seconds");
}

/**
 * Get the time into the event
 */
export function useEventTime() {
  const { time } = useContext(EventTimeContext);
  return time;
}

export interface LiveEventTimeProviderProps {
  event: EventObject;
}
export const LiveEventTimeProvider: React.FC<LiveEventTimeProviderProps> = ({
  event,
  children,
}) => {
  const [time, setTime] = useState(getTimeSinceEventStart(event));
  const duration = useMemo(
    () => moment(event.start).diff(moment(event.end)) / 1000,
    [event]
  );

  useInterval(
    useCallback(() => {
      setTime(getTimeSinceEventStart(event));
    }, [event]),
    1000
  );

  const triggers = useRef<
    {
      type: "watchTime";
      value: any;
      callback: Function;
      called: boolean;
    }[]
  >([]);

  useEffect(() => {
    (window as any).justWebinar = {
      addTrigger: (type: "watchTime", value: any, callback: Function) => {
        triggers.current.push({
          type,
          value,
          callback,
          called: false,
        });
      },
    };
  }, []);

  useEffect(() => {
    const progress = time / duration;

    for (const trigger of triggers.current) {
      if (
        !trigger.called &&
        trigger.type === "watchTime" &&
        trigger.value > progress
      ) {
        trigger.called = true;
        trigger.callback();
      }
    }
  }, [time, duration]);

  return (
    <EventTimeContext.Provider value={{ time, isLive: true }}>
      {children}
    </EventTimeContext.Provider>
  );
};

export interface ReplayEventTimeProviderProps {}
export const ReplayEventTimeProvider: React.FC<ReplayEventTimeProviderProps> = ({
  children,
}) => {
  const [time, setTime] = useState(0);
  return (
    <EventTimeContext.Provider value={{ time, setTime, isLive: false }}>
      {children}
    </EventTimeContext.Provider>
  );
};
