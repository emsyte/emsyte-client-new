import { faVideo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment-timezone";
import React, { useState } from "react";
import { Button, Card, Col, Container, Modal, Row } from "react-bootstrap";
import styled from "styled-components";

const CountdownStyle = styled.div`
  display: flex;
  .v-col {
    flex: 1;
    margin-right: 15px;
    &:last-child {
      margin-right: 0px;
    }
    .v-row {
      display: flex;
      margin-bottom: 10px;
    }
    .rounded {
      flex: 1;
      margin: 3px;
    }
  }
`;

const abbrs = {
  EST: "Eastern Standard Time",
  EDT: "Eastern Daylight Time",
  CST: "Central Standard Time",
  CDT: "Central Daylight Time",
  MST: "Mountain Standard Time",
  MDT: "Mountain Daylight Time",
  PST: "Pacific Standard Time",
  PDT: "Pacific Daylight Time",
};

moment.fn.zoneName = function () {
  var abbr = this.zoneAbbr();
  return abbrs[abbr] || abbr;
};

const getTimezone = (date) => {
  const utcOffset = moment(date).format("ZZ");
  const timezone = moment.tz.guess();

  return `${moment.tz(date, timezone).format("zz")} GMT ${utcOffset}`;
};

const pad2 = (number, _charAt) => {
  number = (number < 10 ? "0" : "") + number;
  return number.toString().charAt(_charAt);
};

const CountdownView = ({
  dateTime: { date, days, hours, minutes, seconds },
  webinar,
}) => {
  const [learnMore, setLearnMore] = useState(false);

  return (
    <Container fluid className="main-content-container p-0">
      <div className="text-center">
        <img
          className="auth-form__logo mt-4"
          src={require("../../../images/Large_Icon.svg")}
          alt="Just Webinar"
        />
      </div>
      <Row className="h-100">
        <Col className="d-none d-md-block"></Col>
        <Col xs={12} md={9} className="my-auto text-center">
          <Card className="bg-transparent no-shadow">
            <Card.Body>
              {/* Logo */}
              <h3>{webinar.name}</h3>
              <hr style={{ width: "75%" }} />

              <Row className={"mt-4"}>
                <Col>
                  <h5>{moment(date).format("dddd, Do MMMM YYYY, h:mm a")}</h5>
                  <p className="text-muted">{getTimezone(date)}</p>
                </Col>
              </Row>

              {/* Title */}
              <h5 className="text-center mb-5" data-testid="title">
                This event will start soon
              </h5>
              <CountdownStyle data-testid="countdown">
                {/* <Row> */}
                <div className="v-col days">
                  <div className="v-row">
                    <div className=" mr-1 bg-light rounded">
                      <h1 className="py-2">{pad2(days, 0)}</h1>
                    </div>
                    <div className="bg-light rounded">
                      <h1 className="py-2">{pad2(days, 1)}</h1>
                    </div>
                  </div>
                  <p>DAYS</p>
                </div>
                <div className="v-col hours">
                  <div className="v-row">
                    <div className="bg-light rounded">
                      <h1 className="py-2">{pad2(hours, 0)}</h1>
                    </div>
                    <div className="bg-light rounded">
                      <h1 className="py-2">{pad2(hours, 1)}</h1>
                    </div>
                  </div>
                  <p>HOURS</p>
                </div>
                <div className="v-col minutes">
                  <div className="v-row">
                    <div className="bg-light rounded">
                      <h1 className="py-2">{pad2(minutes, 0)}</h1>
                    </div>
                    <div className="bg-light rounded">
                      <h1 className="py-2">{pad2(minutes, 1)}</h1>
                    </div>
                  </div>
                  <p>MINUTES</p>
                </div>
                <div className="v-col seconds">
                  <div className="v-row">
                    <div className="bg-light rounded">
                      <h1 className="py-2">{pad2(seconds, 0)}</h1>
                    </div>
                    <div className="bg-light rounded">
                      <h1 className="py-2">{pad2(seconds, 1)}</h1>
                    </div>
                  </div>
                  <p>SECONDS</p>
                </div>
              </CountdownStyle>
            </Card.Body>
          </Card>
        </Col>
        <Col className="d-none d-md-block"></Col>
      </Row>
      <div className="mb-4">
        <Card
          style={{
            maxWidth: 800,
            margin: "0 auto",
          }}
        >
          <Card.Header>
            <Card.Title>
              <FontAwesomeIcon icon={faVideo}></FontAwesomeIcon>
              <span className="ml-2">
                We're going to ask to use your camera
              </span>
            </Card.Title>
          </Card.Header>
          <Card.Body>
            <p>
              When the event starts, your browser will request to access your
              camera. We use this to provide a more engaging experience.{" "}
              <Button
                variant="link"
                onClick={() => setLearnMore(true)}
                data-testid="open-learn-more"
              >
                Learn more
              </Button>
            </p>
            <small>
              For more information on how video from your camera is used and
              stored, view our{" "}
              <a
                href="https://app.termly.io/document/privacy-policy/79f3e9f8-7183-4ac2-a44e-1661e4455f08"
                target="_blank"
                rel="noopener noreferrer"
                className="text-muted font-weight-normal"
              >
                Privacy Policy
              </a>
            </small>
          </Card.Body>
        </Card>
      </div>
      <div className="text-center">
        <div>Copyright &copy; {moment().year()} Emsyte LLC.</div>
        <div>
          <a
            href="https://app.termly.io/document/terms-and-conditions/7a965e8e-6637-494b-a432-0b092decef21"
            target="_blank"
            rel="noopener noreferrer"
            className="text-muted font-weight-normal"
          >
            Terms &amp; Conditions
          </a>
          <span className="mx-1">|</span>
          <a
            href="https://app.termly.io/document/privacy-policy/79f3e9f8-7183-4ac2-a44e-1661e4455f08"
            target="_blank"
            rel="noopener noreferrer"
            className="text-muted font-weight-normal"
          >
            Privacy Policy
          </a>
        </div>
      </div>
      <Modal show={learnMore} onHide={() => setLearnMore(false)}>
        <Modal.Header>
          <Modal.Title>Webcam Access For Research</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
            <h4>What are we researching?</h4>
            We are seeking to understand what engages the
            viewer during their webinar experience by analyzing their emotional responses and attention levels. 
            What makes you smile, frown or get
            bored during a webinar? We are evaluating these emotions to
            ensure you a better experience. Reading how you feel is the key
            factor to provide you a more pleasant and engaging experience.
          </p>
          <hr />
          <p>
            <h4>Data Analysis and Processing</h4>
            Our software uses camera access to analyze emotional behaviors and
            measure your engagement. By giving access to your camera you will be
            participating in the cutting edge artificial intelligence (AI)
            research and help in driving innovative methods of delivering
            interactive online webinars.
          </p>
          <hr />
          <p>
            <h4>Data Security</h4>
            We understand emotions are a personal, delicate matter: that's why
            customer data security and confidentiality is our top priority. We
            treat your data in accordance with the applicable laws and
            regulations. We analyse your data solely for the purpose for which
            we have obtained them. No other participant has access to the
            information. Recorded data will not be shared without your
            permission or used for any other purposes. The privacy policy
            explains how we safeguard your privacy and the protection of your
            personal data.
          </p>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setLearnMore(false)}
            data-testid="close-learn-more"
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
};

export default CountdownView;
