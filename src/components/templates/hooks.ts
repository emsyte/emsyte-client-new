import { TemplateType } from "@emsyte/common/template";
import { useEffect, useMemo, useRef, useState } from "react";
import { useParams } from "react-router-dom";
import { getPublicHttpService } from "../../utils/PublicHttpService";

export function useWebinarTemplate(type: TemplateType, extra?: string) {
  const { id } = useParams<{ id: string }>();

  const webinarId = useMemo(() => {
    const buff = Buffer.from(id, "base64");
    const data = JSON.parse(buff.toString("ascii"));
    return data.id;
  }, [id]);

  const [loading, setLoading] = useState(true);
  const written = useRef(false);

  useEffect(() => {
    if (!webinarId) {
      return;
    }

    setLoading(true);

    getPublicHttpService()
      .doRenderPageTemplate(webinarId, type, extra ?? "")
      .then((content) => {
        if (!written.current) {
          // Overwrite current document with template data
          document.write(content);
          written.current = true;

          (window as any).APP_API = process.env.REACT_APP_API;
        }
      })
      .catch(() => setLoading(false));
  }, [webinarId, extra, type]);

  return { loading, webinarId };
}
