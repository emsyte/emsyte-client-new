import React from "react";
import { Col, Container, Row, Spinner } from "react-bootstrap";

export interface TemplateLoadingProps {}

const TemplateLoading: React.FC<TemplateLoadingProps> = () => {
  return (
    <Container fluid className="main-content-container p-0">
      <Row>
        <Col className="text-center mt-5">
          <h1>Loading...</h1>
          <Spinner
            as="span"
            animation="border"
            role="status"
            aria-hidden="true"
          />
        </Col>
      </Row>
    </Container>
  );
};

export default TemplateLoading;
