import React from "react";

export default function Countdown(editor, opt = {}) {
  // const c = opt;
  const domc = editor.DomComponents;
  const defaultType = domc.getType("default");
  const defaultModel = defaultType.model;
  const defaultView = defaultType.view;
  const pfx = "jw-countdown";
  const COUNTDOWN_TYPE = "countdown";

  domc.addType(COUNTDOWN_TYPE, {
    model: defaultModel.extend(
      {
        defaults: {
          ...defaultModel.prototype.defaults,
          droppable: false,

          script: function () {
            var countDownDate = window._isEditor
              ? new Date().getTime() + 1000 * 60 * 60 * 24 * 1.25
              : new Date("{{ events.[0].start }}").getTime();

            var endTxt = "EXPIRED";
            var countdownEl = this.querySelector("[data-js=countdown]");
            var endTextEl = this.querySelector("[data-js=countdown-endtext]");
            var dayEl = this.querySelector("[data-js=countdown-day]");
            var hourEl = this.querySelector("[data-js=countdown-hour]");
            var minuteEl = this.querySelector("[data-js=countdown-minute]");
            var secondEl = this.querySelector("[data-js=countdown-second]");
            var oldInterval = this.gjs_countdown_interval;
            if (oldInterval) {
              oldInterval && clearInterval(oldInterval);
            }

            var setTimer = function (days, hours, minutes, seconds) {
              dayEl.innerHTML = days < 10 ? "0" + days : days;
              hourEl.innerHTML = hours < 10 ? "0" + hours : hours;
              minuteEl.innerHTML = minutes < 10 ? "0" + minutes : minutes;
              secondEl.innerHTML = seconds < 10 ? "0" + seconds : seconds;
            };

            var moveTimer = function () {
              var now = new Date().getTime();
              var distance = countDownDate - now;
              var days = Math.floor(distance / 86400000);
              var hours = Math.floor((distance % 86400000) / 3600000);
              var minutes = Math.floor((distance % 3600000) / 60000);
              var seconds = Math.floor((distance % 60000) / 1000);

              setTimer(days, hours, minutes, seconds);

              /* If the count down is finished, write some text */
              if (distance < 0) {
                clearInterval(interval);
                endTextEl.innerHTML = endTxt;
                countdownEl.style.display = "none";
                endTextEl.style.display = "";
              }
            };

            if (countDownDate) {
              var interval = setInterval(moveTimer, 1000);
              this.gjs_countdown_interval = interval;
              endTextEl.style.display = "none";
              countdownEl.style.display = "";
              moveTimer();
            } else {
              setTimer(0, 0, 0, 0);
            }
          },
        },
      },
      {
        isComponent(el) {
          if (
            el.getAttribute &&
            el.getAttribute("data-gjs-type") === COUNTDOWN_TYPE
          ) {
            return {
              type: COUNTDOWN_TYPE,
            };
          }
        },
      }
    ),

    view: defaultView.extend({
      init() {
        const comps = this.model.get("components");

        // Add a basic countdown template if it's not yet initialized
        if (!comps.length) {
          comps.reset();
          comps.add(
            <span>
              <span data-js="countdown" class={`${pfx}-cont`}>
                <div class={`${pfx}-block`}>
                  <div data-js="countdown-day" class={`${pfx}-digit`}>
                    00
                  </div>
                  <text class={`${pfx}-label`}>Days</text>
                </div>
                <div class={`${pfx}-block`}>
                  <div data-js="countdown-hour" class={`${pfx}-digit`}>
                    00
                  </div>
                  <text class={`${pfx}-label`}>Hours</text>
                </div>
                <div class={`${pfx}-block`}>
                  <div data-js="countdown-minute" class={`${pfx}-digit`}>
                    00
                  </div>
                  <text class={`${pfx}-label`}>Minutes</text>
                </div>
                <div class={`${pfx}-block`}>
                  <div data-js="countdown-second" class={`${pfx}-digit`}>
                    00
                  </div>
                  <text class={`${pfx}-label`}>Seconds</text>
                </div>
              </span>
              <span data-js="countdown-endtext" class={`${pfx}-endtext`}></span>
            </span>
          );
        }
      },
    }),
  });

  const style = `
    .${pfx} {
        text-align: center;
    }
    .${pfx}-block {
        display: inline-block;
        margin: 0 10px;
        padding: 10px;
    }
    .${pfx}-digit {
        font-size: 5rem;
    }
    .${pfx}-endtext {
        font-size: 5rem;
    }
    .${pfx}-cont,
    .${pfx}-block {
        display: inline-block;
    }
  `;

  // Add blocks
  const bm = editor.BlockManager;
  //   if (c.blocks.indexOf(countdownRef) >= 0) {
  bm.add(countdownRef, {
    label: "Event Countdown",
    category: "Extra",
    attributes: { class: "fa fa-clock-o" },
    content: `
    <div class="${pfx}" data-gjs-type="countdown"></div>
    <style>${style}</style>
    `,
  });
  //   }
}
const countdownRef = "countdown";
