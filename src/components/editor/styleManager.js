export default [
  {
    name: "General",
    open: false,
    buildProps: [
      "float",
      "display",
      "position",
      "top",
      "right",
      "left",
      "bottom",
    ],
  },
  {
    name: "Flex",
    open: false,
    buildProps: [
      "flex-direction",
      "flex-wrap",
      "justify-content",
      "align-items",
      "align-content",
      "order",
      "flex-basis",
      "flex-grow",
      "flex-shrink",
      "align-self",
    ],
  },
  {
    name: "Dimensions",
    open: false,
    buildProps: [
      "width",
      "height",
      "max-width",
      "min-height",
      "margin",
      "padding",
    ],
  },
  {
    name: "Typography",
    open: false,
    buildProps: [
      "font-family",
      "font-size",
      "font-weight",
      "font-style",
      "color",
      "letter-spacing",
      "line-height",
      "text-decoration",
      "text-align",
      "text-shadow",
    ],
    properties: [
      {
        property: "text-align",
        list: [
          { value: "left", className: "fa fa-align-left" },
          { value: "center", className: "fa fa-align-center" },
          { value: "right", className: "fa fa-align-right" },
          { value: "justify", className: "fa fa-align-justify" },
        ],
      },
      {
        property: "font-style",
        type: "select",
        defaults: "normal",
        list: [
          { value: "normal", name: "Normal" },
          { value: "italic", name: "Italic" },
        ],
      },
      {
        property: "text-decoration",
        type: "radio",
        defaults: "none",
        list: [
          { value: "none", name: "None", className: "fa fa-times" },
          {
            value: "underline",
            name: "underline",
            className: "fa fa-underline",
          },
          {
            value: "line-through",
            name: "Line-through",
            className: "fa fa-strikethrough",
          },
        ],
      },
    ],
  },
  {
    name: "Decorations",
    open: false,
    buildProps: [
      "border-radius-c",
      "background-color",
      "border-radius",
      "border",
      "box-shadow",
      "background",
    ],
  },
  {
    name: "Extra",
    open: false,
    buildProps: ["transition", "perspective", "transform"],
  },
];
