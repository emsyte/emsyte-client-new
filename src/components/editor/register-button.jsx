const registerButtonRef = "register-button";

export default function RegisterButton(editor) {
  // Add blocks
  const bm = editor.BlockManager;
  bm.add(registerButtonRef, {
    label: "Register Button",
    category: "Extra",
    attributes: { class: "fa fa-shopping-cart" },
    content: `
    <div>
      <button
        type="button"
        data-webinarId="{{ webinarHash }}"
        class="btn btn-primary register-btn justwebinar-registration"
      >
        <text>Register Now</text>
      </button>
    </div>
    `,
  });
}
