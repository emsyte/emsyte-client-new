import PropTypes from "prop-types";
import React, { useCallback, useEffect, useState } from "react";
import { Spinner } from "react-bootstrap";
import { Link } from "react-router-dom";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
} from "shards-react";
import { useHttpService } from "../../utils/HttpService";
import styled from "styled-components"
import colors from "../../utils/colors";
import illustrationRocket from "../../images/illustration-rocket.svg"

const Header = styled.span`
  font-size: 1rem;
  font-weight: 700;
`

const TopWebinars = () => {
  const api = useHttpService();
  const [referralData, setReferralData] = useState<any>([]);
  const [loading, setLoading] = useState(false);
  const handleGetWebinars = useCallback(async () => {
    setLoading(true);
    try {
      const response = await api.doGetAllWebinarsForUser();
      setReferralData(response.data);
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
  }, [api]);

  useEffect(() => {
    const fetchData = async () => await handleGetWebinars();
    fetchData();
  }, [handleGetWebinars]);

  return (
    <Card style={{minHeight: '23.75rem'}}>
      <CardHeader style={{paddingBottom: 0}}>
        <Row>
          <h6 className="m-0 text-uppercase">Top Webinars</h6>
        </Row>
      </CardHeader>

      <CardBody className="p-0">
        {loading ? (
          <Row>
            <Col className="text-center p-4">
              <Spinner animation="border" />
            </Col>
          </Row>
        ) : referralData.length === 0 ? (
          <Row>
            <Col className="text-center p-4 d-flex flex-column justify-content-center align-items-center">
              <img src={illustrationRocket} width="150px" height="150px" alt="Rocket Illustration"/>
              <span>Keep track of your top performing webinars.</span>
            </Col>
          </Row>
        ) : (
          <React.Fragment>
          <ListGroup small flush className="list-group-small" style={{border: 0}}>
            <ListGroupItem>
              <div className="d-flex justify-content-between" style={{borderBottom: '2px solid ' + colors.gainsboro.toHex()}}>
                <Header>Webinar</Header>
                <Header>Conversion Rate</Header>
              </div>
            </ListGroupItem>
            {referralData.map((item) => (
              <React.Fragment>
                <ListGroupItem key={item.id} className="d-flex px-3" style={{border: 0}}>
                  <span className="text-semibold text-fiord-blue">
                    <Link
                      to={`/analytics/${btoa(JSON.stringify({id: item.id}))}`}
                    >
                      {item.name}
                      </Link>
                  </span>
                  <span className="ml-auto">
                    0%
                  </span>
                </ListGroupItem>
                <div style={{backgroundColor: colors.gainsboro.toHex(), height: '2px', margin: '0 1rem'}}/>
              </React.Fragment>
            ))}
          </ListGroup>

          </React.Fragment>
        )}
      </CardBody>

      <CardFooter>
        <Row>
          {/* Time Span */}
          <Col>
            {/* <FormSelect
            size="sm"
            value="last-week"
            style={{ maxWidth: "130px" }}
            onChange={() => {}}
          >
            <option value="last-week">Last Week</option>
            <option value="today">Today</option>
            <option value="last-month">Last Month</option>
            <option value="last-year">Last Year</option>
          </FormSelect> */}
          </Col>
        </Row>
      </CardFooter>
    </Card>
  );
};

TopWebinars.propTypes = {
  /**
   * The component's title.
   */
  title: PropTypes.string,
  /**
   * The referral data.
   */
  referralData: PropTypes.array,
};

TopWebinars.defaultProps = {
  title: "Top Webinars",
  referralData: [
    {
      title: "GitHub",
      value: "19,291",
    },
    {
      title: "Stack Overflow",
      value: "11,201",
    },
    {
      title: "Hacker News",
      value: "9,291",
    },
    {
      title: "Reddit",
      value: "8,281",
    },
    {
      title: "The Next Web",
      value: "7,128",
    },
    {
      title: "Tech Crunch",
      value: "6,218",
    },
    {
      title: "YouTube",
      value: "1,218",
    },
    {
      title: "Adobe",
      value: "1,171",
    },
  ],
};

export default TopWebinars;
