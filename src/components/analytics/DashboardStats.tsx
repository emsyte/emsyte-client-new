import React, {useMemo} from "react";
import { Card, Col, Spinner } from "react-bootstrap";
import colors from "../../utils/colors";
import { formatPercentage } from "../../utils/formatters";
import styled from "styled-components";

function getPercentageChange(data: number[]) {
  if (data.length < 2) {
    return 0;
  }
  const current = data[data.length - 1];
  const previous = data[data.length - 2];
  return (current - previous) / previous;
}

function getChangeOptions(data: number[]) {
  const change = getPercentageChange(data);
  return {
    percentage: formatPercentage(Math.abs(change)),
    increase: change > 0,
    decase: change < 0,
  };
}

const Title = styled.span`
  font-size: 1rem;
`

const Value = styled.span`
  font-size: 2.5rem;
`

const SmallValue = styled.span`
  font-size: 1rem;
  margin-left: 0.625rem;
`

interface StatsCardProps {
  label: string;
  property: string;
  value: string;
  lineColor: string;
  smallValue?: string;
  enabled?: boolean;
  setEnabled?: (enabled: string) => void;
  style?: object;
}

const StatsCard: React.FC<StatsCardProps> = props => {

  const { smallValue = '', enabled = false, setEnabled = enabled => {}, style = {} } = props;

  return (
    <div onClick={() => setEnabled(props.property)} style={!enabled ? {color: 'rgba(46, 51, 56, 0.6)', ...style} : style} className="stats-card">
      <div style={enabled ? {borderBottom: '4px solid ' + props.lineColor} : {}}>
        <div className="d-flex flex-column">
          <div className="d-flex align-items-end" style={{height: '3rem'}}>
            <Title className="text-uppercase">{props.label}</Title>
          </div>
          <div><Value>{props.value}</Value><SmallValue>{smallValue}</SmallValue></div>
        </div>
      </div>
    </div>
  )
}

export interface DashboardStatsProps {
  stats: any;
  loading: boolean;
  enabledLabel: string;
  setEnabledLabel: (label: string) => void;
}

export const chartColors = {
  registrationVisits: '#17C671',
  registered: '#FFB400',
  attendees: '#C4183C',
  offerClicks: '#674EEC',
}

const DashboardStats: React.FC<DashboardStatsProps> = ({ stats, loading, enabledLabel, setEnabledLabel }) => {
  const smallStats = useMemo(
    () =>
      stats
        ? [
            {
              label: "Total Registration Page Views",
              value: stats.total.registrationVisits > 0 ? stats.total.registrationVisits : '-',
              smallValue: '',
              property: 'registrationVisits',
              ...getChangeOptions(stats.series.registrationVisits),
              chartLabels: new Array(stats.series.registrationVisits.length),
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: colors.blue.toRGBA(0.1),
                  borderColor: chartColors['registrationVisits'],
                  data: stats.series.registrationVisits,
                },
              ],
            },
            {
              label: "Total Registrations",
              value: stats.total.registrationVisits > 0 ? `${(stats.total.registered/stats.total.registrationVisits*100 || 0).toFixed(0)}%` : '-',
              smallValue: stats.total.registrationVisits > 0 ? `(${stats.total.registered})` : '',
              property: 'registered',
              ...getChangeOptions(stats.series.registered),
              chartLabels: new Array(stats.series.registered.length),
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: colors.yellow.toRGBA(0.1),
                  borderColor: chartColors['registered'],
                  data: stats.series.registered,
                },
              ],
            },
            {
              label: "Total Attendees",
              value: stats.total.registrationVisits > 0 ? `${(stats.total.attendees/stats.total.registered*100 || 0).toFixed(0)}%` : '-',
              smallValue: stats.total.registrationVisits > 0 ? `(${stats.total.attendees})` : '',
              property: 'attendees',
              ...getChangeOptions(stats.series.attendees),
              chartLabels: new Array(stats.series.attendees.length),
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: colors.orange.toRGBA(0.1),
                  borderColor: chartColors['attendees'],
                  data: stats.series.attendees,
                },
              ],
            },
            {
              label: "Offer Clicks",
              value: stats.total.registrationVisits > 0 ? `${(stats.total.offerClicks/stats.total.attendees*100 || 0).toFixed(0)}%` : '-',
              smallValue: stats.total.registrationVisits > 0 ? `(${stats.total.offerClicks})` : '',
              property: 'offerClicks',
              ...getChangeOptions(stats.series.offerClicks),
              chartLabels: new Array(stats.series.offerClicks.length),
              datasets: [
                {
                  label: "Today",
                  fill: "start",
                  borderWidth: 1.5,
                  backgroundColor: colors.green.toRGBA(0.1),
                  borderColor: chartColors['offerClicks'],
                  data: stats.series.offerClicks,
                },
              ],
            },
          ]
        : ([null, null, null, null] as any[]),
    [stats]
  );

  return (
    <React.Fragment>
      {smallStats.map((stats, i) => (
        <Col md="6" lg="3" className="mb-4 d-flex" key={i} style={{padding: '10px 0'}}>
          {loading ? (
            <div className="w-100 d-flex align-items-center justify-content-center">
                <Spinner animation="border" />
            </div>
          ) : stats === null ? (
            <Card className="w-100">
              <Card.Body>
                <p className="text-danger">Failed to load stats</p>
              </Card.Body>
            </Card>
          ) : (
            <StatsCard
              label={stats.label}
              property={stats.property}
              value={stats.value}
              lineColor={stats.datasets[0].borderColor}
              smallValue={stats.smallValue}
              setEnabled={setEnabledLabel}
              enabled={enabledLabel === stats.property}
              style={i !== smallStats.length - 1 ? {borderRight: `1px solid ${colors.gainsboro.toHex()}`} : {}}
            />
          )}
        </Col>
      ))}
    </React.Fragment>
  );
};

export default DashboardStats;
