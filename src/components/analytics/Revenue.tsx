import moment from "moment";
import React, { useEffect, useMemo } from "react";
import { Spinner } from "react-bootstrap";
import { Range } from "react-date-range";
import { Card, CardBody, CardHeader } from "shards-react";
import colors from "../../utils/colors";
import { formatCurrency } from "../../utils/formatters";
import { useChart } from "../../utils/hooks/chart";

function createLabels(range: Range) {
  const current = moment(range.startDate);
  const end = moment(range.endDate);

  const labels: string[] = [];

  const days = end.diff(current, "days") + 1;

  for (let i = 0; i < days; i++) {
    labels.push(current.format("MM/DD/YYYY"));
    current.add(1, "day");
  }
  return labels;
}

export interface RevenueChartProps {
  stats: any;
  range: Range;
}

const RevenueChart: React.FC<RevenueChartProps> = ({ stats, range }) => {
  const data = useMemo(() => {
    const labels = createLabels(range);
    return {
      labels,
      datasets: [
        {
          label: "Revenue",
          fill: "start",
          data: [...stats.series.revenue],
          backgroundColor: colors.primary.toRGBA(0.1),
          borderColor: colors.primary.toRGBA(1),
          pointBackgroundColor: colors.white.toHex(),
          pointHoverBackgroundColor: colors.primary.toRGBA(1),
          borderWidth: 1.5,
        },
      ],
    };
  }, [stats, range]);

  const { canvasRef, chart } = useChart({
    type: "line",
    data: data,
    options: {
      responsive: true,
      legend: false,
      elements: {
        line: {
          // A higher value makes the line look skewed at this ratio.
          tension: 0.38,
        },
      },
      scales: {
        xAxes: [
          {
            gridLines: false,
            ticks: {
              callback(tick, index) {
                if (tick) {
                  return moment(tick).format("MM/DD/YYYY");
                }
              },
            },
          },
        ],
        yAxes: [
          {
            ticks: {
              callback(value) {
                return formatCurrency(value);
              },
            },
          },
        ],
      },
      tooltips: {
        enabled: false,
        mode: "index",
        position: "nearest",
        callbacks: {
          label: (data) => {
            return formatCurrency(data.value);
          },
        },
      },
    },
  });

  useEffect(() => {
    if (chart) {
      chart.data = data;
      chart.update();
    }
  }, [data, chart]);

  return (
    <canvas
      height="120"
      ref={canvasRef}
      style={{ maxWidth: "100% !important" }}
      className="analytics-overview-sessions"
    />
  );
};

export interface RevenueProps {
  stats: any;
  loading: boolean;
  range: Range;
}

const Revenue: React.FC<RevenueProps> = ({ stats, loading, range }) => {
  return (
    <Card small className="h-100">
      {/* Card Header */}
      <CardHeader className="border-bottom">
        <h6 className="m-0">
          {stats ? (
            <span>Total Revenue: {formatCurrency(stats.total.revenue)}</span>
          ) : (
            <span>Revenue</span>
          )}
        </h6>
      </CardHeader>

      <CardBody className="pt-0">
        {loading ? (
          <Spinner animation="border"></Spinner>
        ) : stats ? (
          <RevenueChart stats={stats} range={range}></RevenueChart>
        ) : (
          <p className="text-danger">Failed to load revenue data</p>
        )}
      </CardBody>
    </Card>
  );
};

export default Revenue;
