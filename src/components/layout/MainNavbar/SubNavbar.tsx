import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import {
  Collapse,
  Container,
  Nav,
  Navbar,
  NavbarBrand,
  NavItem,
  NavLink,
} from "shards-react";
import { LAYOUT_TYPES } from "../../../utils/constants";

const SubNavbar = ({ layout, stickyTop, match: { url } }) => {
  const _page = url.split("/").pop();
  const _base = "/tools";

  let Title;
  switch (_page) {
    case "broadcast":
      Title = "Broadcasts";
      break;
    case "hashtag-generator":
      Title = "Hashtag Generator";
      break;
    case "welcome-messages":
      Title = "Welcome Message";
      break;
    default:
      Title = "Broadcast";
      break;
  }

  const isHeaderNav = layout === LAYOUT_TYPES.HEADER_NAVIGATION;
  const classes = classNames(
    "main-navbar",
    "px-3",
    "bg-transparent",
    "no-shadow"
  );

  return (
    <div className={classes}>
      <Container fluid={!isHeaderNav || null} className="p-0">
        <Navbar className="align-items-center flex-md-nowrap p-0">
          <NavbarBrand>
            <h4>{Title}</h4>
          </NavbarBrand>
          <Collapse navbar className="justify-content-end">
            <Nav navbar className="flex-row">
              <NavItem className="mx-3">
                <NavLink
                  active={_page === "broadcast"}
                  className={"text-secondary"}
                  href={`${_base}/broadcast`}
                >
                  Broadcast
                </NavLink>
              </NavItem>
              <NavItem className="mx-3">
                <NavLink
                  active={_page === "welcome-messages"}
                  className={"text-secondary"}
                  href={`${_base}/welcome-messages`}
                >
                  Welcome Messages
                </NavLink>
              </NavItem>
              <NavItem className="mx-3">
                <NavLink
                  active={_page === "hashtag-generator"}
                  className={"text-secondary"}
                  href={`${_base}/hashtag-generator`}
                >
                  Hashtag Generator
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </Container>
    </div>
  );
};

SubNavbar.propTypes = {
  /**
   * The layout type where the SubNavbar is used.
   */
  layout: PropTypes.string,
  /**
   * Whether the main navbar is sticky to the top, or not.
   */
  stickyTop: PropTypes.bool,
};

SubNavbar.defaultProps = {
  stickyTop: true,
};

const enhance = compose(withRouter)(SubNavbar);

export default enhance;
