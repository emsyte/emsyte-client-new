import React from "react";
import { Button } from "react-bootstrap";
import styled from "styled-components";
import ProfileIcon from "./ProfileIcon";
import SidebarTeamDropdown from "./SidebarTeamDropdown";
import { ReactComponent as Collapse } from "../../../images/main-sidebar-collapse.svg";

const AppBarWrapper = styled.div`
  /* Auto Layout */

  display: flex;
  flex-direction: row;
  align-items: center;
  padding: 0px 30px;

  height: 50px;
  min-height: 50px;

  /* margin-bottom: 1.5rem; */

  /* Midnight - Tint */

  background: #33455b;
  /* Row line */

  box-shadow: inset 0px -1px 0px #dee2e6;
`;

const AppBarTitle = styled.div`
  font-size: 20px;
  line-height: 24px;

  margin-right: auto;

  /* White */

  color: #ffffff;
`;

const AppBarDivider = styled.div`
  /* Rectangle 56 */
  width: 1px;
  height: 30px;

  margin-right: 0.75rem;

  /* White */

  background: #ffffff;
`;

const CollapseButton = styled(Button)`
  background: none !important;
  border: none !important;
  outline: none !important;
  box-shadow: none !important;
  padding-left: 0;
`;

export interface AppBarProps {
  title: React.ReactNode;
  onOpen: () => void;
}

const AppBar: React.FC<AppBarProps> = ({ title, onOpen }) => {
  return (
    <AppBarWrapper>
      <CollapseButton onClick={onOpen} className="d-block d-md-none">
        <Collapse aria-hidden="true"></Collapse>
        <span className="sr-only">Open Sidebar</span>
      </CollapseButton>
      <AppBarTitle>{title}</AppBarTitle>
      <SidebarTeamDropdown></SidebarTeamDropdown>
      <AppBarDivider></AppBarDivider>
      <ProfileIcon></ProfileIcon>
    </AppBarWrapper>
  );
};

export default AppBar;
