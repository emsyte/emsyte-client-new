import React from "react";
import { Button } from "react-bootstrap";
import { ReactComponent as Collapse } from "../../../images/main-sidebar-collapse.svg";

export interface MainNavbarProps {
  onOpen: () => void;
}

const MainNavbar: React.FC<MainNavbarProps> = ({ onOpen }) => {
  return (
    <div className="main-navbar">
      <Button onClick={onOpen} className="show-sidebar-button">
        <Collapse aria-hidden="true"></Collapse>
        <span className="sr-only">Open Sidebar</span>
      </Button>
      <img
        className="mr-1"
        style={{ maxHeight: "2rem" }}
        src={require("../../../images/Large_Icon.svg")}
        alt="Just Webinar"
      />
      <img
        style={{ maxHeight: "2rem" }}
        src={require("../../../images/justWebinarWords.svg")}
        alt="Just Webinar"
      />
    </div>
  );
};

export default MainNavbar;
