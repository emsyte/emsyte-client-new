import { TeamUserRole } from "@emsyte/common/team";
import {
  faCog,
  faUser,
  faUserShield,
  faUserTie,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useMemo } from "react";
import { Dropdown, OverlayTrigger, Spinner, Tooltip } from "react-bootstrap";

import { Link } from "react-router-dom";
import { useAuth } from "../../../utils/Authentication";
import { unknownValue } from "../../../utils/conditions";
import { Gear } from "react-bootstrap-icons";

import styled from "styled-components";

const DropdownToggle = styled(Dropdown.Toggle)`
  background-color: transparent;
  border: none;

  font-weight: 500;
  font-size: 16px;
  line-height: 24px;
  color: #ffffff;

  opacity: 0.75;
`;

export interface RoleIconProps {
  role: TeamUserRole;
}

const RoleIcon: React.FC<RoleIconProps> = ({ role }) => {
  switch (role) {
    case TeamUserRole.OWNER:
      return (
        <OverlayTrigger overlay={<Tooltip id="owner">Team Owner</Tooltip>}>
          <FontAwesomeIcon icon={faUserTie}></FontAwesomeIcon>
        </OverlayTrigger>
      );
    case TeamUserRole.ADMIN:
      return (
        <OverlayTrigger overlay={<Tooltip id="admin">Team Admin</Tooltip>}>
          <FontAwesomeIcon icon={faUserShield}></FontAwesomeIcon>
        </OverlayTrigger>
      );
    case TeamUserRole.MEMBER:
      return (
        <OverlayTrigger overlay={<Tooltip id="member">Team Member</Tooltip>}>
          <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
        </OverlayTrigger>
      );
    default:
      unknownValue(role);
      return null;
  }
};

const SidebarTeamDropdown = () => {
  const { currentTeam, setCurrentTeam, teams } = useAuth();
  const team = useMemo(
    () => teams.find((team) => team.team.slug === currentTeam),
    [currentTeam, teams]
  );
  return (
    <Dropdown>
      <DropdownToggle disabled={!team} variant="secondary">
        {team ? (
          team.team.name
        ) : (
          <span>
            <Spinner animation="border" size="sm"></Spinner>
            <span className="ml-2">Loading...</span>
          </span>
        )}
      </DropdownToggle>
      <Dropdown.Menu>
        <Dropdown.Header>Teams</Dropdown.Header>
        {teams.map((userTeam) => (
          <Dropdown.Item
            onClick={() => setCurrentTeam(userTeam.team.slug)}
            key={userTeam.team.slug}
            value={userTeam.team.slug}
          >
            <div className="d-flex">
              <div className="mr-auto">{userTeam.team?.name}</div>
              {/* <div className="pl-2" style={{ width: "1.25rem" }}>
                  <RoleIcon role={userTeam.role}></RoleIcon>
                </div> */}
            </div>
          </Dropdown.Item>
        ))}
        <Dropdown.Divider></Dropdown.Divider>
        <Dropdown.Item as={Link} to="/team-settings" className="d-flex">
          Team Settings <Gear className="ml-auto"></Gear>
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  );
};
SidebarTeamDropdown.propTypes = {};

export default SidebarTeamDropdown;
