import React, { useRef, useState } from "react";
import {
  Button,
  Dropdown,
  Overlay,
  OverlayTrigger,
  Popover,
  Tooltip,
} from "react-bootstrap";
import { useAuth } from "../../../utils/Authentication";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { BoxArrowLeft } from "react-bootstrap-icons";

const Menu = styled(Dropdown.Menu)`
  position: relative !important;
  pointer-events: inherit !important;
  opacity: 1 !important;
  display: block !important;
  top: auto !important;
  bottom: auto !important;
  left: auto !important;
  right: auto !important;
`;

const ProfileButton = styled(Button)`
  background: none;
  padding: 0;
  border: none;
  border-radius: 100%;
`;

export interface ProfileIconProps {}

const ProfileIcon: React.FC<ProfileIconProps> = () => {
  const { authUser, logout } = useAuth();
  const name = authUser ? authUser.firstName + " " + authUser.lastName : "";
  const target = useRef<HTMLButtonElement>(null);
  const [show, setShow] = useState(false);
  return (
    <>
      <OverlayTrigger
        overlay={<Tooltip id="appbar-profile-name">{name}</Tooltip>}
        placement="bottom"
        trigger={["hover", "focus"]}
        rootClose
      >
        <ProfileButton
          id="profile-icon-button"
          ref={target}
          variant="secondary"
          onClick={() => setShow(true)}
        >
          <img
            className="rounded-circle"
            src={`https://ui-avatars.com/api/?name=${name}`}
            alt={name}
            width="30"
          />
        </ProfileButton>
      </OverlayTrigger>

      <Overlay
        rootClose
        target={target.current}
        placement="bottom"
        show={show}
        onHide={() => setShow(false)}
      >
        {({ placement, arrowProps, show: _show, popper, ...props }) => {
          console.log(props);
          return (
            <Popover id="appbar-profile-menu" {...props}>
              <Menu show>
                <Dropdown.Header>{name}</Dropdown.Header>
                <Dropdown.Item as={Link} to="/account">
                  Profile Settings
                </Dropdown.Item>

                <Dropdown.Divider />
                <Dropdown.Item onClick={() => logout()} className="d-flex">
                  Sign Out
                  <BoxArrowLeft className="ml-auto"></BoxArrowLeft>
                </Dropdown.Item>
              </Menu>
            </Popover>
          );
        }}
      </Overlay>
    </>
  );
};

export default ProfileIcon;
