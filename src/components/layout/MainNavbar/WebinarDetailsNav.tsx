import React, { useEffect, useState } from "react";
import { withRouter, Link } from "react-router-dom";
import { withAuth } from "../../../utils/Authentication";
import { compose } from "recompose";

import { Path } from "path-parser";

import { Navbar, Nav, NavItem, NavLink } from "shards-react";

const path = new Path("/webinars/:id/:route");

const steps = [
  "Back",
  "Basic",
  "Templates",
  "Notifications",
  "Monetize",
  // "Chats",
  "Advanced",
  "Preview",
];

const WebinarDetailsNav = ({ match, history, disabled }) => {
  const { id, route } = match.params;
  const [routeIndex, setRouteIndex] = useState<any>();

  const getPath = (_route) => {
    const newPath = path.build(
      { id, route: _route.toLowerCase() },
      { urlParamsEncoding: "none" }
    );
    return newPath;
  };

  useEffect(() => {
    if (route)
      setRouteIndex(
        steps.findIndex(
          (element) =>
            element.toString().toLowerCase() === route.toString().toLowerCase()
        )
      );
  }, [route]);

  return (
    <Navbar
      theme={route === "preview" ? "primary" : "light"}
      expand="md"
      className={`webinar-details-navbar p-0 w-100`}
    >
      <Nav navbar justified className="w-100 d-table">
        {route === "preview" ? (
          <>
            <NavItem className="position-relative h-100 d-table-cell pointer">
              <Link
                className="nav-link"
                to={routeIndex ? getPath(steps[routeIndex - 1]) : "#"}
              >
                <div className="align-middle">
                  <i className="fas fa-arrow-left fa-2x text-white"></i>
                </div>
              </Link>
            </NavItem>
            <NavItem
              className={`position-relative h-100 d-table-cell pointer w-100`}
            >
              <NavLink>
                <div className="align-middle text-white">Preview Webinar</div>
              </NavLink>
            </NavItem>
          </>
        ) : (
          steps.map((item, i) => {
            if (item === "Back") {
              return (
                <NavItem
                  key={i}
                  className="position-relative border-right h-100 d-table-cell pointer highlight"
                >
                  <Link
                    className="nav-link"
                    to={routeIndex ? getPath(steps[routeIndex - 1]) : "#"}
                  >
                    <div className="align-middle">
                      <i className="fas fa-arrow-left fa-2x"></i>
                    </div>
                  </Link>
                </NavItem>
              );
            } else {
              return (
                <NavItem
                  key={i}
                  md={1}
                  disabled={disabled}
                  className={`position-relative border-right h-100 d-table-cell pointer ${
                    i <= routeIndex ? "highlight" : null
                  }`}
                >
                  <Link
                    to={getPath(item)}
                    className={"nav-link" + (disabled ? " disabled" : "")}
                  >
                    <div className="align-middle">{item}</div>
                  </Link>
                </NavItem>
              );
            }
          })
        )}
      </Nav>
    </Navbar>
  );
};

const enhance = compose(withRouter, withAuth)(WebinarDetailsNav);

export default enhance;
