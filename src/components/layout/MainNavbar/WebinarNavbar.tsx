import classNames from "classnames";
import PropTypes from "prop-types";
import React from "react";
import { useParams } from "react-router-dom";
import { Container, Navbar } from "shards-react";
import { LAYOUT_TYPES } from "../../../utils/constants";
import WebinarDetailsNav from "./WebinarDetailsNav";

const WebinarNavbar = ({ layout, stickyTop }) => {
  const { id: encId } = useParams<{ id: string }>();
  const { id } = JSON.parse(atob(encId));
  const disabled = isNaN(parseInt(id));

  const isHeaderNav = layout === LAYOUT_TYPES.HEADER_NAVIGATION;
  const classes = classNames(
    "main-navbar",
    "bg-white",
    stickyTop && "sticky-top"
  );

  return (
    <div className={classes}>
      <Container fluid={!isHeaderNav || null} className="p-0">
        <Navbar type="light" className="align-items-stretch flex-md-nowrap p-0">
          <WebinarDetailsNav disabled={disabled} />
        </Navbar>
      </Container>
    </div>
  );
};

WebinarNavbar.propTypes = {
  /**
   * The layout type where the WebinarNavbar is used.
   */
  layout: PropTypes.string,
  /**
   * Whether the main navbar is sticky to the top, or not.
   */
  stickyTop: PropTypes.bool,
};

WebinarNavbar.defaultProps = {
  stickyTop: true,
};

export default WebinarNavbar;
