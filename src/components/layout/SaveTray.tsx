import React from "react";
import styled from "styled-components";

const Tray = styled.div`
  width: 100%;

  z-index: 100;

  display: flex;
  justify-content: flex-end;

  background: white;
  padding: 0.75rem;
  box-shadow: 0px -1px 5px rgba(0, 0, 0, 0.1);
`;

export interface SaveTrayProps {}

const SaveTray: React.FC<SaveTrayProps> = ({ children }) => {
  return <Tray>{children}</Tray>;
};

export default SaveTray;
