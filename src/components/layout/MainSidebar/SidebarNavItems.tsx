import React from "react";
import { Nav } from "shards-react";
import _ from "lodash";
import styled from "styled-components";
import getItems from "../../../data/sidebar-nav-items";
import SidebarNavItem from "./SidebarNavItem";
import SidebarHelpDropdown from "./SidebarHelpDropdown";
import WebinarChannels from "./WebinarChannels";

const Separator = styled.div`
  height: 0.0625rem;
  background-color: rgba(255, 255, 255, 0.3);
  margin: 1.875rem 1.25rem 1.25rem 1.25rem;
`;

interface SidebarNavItemsProps {}

const SidebarNavItems: React.FC<SidebarNavItemsProps> = () => {
  return (
    <div className="nav-wrapper">
      {getItems().map((nav, idx) => (
        <div key={idx}>
          {typeof nav.items !== "undefined" && nav.items.length && (
            <Nav
              className="nav--no-borders flex-column border-0"
              style={{
                paddingLeft: "0.625rem",
                paddingRight: "0.625rem",
              }}
            >
              {nav.items.map((item, idx) => (
                <SidebarNavItem key={idx} item={item} />
              ))}
              {/* <SidebarHelpDropdown /> */}
            </Nav>
          )}
        </div>
      ))}
      <WebinarChannels></WebinarChannels>
    </div>
  );
};

export default SidebarNavItems;
