import PropTypes from "prop-types";
import React from "react";
import { compose } from "recompose";

import { withAuth } from "../../../utils/Authentication";

import { NavLink as RouteNavLink, Link, withRouter } from "react-router-dom";
import { Collapse, DropdownItem, DropdownMenu, NavItem } from "shards-react";

import Parser from "html-react-parser";

import styled from "styled-components";

const StyledNavItem = styled(NavItem)`
  position: relative;
  font-size: 1rem;
  border-radius: 2px;
`;

const SidebarNavItem = ({ item, logout, style = {} }) => {
  const hasSubItems = item.items && item.items.length;

  return (
    <StyledNavItem style={{ ...style }}>
      {hasSubItems ? (
        <Link
          className={`nav-link d-flex align-items-center ${
            hasSubItems && "dropdown-toggle"
          }`}
          to={hasSubItems ? "#" : item.to}
        >
          {item.htmlBefore && (
            <div className={`d-inline-block item-icon-wrapper`}>
              {Parser(item.htmlBefore)}
            </div>
          )}
          {item.title && <span>{item.title}</span>}
          {item.htmlAfter && (
            <div className="d-inline-block item-icon-wrapper">
              {Parser(item.htmlAfter)}
            </div>
          )}
        </Link>
      ) : (
        <RouteNavLink
          className={`nav-link d-flex align-items-center ${
            hasSubItems ? "dropdown-toggle" : ""
          }`}
          to={hasSubItems ? "#" : item.to}
        >
          {item.htmlBefore && (
            <div className="d-inline-block item-icon-wrapper">
              {Parser(item.htmlBefore)}
            </div>
          )}
          <span>{item.title}</span>
          {item.htmlAfter && (
            <div className={`d-inline-block item-icon-wrapper`}>
              {Parser(item.htmlAfter)}
            </div>
          )}
        </RouteNavLink>
      )}
      {hasSubItems && (
        <Collapse tag={DropdownMenu} small open={item.open} style={{ top: 0 }}>
          {item.items.map((subItem, idx) => (
            <Link key={idx} to={subItem.to}>
              <DropdownItem>{subItem.title}</DropdownItem>
            </Link>
          ))}
        </Collapse>
      )}
    </StyledNavItem>
  );
};

SidebarNavItem.propTypes = {
  /**
   * The item object.
   */
  item: PropTypes.object,
};

const enhance = compose(withRouter, withAuth)(SidebarNavItem);

export default enhance;
