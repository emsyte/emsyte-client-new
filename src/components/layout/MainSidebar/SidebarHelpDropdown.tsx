import { faCommentAlt, faInfo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Dropdown } from "react-bootstrap";
import DropdownItem from "react-bootstrap/esm/DropdownItem";
import DropdownMenu from "react-bootstrap/esm/DropdownMenu";
import DropdownToggle from "react-bootstrap/esm/DropdownToggle";
import colors from "../../../utils/colors";

export interface SidebarHelpDropdownProps {}

const SidebarHelpDropdown: React.FC<SidebarHelpDropdownProps> = () => {
  return (
    <div className="w-100 pr-4 pl-4">
      <Dropdown className="w-100" style={{ overflow: "visible" }}>
        <DropdownToggle
          className="w-100"
          style={{
            backgroundColor: "transparent",
            border: "1px solid " + colors.notWhite.toHex(),
          }}
        >
          <FontAwesomeIcon icon={faInfo}></FontAwesomeIcon> Get Help
        </DropdownToggle>
        <DropdownMenu style={{ width: "auto" }}>
          <DropdownItem href="https://justwebinar.tawk.help/" target="_blank">
            Knowledge Base
          </DropdownItem>
          <DropdownItem href="mailto:support@justwebinar.com" target="_blank">
            support@justwebinar.com
          </DropdownItem>
          <DropdownItem
            href="https://justwebinar.tawk.help/article/report-a-bug"
            target="_blank"
          >
            Report A Bug
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </div>
  );
};

export default SidebarHelpDropdown;
