import classNames from "classnames";
import React from "react";
import { Button } from "react-bootstrap";
import SidebarMainNavbar from "./SidebarMainNavbar";
import SidebarNavItems from "./SidebarNavItems";

export interface MainSidebarProps {
  open: boolean;
  setOpen: (open: boolean) => void;
}

const MainSidebar: React.FC<MainSidebarProps> = ({ open, setOpen }) => {
  const classes = classNames("main-sidebar", open && "open");

  return (
    <aside className={classes}>
      <SidebarMainNavbar onClose={() => setOpen(false)} />
      <SidebarNavItems />
      {/* <UserActions /> */}
    </aside>
  );
};
export default MainSidebar;
