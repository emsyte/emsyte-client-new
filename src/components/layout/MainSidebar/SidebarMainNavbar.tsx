import React from "react";
import { Button } from "react-bootstrap";
import { X } from "react-bootstrap-icons";
import { Navbar } from "shards-react";
export interface SidebarMainNavbarProps {
  onClose: () => void;
}

const SidebarMainNavbar: React.FC<SidebarMainNavbarProps> = ({ onClose }) => {
  return (
    <div>
      <Navbar
        className="align-items-stretch flex-md-nowrap border-0"
        type="light"
      >
        <div>
          <img
            id="main-logo"
            style={{ maxHeight: "2rem" }}
            src={require("../../../images/Large_Icon.svg")}
            alt="Just Webinar"
          />
          <img
            id="main-logo-text"
            style={{ maxHeight: "2rem" }}
            src={require("../../../images/justWebinarWords.svg")}
            alt="Just Webinar words"
          />
        </div>
        {/* eslint-disable-next-line */}
        <Button onClick={onClose} className="hide-sidebar-button">
          <X aria-hidden="true"></X>
          <span className="sr-only">Close Sidebar</span>
        </Button>
      </Navbar>
    </div>
  );
};

export default SidebarMainNavbar;
