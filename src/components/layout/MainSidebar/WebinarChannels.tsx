import { CalendarEventObject, WebinarType } from "@emsyte/common/webinar";
import moment from "moment";
import "moment-timezone";
import React, { useMemo, useState } from "react";
import {
  Clock as ClockIcon,
  ClockFill as ClockFillIcon,
} from "react-bootstrap-icons";
import { useQuery } from "react-query";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { useHttpService } from "../../../utils/HttpService";
import { useInterval } from "../../webinar/viewer/video/hooks/utils";
const Wrapper = styled.div`
  color: #ffffff;
  border-top: 1px solid rgba(255, 255, 255, 0.3);

  padding: 10px 16px;

  .title {
    font-weight: 500;
    text-transform: uppercase;
    opacity: 0.6;
  }

  .empty-state {
    text-align: center;
    font-weight: 500;
    opacity: 0.8;
  }

  .indicator {
    width: 10px;
    height: 10px;
    border-radius: 100%;
    display: inline-block;
    vertical-align: middle;
  }

  .webinar {
    margin-bottom: 12px;

    .webinar-name {
      opacity: 0.8;
      flex: none;
      order: 1;
      flex-grow: 1;
      margin: 0px 8px;
      color: #ffffff;
    }

    .detail {
      display: flex;
      margin-left: 18px;

      justify-content: space-between;

      .text {
        opacity: 0.48;
      }

      svg {
        opacity: 0.6;
        margin-right: 0.5rem;
        place-self: center;
      }
    }
  }
`;

interface WebinarSkeletonProps {}

const WebinarSkeleton: React.FC<WebinarSkeletonProps> = () => {
  return (
    <div style={{ marginBottom: 20 }}>
      <div className="skeleton text"></div>
      <div style={{ marginLeft: 20 }}>
        <div className="skeleton text" style={{ height: 10 }}></div>
      </div>
    </div>
  );
};

interface WebinarIndicatorProps {
  item: CalendarEventObject;
}

const HONEY = "#F6AE2D";
const TURQUOISE = "#4ECDC4";

const WebinarIndicator: React.FC<WebinarIndicatorProps> = ({ item }) => {
  const color = item.webinar.type === WebinarType.LIVE ? HONEY : TURQUOISE;
  return (
    <div
      className="indicator"
      style={
        moment(item.start).isBefore(moment())
          ? { background: color }
          : { border: "2px solid " + color }
      }
    ></div>
  );
};

interface WebinarItemProps {
  item: CalendarEventObject;
}

const WebinarItem: React.FC<WebinarItemProps> = ({ item }) => {
  const start = moment(item.start);
  const now = moment();
  const isBefore = start.isBefore(moment());
  const Icon = isBefore ? ClockIcon : ClockFillIcon;
  const [duration, setDuration] = useState(
    moment.duration(now.diff(start)).humanize()
  );

  useInterval(() => {
    setDuration(moment.duration(now.diff(start)).humanize());
  }, 60 * 1000);

  return (
    <div className="webinar">
      <div>
        <WebinarIndicator item={item}></WebinarIndicator>
        <Link
          className="webinar-name"
          to={`/events/${btoa(
            JSON.stringify({ eventId: item.id, webinarId: item.webinar.id })
          )}`}
        >
          {item.webinar.name}
        </Link>
      </div>
      <div className="detail">
        <div>
          <Icon size={14}></Icon>
          <span className="text">
            {!isBefore && "Starts in "}
            {duration}
          </span>
        </div>
        <div className="text">{item.attending} attending</div>
      </div>
    </div>
  );
};

export interface WebinarChannelsProps {}

const WebinarChannels: React.FC<WebinarChannelsProps> = () => {
  const api = useHttpService();
  const today = moment();
  const { data, isFetched } = useQuery("today-events", () =>
    api.doGetAllEventsForUser({
      startDate: today.startOf("day").toDate(),
      endDate: today.endOf("day").toDate(),
      includeTime: true,
    })
  );
  const webinars = useMemo(() => {
    const now = moment();

    return (
      data?.data
        .filter((item) => moment(item.end).isAfter(now))
        .sort(
          (a, b) => new Date(b.start).valueOf() - new Date(a.start).valueOf()
        ) ?? []
    );
  }, [data]);

  return (
    <Wrapper>
      <div className="title">Today</div>
      {isFetched ? (
        webinars.length ? (
          webinars.map((item) => (
            <WebinarItem item={item} key={item.id}></WebinarItem>
          ))
        ) : (
          <div className="empty-state">No webinars today</div>
        )
      ) : (
        <div>
          <WebinarSkeleton></WebinarSkeleton>
          <WebinarSkeleton></WebinarSkeleton>
          <WebinarSkeleton></WebinarSkeleton>
        </div>
      )}
    </Wrapper>
  );
};

export default WebinarChannels;
