import PropTypes from "prop-types";
import React from "react";
import { Container, Nav, NavItem, NavLink } from "shards-react";
import colors from "../../utils/colors";

const MainFooter = ({ contained, menuItems, copyright }) => (
  <footer className="main-footer d-flex p-2 px-3">
    <Container fluid={contained}>
      <div className="d-flex align-content-center justify-content-center">
        <span
          className="copyright my-auto"
          style={{ color: colors.gray600.toHex() }}
        >
          {copyright}
        </span>
        <Nav>
          {menuItems.map((item, idx) => (
            <React.Fragment key={idx}>
              <NavItem>
                <NavLink className="nav-link copyright-link" to={item.to}>
                  {item.title}
                </NavLink>
              </NavItem>
              {idx < menuItems.length - 1 && (
                <div
                  className="my-auto"
                  style={{
                    backgroundColor: colors.gray600.toHex(),
                    height: "1rem",
                    width: "0.063rem",
                  }}
                />
              )}
            </React.Fragment>
          ))}
        </Nav>
      </div>
    </Container>
  </footer>
);

MainFooter.propTypes = {
  /**
   * Whether the content is contained, or not.
   */
  contained: PropTypes.bool,
  /**
   * The menu items array.
   */
  menuItems: PropTypes.array,
  /**
   * The copyright info.
   */
  copyright: PropTypes.string,
};

MainFooter.defaultProps = {
  contained: false,
  copyright: `© ${new Date().getFullYear()} Emsyte LLC, All Rights Reserved.`,
  menuItems: [
    {
      title: "Privacy Policy",
      to: "#",
    },
    {
      title: "Terms and Conditions",
      to: "#",
    },
  ],
};

export default MainFooter;
