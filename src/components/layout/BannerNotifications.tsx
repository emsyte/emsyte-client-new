import {
  APIWebinarAddEventVideoUrl,
  CalendarEventObject,
  WebinarType,
} from "@emsyte/common/webinar";
import { faClock } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import moment from "moment";
import "moment-timezone";
import React, { useMemo, useState } from "react";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { Container } from "shards-react";
import styled from "styled-components";
import { useHttpService } from "../../utils/HttpService";
import LoaderButton from "../common/LoaderButton";
import { useInterval } from "../webinar/viewer/video/hooks/utils";

const NewUserModalContainer = styled.div`
  color: #2e3338;
  letter-spacing: 1.2px;
`;

enum NotificationType {
  UPCOMING = "upcoming",
  LIVE = "live",
}
interface BannerNotification {
  type: NotificationType;
  event: CalendarEventObject;
}

interface LiveNotificationProps {
  notification: BannerNotification;
}

const BannerNotificationItem: React.FC<LiveNotificationProps> = ({
  notification,
}) => {
  const [minutesTillStart, setMinutesTillStart] = useState(
    moment(notification.event.start).diff(moment(), "minute")
  );
  const [showLiveStreamModal, setShowLiveStreamModal] = useState(false);
  const [liveStreamURL, setLiveStreamUrl] = useState("");

  const api = useHttpService();
  const queryClient = useQueryClient();

  useInterval(() => {
    setMinutesTillStart(
      moment(notification.event.start).diff(moment(), "minute")
    );
  }, 60 * 1000);

  const saveMut = useMutation(
    (data: APIWebinarAddEventVideoUrl) =>
      api.doSetEventUrl(
        notification.event.webinar.id,
        notification.event.id,
        data
      ),
    {
      async onSuccess() {
        await queryClient.refetchQueries("upcoming-events");
        setShowLiveStreamModal(false);
      },
    }
  );

  return (
    <>
      <Alert
        variant={
          notification.type === NotificationType.LIVE ? "danger" : "warning"
        }
      >
        <Container fluid className="px-4">
          {notification.type === NotificationType.LIVE ? (
            <span>
              Your live stream "{notification.event.webinar.name}" requires a
              URL.
            </span>
          ) : (
            <span>
              Your upcoming live stream "{notification.event.webinar.name}"
              requires a URL.
            </span>
          )}
          <Alert.Link href="#" onClick={() => setShowLiveStreamModal(true)}>
            {" "}
            Add live stream URL
          </Alert.Link>

          {notification.type === NotificationType.UPCOMING && (
            <span className="pull-right bold">
              <FontAwesomeIcon icon={faClock} /> starting in {minutesTillStart}{" "}
              min
            </span>
          )}
        </Container>
      </Alert>

      {/* Live Stream Modal */}
      <Modal
        show={showLiveStreamModal}
        onHide={() => setShowLiveStreamModal(false)}
        centered
      >
        <NewUserModalContainer>
          <Modal.Header closeButton>
            <Modal.Title>Live Stream Settings</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {/* <Alert variant="warning">
              Your sessions requires a live stream URL before you can start your
              webinar.
              <br />
              Attendees will not be admitted to the webinar until the URL is
              added.
            </Alert> */}

            <Form.Group controlId="event_url">
              <Form.Label>Live Stream URL</Form.Label>
              <Form.Control
                value={liveStreamURL}
                name="videoUrl"
                onChange={(event) => setLiveStreamUrl(event.target.value)}
              />
            </Form.Group>

            {/* <a href="#">
                <FontAwesomeIcon icon={faQuestionCircle} />
                How to find this link in your streaming source
              </a> */}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary">Cancel</Button>
            <LoaderButton
              variant="primary"
              disabled={liveStreamURL.length === 0}
              loading={saveMut.isLoading}
              onClick={() =>
                saveMut.mutate({
                  videoUrl: liveStreamURL,
                })
              }
            >
              Save
            </LoaderButton>
          </Modal.Footer>
        </NewUserModalContainer>
      </Modal>
    </>
  );
};

interface LiveStreamNotificationsProps {}

export const LiveStreamNotifications: React.FC<LiveStreamNotificationsProps> =
  () => {
    const api = useHttpService();

    const { data } = useQuery(
      "upcoming-events",
      () =>
        api.doGetAllEventsForUser({
          startDate: moment().subtract(1, "hour").toDate(),
          endDate: moment().add(15, "minutes").toDate(),
          includeTime: true,
        }),
      {
        refetchInterval: 1000 * 60 * 2,
      }
    );

    const notifications: BannerNotification[] = useMemo(() => {
      if (!data) {
        return [];
      }
      return data.data
        .filter(
          (event) =>
            !event.videoUrl &&
            !event.canceled &&
            event.webinar.type === WebinarType.LIVE
        )
        .map((event) => ({
          type: moment(event.start).isBefore(moment())
            ? NotificationType.LIVE
            : NotificationType.UPCOMING,
          event,
        }));
    }, [data]);

    return (
      <>
        {notifications.map((notification) => (
          <BannerNotificationItem
            key={notification.event.id}
            notification={notification}
          ></BannerNotificationItem>
        ))}
      </>
    );
  };
