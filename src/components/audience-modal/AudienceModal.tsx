import { APIResponse } from "@emsyte/common/api";
import moment from "moment";
import React, { useCallback, useEffect, useState } from "react";
import * as Icon from "react-bootstrap-icons";
import {
  Mailbox,
  PersonCircle,
  PhoneLandscape,
  ThreeDotsVertical,
} from "react-bootstrap-icons";
import { Col, ListGroup, ListGroupItem, Row } from "shards-react";
import { secondsToDuration } from "../../utils/duration";
import { formatCurrency, formatDuration } from "../../utils/formatters";

export type AudienceDetailModel = {
  name: string;
  when: string;
  type: "offer_click" | "offer_purchase" | "registration";
};

type Props = {
  attendee: any;
  email: string;
  fetchAudienceDetails: (
    email: string
  ) => Promise<APIResponse<AudienceDetailModel[]>>;
};

const AudienceModal = ({ attendee, email, fetchAudienceDetails }: Props) => {
  const [details, setDetails] = useState<AudienceDetailModel[] | null>(null);

  const handleGetDetail = useCallback(async () => {
    try {
      const response = await fetchAudienceDetails(email);
      setDetails(response.data);
    } catch (error) {
      console.log(error);
    }
  }, [fetchAudienceDetails, email]);

  useEffect(() => {
    const fetchData = async () => await handleGetDetail();
    fetchData();
  }, [handleGetDetail]);

  return (
    <Row>
      <Col xs={11} className="ml-auto mr-auto">
        {details && details.length > 0 ? (
          <>
            <ListGroup small flush className="list-group-small">
              <ListGroupItem className="d-flex px-3">
                <PersonCircle className="text-semibold text-fiord-blue font-size-10" />
                <span className="text-semibold text-fiord-blue px-3">Name</span>
                <span className="ml-auto text-right text-semibold text-reagent-gray">
                  {attendee.name}
                </span>
              </ListGroupItem>
              <ListGroupItem className="d-flex px-3">
                <Mailbox className="text-semibold text-fiord-blue font-size-10" />
                <span className="text-semibold text-fiord-blue px-3">
                  Email
                </span>
                <span className="ml-auto text-right text-semibold text-reagent-gray">
                  {attendee.email}
                </span>
              </ListGroupItem>
              <ListGroupItem className="d-flex px-3">
                <PhoneLandscape className="text-semibold text-fiord-blue font-size-10" />
                <span className="text-semibold text-fiord-blue px-3">
                  Phone
                </span>
                <span className="ml-auto text-right text-semibold text-reagent-gray">
                  {attendee.phone}
                </span>
              </ListGroupItem>
            </ListGroup>
            <hr />
            <span className="text-left">
              {details.map((event) => (
                <ActivityEvent key={event.when} event={event}></ActivityEvent>
              ))}
            </span>
            <hr />
          </>
        ) : (
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        )}
      </Col>
    </Row>
  );
};

export default AudienceModal;

interface ActivityObject {
  when: string;
  name: string;
  duration?: number;
  price?: number;
  type: "offer_click" | "offer_purchase" | "registration" | "watch_webinar";
}

interface ActivityEventProps {
  event: ActivityObject;
}

const ActivityEvent: React.FC<ActivityEventProps> = ({ event }) => {
  if (event.type === "offer_purchase") {
    return <OfferPurchaseActivity event={event} />;
  }
  if (event.type === "offer_click") {
    return <OfferClickActivity event={event} />;
  }
  if (event.type === "registration") {
    return <RegistrationActivity event={event} />;
  }
  if (event.type === "watch_webinar") {
    return <WatchWebinarActivity event={event} />;
  }
  return null;
};

interface OfferPurchaseActivityProps {
  event: ActivityObject;
}

const OfferPurchaseActivity: React.FC<OfferPurchaseActivityProps> = ({
  event,
}) => {
  return (
    <ListGroup small flush className="list-group-small">
      <ListGroupItem>
        <ThreeDotsVertical className="text-semibold text-fiord-blue font-size-18" />
      </ListGroupItem>
      <ListGroupItem className="d-flex px-3">
        <span>
          <Icon.CheckCircle className="text-semibold text-fiord-blue font-size-14" />
        </span>
        <span className="text-semibold text-fiord-blue px-3">
          Purchased Offer
        </span>
        <span className="ml-auto text-right text-semibold text-reagent-gray">
          {moment(event.when).format("YYYY-MM-DD")}
        </span>
        <span className="text-small">
          {moment(event.when).format("hh:mm A")}
        </span>
      </ListGroupItem>
      <ListGroupItem className="d-flex px-3">
        <span className="text-semibold text-fiord-blue px-3">{event.name}</span>
        <span className="ml-auto text-right text-semibold text-reagent-gray">
          {" "}
          Price:{" "}
        </span>
        <span className="text-small">
          {event.price ? formatCurrency(event.price) : "Free"}
        </span>
      </ListGroupItem>
    </ListGroup>
  );
};

interface OfferClickActivityProps {
  event: ActivityObject;
}

const OfferClickActivity: React.FC<OfferClickActivityProps> = ({ event }) => {
  return (
    <ListGroup small flush className="list-group-small">
      <ListGroupItem>
        <ThreeDotsVertical className="text-semibold text-fiord-blue font-size-18" />
      </ListGroupItem>
      <ListGroupItem className="d-flex px-3">
        <span>
          <Icon.CheckCircle className="text-semibold text-fiord-blue font-size-14" />
        </span>
        <span className="text-semibold text-fiord-blue px-3">
          Clicked Offer
        </span>
        <span className="ml-auto text-right text-semibold text-reagent-gray">
          {moment(event.when).format("YYYY-MM-DD")}
        </span>
        <span className="text-small">
          {moment(event.when).format("hh:mm A")}
        </span>
      </ListGroupItem>
      <ListGroupItem className="d-flex px-3">
        <span className="text-semibold text-fiord-blue px-3">{event.name}</span>
      </ListGroupItem>
    </ListGroup>
  );
};

interface RegistrationActivityProps {
  event: ActivityObject;
}

const RegistrationActivity: React.FC<RegistrationActivityProps> = ({
  event,
}) => {
  return (
    <ListGroup small flush className="list-group-small">
      <ListGroupItem className="d-flex px-3">
        <span>
          <Icon.CheckCircle className="text-semibold text-fiord-blue font-size-18" />
        </span>
        <span className="text-semibold text-fiord-blue px-3">Registered</span>
        <span className="ml-auto text-right text-semibold text-reagent-gray">
          {moment(event.when).format("YYYY-MM-DD")}
        </span>
        <span className="text-small">
          &nbsp;&nbsp;{moment(event.when).format("hh:mm A")}
        </span>
      </ListGroupItem>
      <ListGroupItem className="d-flex px-3">
        <span className="text-semibold text-fiord-blue px-3">{event.name}</span>
        <span className="ml-auto text-right text-semibold text-reagent-gray">
          {" "}
          Price
        </span>
        <span className="text-small">
          {event.price ? formatCurrency(event.price) : ":   Free"}
        </span>
      </ListGroupItem>
    </ListGroup>
  );
};

interface WatchWebinarActivityProps {
  event: ActivityObject;
}

const WatchWebinarActivity: React.FC<WatchWebinarActivityProps> = ({
  event,
}) => {
  return (
    <ListGroup small flush className="list-group-small">
      <ListGroupItem>
        <ThreeDotsVertical className="text-semibold text-fiord-blue h5" />
      </ListGroupItem>
      <ListGroupItem className="d-flex px-3">
        <span>
          <Icon.CheckCircle className="text-semibold text-fiord-blue h5" />
        </span>
        <span className="text-semibold text-fiord-blue px-3">
          Watched Webinar
        </span>
        <span className="ml-auto text-right text-semibold text-reagent-gray">
          {moment(event.when).format("YYYY-MM-DD")}
        </span>
        <span className="text-small">
          {moment(event.when).format("hh:mm A")}
        </span>
      </ListGroupItem>
      <ListGroupItem className="d-flex px-3">
        <span className="text-semibold text-fiord-blue px-3">{event.name}</span>
        <span className="ml-auto text-right text-semibold text-reagent-gray">
          {" "}
          Duration:{" "}
        </span>
        <span className="text-small">
          {formatDuration(secondsToDuration(event.duration ?? 0))}
        </span>
      </ListGroupItem>
    </ListGroup>
  );
};
