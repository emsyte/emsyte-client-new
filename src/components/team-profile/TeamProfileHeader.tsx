import React from "react";
import { Nav } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import styled from "styled-components";

const TeamProfileNav = styled(Nav)`
  background-color: #fff;
  margin-bottom: 0;

  padding-top: 2em;

  margin-bottom: 2em;

  .nav-link .nav-link.active {
    background-color: #ccc;
    color: black;

    &:hover,
    &:focus {
      background-color: #f5f5f5;
    }
  }
`;

const teamProfile = "team-settings";

export interface TeamProfileHeaderProps {}

const TeamProfileHeader: React.FC<TeamProfileHeaderProps> = () => {
  return (
    <TeamProfileNav className="settings-nav" fill variant="tabs">
      <Nav.Item>
        <Nav.Link as={NavLink} to={`/${teamProfile}/members`}>
          Team Members
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link as={NavLink} to={`/${teamProfile}/subscription`}>
          Plan &amp; Payment
        </Nav.Link>
      </Nav.Item>
      <Nav.Item>
        <Nav.Link as={NavLink} to={`/${teamProfile}/integrations`}>
          Integrations
        </Nav.Link>
      </Nav.Item>
    </TeamProfileNav>
  );
};

export default TeamProfileHeader;
