import React, { useState, useCallback } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter } from "shards-react";
import LoaderButton from "../common/LoaderButton";

const TeamMemberDeleteModal = ({
  handleTeamMemberFetch,
  memberId,
  member,
  setDeleteId,
  handleDelete,
}) => {
  const [loading, setLoading] = useState<boolean>(false);
  // Invite Team Member
  // TODO: Add Loading on Invite Button
  const onDelete = useCallback(
    async (event) => {
      event.preventDefault();
      try {
        setLoading(true);
        await handleDelete();
        setLoading(false);
      } catch (error) {
        setLoading(false);
        console.log(error);
      }
    },
    [handleDelete]
  );

  return (
    <Modal open={!!member} toggle={() => setDeleteId(-1)}>
      <ModalHeader>
        Delete Team Member
        <br />
      </ModalHeader>
      <ModalBody>
        Do you want to delete member{" "}
        {`${member?.user?.firstName} ${member?.user?.lastName}`}?
      </ModalBody>
      <ModalFooter>
        <LoaderButton
          loading={loading}
          className="mx-auto"
          theme="accent"
          onClick={onDelete}
        >
          Delete
        </LoaderButton>
      </ModalFooter>
    </Modal>
  );
};

export default TeamMemberDeleteModal;
