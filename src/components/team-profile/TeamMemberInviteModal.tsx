import React, { useState, useCallback } from "react";
import { useHttpService } from "../../utils/HttpService";
import { Form } from 'react-bootstrap'

import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "shards-react";

import { TeamUserRole } from "@emsyte/common/team";
import { useNotification } from "../common/Notification";
import LoaderButton from "../common/LoaderButton";

const TeamMemberInviteModal = ({
  handleTeamMemberFetch,
  showInvite,
  setShowInvite,
}) => {
  const api = useHttpService();
  const notification = useNotification();
  const [loading, setLoading] = useState<boolean>(false);
  // Invite Team Member
  const [email, setEmail] = useState<any>();
  // TODO: Add Loading on Invite Button
  const handleInvite = useCallback(
    async (event) => {
      event.preventDefault();
      try {
        setLoading(true);
        await api.doInviteTeamMember({
          email,
          role: TeamUserRole.MEMBER,
        });
        setLoading(false);
        setEmail("");
        setShowInvite(false);

        notification.showMessage({
          title: "Invite Team Member successfully!",
          description: `${email} has been invited!`,
          type: "success",
        });
        await handleTeamMemberFetch();
      } catch (error) {
        setLoading(false);
        notification.showMessage({
          title: "Oops!",
          description:
            error.response?.data?.message ?? "Failed to invite team member",
          type: "error",
        });
        console.log(error);
      }
    },
    [api, email, handleTeamMemberFetch, notification, setShowInvite]
  );

  return (
    <Modal style={{"justify-content": "left"}} open={showInvite} toggle={setShowInvite}>
      <ModalHeader>
        Add a Team Member
      </ModalHeader>
      <ModalBody>
      <Form className="ml-3 mr-3">
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label style={{textAlign: "left"}}>Email Address</Form.Label>
                  <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    id="member-email"
                    onChange={(e) => setEmail(e.target.value)}
                    />
                  <Form.Text className="text-muted">
                    An invite will be sent to this email address.
                  </Form.Text>
              </Form.Group>
            </Form>
      </ModalBody>
      <ModalFooter>
        <LoaderButton
          loading={loading}
          className="mx-auto"
          theme="accent"
          onClick={handleInvite}
        >
          Send Invite
        </LoaderButton>
      </ModalFooter>
    </Modal>
  );
};

export default TeamMemberInviteModal;
