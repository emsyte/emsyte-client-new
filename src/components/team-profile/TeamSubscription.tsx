import React from "react";
import { Container } from "react-bootstrap";
import MainFooter from "../layout/MainFooter";
import SubscriptionPlan from "../user-profile-lite/SubscriptionPlan";
import TeamProfileHeader from "./TeamProfileHeader";

export interface TeamSubscriptionProps {}

const TeamSubscription: React.FC<TeamSubscriptionProps> = () => {
  return (
    <>
      <TeamProfileHeader></TeamProfileHeader>
      <Container>
        <SubscriptionPlan />
      </Container>
      <MainFooter />
    </>
  );
};

export default TeamSubscription;
