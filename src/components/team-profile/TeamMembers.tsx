import { TeamUserStatus } from "@emsyte/common/team";
import React, { useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { useQuery } from "react-query";
import { Badge, Button, ButtonGroup, Col, Container, Row } from "shards-react";
import styled from "styled-components";
import { useHttpService } from "../../utils/HttpService";
import { useNotification } from "../common/Notification";
import Indicator from "../Indicator";
import { SkeletonTable } from "../skeletons/SkeletonTable";
import TeamMemberDeleteModal from "./TeamMemberDeleteModal";
import TeamMemberEditModal from "./TeamMemberEditModal";
import TeamMemberInviteModal from "./TeamMemberInviteModal";

const Styles = styled.div`
  .rt-table {
    border-spacing: 0;
    border: none;

    .rt-tr {
      :last-child {
        .rt-td {
          border-bottom: 1;
        }
      }
    }

    .rt-th,
    .rt-td {
      padding: 0.5rem;
      align-items: center;
      align-self: stretch;
      text-align: left;
      justify-content: left;
      border: none;

      :last-child {
        border-right: 0;
      }
    }

    .rt-td {
      line-height: 24px;
    }

    .rt-th {
      background-color: #e9ecef;
      color: #2e3338;
      flex-direction: row;
      padding-top: 12px;
      padding-bottom: 12px;
      border-bottom: none;
      font-weight: bold;
      font-size: 16px !important;
    }
  }
`;

const TeamMembers = () => {
  const api = useHttpService();
  const notification = useNotification();
  const [pageSize, setPageSize] = useState(5);
  const [selectedId, setSelectedId] = useState<number>(-1);
  const [deleteId, setDeleteId] = useState<number>(-1);
  const [toggle, setToggle] = useState(false);

  // TODO: Global Error Handling

  const { data, isFetched, refetch } = useQuery("team-members", () =>
    api.doGetTeamMembers()
  );

  const teamMembers = data?.data ?? [];

  useEffect(() => {
    setPageSize(teamMembers.length);
  }, [teamMembers.length]);

  // TODO: Add Loading on Delete Button
  const handleDelete = async () => {
    try {
      await api.doRemoveTeamMember(deleteId);
      const newTeamMembers = teamMembers.filter(
        (e, index) => index !== deleteId
      );
      setPageSize(newTeamMembers.length);
      // setTeamMembers(newTeamMembers);
      setDeleteId(-1);

      notification.showMessage({
        title: "Delete Team Member successfully!",
        description: `Team Member has been deleted!`,
        type: "success",
      });
      await refetch();
    } catch (error) {
      notification.showMessage({
        title: "Oops!",
        description:
          error.response?.data?.message ?? "Failed to invite team member",
        type: "error",
      });
    }
  };

  if (!isFetched) {
    return <SkeletonTable />;
  }

  return (
    <>
      {teamMembers.length ? (
        <Styles>
          <Container fluid className="file-manager__filters border-bottom">
            <Row className="mt-2">
              {/* Invite: Member */}
              <Button onClick={setToggle}>Add Team Member</Button>
            </Row>
            <Row className="mt-4">
              <Table>
                <thead>
                  <tr>
                    <th>Role</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Status</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {teamMembers.map((member) => (
                    <tr key={member.user.id}>
                      <td>{member.role}</td>
                      <td>
                        {member.user.firstName} {member.user.lastName}
                      </td>
                      <td>{member.user.email}</td>
                      <td>
                        {member.status === TeamUserStatus.ACTIVATED ? (
                          <>
                            <Indicator variant="success"></Indicator>
                            Activated
                          </>
                        ) : (
                          <>
                            <Indicator variant="secondary"></Indicator>
                            Not Accepted
                          </>
                        )}
                      </td>
                      <td>
                        <ButtonGroup size="sm">
                          <Button
                            outline
                            className="nohover"
                            theme="white"
                            onClick={() => setDeleteId(member.user.id)}
                          >
                            <i className="material-icons text-dark">&#xE872;</i>
                          </Button>
                          <Button
                            outline
                            className="nohover"
                            theme="white"
                            onClick={() => setSelectedId(member.user.id)}
                          >
                            <i className="material-icons text-dark">edit</i>
                          </Button>
                        </ButtonGroup>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Row>
          </Container>
        </Styles>
      ) : (
        <Row>
          <Col className="text-center">
            <h6>Looks, like you do not have a team yet.</h6>
            <Button onClick={setToggle}>Add Team Member</Button>
          </Col>
        </Row>
      )}

      <TeamMemberEditModal
        setSelectedId={setSelectedId}
        member={teamMembers?.find((e) => e.user.id === selectedId)}
        memberId={selectedId}
        handleTeamMemberFetch={refetch}
      />

      <TeamMemberInviteModal
        setShowInvite={setToggle}
        showInvite={toggle}
        handleTeamMemberFetch={refetch}
      />

      <TeamMemberDeleteModal
        setDeleteId={setDeleteId}
        memberId={deleteId}
        member={teamMembers?.find((e) => e.user.id === deleteId)}
        handleTeamMemberFetch={refetch}
        handleDelete={handleDelete}
      />
    </>
  );
};

export default TeamMembers;
