import React from "react";
import { Container } from "react-bootstrap";
import MainFooter from "../layout/MainFooter";
import Integrations from "../user-profile-lite/Integrations/Integrations";
import TeamProfileHeader from "./TeamProfileHeader";

export interface TeamIntegrationsProps {}

const TeamIntegrations: React.FC<TeamIntegrationsProps> = () => {
  return (
    <>
      <TeamProfileHeader></TeamProfileHeader>
      <Container>
        <Integrations />
      </Container>
      <MainFooter />
    </>
  );
};

export default TeamIntegrations;
