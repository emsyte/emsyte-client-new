import React, { useState, useCallback } from "react";

import { useHttpService } from "../../utils/HttpService";

import {
  Row,
  Col,
  Form,
  FormGroup,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormSelect,
} from "shards-react";
import { TeamUserRole } from "@emsyte/common/team";
import { useNotification } from "../common/Notification";
import LoaderButton from "../common/LoaderButton";

const TeamMemberEditModal = ({
  member,
  setSelectedId,
  memberId,
  handleTeamMemberFetch,
}) => {
  const api = useHttpService();
  const notification = useNotification();
  const [loading, setLoading] = useState<boolean>(false);
  const [role, setRole] = useState<TeamUserRole>(
    member?.role || TeamUserRole.MEMBER
  );

  const handleChange = (e) => {
    console.log(e.currentTarget.value);
    setRole(e.currentTarget.value);
  };

  // TODO: Add Loading on Invite Button
  const handleUpdate = useCallback(
    async (event) => {
      event.preventDefault();
      try {
        setLoading(true);
        await api.doEditTeamMember(memberId, {
          role,
        });
        setSelectedId(-1);

        notification.showMessage({
          title: "Edit Team Member successfully!",
          description: " Team Member has been updated!",
          type: "success",
        });
        await handleTeamMemberFetch();
        setLoading(false);
      } catch (error) {
        notification.showMessage({
          title: "Oops!",
          description: error.response?.data?.message ?? "Failed to update role",
          type: "error",
        });
        setLoading(false);
        console.log(error);
      }
    },
    [api, handleTeamMemberFetch, memberId, notification, role, setSelectedId]
  );

  return (
    <Modal open={!!member} toggle={() => setSelectedId(-1)}>
      <ModalHeader>
        Edit Member Role
        <br />
        <small>Update role for member of team.</small>
      </ModalHeader>
      <ModalBody>
        <Form>
          <Row row>
            <Col sm="12" md="12">
              <label htmlFor="inputEmailAddress">Role</label>
              <FormGroup>
                <FormSelect value={role} onChange={handleChange}>
                  <option value={TeamUserRole.ADMIN}>
                    {TeamUserRole.ADMIN}
                  </option>
                  <option value={TeamUserRole.OWNER}>
                    {TeamUserRole.OWNER}
                  </option>
                  <option value={TeamUserRole.MEMBER}>
                    {TeamUserRole.MEMBER}
                  </option>
                </FormSelect>
              </FormGroup>
            </Col>
          </Row>
        </Form>
      </ModalBody>
      <ModalFooter>
        <LoaderButton
          loading={loading}
          className="mx-auto"
          theme="accent"
          onClick={handleUpdate}
        >
          Update Role
        </LoaderButton>
      </ModalFooter>
    </Modal>
  );
};

export default TeamMemberEditModal;
