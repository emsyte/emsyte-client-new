import React, { useEffect, useRef, useState } from "react";
import { Card, Container, Form } from "react-bootstrap";
import { useAuth } from "../../utils/Authentication";
import LoaderButton from "../common/LoaderButton";
import MainFooter from "../layout/MainFooter";
import SaveTray from "../layout/SaveTray";
import TeamMembers from "./TeamMembers";
import TeamProfileHeader from "./TeamProfileHeader";
import styled from "styled-components";
import { useForm } from "react-hook-form";
import { useMutation } from "react-query";
import { useHttpService } from "../../utils/HttpService";

const Wrapper = styled.div`
  width: 100%;
  height: 100%;

  display: flex;
  flex-direction: column;
  overflow: hidden;

  .content {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    overflow: auto;
  }
`;

const GeneralTeamProfile = () => {
  const { currentTeam, teams, setTeams } = useAuth();
  const currentTeamData = teams.find((t) => t.team.slug === currentTeam);
  const api = useHttpService();

  const teamNameMutation = useMutation(api.doEditTeam, {
    onSuccess({ data }) {
      setTeams(
        teams.map((team) =>
          team.team.slug === data.slug
            ? {
                ...team,
                team: data,
              }
            : team
        )
      );
    },
  });

  const formRef = useRef<HTMLFormElement | null>(null);

  const { register, handleSubmit, getValues } = useForm({
    defaultValues: {
      name: currentTeamData?.team?.name || "",
    },
  });

  const doSubmit = handleSubmit((values) => teamNameMutation.mutate(values));

  return (
    <Wrapper>
      <div className="content">
        <TeamProfileHeader></TeamProfileHeader>
        <Container>
          <Card className="mb-4 border-bottom">
            <Card.Body>
              <Form className="border-bottom" ref={formRef} onSubmit={doSubmit}>
                <Form.Group className="mb-3" controlId="formBasicEmail">
                  <Form.Label>Team</Form.Label>
                  <Form.Control
                    disabled={teamNameMutation.isLoading}
                    type="text"
                    placeholder="Team Name"
                    name="name"
                    ref={register()}
                  />
                </Form.Group>
              </Form>
              <div className="border-bottom">
                <TeamMembers />
              </div>
            </Card.Body>
          </Card>
        </Container>

        <MainFooter />
      </div>

      <SaveTray>
        <LoaderButton
          loading={teamNameMutation.isLoading}
          variant="secondary"
          style={{ marginRight: "0.675rem", fontSize: "1rem" }}
          onClick={() => teamNameMutation.mutate(getValues())}
        >
          Save Changes
        </LoaderButton>
      </SaveTray>
    </Wrapper>
  );
};

export default GeneralTeamProfile;
