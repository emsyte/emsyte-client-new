import { ViewerEventObject } from "@emsyte/common/webinar";
import React, { useEffect, useMemo, useRef, useState } from "react";
import styled from "styled-components";
import moment from "moment";
import ReactPlayer from "react-player";
import { useInterval } from "../webinar/viewer/video/hooks/utils";

const ChartWrapper = styled.div`
  display: flex;
  background-color: white;
  width: 100%;
  height: 11.5rem;
  border: 1px solid #ddd;
  border-radius: 4px;
  overflow: hidden;

  button {
    padding: 0.25rem 0.5rem;
    font-size: 0.8rem;
    border: none;
  }
  button:first-child {
    border-radius: 4px 0px 0px 4px;
    background-color: #f0f0f0;
  }
  button:last-child {
    color: white;
    background-color: #007bff;
    border-radius: 0px 4px 4px 0px;
  }

  .live {
    position: absolute;
    left: 0.5rem;
    top: 0.5rem;
    padding: 0.175rem 0.25rem;
    border-radius: 4px;
    background: red;
    font-weight: bold;
    font-size: 0.8rem;
    color: white;
  }
`;

const Chart = ({
  event,
  getBarsData,
  barsData,
}: {
  event: ViewerEventObject;
  getBarsData: () => void;
  barsData: Array<{ count: number; question: boolean }>;
}) => {
  const [reactPlayer, setReactPlayer] = useState<ReactPlayer | null>();

  const [bars, setBars] = useState<Array<{ count: number; question: boolean }>>(
    []
  );
  const [barsCount, setBarsCount] = useState<number>(1);
  const [needlePercent, setNeedlePercent] = useState<number>(0);
  const [highestCount, setHighestCount] = useState(0);

  const containerRef = useRef<HTMLDivElement | null>(null);

  const eventStart = useMemo(() => moment(event.start), [event]);
  const eventEnd = useMemo(() => moment(event.end), [event]);
  const isLive = moment().isAfter(event.start) && !moment().isAfter(event.end);
  const timeSinceEventStart = moment(moment()).diff(event.start);

  useInterval(() => {
    const element = containerRef.current;
    if (element) {
      const count = element.getBoundingClientRect().width / 7;
      setBarsCount(Math.floor(count));

      const diff =
        moment().diff(eventStart, "seconds") /
        eventEnd.diff(eventStart, "seconds");
      setNeedlePercent(Math.max(0, Math.min(diff, 1)));
    }

    if (isLive && reactPlayer) {
      const videoTime = moment.duration(timeSinceEventStart).asSeconds();
      const syncError = videoTime - reactPlayer?.getCurrentTime();

      if (Math.abs(syncError) > 5) {
        console.log("Fixing drift");
        reactPlayer.seekTo(videoTime, "seconds");
      }
    }
  }, 1000);

  // useEffect(() => {
  //   if (isLive) {
  //     const ms = eventEnd.diff(eventStart, "milliseconds") / barsCount;
  //     const intervalId = setInterval(() => {
  //       getBarsData();
  //     }, ms);
  //     return () => {
  //       clearInterval(intervalId);
  //     };
  //   } else {
  //     getBarsData();
  //   }
  // }, [isLive, eventStart, eventEnd, barsCount, getBarsData]);

  useEffect(() => {
    const receivedCount = barsData.length;
    const ratio = receivedCount / barsCount;
    const processedBarsData = Array(barsCount)
      .fill("")
      .map((_, i) => {
        const lower = Math.floor(i * ratio);
        const upper = Math.floor((i + 1) * ratio);
        const slice = barsData.slice(lower, upper);
        return slice.reduce(
          (ac, v) => ({
            count: ac.count + v.count,
            question: ac.question || v.question,
          }),
          { count: 0, question: false }
        );
      });
    setHighestCount(
      processedBarsData.reduce((ac, v) => Math.max(ac, v.count), 0)
    );
    setBars(processedBarsData);
  }, [barsData, barsCount]);

  return (
    <ChartWrapper>
      <div style={{ minWidth: "20rem", position: "relative" }}>
        <ReactPlayer
          ref={setReactPlayer}
          controls
          width="100%"
          height="100%"
          url={event.videoUrl}
          playing={isLive}
        ></ReactPlayer>
        {isLive && <span className="live">LIVE</span>}
      </div>
      <div
        style={{
          flex: 1,
          display: "flex",
          flexDirection: "column",
          padding: "0.75rem 1.25rem",
          width: 0,
        }}
      >
        {/* <div style={{ alignSelf: "flex-end" }}>
          <button>Engagement</button>
          <button>Discussion</button>
        </div> */}
        <div
          ref={containerRef}
          style={{
            flex: 1,
            display: "flex",
            flexDirection: "column",
            position: "relative",
          }}
        >
          <div // Red Needle
            style={{
              position: "absolute",
              background: "red",
              width: "2px",
              height: "86%",
              top: "2px",
              left: `${needlePercent * 100}%`, //63%
              zIndex: 3,
            }}
          ></div>
          <div
            style={{
              flex: 1,
              display: "flex",
              alignItems: "flex-end",
              zIndex: 2,
              borderBottom: "2px solid rgb(134, 142, 150, 0.5)",
              paddingBottom: "1px",
              minWidth: 0,
            }}
          >
            {bars.map((_, i) => (
              <div
                key={i}
                style={{
                  height: `${
                    i / barsCount < needlePercent
                      ? 5 + (bars[i].count / highestCount) * 95.0
                      : 5
                  }%`,
                  width: "4px",
                  background:
                    i / barsCount < needlePercent
                      ? bars[i].question
                        ? "#007BFF"
                        : "rgb(178, 207, 211)"
                      : "rgb(238, 239, 240)",
                  borderRadius: "2px",
                  marginRight: "3px",
                }}
              ></div>
            ))}
          </div>
          <div
            style={{
              height: "1rem",
              fontSize: "0.7rem",
              fontWeight: 400,
              position: "relative",
            }}
          >
            <span style={{ position: "absolute", left: "0" }}>00:00:00</span>
            {isLive && (
              <span
                style={{
                  position: "absolute",
                  left: `${needlePercent * 100}%`,
                  transform: "translateX(-50%)",
                  zIndex: 1,
                  background: "white",
                  boxShadow: "white 0px 0px 8px 8px",
                }}
              >
                {moment.utc(timeSinceEventStart).format("HH:mm:ss")}
              </span>
            )}
            <span style={{ position: "absolute", right: "0" }}>
              {moment
                .utc(moment(event.end).diff(event.start))
                .format("HH:mm:ss")}
            </span>
          </div>
        </div>
      </div>
    </ChartWrapper>
  );
};

export default Chart;
