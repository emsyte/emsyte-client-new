import clsx from "clsx";
import moment from "moment";
import "moment-timezone";
import React from "react";
import { Button, Spinner } from "react-bootstrap";
import { scroller } from "react-scroll";
import { Col, Row } from "shards-react";
import styled from "styled-components";
import CreationButtonGroup from "../../webinar/quick-links/CreationButtonGroup";

const WebinarsTitle = styled.h3`
  font-style: normal;
  font-weight: 500;
  font-size: 28px;
  line-height: 24px;
  letter-spacing: 1.2px;
`;

const LoadPastSessionsButton = styled.button`
  background: #ffffff !important;

  border: 1px solid #007bff !important;
  color: #007bff !important;
  border-radius: 44px !important;
`;

const Toolbar =
  ({ handleViewChange, loading, eventsQuery, page, setPage }) =>
  (props) => {
    let {
      localizer: { messages },
      label,
    } = props;

    const navigate = (action) => {
      props.onNavigate(action);
    };

    const handleView = (view) => {
      handleViewChange(view);
      props.onView(view);
    };

    const ViewGroupIcon = (messages, name) => {
      switch (name) {
        case "agenda":
          return (
            <div style={{ display: "block", transform: "rotate(90deg)" }}>
              <i className="bi bi-collection-fill" />
            </div>
          );
        case "day":
          return <i className="fas fa-bars" />;
        case "week":
          return <i className="bi bi-grid-3x3-gap-fill" />;
        default:
          return messages[name];
      }
    };

    const viewNamesGroup = (messages) => {
      // let viewNames = props.views;
      let viewNames = ["agenda", "day", "week"];
      const view = props.view;

      if (viewNames.length > 1) {
        return viewNames.map((name) => (
          <Button
            key={name}
            id={"view-" + name}
            className={clsx({ "rbc-active": view === name })}
            onClick={() => handleView(name)}
          >
            {ViewGroupIcon(messages, name)}
          </Button>
        ));
      }
    };

    return (
      <>
        <div className="rbc-toolbar mt-4">
          <Row className="w-100">
            <CreationButtonGroup />
            <Col className="text-left pl-0">
              {props.view === "week" && (
                <>
                  <Button
                    size="sm"
                    className="shadow-none"
                    onClick={() => navigate("PREV")}
                  >
                    <i className="fas fa-chevron-left text-muted"></i>
                  </Button>
                  <Button
                    size="sm"
                    className="border-0 shadow-none"
                    onClick={() => navigate("NEXT")}
                  >
                    <i className="fas fa-chevron-right text-muted"></i>
                  </Button>
                  <span className="rbc-toolbar-label">{label}</span>
                </>
              )}
              {props.view === "agenda" && (
                <>
                  <span className="rbc-toolbar-label">All Webinars</span>
                </>
              )}
            </Col>
            <Col className="text-center">
              {props.view === "week" && (
                <div>
                  <span className="text-muted">
                    Timezone: {moment.tz.guess().replace("_", " ")}
                  </span>
                  {loading && <Spinner animation="border"></Spinner>}
                </div>
              )}
              {props.view === "day" && (
                <div>
                  <LoadPastSessionsButton
                    onClick={() => {
                      eventsQuery.fetchNextPage({ pageParam: page });
                      setPage(page + 1);
                    }}
                  >
                    Load Past Sessions
                  </LoadPastSessionsButton>
                </div>
              )}
            </Col>
            <Col className="text-right">
              {props.view === "week" && (
                <span className="rbc-btn-group">
                  <Button onClick={() => navigate("TODAY")}>
                    {messages.today}
                  </Button>
                </span>
              )}
              {props.view === "day" && (
                <span className="rbc-btn-group">
                  <Button
                    onClick={() => {
                      scroller.scrollTo("today-background", {
                        duration: 800,
                        delay: 0,
                        smooth: "easeInOutQuart",
                      });
                    }}
                    variant="secondary"
                    style={{
                      fontSize: "1rem",
                      paddingTop: "0.25rem",
                      paddingBottom: "0.25rem",
                      fontWeight: 500,
                    }}
                  >
                    {messages.today}
                  </Button>
                </span>
              )}

              <span className="rbc-btn-group">{viewNamesGroup(messages)}</span>
            </Col>
          </Row>
        </div>
      </>
    );
  };

export default Toolbar;
