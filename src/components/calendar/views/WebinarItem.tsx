import { WebinarObject, WebinarStatus } from "@emsyte/common/webinar";
import React, { useEffect, useState } from "react";
import {
  Alert,
  Button,
  Modal,
  OverlayTrigger,
  Spinner,
  Tooltip,
} from "react-bootstrap";
import { Link } from "react-router-dom";
import { Card, CardBody, Row } from "shards-react";
import { Badge } from "react-bootstrap";
import { useHttpService } from "../../../utils/HttpService";
import LoaderButton from "../../common/LoaderButton";
import { useNotification } from "../../common/Notification";
import EmptyImg from "../../../images/EmptyImg.svg";
import { Archive, Upload } from "react-bootstrap-icons";
import styled from "styled-components";
import { useMutation, useQueryClient } from "react-query";
import { AxiosError } from "axios";

const ImgContainer = styled.div`
  width: 100%;
  padding-top: ${(1 / 1.91) * 100}%;

  position: relative;

  .center-layer {
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    display: flex;

    align-items: center;
    justify-content: center;
  }

  .thumbnail {
    position: absolute;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;

    background-size: cover;
  }
`;

interface WebinarThumbnailProps {
  thumbnail?: string;
}

const WebinarThumbnail: React.FC<WebinarThumbnailProps> = ({ thumbnail }) => {
  const [showFallbackImage, setShowFallbackImage] = useState(false);

  useEffect(() => {
    if (!thumbnail) {
      setShowFallbackImage(true);
      return;
    }

    const image = new Image();
    image.src = thumbnail;

    image.onerror = () => {
      setShowFallbackImage(true);
    };
  }, [thumbnail]);

  return (
    <ImgContainer>
      {showFallbackImage ? (
        <div className="center-layer">
          <img src={EmptyImg} alt="" width="50" />
        </div>
      ) : (
        <div className="center-layer">
          <Spinner animation="border"></Spinner>
        </div>
      )}

      <div
        className="thumbnail"
        data-thumbnail={thumbnail}
        style={{
          backgroundImage: `url("${thumbnail}")`,
        }}
      ></div>
    </ImgContainer>
  );
};

function getWebinarId(webinar: WebinarObject): string {
  let buff = Buffer.from(JSON.stringify({ id: webinar.id }));
  return buff.toString("base64");
}

function getDetailRoute(webinar: WebinarObject) {
  return `/webinars/${getWebinarId(webinar)}/basic`;
}

function getAnalyticsRoute(webinar: WebinarObject) {
  return `/analytics/${getWebinarId(webinar)}`;
}

interface WebinarStatusPillProps {
  status: WebinarStatus;
}

const WebinarStatusPill: React.FC<WebinarStatusPillProps> = ({ status }) => {
  const variant =
    status === WebinarStatus.ACTIVE
      ? "success"
      : status === WebinarStatus.DRAFT
      ? "secondary"
      : "dark";

  return (
    <Badge variant={variant} className={`card-post__category`}>
      {status}
    </Badge>
  );
};

interface WebinarItemProps {
  webinar: WebinarObject;
}

const WebinarItem: React.FC<WebinarItemProps> = ({ webinar }) => {
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const api = useHttpService();
  const [deleting, setDeleting] = useState(false);
  const notification = useNotification();
  const queryClient = useQueryClient();

  const handleDelete = async () => {
    try {
      setDeleting(true);
      await api.doDeleteWebinar(webinar.id);
      setShowDeleteModal(false);
      notification.showMessage({
        title: "Webinar deleted successfully",
        type: "success",
      });
    } catch (error) {
      notification.showMessage({
        title: "Failed to delete webinar",
        description: error?.response?.data?.message ?? error.message,
        type: "error",
      });
    } finally {
      setDeleting(false);
    }
  };

  const archiveMut = useMutation(api.doArchiveWebinar, {
    async onSuccess(result) {
      await queryClient.invalidateQueries("webinars");

      notification.showMessage({
        title: "Webinar archived",
        type: "success",
      });
    },
    onError(error: AxiosError) {
      notification.showMessage({
        title: "Failed to archive webinar",
        description: error.response?.data.message ?? error.message,
        type: "error",
      });
    },
  });
  const activateMut = useMutation(api.doActivateWebinar, {
    async onSuccess(result) {
      await queryClient.invalidateQueries("webinars");

      notification.showMessage({
        title: "Webinar activated",
        type: "success",
      });
    },
    onError(error: AxiosError) {
      notification.showMessage({
        title: "Failed to activate webinar",
        description: error.response?.data.message ?? error.message,
        type: "error",
      });
    },
  });

  return (
    <Card
      small
      className="card-post card-post--aside card-post--1"
      data-testid={`webinar-${webinar.id}`}
    >
      <div>
        <WebinarThumbnail thumbnail={webinar.thumbnail}></WebinarThumbnail>
        <WebinarStatusPill status={webinar.status}></WebinarStatusPill>

        {/* <div className="card-post__author d-flex">
            <a
              href="#"
              className="card-post__author-avatar card-post__author-avatar--small"
              style={{ backgroundImage: `url('${webinar.authorAvatar}')` }}
            ></a>
          </div> */}
      </div>
      <CardBody>
        <h5 className="card-title">
          <span className="text-fiord-blue" data-testid="webinar-name">
            {webinar.name}
          </span>
        </h5>
        <p className="card-text d-inline-block mb-3">{webinar.description}</p>
        <br />
        {/* Schedule: <br />
        <span className="text-muted">{handleScheduleFormat(webinar)}</span> */}
        <Row className="d-flex mt-4">
          <Link
            to={getDetailRoute(webinar)}
            style={{ flex: 1 }}
            className="btn btn-primary d-block"
          >
            Modify
          </Link>
          <OverlayTrigger
            overlay={
              <Tooltip id={`webinar-analytics-${webinar.id}`}>
                View Analytics
              </Tooltip>
            }
          >
            <Link
              to={getAnalyticsRoute(webinar)}
              className="btn btn-secondary ml-2"
            >
              <i className="fas fa-chart-line"></i>
            </Link>
          </OverlayTrigger>
          {webinar.status === WebinarStatus.ACTIVE && (
            <OverlayTrigger
              overlay={
                <Tooltip id={`archive-webinar-${webinar.id}`}>Archive</Tooltip>
              }
            >
              <LoaderButton
                className="ml-2"
                variant="danger"
                onClick={() => archiveMut.mutate(webinar.id)}
                loading={archiveMut.isLoading}
              >
                <Archive></Archive>
              </LoaderButton>
            </OverlayTrigger>
          )}
          {webinar.status === WebinarStatus.DRAFT && (
            <OverlayTrigger
              overlay={
                <Tooltip id={`activate-webinar-${webinar.id}`}>
                  Activate
                </Tooltip>
              }
            >
              <LoaderButton
                className="ml-2"
                variant="success"
                onClick={() => activateMut.mutate(webinar.id)}
                loading={activateMut.isLoading}
              >
                <Upload></Upload>
              </LoaderButton>
            </OverlayTrigger>
          )}
        </Row>
      </CardBody>
      <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)}>
        <Modal.Header>
          <Modal.Title>Delete Webinar?</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Alert>Are you sure you want to delete this webinar?</Alert>
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="secondary"
            onClick={() => setShowDeleteModal(false)}
            className="mr-auto"
          >
            Cancel
          </Button>
          <LoaderButton
            loading={deleting}
            theme="danger"
            onClick={handleDelete}
          >
            Delete
          </LoaderButton>
        </Modal.Footer>
      </Modal>
    </Card>
  );
};
WebinarItem.propTypes = {};

export default WebinarItem;
