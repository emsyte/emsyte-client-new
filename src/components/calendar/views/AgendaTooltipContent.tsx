import { WebinarObject } from "@emsyte/common/webinar";
import moment from "moment";
import React, { useState } from "react";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { secondsToDuration } from "../../../utils/duration";
import { formatDuration } from "../../../utils/formatters";
import EmptyImg from "../../../images/EmptyImg.svg";
import { useFeatureFlag } from "../../../utils/FeatureFlags";

const ImgContainer = styled.div`
  display: flex;
  height: ${276 * (200 / 300)}px;
  justify-content: center;
  align-items: center;

  img {
    width: 50px;
  }
`;

const TooltipWrapper = styled.div`
  border-radius: 5px;
  width: 276px;

  .image {
    width: 100%;
    min-height: ${276 * (200 / 300)}px;
    object-fit: cover;
    border-radius: 10px 10px 0px 0px;
  }
  .tooltip-content {
    padding: 10px;
    flex: 1;
    i {
      margin-right: 5px;
    }
    &_time {
      margin-top: 5px;
    }
  }

  .tooltip-header {
    font-size: 20px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
  .duration {
    color: #007bff;
    font-size: 16px;
    path {
      fill: #007bff;
    }
  }
`;

interface AgendaTooltipContentProps {
  webinar: WebinarObject;
  eventId: number;
  startTime: Date;
  endTime: Date;
}

const AgendaTooltipContent = ({
  webinar,
  eventId,
  startTime,
  endTime,
}: AgendaTooltipContentProps) => {
  const [showFallbackImage, setShowFallbackImage] = useState(false);
  const webinarSlug = btoa(JSON.stringify({ id: webinar.id }));
  const liveChatEnabled = useFeatureFlag("live_chat");
  return (
    <TooltipWrapper>
      {showFallbackImage ? (
        <ImgContainer>
          <img src={EmptyImg} alt="" />
        </ImgContainer>
      ) : (
        <img
          onError={(e) => {
            setShowFallbackImage(true);
          }}
          src={webinar.thumbnail}
          alt=""
          className="image"
        />
      )}
      <div className="tooltip-content">
        <div className="tooltip-header pr-10 pl-10">
          <b>
            <Button variant="link" className="text-fiord-blue">
              {webinar.name}
            </Button>
          </b>
          <div className="duration">
            <i className="fas fa-hourglass-half"></i>
            {` ${formatDuration(secondsToDuration(webinar.duration))}`}
          </div>
        </div>
        <div className="tooltip-content_time">
          <i className="far fa-clock"></i>
          {` ${moment(startTime).format("hh:mm A")} to ${moment(endTime).format(
            "hh:mm A"
          )}`}
        </div>
        <div className="d-flex">
          <Link
            to={`/webinars/${webinarSlug}/basic`}
            className="btn btn-primary mr-auto"
          >
            Edit Webinar
          </Link>

          {liveChatEnabled && (
            <Link
              to={`/events/${btoa(
                JSON.stringify({ webinarId: webinar.id, eventId: eventId })
              )}`}
              className="btn btn-secondary"
            >
              Moderator View
            </Link>
          )}
        </div>
      </div>
    </TooltipWrapper>
  );
};
AgendaTooltipContent.propTypes = {};

export default AgendaTooltipContent;
