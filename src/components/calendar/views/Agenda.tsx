import { WebinarObject } from "@emsyte/common/webinar";
import "moment-timezone";
import React from "react";
import { Spinner } from "react-bootstrap";
import { useQuery } from "react-query";
import { Col, Row } from "shards-react";
import styled from "styled-components";
import EmptyState from "../../../images/empty-state.svg";
import { useHttpService } from "../../../utils/HttpService";
import CreationButtonGroup from "../../webinar/quick-links/CreationButtonGroup";
import WebinarItem from "./WebinarItem";

const Container = styled.div``;

export const AgendaLoading = () => (
  <Row>
    <Col className="text-center mt-5">
      <h1>Loading Webinars</h1>
      <Spinner as="span" animation="border" role="status" aria-hidden="true" />
    </Col>
  </Row>
);

interface AgendaListProps {
  webinars: WebinarObject[];
}

const AgendaList: React.FC<AgendaListProps> = ({ webinars }) => {
  return (
    <>
      {webinars && webinars.length ? (
        <Container className="rbc-event-card mb-3">
          <Row className="">
            {webinars.map((webinar, idx) => (
              <Col key={webinar.id} lg="4" md="6" sm="12" className="mb-4">
                <WebinarItem webinar={webinar} />
              </Col>
            ))}
          </Row>
        </Container>
      ) : (
        <Container key={`000`} className="rbc-event-card">
          <Row>
            <Col md="8" className="text-center mt-5">
              <img
                width="500px"
                src={EmptyState}
                alt="React Logo"
                className="rounded"
              />
            </Col>
            <Col md="4" className="text-left align-self-center">
              <h1>Oops!</h1>
              <h6>
                No webinars found <br />
              </h6>
              <CreationButtonGroup />
            </Col>
          </Row>
        </Container>
      )}
    </>
  );
};

const Agenda = () => {
  const api = useHttpService();

  const { data: webinars, isLoading } = useQuery(
    "webinars",
    api.doGetAllWebinarsForUser
  );

  return isLoading ? (
    <AgendaLoading></AgendaLoading>
  ) : (
    <AgendaList webinars={webinars!.data}></AgendaList>
  );
};

Agenda.navigate = (date, action) => new Date();

Agenda.title = (date) => "";

export default Agenda;
