import { CalendarEventObject, WebinarType } from "@emsyte/common/webinar";
import moment from "moment";
import "moment-timezone";
import React from "react";
import { Badge, Dropdown } from "react-bootstrap";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { ANALYTIC_VIEWS } from "../../../views/WebinarAnalytics";
import { BroadcastPin, Calendar2Event } from "react-bootstrap-icons";

enum EventItemType {
  LIVE_NOW = "live_now",
  LIVE_SOON = "live_soon",
  PRE_RECORDED = "pre_recorded",
}

const EventRow = styled.div`
  margin-bottom: 0.625rem;
  margin-left: 0.75rem;

  border-radius: 5px;
  border: 1px solid rgba(134, 142, 150, 0.5);

  &.${EventItemType.LIVE_NOW} {
    background: rgba(0, 123, 255, 0.1);
  }

  .inner {
    border-radius: 4px;
    border-left-width: 6px;
    border-left-style: solid;
    padding: 0.75rem 1.25rem;
    padding-left: 0.5rem;

    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .badge-danger {
    background-color: #d72e3d;
  }

  &.${EventItemType.LIVE_NOW} .inner {
    border-color: #d72e3d;
  }

  &.${EventItemType.LIVE_SOON} .inner {
    border-color: #eb9c0a;
  }

  &.${EventItemType.PRE_RECORDED} .inner {
    border-color: #4ecdc4;
  }
`;

const TimeLabel = styled.span`
  font-size: 1rem;
  font-weight: 700;
  line-height: 1.125rem;

  margin-left: 0.5rem;
`;

const WebinarNameLabel = styled.span`
  font-size: 1rem;
  font-weight: 400;
  line-height: 1.125rem;
  margin-right: 0.75rem;
`;

// @ts-ignore
const ThreeDotsToggleButton = React.forwardRef(({ children, onClick }, ref) => (
  // @ts-ignore
  <i
    className="bi bi-three-dots-vertical"
    ref={ref as any}
    onClick={(e) => {
      e.preventDefault();
      onClick(e);
    }}
    style={{ cursor: "pointer", fontSize: "18px" }}
  >
    {/* custom icon */}
    {children}
  </i>
));
const AmPmFormat = "hh:mm A";

function getEventType(event: CalendarEventObject) {
  if (event.webinar.type === WebinarType.RECORDED) {
    return EventItemType.PRE_RECORDED;
  }

  const startTime = moment(event.start);
  const endTime = moment(event.end);
  const now = moment();

  if (now.isBetween(startTime, endTime)) {
    return EventItemType.LIVE_NOW;
  }

  return EventItemType.LIVE_SOON;
}

export interface EventItemProps {
  event: CalendarEventObject;
  showRegisteredWord?: boolean;
}

const EventItem: React.FC<EventItemProps> = ({ event, showRegisteredWord }) => {
  const startTime = moment(event.start);
  const endTime = moment(event.end);

  const type = getEventType(event);

  return (
    <EventRow style={{ marginLeft: "20px" }} className={type}>
      <div className="inner">
        <div className="d-flex" style={{ alignItems: "center" }}>
          {type === EventItemType.PRE_RECORDED && (
            <Calendar2Event size={16}></Calendar2Event>
          )}
          {type === EventItemType.LIVE_SOON && (
            <BroadcastPin size={16}></BroadcastPin>
          )}
          {type === EventItemType.LIVE_NOW && (
            <Badge variant="danger">LIVE</Badge>
          )}
          <TimeLabel>{`${startTime.format(AmPmFormat)} - ${endTime.format(
            AmPmFormat
          )} `}</TimeLabel>
          <WebinarNameLabel>| {event.webinar.name}</WebinarNameLabel>
        </div>
        <div className="d-flex align-items-center justify-content-center">
          <WebinarNameLabel>
            {event.registered}
            {showRegisteredWord ? " registered" : ""}
          </WebinarNameLabel>
          <Dropdown>
            <Dropdown.Toggle id="dropdown-basic" as={ThreeDotsToggleButton} />
            <Dropdown.Menu>
              <Dropdown.Item
                as={Link}
                to={`/analytics/${btoa(
                  JSON.stringify({ id: event.webinar.id })
                )}?view=${ANALYTIC_VIEWS.details}`}
              >
                Webinar Analytics
              </Dropdown.Item>
              {/* <Dropdown.Item href="#/action-2">All Sessions</Dropdown.Item> */}
              <Dropdown.Item
                to={`/analytics/${btoa(
                  JSON.stringify({ id: event.webinar.id })
                )}?view=${ANALYTIC_VIEWS.details}`}
                as={Link}
              >
                Webinar Details
              </Dropdown.Item>
              <Dropdown.Item
                as={Link}
                to={`/events/${btoa(
                  JSON.stringify({
                    eventId: event.id,
                    webinarId: event.webinar.id,
                  })
                )}`}
              >
                Moderator View
              </Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
        </div>
      </div>
    </EventRow>
  );
};

export default EventItem;
