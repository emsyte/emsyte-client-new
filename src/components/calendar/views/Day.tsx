import moment from "moment";
import "moment-timezone";
import React from "react";
import { Spinner } from "react-bootstrap";
import { useInfiniteQuery } from "react-query";
import styled from "styled-components";
import colors from "../../../utils/colors";
import { useHttpService } from "../../../utils/HttpService";
import EventItem from "./EventItem";

const NewMonth = styled.div`
  width: 100%;
  border-bottom: 1px solid ${colors.gainsboro.toHex()};
  margin-bottom: 1.25rem;
  margin-top: 1.25rem;
  padding-bottom: 0.25rem;
  font-weight: bold;
  font-size: 20px;
  line-height: 24px;
  letter-spacing: 1.2px;
  color: #2e3338;
`;

const CircleDiv = styled.div`
  min-height: 3rem;
  height: 3rem;
  min-width: 3rem;
  width: 3rem;
  border-radius: 3rem;
  background-color: white;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 24px;
  line-height: 24px;
  color: rgba(46, 51, 56, 0.8);
`;

const DayOfWeek = styled.span`
  font-size: 16px;
  line-height: 24px;
  color: ${colors.pewter.toHex()};
`;

export const DayWrapper = () => {
  return <Day />;
};

interface theProps {
  includePastSessions?: boolean;
  style?: Object;
  className?: string;
  showRegisteredWord?: boolean;
}

export const fetchEventsInfinite =
  (api) =>
  async ({ pageParam = 0 }) => {
    const startDate = moment();
    const endDate = moment();
    startDate.add(-pageParam, "month");
    // On first load the events in the next 6 months, next pages will be previous 1 month per page
    endDate.add(-pageParam + (pageParam === 0 ? 6 : 1), "month");
    const events = await api.doGetAllEventsForUser({
      startDate: startDate.toDate(),
      endDate: endDate.toDate(),
    });

    const now = moment();

    return {
      data: events.data.filter((event) => moment(event.end).isAfter(now)),
    };
  };

export const INFINITE_EVENTS_QUERY_KEY = "infiniteEvents";

const Day = ({
  style = {},
  showRegisteredWord = true,
  className = "",
}: theProps) => {
  const api = useHttpService();

  const eventsQuery = useInfiniteQuery(
    INFINITE_EVENTS_QUERY_KEY,
    fetchEventsInfinite(api)
  );
  if (eventsQuery.isLoading) {
    return <Spinner animation="border"></Spinner>;
  }

  // To know if need to print a new month
  let previousMonth = "";
  let previousDay = "";

  let data: any[] = [];
  eventsQuery.data?.pages.forEach((page) => {
    data = [...data, ...page.data];
  });
  data = data.sort(
    (a, b) => moment(a.start).valueOf() - moment(b.start).valueOf()
  );

  return (
    <div style={style} className={className}>
      {data.map((event) => {
        const day = moment(event.start);
        const isToday = moment().isSame(event.start, "day");
        const newMonth =
          previousMonth === "" || previousMonth !== day.format("MM");
        const newDay = previousDay === "" || previousDay !== day.format("DD");
        previousMonth = day.format("MM");
        previousDay = day.format("DD");

        return (
          <React.Fragment>
            {newMonth && (
              <NewMonth>{day.format("MMMM").toUpperCase()}</NewMonth>
            )}
            <div
              className="d-flex"
              style={newDay ? { marginTop: "1.25rem" } : {}}
            >
              <div
                className="d-flex flex-column align-items-center"
                style={{ width: "48px", height: "69px" }}
              >
                {newDay || newMonth ? (
                  <React.Fragment>
                    <CircleDiv
                      className={isToday ? "today-background" : ""}
                      id={isToday ? "today-element" : ""}
                    >
                      {day.format("DD")}
                    </CircleDiv>
                    <DayOfWeek>{day.format("dddd").substr(0, 3)}</DayOfWeek>
                    {isToday && (
                      <DayOfWeek className="today-color">Today</DayOfWeek>
                    )}
                  </React.Fragment>
                ) : (
                  ""
                )}
              </div>
              <div className="flex-grow-1">
                <EventItem
                  event={event}
                  showRegisteredWord={showRegisteredWord}
                ></EventItem>
              </div>
            </div>
          </React.Fragment>
        );
      })}
    </div>
  );
};

DayWrapper.navigate = (date, action) => new Date();

DayWrapper.title = (date) => "";

export default Day;
