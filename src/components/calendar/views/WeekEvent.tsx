import moment from 'moment';
import React from "react";
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { Col, Row } from "shards-react";



const WeekEvent = ({event}) => { 
  return ( 
      <Row className="rbc-event-week-time">
          <Col md="3"> 
            <i className="fab fa-instagram"></i>
          </Col>
          <Col md="9" className="text-right pr-0"> 
              <span className="text-white bg-secondary rounded px-1">{moment(event.start).format('h:mm A')}</span>
          </Col>
      </Row>
  ) 
}


const enhance = compose(
  withRouter,
)(WeekEvent);

export default enhance;