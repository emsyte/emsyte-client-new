import React, { useEffect, useState } from "react";
import { Image } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { Button, Col, Form, FormGroup, FormInput, Row } from "shards-react";
import { withAuth } from "../../utils/Authentication";

const ImageCollection = ({ event, handleUploadImages }: any) => {
  const [images, setImages] = useState<any>([]);
  const [uploadImages, setUploadImages] = useState<any>([]);

  const deleteImage = (idx) => {
    setImages([...images.slice(0, idx), ...images.slice(idx + 1)]);
  };

  const handeImageFile = (e) => {
    let url = URL?.createObjectURL(e.target.files[0]);
    setImages([...images, url]);
    setUploadImages([...uploadImages, e.target.files[0]]);
    handleUploadImages([...uploadImages, e.target.files[0]]);
  };

  useEffect(() => {
    if (event && event.media) {
      let _media = event.media.map((o) => o.public_url);
      setImages(_media);
    } else setImages([]);
  }, [event]);

  return (
    <div className="post-image-collection">
      <Row className="pt-2">
        <Col>
          <Image
            width="250"
            height="250"
            rounded
            className="border"
            src={
              images[0]
                ? images[0]
                : require("../../images/image-placeholder.jpg")
            }
          />
        </Col>
      </Row>
      <Row>
        <Col className="text-left">
          {images.slice(0, 5).map((item, idx) => (
            <div
              key={idx}
              className={`container d-inline-block `}
              onClick={() => deleteImage(idx)}
            >
              <Image
                rounded
                className="border"
                src={
                  item ? item : require("../../images/image-placeholder.jpg")
                }
              />
              <Button theme="dark" className="rounded-circle text-muted">
                <i className="fas fa-times"></i>
              </Button>
            </div>
          ))}
          {images.length < 5 && (
            <div className={`container d-inline-block pt-2`}>
              <Form>
                <FormGroup>
                  <label htmlFor="#image">
                    <i className="far fa-plus-square fa-2x mr-2"></i>
                  </label>
                  <FormInput
                    type="file"
                    className="d-none"
                    id="#image"
                    onChange={handeImageFile}
                  />
                </FormGroup>
              </Form>
            </div>
          )}
        </Col>
      </Row>
    </div>
  );
};

const enhance = compose(withRouter, withAuth)(ImageCollection);

export default enhance;
