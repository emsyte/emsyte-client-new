import React from "react";
import SkeletonElement from "./SkeletonElement";

export const SkeletonTable = () => {
  return (
    <div className="skeleton-wrapper">
      <div className="skeleton-table">
        <SkeletonElement type="title" />
        <SkeletonElement type="text" />
        <SkeletonElement type="text" />
        <SkeletonElement type="text" />
        <SkeletonElement type="text" />
        <SkeletonElement type="text" />
        <SkeletonElement type="text" />
      </div>
    </div>
  );
};
