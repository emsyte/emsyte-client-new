import {
  IntegrationObjectFull,
  IntegrationProvider,
} from "@emsyte/common/integration";
import { AxiosError } from "axios";
import React, { useEffect, useState } from "react";
import { Button, Form, Modal, Row, Col } from "react-bootstrap";
import { useForm, UseFormMethods } from "react-hook-form";
import { useMutation, useQueryClient } from "react-query";
import { unknownValue } from "../../../../utils/conditions";
import { setRemoteFormErrors } from "../../../../utils/form";
import { useHttpService } from "../../../../utils/HttpService";
import { APIResult } from "../../../../utils/types";
import LoaderButton from "../../../common/LoaderButton";
import { useNotification } from "../../../common/Notification";
import TextInput from "../../../form/TextInput";
import { getIntegrationImage } from "../../../../data/integrations-data";

interface FormElementProps extends UseFormMethods {
  provider: IntegrationProvider;
}

const SendGridForm: React.FC<FormElementProps> = ({ register, errors }) => (
  <div>
    <Row>
      <Form.Group as={Col} controlId="sendgrid-fromName">
        <Form.Label>From Name</Form.Label>
        <TextInput
          errors={errors}
          name="fromName"
          placeholder="John Doe"
          ref={register({
            required: "Required",
          })}
        />
      </Form.Group>
      <Form.Group as={Col} controlId="sendgrid-fromEmail">
        <Form.Label>From Email</Form.Label>
        <TextInput
          errors={errors}
          name="fromEmail"
          placeholder="johndoe@example.com"
          ref={register({
            required: "Required",
          })}
        />
      </Form.Group>
    </Row>
    <Form.Group controlId="sendgrid-apiKey">
      <Form.Label>API Key</Form.Label>
      <TextInput
        errors={errors}
        name="apiKey"
        placeholder="Add API Key"
        ref={register({
          required: "Required",
        })}
      />
      <small>The API key from Send Grid</small>
    </Form.Group>
  </div>
);

const TwilioForm: React.FC<FormElementProps> = ({ register, errors }) => (
  <div>
    <Form.Group controlId="twilio-fromPhone">
      <Form.Label>From Phone Number</Form.Label>
      <TextInput
        errors={errors}
        name="fromPhone"
        placeholder="+15555551234"
        ref={register({
          required: "Required",
        })}
      />
    </Form.Group>
    <Form.Group controlId="twilio-accountSid">
      <Form.Label>Account Sid</Form.Label>
      <TextInput
        errors={errors}
        name="accountSid"
        placeholder="Account Sid"
        ref={register({
          required: "Required",
        })}
      />
    </Form.Group>
    <Form.Group controlId="twilio-authToken">
      <Form.Label>API Key</Form.Label>
      <TextInput
        errors={errors}
        name="authToken"
        placeholder="Add API Key"
        ref={register({
          required: "Required",
        })}
      />
    </Form.Group>
  </div>
);

const ActiveCampaignForm: React.FC<FormElementProps> = ({
  register,
  errors,
}) => (
  <div>
    <Form.Group controlId="api_url">
      <Form.Label>API URL</Form.Label>
      <TextInput
        errors={errors}
        name="api_url"
        ref={register({
          required: "Required",
        })}
      />
    </Form.Group>
    <Form.Group controlId="api_key">
      <Form.Label>API Key</Form.Label>
      <TextInput
        errors={errors}
        name="api_key"
        ref={register({
          required: "Required",
        })}
      />
      <small>The API key from ActiveCampaign</small>
    </Form.Group>
  </div>
);

const HubSpotForm: React.FC<FormElementProps> = ({ register, errors }) => (
  <div>
    <Form.Group controlId="api_key">
      <Form.Label>API Key</Form.Label>
      <TextInput
        errors={errors}
        name="api_key"
        ref={register({
          required: "Required",
        })}
      />
      <small>The API key from HubSpot</small>
    </Form.Group>
  </div>
);

const client_id = "b293c218-25e5-41ef-9a44-bdadf0653cff";
const baseURL = "https://api.cc.email/v3/idfed";
const redirect_uri = window.location.origin + "/oauth/constantcontact";

const openNewWindow = () => {
  const url = `${baseURL}?client_id=${client_id}&scope=contact_data&response_type=code&redirect_uri=${redirect_uri}`;

  const width = 500;
  const height = 650;
  const top = window.screenY + window.outerHeight / 2 - height / 2;
  const left = window.screenX + window.outerWidth / 2 - width / 2;

  const windowFeatures = `toolbar=no, menubar=no, width=${width}, height=${height}, top=${top}, left=${left}`;
  const reference = window.open(url, "Authenticate", windowFeatures);
  return reference;
};

const ConstantContactForm: React.FC<FormElementProps> = ({
  register,
  setValue,
  watch,
}) => {
  const [windowReference, setWindowReference] = useState<Window | null>(null);
  const [connected, setConnected] = useState(!!watch("access_token"));

  const handleAuthenticate = () => {
    if (windowReference === null || windowReference.closed) {
      setWindowReference(openNewWindow());
    } else {
      windowReference.focus();
    }
  };
  useEffect(() => {
    const handleMessage = async (event: MessageEvent) => {
      if (!windowReference) {
        return;
      }

      windowReference.close();
      setWindowReference(null);

      const data = new URLSearchParams(event.data);
      const code = data.get("code");

      setValue("access_token", "");
      setValue("refresh_token", "");
      setValue("authorization_code", code);
      setConnected(true);
    };

    console.log("adding handler");

    window.addEventListener("message", handleMessage, false);
    return () => window.removeEventListener("message", handleMessage, false);
  }, [windowReference, setValue]);

  return (
    <div>
      <input type="hidden" name="access_token" ref={register()} />
      <input type="hidden" name="refresh_token" ref={register()} />
      <input type="hidden" name="authorization_code" ref={register()} />
      <div className="d-flex justify-content-center align-items-center p-4">
        {connected ? (
          <div>Connected!</div>
        ) : (
          <div>
            <Button variant="success" onClick={handleAuthenticate}>
              Connect to Constant Contact
            </Button>
          </div>
        )}
      </div>
    </div>
  );
};

const FormElement: React.FC<FormElementProps> = (props) => {
  switch (props.provider) {
    case IntegrationProvider.SEND_GRID:
      return <SendGridForm {...props} />;
    case IntegrationProvider.TWILIO:
      return <TwilioForm {...props} />;
    case IntegrationProvider.ACTIVE_CAMPAIGN:
      return <ActiveCampaignForm {...props} />;

    case IntegrationProvider.MAILCHIMP:
      return <div>Coming soon...</div>;
    case IntegrationProvider.HUB_SPOT:
      return <HubSpotForm {...props} />;
    case IntegrationProvider.CONSTANT_CONTACT:
      return <ConstantContactForm {...props} />;
    default:
      unknownValue(props.provider);
      return null;
  }
};

type Props = {
  integration: IntegrationObjectFull;
  show: boolean;
  onClose: () => void;
};

const EditIntegrationModal: React.FC<Props> = ({
  integration,
  onClose,
  show,
}) => {
  const notification = useNotification();
  const queryClient = useQueryClient();
  const api = useHttpService();

  const methods = useForm();
  const { handleSubmit, reset, setError } = methods;

  useEffect(() => {
    if (!show) {
      return;
    }
    reset({ ...integration?.settings });
  }, [show, reset, integration]);

  const editMut = useMutation(api.doEditIntegration, {
    onSuccess(result) {
      onClose();

      // Immediately update the cached result
      queryClient.setQueryData(
        "integrations",
        (response?: APIResult<typeof api.doGetIntegrations>) => {
          return response
            ? {
                data: response.data.map((integration) =>
                  integration.provider === result.data.provider
                    ? result.data
                    : integration
                ),
              }
            : { data: [] };
        }
      );

      // Tell react-query to refetch the results at it's earliest convenience
      queryClient.invalidateQueries("integrations");

      notification.showMessage({
        title: "Integration configuration saved",
        type: "success",
      });
    },
    onError(error: AxiosError) {
      notification.showMessage({
        title: "Failed to save integration configuration",
        description: error.response?.data.message ?? error.message,
        type: "error",
      });
      setRemoteFormErrors(error.response?.data.errors, setError);
    },
  });

  return (
    <Modal show={show} onHide={onClose}>
      <Modal.Header closeButton className="py-3 pl-3">
        Set up {integration.title}
      </Modal.Header>
      <div className="d-flex mt-2 px-3">
        <div className="d-flex" style={{ flex: "0 0 25%" }}>
          <Button
            variant="link"
            onClick={onClose}
            style={{ fontSize: "1rem" }}
            className="p-0"
          >
            <i className="bi bi-chevron-left" /> Back
          </Button>
        </div>
        <div
          style={{ flex: "0 1 50%" }}
          className="d-flex justify-content-center"
        >
          <img
            src={getIntegrationImage(integration.provider)}
            style={{
              height: "2rem",
              width: "11.25rem",
              objectFit: "scale-down",
            }}
            alt={`${integration.title} logo`}
          />
        </div>
        <div style={{ flex: "0 2 25%" }}></div>
      </div>
      <Form
        onSubmit={handleSubmit((settings) =>
          editMut.mutate({
            provider: integration.provider,
            settings,
          })
        )}
      >
        <div className="p-3">
          {show && (
            <FormElement
              provider={integration.provider}
              {...methods}
            ></FormElement>
          )}
        </div>
        <div className="px-3 pb-4 d-flex justify-content-end">
          <Button
            variant="outline-secondary"
            onClick={onClose}
            style={{ fontSize: "1rem" }}
          >
            Cancel
          </Button>
          <LoaderButton
            type="submit"
            className="float-right"
            loading={editMut.isLoading}
            style={{ marginLeft: "0.5rem", fontSize: "1rem" }}
          >
            Apply
          </LoaderButton>
        </div>
      </Form>
    </Modal>
  );
};
export default EditIntegrationModal;
