import React, { useCallback, useEffect, useState } from "react";
import { Button, Tab, Tabs } from "react-bootstrap";
import {
  Badge,
  Card,
  CardBody,
  Col,
  Form,
  FormGroup,
  FormRadio,
  Row,
} from "shards-react";
import { apiWrapper } from "../../../utils/api_helpers";
import { ROLES, STRIPE } from "../../../utils/constants";
import LoaderButton from "../../common/LoaderButton";

const tiers = [
  {
    nickname: STRIPE.FREE,
    role: ROLES.FREE,
    color: "success",
    // discount: "14 days trail",
  },
  {
    nickname: STRIPE.BASIC,
    role: ROLES.BASIC,
    color: "secondary",
    // discount: "20% off",
  },
  {
    nickname: STRIPE.PREMIUM,
    role: ROLES.PREMIUM,
    color: "primary",
    // discount: "20% off",
  },
  {
    nickname: STRIPE.ENTERPRISE,
    role: ROLES.ENTERPRISE,
    color: "info",
    // discount: "20% off",
  },
];

// const getTier = (name) => tiers.find((x) => x.nickname === name);

const tierNicknames = tiers.map((o) => o.nickname);

const tierSort = (a, b) =>
  tierNicknames.indexOf(a.name) > tierNicknames.indexOf(b.name);

const ProductList = ({ productList, product, setProduct, billingInterval }) => {
  return (
    <FormGroup row>
      {productList &&
        productList.map((tier) => (
          // Enterprise card is full width at sm breakpoint
          <Col lg={12} md={12} sm={12} key={tier.name} className="my-2">
            <FormRadio
              name="productType"
              checked={product?.name === tier.name}
              onChange={() => setProduct(tier)}
            >
              <Card
                className={`product no-shadow ${
                  product?.name === tier.name && "active"
                }`}
              >
                <CardBody className="p-3">
                  <Row>
                    <Col lg={6} md={6}>
                      {tier.name}
                      <br />
                      <Badge
                        pill
                        theme={`${
                          product?.name === tier.name ? "info" : "primary"
                        }`}
                      >
                        {/* {getTier(tier.name)?.discount} */}
                      </Badge>
                    </Col>
                    <Col lg={6} md={6} className="text-right">
                      <h4 className="d-inline-block font-weight-bold">
                        $
                        {tier.prices
                          .filter(
                            (x) => x.recurring.interval === billingInterval
                          )[0]
                          .amount_usd.toLocaleString()}{" "}
                      </h4>
                      <small
                        style={{
                          textTransform: "capitalize",
                        }}
                      >{`/${billingInterval}`}</small>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </FormRadio>
          </Col>
        ))}
    </FormGroup>
  );
};

const ChangePlan = ({ stripe, plan, onUpdate }) => {
  const [product, setProduct] = useState<any>(plan?.product);
  const [productList, setProductList] = useState<any>();
  const [billingInterval, setBillingInterval] = useState(
    plan?.interval ?? "month"
  );
  const [loading, setLoading] = useState(true);

  const getStripePlans = useCallback(async () => {
    setLoading(true);
    try {
      let plans = await stripe.getPlans();
      plans = plans.sort(tierSort);
      setProductList(plans);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  }, [stripe]);

  useEffect(() => {
    getStripePlans();
  }, [getStripePlans]);

  const handleChoosePlan = (event) => {
    event.preventDefault();

    apiWrapper(async () => {
      await stripe.deactivateSubscription();
      const price = product.prices.find(
        (price) => price.recurring.interval === billingInterval
      );
      await stripe.createSubscription(price.id);
      onUpdate();
    }, setLoading);
  };

  return (
    <div className="membership-details">
      <h6>Select a new Plan</h6>
      <Form className="mt-4" onSubmit={handleChoosePlan}>
        <Tabs
          id="membership-plans"
          className="nav-justified"
          activeKey={billingInterval}
          onSelect={setBillingInterval}
        >
          <Tab eventKey="month" title="Monthly">
            {productList && (
              <ProductList
                productList={productList}
                product={product}
                setProduct={setProduct}
                billingInterval="month"
              ></ProductList>
            )}
          </Tab>
          <Tab eventKey="year" title="Yearly">
            {productList && (
              <ProductList
                productList={productList}
                product={product}
                setProduct={setProduct}
                billingInterval="year"
              ></ProductList>
            )}
          </Tab>
        </Tabs>
        <small className="mx-auto">
          Compare Emsyte plans at our{" "}
          <Button variant="link" className="text-primary pointer">
            Pricing Details
          </Button>
        </small>
        <LoaderButton
          loading={loading}
          block
          variant="primary"
          className="d-table mx-auto ml-1 mt-4 w-100"
          type="submit"
        >
          Select Plan
        </LoaderButton>
      </Form>
    </div>
  );
};

export default ChangePlan;
