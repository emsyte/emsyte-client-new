import React from "react";
import { Form } from "react-bootstrap";
// Stripe
import { Elements, StripeProvider } from "react-stripe-elements";
import StripeScriptLoader from "react-stripe-script-loader";
import { Row } from "shards-react";
import StripeCard from "../../../components/Stripe/methodUpdate";

const { REACT_APP_STRIPE_PUBLISHABLE_KEY } = process.env;

const ChangePaymentMethod = ({ handleUpdatePaymentMethods }) => {
  return (
    <>
      <h5>Change Payment Card</h5>
      <small>
        Change a payment card at Emsyte.{" "}
        <a href="http://example.com/TODO">Terms Apply</a>
      </small>
      <Form className="mt-4">
        <Row>
          <StripeScriptLoader
            uniqueId="myUniqueId"
            script="https://js.stripe.com/v3/"
            loader="Loading..."
          >
            <StripeProvider apiKey={REACT_APP_STRIPE_PUBLISHABLE_KEY}>
              <Elements>
                <StripeCard handleResult={handleUpdatePaymentMethods} />
              </Elements>
            </StripeProvider>
          </StripeScriptLoader>
        </Row>
      </Form>
    </>
  );
};

export default ChangePaymentMethod;
