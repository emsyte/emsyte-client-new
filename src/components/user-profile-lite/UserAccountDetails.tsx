import React, { useState, useEffect, useCallback } from "react";
import DeactivateAccount from "./DeactivateAccount";

import {
  Card,
  CardHeader,
  ListGroup,
  ListGroupItem,
  Row,
  Col,
  Form,
  FormInput,
} from "shards-react";
import { useHttpService } from "../../utils/HttpService";
import LoaderButton from "../common/LoaderButton";
import ChangePasswordSection from "./ChangePasswordSection";
import { useAuth } from "../../utils/Authentication";

const UserAccountDetails = () => {
  const { authUser } = useAuth();
  const api = useHttpService();
  const [user, setUser] = useState<any>(authUser);
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    setUser(authUser);
  }, [authUser]);

  const handleUpdateAccount = useCallback(
    async (event) => {
      try {
        setLoading(true);
        await api.doUpdateUser({
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
        });
        setLoading(false);
      } catch (error) {
        setLoading(false);
      }
    },
    [user, api]
  );

  return (
    <Card small className="mb-4">
      <CardHeader className="border-0">
        <h6 className="m-0">Account Details</h6>
      </CardHeader>
      <ListGroup flush>
        <ListGroupItem className="p-3">
          <Row>
            <Col>
              <Form noValidate>
                <Row form>
                  {/* First Name */}
                  <Col md="6" className="form-group">
                    <label htmlFor="feFirstName">First Name</label>
                    <FormInput
                      id="feFirstName"
                      placeholder="First Name"
                      value={user?.firstName || ""}
                      onChange={(e) =>
                        setUser({ ...user, firstName: e.target.value })
                      }
                    />
                  </Col>
                  {/* Last Name */}
                  <Col md="6" className="form-group">
                    <label htmlFor="feLastName">Last Name</label>
                    <FormInput
                      id="feLastName"
                      placeholder="Last Name"
                      value={user?.lastName || ""}
                      onChange={(e) =>
                        setUser({ ...user, lastName: e.target.value })
                      }
                    />
                  </Col>
                </Row>
                <Row form>
                  {/* Email */}
                  <Col md="12" className="form-group">
                    <label htmlFor="feEmail">Email</label>
                    <FormInput
                      type="email"
                      id="feEmail"
                      placeholder="Email Address"
                      value={user?.email || ""}
                      onChange={(e) =>
                        setUser({ ...user, email: e.target.value })
                      }
                      autoComplete="email"
                    />
                  </Col>
                </Row>

                <LoaderButton
                  loading={loading}
                  theme="accent"
                  onClick={handleUpdateAccount}
                >
                  Update Account
                </LoaderButton>

                <hr />
                <ChangePasswordSection />
                <hr />
                {/* Deactivate Acount */}
                <DeactivateAccount />
              </Form>
            </Col>
          </Row>
        </ListGroupItem>
      </ListGroup>
    </Card>
  );
};
// UserAccountDetails.propTypes = {
//   /**
//    * The component's title.
//    */
//   title: PropTypes.string
// };

// UserAccountDetails.defaultProps = {
//   title: "Account Details"
// };

export default UserAccountDetails;
