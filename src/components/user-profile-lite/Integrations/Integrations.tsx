import { IntegrationType } from "@emsyte/common/integration";
import React from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { useQuery } from "react-query";
import { useHttpService } from "../../../utils/HttpService";
import IntegrationCard from "./IntegrationCard";

interface IntegrationSkeletonProps {}

const IntegrationSkeleton: React.FC<IntegrationSkeletonProps> = () => {
  return (
    <div className="skeleton-wrapper">
      <div className="d-flex" style={{ justifyContent: "space-between" }}>
        <div
          className="skeleton"
          style={{ width: "calc(33% - 10px)", height: 250 }}
        ></div>
        <div
          className="skeleton"
          style={{ width: "calc(33% - 10px)", height: 250 }}
        ></div>
        <div
          className="skeleton"
          style={{ width: "calc(33% - 10px)", height: 250 }}
        ></div>
      </div>
    </div>
  );
};

const sections = [
  {
    type: IntegrationType.EMAIL,
    title: "Integrated email senders",
    caption: (
      <span>
        Connect to an external sender for more customization and control of
        emails sent to participants. Emails are managed on the external
        platform.
      </span>
    ),
  },
  {
    type: IntegrationType.CRM,
    title: "External email senders",
    caption: (
      <span>
        Connect to an external sender for more customization and control of
        emails sent to participants. Emails are managed on the external
        platform.
      </span>
    ),
  },
  {
    type: IntegrationType.SMS,
    title: "SMS Integrations",
    caption: <span>Notify your webinar participants through SMS.</span>,
  },
];

const Integrations = () => {
  const api = useHttpService();

  const { data, isFetched } = useQuery("integrations", api.doGetIntegrations);

  const integrations = data?.data ?? [];

  return (
    <Card>
      {sections.map((section, i) => (
        <React.Fragment key={section.type}>
          <Card.Header>
            <Card.Title>{section.title}</Card.Title>
            {section.caption}
          </Card.Header>
          <Card.Body className={i < sections.length - 1 ? "border-bottom" : ""}>
            {!isFetched ? (
              <IntegrationSkeleton></IntegrationSkeleton>
            ) : (
              <Container>
                <Row>
                  {integrations
                    .filter((int) => int.type === section.type)
                    .map((integration) => (
                      <Col md="6" lg="6" xl="4" className="mb-4">
                        <IntegrationCard
                          integration={integration}
                          key={integration.provider}
                        />
                      </Col>
                    ))}
                </Row>
              </Container>
            )}
          </Card.Body>
        </React.Fragment>
      ))}
    </Card>
  );
};

export default Integrations;
