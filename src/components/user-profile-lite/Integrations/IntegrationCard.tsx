import { IntegrationObjectFull } from "@emsyte/common/integration";
import { faCheckCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import classnames from "classnames";
import React, { useState } from "react";
import { Image } from "react-bootstrap";
import { Button, Card, Col, Row } from "shards-react";
import { getIntegrationImage } from "../../../data/integrations-data";
import EditIntegrationModal from "../modals/EditIntegration/EditIntegration";
import cls from "./Integrations.module.scss";

type Props = {
  integration: IntegrationObjectFull;
};

const IntegrationCard: React.FC<Props> = ({ integration }) => {
  const [showModal, setShowModal] = useState(false);

  return (
    <>
      <Card className="px-4 pt-4 pb-3 mb-2">
        {integration.active && (
          <FontAwesomeIcon
            icon={faCheckCircle}
            className={"text-success " + cls.active}
          ></FontAwesomeIcon>
        )}
        <Row>
          <Col xs="12" className="d-flex justify-content-center my-3">
            <Image
              src={getIntegrationImage(integration.provider)}
              className={cls["icon"]}
            />
          </Col>
          <Col xs="12">
            <h5 className={cls["title"]}>{integration.title}</h5>
            <p className={cls["desc"]}>{integration.description}</p>
            <div className="d-flex flex-column justify-content-center align-items-center">
              <div style={{ width: "fit-content" }}>
                <Button
                  className={classnames("w-100", cls["button"])}
                  theme={integration.active ? "success" : "secondary"}
                  onClick={(e) => {
                    e.preventDefault();
                    setShowModal(true);
                  }}
                >
                  {integration.active ? "Edit Settings" : "Configure"}
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Card>

      <EditIntegrationModal
        integration={integration}
        show={showModal}
        onClose={() => setShowModal(false)}
      />
    </>
  );
};

export default IntegrationCard;
