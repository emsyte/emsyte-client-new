import { TeamPlan } from "@emsyte/common/team";
import moment from "moment";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import { Button } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  ListGroup,
  ListGroupItem,
  Row,
} from "shards-react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { withAuth } from "../../../utils/Authentication";
import { withStripe } from "../../../utils/Stripe";
import ChangePlan from "../modals/ChangePlan";

const MySwal = withReactContent(Swal);

// TODO: Write out these benefits
const PLAN_BENEFITS = {
  [TeamPlan.BASIC]: ["You can login"],
  [TeamPlan.BASIC]: ["You can create webinars"],
  [TeamPlan.PREMIUM]: ["You can unleash the full power of the dark side"],
};

const PlanDetails = ({ history, auth, stripe }) => {
  const [subscription, setSubscription] = useState<any>(null);

  const planDescriptions = useMemo(() => {
    if (!subscription) {
      return [];
    }
    return PLAN_BENEFITS[subscription.plan.product.metadata.plan] ?? [];
  }, [subscription]);

  const handlePlanDetails = useCallback(async () => {
    try {
      const subscription = await stripe.getSubscription(auth.id);
      setSubscription(subscription);
    } catch (error) {
      console.log(error);
    }
  }, [stripe, auth]);

  const handleChangePlanModal = () => {
    MySwal.fire({
      title: "",
      html: (
        <ChangePlan
          stripe={stripe}
          plan={subscription?.plan}
          onUpdate={handlePlanDetails}
        />
      ),
      showCloseButton: true,
      showConfirmButton: false,
    });
  };

  useEffect(() => {
    const fetchData = async () => await handlePlanDetails();
    fetchData();
  }, [handlePlanDetails]);

  return (
    <>
      <Card small className="mb-4">
        <CardHeader className="border-0">
          <Row>
            <Col>
              <h6 className="m-0">Subscription Plan Details</h6>
            </Col>
            <Col className="text-right">
              <small>
                <Button
                  variant="link"
                  className="text-primary pointer"
                  onClick={() => handleChangePlanModal()}
                >
                  Change Plan
                </Button>
              </small>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          {subscription && (
            <>
              <Row>
                <Col>
                  <small>Plan</small>
                  <br />
                  <h6>{subscription.plan.product.name}</h6>
                </Col>
                <Col>
                  <small>Renewal Date</small>
                  <br />
                  <h6>
                    {moment.unix(subscription.current_period_end).format("ll")}
                  </h6>
                </Col>
                <Col>
                  <small>Cost</small>
                  <br />
                  <h6>{`$${(subscription.plan.amount / 100).toFixed(2)}/${
                    subscription.plan.interval
                  }`}</h6>
                </Col>
              </Row>
              {/* <Card className="no-shadow border-0 py-3 px-4" theme="light">
                You have discounted pricing until May 12, 2021.
                <small className="text-muted">
                  After this date, your pricing will change to $299/month.{" "}
                  <Link to="">Learn more.</Link>
                </small>
              </Card> */}
              <Row className="mt-4">
                <Col>
                  <small>Plan Benefits:</small>
                  <br />
                  <ListGroup>
                    {planDescriptions &&
                      planDescriptions.map((item, i) => (
                        <ListGroupItem key={i} className="border-0 p-1">
                          <i className="fas fa-check text-success pr-2" />
                          {item}
                        </ListGroupItem>
                      ))}
                    {/* <ListGroupItem className="border-0 p-1"><i className="fas fa-check text-success" /> 5,000 orders per month (plus 10% buffer)</ListGroupItem>
                    <ListGroupItem className="border-0 p-1"><i className="fas fa-check text-success" /> Unlimited Integrations</ListGroupItem>
                    <ListGroupItem className="border-0 p-1"><i className="fas fa-check text-success" /> Truly Human Support</ListGroupItem> */}
                  </ListGroup>
                </Col>
              </Row>
            </>
          )}
          {!subscription && <>No Plan Details Found</>}
        </CardBody>
      </Card>
    </>
  );
};

const enhance = compose(withRouter, withAuth, withStripe)(PlanDetails);

export default enhance;
