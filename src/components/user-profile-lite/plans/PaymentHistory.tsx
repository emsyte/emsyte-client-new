import * as _ from "lodash";
import moment from "moment";
import React, { useCallback, useEffect, useState } from "react";
import { Table } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { Card, CardBody, CardHeader, Col, Row } from "shards-react";
import { withAuth } from "../../../utils/Authentication";
import { withStripe } from "../../../utils/Stripe";

const PaymentHistory = ({ auth, stripe }) => {
  const [transactions, setTransactions] = useState<any>(null);

  const handleTransactions = useCallback(async () => {
    try {
      const { charges } = await stripe.doGetTransactionHistory(auth.id);
      setTransactions(charges);
    } catch (error) {
      console.log(error);
    }
  }, [auth, stripe]);

  useEffect(() => {
    const fetchData = async () => await handleTransactions();
    fetchData();
  }, [handleTransactions]);

  return (
    <>
      <Card small className="mb-4">
        <CardHeader className="border-0">
          <Row>
            <Col>
              <h6 className="m-0">Payment History</h6>
            </Col>
          </Row>
        </CardHeader>
        <CardBody>
          {!_.isEmpty(transactions) ? (
            <Table responsive>
              <thead>
                <tr>
                  <th className="text-muted font-weight-light">Date</th>
                  <th className="text-muted font-weight-light">Amount</th>
                  <th className="text-muted font-weight-light">Status</th>
                  <th className="text-muted font-weight-light">PDF</th>
                </tr>
              </thead>
              <tbody>
                {transactions.map((item, i) => {
                  return (
                    <tr key={i}>
                      <td>{moment.unix(item.created).format("ll")}</td>
                      <td>${item.amount_usd}</td>
                      {item.status === "succeeded" ? (
                        <td className="text-success">
                          {item.paid ? "Paid" : "Scheduled"}
                        </td>
                      ) : (
                        <td className="text-warning">
                          {item.paid ? "Paid" : "Scheduled"}
                        </td>
                      )}
                      <td>
                        <a
                          href={item.receipt_url}
                          target="_blank"
                          rel="noopener noreferrer"
                        >
                          Invoice
                        </a>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          ) : (
            <span>No Transactions</span>
          )}
        </CardBody>
      </Card>
    </>
  );
};

const enhance = compose(withRouter, withAuth, withStripe)(PaymentHistory);

export default enhance;
