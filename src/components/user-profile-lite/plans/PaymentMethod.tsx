import * as _ from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import { Button, Spinner } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import { compose } from "recompose";
import { Alert, Card, CardBody, CardHeader, Col, Row } from "shards-react";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import VisaCardIcon from "../../../images/payment_icons/1.png";
import DiscoverCardIcon from "../../../images/payment_icons/14.png";
import MasterCardIcon from "../../../images/payment_icons/2.png";
import AmericanExpressIcon from "../../../images/payment_icons/22.png";
import { withAuth } from "../../../utils/Authentication";
import { withStripe } from "../../../utils/Stripe";
import ChangePaymentMethod from "../modals/ChangePaymentMethod";

const MySwal = withReactContent(Swal);

const titleCase = (str) => _.startCase(_.toLower(str));

const PaymentMethods = ({ auth, stripe }) => {
  const [paymentMethods, setPaymentMethods] = useState<any>(null);

  const [alertIsVisible, setAlertIsVisible] = useState<any>(false);
  const [alertMessage, setAlertMessage] = useState<any>(
    "Card changed successfully."
  );

  const [loading, setLoading] = useState(false);

  const handlePaymentMethods = useCallback(async () => {
    try {
      const { methods } = await stripe.doGetPaymentMethod(auth.id);
      setPaymentMethods(methods);
    } catch (error) {
      console.log(error);
    }
  }, [auth, stripe]);

  const handleUpdatePaymentMethods = useCallback(
    async (e) => {
      setLoading(true);
      try {
        if (!e.error) {
          try {
            // TODO: Update Payment Method based on card response
            await stripe.doUpdatePaymentMethod(e.token.id);
            await handlePaymentMethods();
          } catch (error) {
            console.log(error);
            setAlertMessage("Error");
          }
        }
      } catch (error) {
        console.log(error);
        setAlertMessage("Error");
      } finally {
        setLoading(false);
      }
      handleAlert(true);
    },
    [stripe, handlePaymentMethods]
  );

  const handleAlert = (state) => {
    setAlertIsVisible(state);
    setTimeout(() => setAlertIsVisible(!state), 1500);
  };

  const handleStripeModal = () => {
    // @ts-ignore
    MySwal.fire({
      title: "",
      html: (
        <ChangePaymentMethod
          handleUpdatePaymentMethods={handleUpdatePaymentMethods}
        />
      ),
      showCloseButton: true,
      showConfirmButton: false,
    });
  };

  const getPaymentIconByBrand = (brand) => {
    switch (brand.toLowerCase()) {
      case "visa":
        return VisaCardIcon;
      case "mastercard":
        return MasterCardIcon;
      case "discover":
        return DiscoverCardIcon;
      case "american express":
        return AmericanExpressIcon;
      default:
        return VisaCardIcon;
    }
  };

  useEffect(() => {
    const fetchData = async () => await handlePaymentMethods();
    fetchData();
  }, [handlePaymentMethods]);

  return (
    <>
      <Card small className="mb-4">
        <CardHeader className="border-0">
          <Row>
            <Col>
              <h6 className="m-0">Payment Method</h6>
            </Col>
            <Col className="text-right">
              <small>
                <Button
                  variant="link"
                  className="text-primary pointer"
                  onClick={() => handleStripeModal()}
                >
                  Edit Payment
                </Button>
              </small>
            </Col>
          </Row>
        </CardHeader>
        {
          <Alert
            className="text-white rounded mx-auto w-50"
            open={alertIsVisible}
            theme="dark"
          >
            <i className="fas fa-check text-success"></i> {alertMessage}
          </Alert>
        }
        <CardBody>
          <Card className="no-shadow border py-3 px-4">
            {loading ? (
              <Spinner animation="border"></Spinner>
            ) : !_.isEmpty(paymentMethods) ? (
              paymentMethods.map((item, i) => {
                return (
                  <Row key={i}>
                    <Col xs={2} className="text-center">
                      <img
                        width={75}
                        height={50}
                        src={getPaymentIconByBrand(item.card.brand)}
                        alt={titleCase(item.card.brand)}
                      />
                    </Col>
                    <Col xs={8}>
                      <span>
                        {titleCase(item.card.brand)} ending in {item.card.last4}
                      </span>
                      <br />
                      <span>
                        Expires:{" "}
                        {`${item.card.exp_month}/${item.card.exp_year}`}
                      </span>
                    </Col>
                    <Col className="text-right">
                      <span className="text-success">Default</span>
                    </Col>
                  </Row>
                );
              })
            ) : (
              <span>No cards are configured for this subscription</span>
            )}
          </Card>
        </CardBody>
      </Card>
    </>
  );
};

const enhance = compose(withRouter, withAuth, withStripe)(PaymentMethods);

export default enhance;
