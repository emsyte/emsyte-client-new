import React from "react";
import { Link, useRouteMatch } from "react-router-dom";
import { Card, CardHeader, ListGroup, ListGroupItem } from "shards-react";
import { useAuth } from "../../utils/Authentication";

const UserDetails = () => {
  const { authUser } = useAuth();

  const displayName = `${authUser!.firstName} ${authUser!.lastName}`;

  const { path } = useRouteMatch();

  return (
    <Card small className="mb-4 pt-3">
      <CardHeader className="border-bottom text-center">
        {/* <div className="mb-3 mx-auto">
          <img
            className="rounded-circle"
            src={`https://ui-avatars.com/api/?name=${currentUser.displayName}`}
            alt={currentUser.displayName}
            width="110"
          />
        </div> */}
        <h4 className="mb-0">{displayName}</h4>
        <span className="text-muted d-block mb-2">Webinar Author</span>
        {/* <Button pill outline size="sm" className="mb-2">
          <i className="material-icons mr-1">person_add</i> Follow
        </Button> */}
      </CardHeader>
      <ListGroup flush className="text-center">
        <ListGroupItem className="px-4">
          <Link className="btn btn-link pointer" to={path}>
            Account Settings
          </Link>
        </ListGroupItem>
        <ListGroupItem className="px-4">
          <Link className="btn btn-link pointer" to={`${path}/subscriptions`}>
            Plan &amp; Payments
          </Link>
        </ListGroupItem>
        <ListGroupItem className="px-4">
          <Link className="btn btn-link pointer" to={`${path}/integrations`}>
            Integration
          </Link>
        </ListGroupItem>
        <ListGroupItem className="px-4">
          <Link to="/team-settings">Team</Link>
        </ListGroupItem>
      </ListGroup>
    </Card>
  );
};

export default UserDetails;
