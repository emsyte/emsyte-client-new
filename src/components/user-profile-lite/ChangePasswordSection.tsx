import React, { useState, useCallback } from "react";
import { Form, FormControl, FormGroup } from "react-bootstrap";

import { Row, Col } from "shards-react";

import { useHttpService } from "../../utils/HttpService";
import LoaderButton from "../common/LoaderButton";
import { useNotification } from "../common/Notification";

const ChangePasswordSection = () => {
  const api = useHttpService();
  const notification = useNotification();
  const [currentPassword, setCurrentPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [error, setError] = useState("");
  const [loading, setLoading] = useState<boolean>(false);

  // TODO: Do we need the error state value?
  console.log(error);

  const handlePasswordChange = useCallback(async () => {
    try {
      if (
        currentPassword.trim() &&
        newPassword.trim() &&
        confirmPassword.trim() &&
        newPassword.trim() === confirmPassword.trim()
      ) {
        setLoading(true);
        await api.doChangePassword({
          password: currentPassword,
          newPassword,
        });
        notification.showMessage({
          title: "Change password Successfully!",
          description: "Your password has been updated.",
          type: "success",
        });
        setLoading(false);
      } else {
        setError("Passwords don't match");
      }
    } catch (error) {
      setLoading(false);
      notification.showMessage({
        title: "Oops!",
        description:
          error.response?.data?.message ?? "Failed to update password",
        type: "error",
      });
      console.log(error);
    }
  }, [currentPassword, newPassword, confirmPassword, api, notification]);

  return (
    <Form>
      {/* Change Password */}
      <Row form className="">
        <Col className="mb-3">
          <h6 className="form-text m-0">Change Password</h6>
          <p className="form-text text-muted m-0">
            Change your current password.
          </p>
        </Col>
      </Row>

      <Row form className="">
        {/* Change Password :: New Password */}
        <Col md="12" className="form-group">
          <label htmlFor="newPassword">Current Password</label>
          <FormGroup>
            <FormControl
              id="currentPassword"
              type="password"
              autoComplete="new-password"
              placeholder="Current Password"
              onChange={(e) => setCurrentPassword(e.target.value)}
            />
          </FormGroup>
        </Col>
        <Col md="6" className="form-group">
          <label htmlFor="newPassword">New Password</label>
          <FormGroup>
            <FormControl
              id="newPassword"
              type="password"
              autoComplete="new-password"
              placeholder="New Password"
              isInvalid={newPassword.length < 8 && newPassword.length !== 0}
              onChange={(e) => setNewPassword(e.target.value)}
            />
            <FormControl.Feedback type="invalid">
              {newPassword.length < 8 &&
                newPassword.length !== 0 &&
                `Password must be at least 8 characters`}
            </FormControl.Feedback>
          </FormGroup>
        </Col>

        {/* Change Password :: Repeat New Password */}
        <Col md="6" className="form-group">
          <label htmlFor="repeatNewPassword">Confirm Password</label>
          <FormGroup>
            <FormControl
              id="repeatNewPassword"
              type="password"
              placeholder="Confirm Password"
              autoComplete="new-password"
              isInvalid={newPassword !== confirmPassword}
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
            <FormControl.Feedback type="invalid">
              {confirmPassword &&
                newPassword &&
                newPassword !== confirmPassword &&
                `Passwords don't match`}
            </FormControl.Feedback>
          </FormGroup>
        </Col>
        <Col md="4" className="form-group">
          <LoaderButton
            loading={loading}
            onClick={handlePasswordChange}
            theme="accent"
          >
            Change Password
          </LoaderButton>
        </Col>
      </Row>
    </Form>
  );
};

export default ChangePasswordSection;
