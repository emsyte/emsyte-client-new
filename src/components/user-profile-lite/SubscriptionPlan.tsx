import React from "react";
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';

// import FormSectionTitle from "../edit-user-profile/FormSectionTitle";
import PaymentHistory  from './plans/PaymentHistory';
import PaymentMethod  from './plans/PaymentMethod';
import PlanDetails  from './plans/PlanDetails';

const SubscriptionPlan = ({ history }: any) => (
  <>
    <PlanDetails />
    <PaymentMethod />
    <PaymentHistory />
  </>
)


const enhance = compose(
  withRouter
)(SubscriptionPlan);

export default enhance;
