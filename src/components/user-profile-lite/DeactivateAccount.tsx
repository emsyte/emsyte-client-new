import React, { useState, useCallback } from "react";

import { useHttpService } from "../../utils/HttpService";
import { useNotification } from "../common/Notification";

import {
  Row,
  Col,
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
} from "shards-react";

const DeactivateAccount = () => {
  const [toggle, setToggle] = useState(false);
  const api = useHttpService();
  const notification = useNotification();

  const confirmDeactivation = () => {
    setToggle(true);
  };

  const handleToggle = () => setToggle(!toggle);

  const handleDeactivateAccount = useCallback(
    async (event) => {
      try {
        await api.doDeactivateAccount();
        setToggle(false);
        notification.showMessage({
          title: "deactivate account Successfully!",
          description: "Your account has been deactivate.",
          type: "success",
        });
      } catch (error) {
        notification.showMessage({
          title: "Oops!",
          description: "Something wrong.",
          type: "error",
        });
        console.log(error);
      }
    },
    [api, notification]
  );

  return (
    <>
      <Row form>
        <Col className="mb-3">
          <h6 className="form-text m-0">Deactivate Your Account</h6>
          <p className="form-text text-muted m-0"></p>
        </Col>
      </Row>

      <Row form>
        <Col className="form-group">
          <Button theme="danger" onClick={confirmDeactivation}>
            Deactivate Account
          </Button>
        </Col>
      </Row>
      <Modal
        className="text-center"
        size="sm"
        open={toggle}
        toggle={handleToggle}
      >
        <ModalHeader titleClass="mx-auto">Sad to See You Go</ModalHeader>
        <ModalBody>Are you sure you want to deactivate your account?</ModalBody>
        <ModalFooter>
          <Button className="mx-auto" theme="accent" onClick={handleToggle}>
            Nevermind
          </Button>
          <Button
            className="border-0 bg-transparent btn-nohover text-muted mx-auto"
            onClick={handleDeactivateAccount}
          >
            No thanks, I want to cancel
          </Button>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default DeactivateAccount;
