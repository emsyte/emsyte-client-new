import React, { useState } from "react";
import styled from "styled-components";

const ToggleWrapper = styled.div`
  position: relative;
  border-radius: 0.5rem;
  height: 1rem;
  width: 1.8rem;
  border: 1px solid #adb5bd;
  cursor: pointer;

  .toggleCircle {
    margin: 1px;
    position: absolute;
    height: calc(1rem - 4px);
    width: calc(1rem - 4px);
    border-radius: 0.5rem;
    background-color: #adb5bd;
    transition: all 0.1s ease;
  }
`;

const Toggler = ({ onChange, ...props }) => {
  const [state, setState] = useState(false);
  return (
    <ToggleWrapper
      {...props}
      onClick={() =>
        setState((prev) => {
          const newState = !prev;
          onChange(newState);
          return newState;
        })
      }
    >
      <div
        className="toggleCircle"
        style={{ left: state ? "0.8rem" : 0 }}
      ></div>
    </ToggleWrapper>
  );
};

export default Toggler;
