import "moment-timezone";
import React, { useState } from "react";
import {
  Accordion,
  Card,
  Col,
  Row,
  Form,
  InputGroup,
  OverlayTrigger,
  Spinner,
  Tooltip,
  useAccordionToggle,
} from "react-bootstrap";
import { useFormContext } from "react-hook-form";
import ReactPlayer from "react-player";
import arrowElbow from "../../images/arrow_elbow.svg";
import styled from "styled-components";
import { secondsToDuration } from "../../utils/duration";
import { useFeatureFlag } from "../../utils/FeatureFlags";
import { WebinarType } from "@emsyte/common/webinar";
import { unknownValue } from "../../utils/conditions";

const PlayerWrapper = styled.div`
  visibility: hidden;
  height: 0;
  width: 0;
`;

const DurationRow = styled(Row)`
  position: relative;
`;

const DurationLoader = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  right: 0;
  bottom: 0;

  display: flex;
  justify-content: center;
  align-items: center;

  background: rgba(0, 0, 0, 0.2);
  margin: 0 1rem;
  border-radius: 0.25rem;
`;

interface DurationProps {
  editable: boolean;
}

const Duration: React.FC<DurationProps> = ({ editable }) => {
  const { register, watch, setValue } = useFormContext();
  const [loadingDuration, setLoadingDuration] = useState(false);
  const videoUrl = watch("videoUrl");

  return (
    <>
      {!editable && (
        <PlayerWrapper>
          <ReactPlayer
            url={videoUrl}
            onError={(event) => console.log(event)}
            onDuration={(seconds) => {
              if (!editable) {
                const duration = secondsToDuration(seconds);
                setValue("duration_hours", duration.hours);
                setValue("duration_minutes", duration.minutes);
                setValue("duration_seconds", duration.seconds);
                setLoadingDuration(false);
              }
            }}
          ></ReactPlayer>
        </PlayerWrapper>
      )}

      <Form.Group>
        <span className="text-muted text-bold">
          {editable ? "Duration of live event" : "Video duration"}
        </span>
        <DurationRow>
          <Col xs={4} className="pr-0">
            <OverlayTrigger
              overlay={<Tooltip id="duration_hours">Hours</Tooltip>}
            >
              <Form.Control
                type="number"
                defaultValue={1}
                min={0}
                placeholder="hours"
                name="duration_hours"
                readOnly={!editable}
                ref={register({
                  max: 100,
                  min: 0,
                })}
              />
            </OverlayTrigger>
          </Col>
          <Col xs={4} className="px-2">
            <OverlayTrigger
              overlay={<Tooltip id="duration_minutes">Minutes</Tooltip>}
            >
              <Form.Control
                type="number"
                placeholder="minutes"
                name="duration_minutes"
                readOnly={!editable}
                defaultValue={0}
                min={0}
                ref={register({
                  max: 59,
                  min: 0,
                })}
              />
            </OverlayTrigger>
          </Col>
          <Col xs={4} className="pl-0">
            <OverlayTrigger
              overlay={<Tooltip id="duration_seconds">Seconds</Tooltip>}
            >
              <Form.Control
                type="number"
                placeholder="seconds"
                name="duration_seconds"
                readOnly={!editable}
                defaultValue={0}
                min={0}
                ref={register({
                  max: 59,
                  min: 0,
                })}
              />
            </OverlayTrigger>
          </Col>
          {loadingDuration && (
            <DurationLoader>
              <Spinner animation="border"></Spinner>
            </DurationLoader>
          )}
        </DurationRow>
      </Form.Group>
    </>
  );
};

function getLabel(type: WebinarType) {
  if (type === WebinarType.LIVE) {
    return "Live Webinar";
  }

  if (type === WebinarType.RECORDED) {
    return "Pre-Recorded";
  }
  unknownValue(type);

  return null;
}

interface WebinarToggleProps {
  type: WebinarType;
}

const WebinarToggle: React.FC<WebinarToggleProps> = ({ type }) => {
  const decoratedOnClick = useAccordionToggle(type, () => {});
  const { register } = useFormContext();

  return (
    <InputGroup className="col-lg-12">
      <Form.Check
        type="radio"
        custom
        name="type"
        id={"webinar_" + type}
        value={type}
        ref={register()}
        onClick={decoratedOnClick}
        label={<span style={{ fontWeight: 500 }}>{getLabel(type)}</span>}
      ></Form.Check>
    </InputGroup>
  );
};

interface WebinarTypeAccordionProps {}

const WebinarTypeAccordion: React.FC<WebinarTypeAccordionProps> = () => {
  const replayEnabled = useFeatureFlag("replay");
  const { register, watch } = useFormContext();

  return (
    <Accordion
      className="col-lg-12"
      style={{ padding: "0px" }}
      activeKey={watch("type")}
    >
      <Card style={{ border: "none" }}>
        <Card.Header style={{ padding: "0px" }}>
          <WebinarToggle type={WebinarType.LIVE}></WebinarToggle>
          <p style={{ marginLeft: "2rem" }}>
            Use YouTube, Facebook or Twitch to stream your webinar and engage in
            real-time.
          </p>
        </Card.Header>
        <Accordion.Collapse eventKey={WebinarType.LIVE}>
          <Card.Body style={{ paddingTop: 0, display: "flex" }}>
            <img
              alt="arrow-img"
              src={arrowElbow}
              style={{ alignSelf: "flex-start", paddingTop: "1rem" }}
            />
            <div>
              <div style={{ background: "#CCE5FF", borderRadius: "4px" }}>
                <p style={{ marginTop: "2%", padding: "3% 3%", margin: "0" }}>
                  A live stream URL must be added before each session. You’ll
                  get a reminder 15 mins prior if it hasn’t been added.
                </p>
              </div>
              {watch("type") === WebinarType.LIVE && (
                <Duration editable></Duration>
              )}
            </div>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
      <Card style={{ border: "none" }}>
        <Card.Header style={{ padding: "0px" }}>
          <WebinarToggle type={WebinarType.RECORDED}></WebinarToggle>
          <p style={{ marginLeft: "2rem" }}>
            Link to a pre-recorded video. Schedule sessions to run on their own,
            then drive the traffic.
          </p>
        </Card.Header>
        <Accordion.Collapse eventKey={WebinarType.RECORDED}>
          <Card.Body style={{ paddingTop: 0, display: "flex" }}>
            <img
              alt="arrow-img"
              src={arrowElbow}
              style={{ alignSelf: "flex-start", paddingTop: "1rem" }}
            />
            <div
              style={{
                background: "#F5F5F5",
                borderRadius: "4px",
                width: "100%",
              }}
            >
              <Form.Group
                controlId="formBasicEmail"
                style={{ padding: "2% 2%" }}
              >
                <Form.Label>Video File/URL</Form.Label>
                <Form.Control name="videoUrl" ref={register()} type="text" />
                <Form.Text className="text-muted">
                  {/* Video duration:  */}
                  {watch("type") === WebinarType.RECORDED && (
                    <Duration editable={false}></Duration>
                  )}
                </Form.Text>
              </Form.Group>
              {replayEnabled && (
                <Form.Group
                  controlId="formBasicEmail"
                  style={{ padding: "2% 2%" }}
                >
                  <Form.Label>Replay Expiration</Form.Label>
                  <Form.Control
                    name="replay_expiration_days"
                    type="number"
                    style={{ width: "75%" }}
                  />
                  <Form.Text className="text-muted">
                    Number of days participants can re-watch the webinar
                  </Form.Text>
                </Form.Group>
              )}
            </div>
          </Card.Body>
        </Accordion.Collapse>
      </Card>
    </Accordion>
  );
};

export default WebinarTypeAccordion;
