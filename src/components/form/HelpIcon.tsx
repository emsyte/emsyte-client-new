import React from "react";
import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";

export interface HelpIconProps {
  id: string;
}

const HelpIcon: React.FC<HelpIconProps> = ({ id, children }) => {
  return (
    <span className="ml-2 p-1">
      <OverlayTrigger overlay={<Tooltip id={id}>{children}</Tooltip>}>
        <FontAwesomeIcon className="text-muted" icon={faInfoCircle} />
      </OverlayTrigger>
    </span>
  );
};

export default HelpIcon;
