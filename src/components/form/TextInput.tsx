import React from "react";
import { FormControlProps, FormControl } from "react-bootstrap";
import { UseFormMethods } from "react-hook-form";

export interface InputProps extends FormControlProps {
  name: string;
  errors: UseFormMethods["errors"];

  // Missing props from form control props
  placeholder?: string;
  defaultValue?: string | number;
  max?: number;
  min?: number;

  as?: React.ElementType;
}

const TextInput = React.forwardRef<any, React.PropsWithChildren<InputProps>>(
  ({ name, errors, ...props }, ref) => (
    <>
      <FormControl
        {...props}
        ref={ref}
        name={name}
        isInvalid={!!errors[name]}
      ></FormControl>
      <FormControl.Feedback type="invalid">
        {errors[name]?.message}
      </FormControl.Feedback>
    </>
  )
);
export default TextInput;
