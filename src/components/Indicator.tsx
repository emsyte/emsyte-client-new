import React from "react";
import { Variant } from "react-bootstrap/esm/types";
import styled from "styled-components";
const Light = styled.span`
  width: 10px;
  height: 10px;
  border-radius: 100%;

  vertical-align: middle;
  display: inline-block;
  margin: 0.5rem;
`;

export interface IndicatorProps {
  variant: Variant;
}

const Indicator: React.FC<IndicatorProps> = ({ variant }) => {
  return <Light className={`bg-${variant}`}></Light>;
};

export default Indicator;
