import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
// import { Auth0Provider } from "@auth0/auth0-react";

import * as serviceWorker from "./serviceWorker";

import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";

if (process.env.NODE_ENV === "production") {
  Sentry.init({
    dsn: "https://3f1309a88efd491bbffd4c0b5c92d35a@o308847.ingest.sentry.io/5837344",
    integrations: [new Integrations.BrowserTracing()],

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 0.8,
  });
}

ReactDOM.render(
  // <Auth0Provider
  //   domain="justwebinar.us.auth0.com"
  //   clientId="QMOV4NIBc1fo67nbxWrodOg6YSGyi1r7"
  //   redirectUri={window.location.origin}
  // >
  <App />,
  // </Auth0Provider>,
  document.getElementById("root")
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
