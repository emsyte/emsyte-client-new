import { IntegrationProvider } from "@emsyte/common/integration";
import ActiveCampaign from "../images/integrations/activecampaign.png";
import HubSpot from "../images/integrations/hubspot.png";
import MailChimp from "../images/integrations/mailchimp.png";
import SendGrid from "../images/integrations/sendgrid.png";
import Twilio from "../images/integrations/twilio.png";
import ConstantContact from "../images/integrations/constantcontact.png";
import { unknownValue } from "../utils/conditions";
// import Zapier from "../images/integrations/zapier.png";
// import iContact from "../images/integrations/icontact.png";

export function getIntegrationImage(provider: IntegrationProvider) {
  switch (provider) {
    case IntegrationProvider.TWILIO:
      return Twilio;
    case IntegrationProvider.ACTIVE_CAMPAIGN:
      return ActiveCampaign;
    case IntegrationProvider.HUB_SPOT:
      return HubSpot;
    case IntegrationProvider.MAILCHIMP:
      return MailChimp;
    case IntegrationProvider.SEND_GRID:
      return SendGrid;
    case IntegrationProvider.CONSTANT_CONTACT:
      return ConstantContact;
    default:
      unknownValue(provider);
  }
}
