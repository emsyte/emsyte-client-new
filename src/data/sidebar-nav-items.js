export default function () {
  const size = "1.125rem";
  return [
    {
      items: [
        {
          title: "Dashboard",
          to: "/dashboard",
          htmlBefore: `<i class="bi bi-grid-1x2-fill" style="font-size: ${size}">`,
          htmlAfter: "",
        },
        {
          title: "Webinars",
          to: "/webinars",
          htmlBefore: `<i class="bi bi-collection-play-fill" style="font-size: ${size}">`,
          htmlAfter: "",
        },
        {
          title: "Audiences",
          to: "/audiences",
          htmlBefore: `<i class="bi bi-people-fill" style="font-size: ${size}"/>`,
          htmlAfter: "",
        },
        // {
        //   title: "Reports",
        //   to: "/reports",
        //   htmlBefore: `<i class="bi bi-file-earmark-ruled-fill" style="font-size: ${size}"/>`,
        //   htmlAfter: "",
        // },
        {
          title: "Preferences",
          to: "/account",
          htmlBefore: `<i class="bi bi-sliders" style="font-size: ${size}"/>`,
          htmlAfter: "",
        },
        // {
        //   title: "Logout",
        //   to: "/login",
        //   htmlBefore: `<i class="bi bi-box-arrow-left" style="font-size: ${size}"/>`,
        //   htmlAfter: "",
        // },
      ],
    },
  ];
}

// {
//   title: "Analytics",
//   to: "/analytics",
//   htmlBefore: '<i class="material-icons">bar_chart</i>',
//   htmlAfter: "",
// },
