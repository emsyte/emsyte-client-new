import { NotificationType } from "@emsyte/common/notification";

export const LAYOUT_TYPES = {
  DEFAULT: "DEFAULT",
  HEADER_NAVIGATION: "HEADER_NAVIGATION",
  ICON_SIDEBAR: "ICON_SIDEBAR",
};

export const COLLECTIONS = {
  USERS: "users",
  TEAMS: "teams",
  ROLES: "roles",
  PERMISSIONS: "permissions",
};

export const ROLES = {
  BASIC: "Basic",
  PREMIUM: "Premium",
  ENTERPRISE: "Enterprise",
};

export const API = {
  USERS: "users",
  LOGIN: "users/login",
  REGISTER: "users/register",
  USER_TEAMS: "account/teams",
  ACCOUNT: "account",
  TEAMS: "teams",
  EMAILS: "emails",
  MEDIA: "media",

  CUSTOMERS: ":team/manage/billing/customer",
  SUBSCRIPTIONS: ":team/manage/billing/subscription",
  TRANSACTIONS: ":team/manage/billing/transactions",
  PAYMENT_METHODS: ":team/manage/billing/payment-methods",
  PRODUCTS: "stripe/products",
  PLANS: "stripe/plans",
  COUPON: "stripe/coupon",
  WEBINAR_PUBLIC: "webinarPublic",
  WEBINAR_VIEWER: "viewer",
  INTEGRATIONS: ":team/integrations",
  TEMPLATES: ":team/templates",
  WEBINARS: ":team/webinars",
  EVENTS: ":team/webinars/events",
  TEAM_MANAGE: ":team/manage",
  ANALYTICS: ":team/analytics",
};

//  Stripe Products
export const STRIPE = {
  FREE: "Free",
  BASIC: "Basic",
  PREMIUM: "Premium",
  ENTERPRISE: "Enterprise",
};

export const ROUTES = {
  LOGIN: "/login",
  REGISTER: "/register",
  MEMBERSHIP: "/membership-details",
  FORGOT_PASSWORD: "/forgot-password",
  CHANGE_PASSWORD: "/change-password",
  ACCOUNT: "/account",
  DASHBOARD: "/dashboard",
  WEBINAR: "/webinar",
};

export const NOTIFICATION_TEMPLATES = [
  {
    title: "Welcome",
    type: NotificationType.WELCOME,
    defaultValue: "",
    description: "Send this email [immediately] upon registration",
    timeStr: "Immediately",
    hasSendEmail: true,
    limit: 1,
    timeUnit: "immediately",
    addonTimeInput: "upon registration",
  },
  {
    title: "Pre Event",
    type: NotificationType.PRE_EVENT,
    defaultValue: "",
    description: "Send this email [hours] before the webinar",
    timeStr: "24 hours before",
    hasSendEmail: true,
    timeUnit: "hours",
    addonTimeInput: "hours before the webinar",
    defaultTime: 24,
  },
  {
    title: "Last Minute",
    type: NotificationType.LAST_MINUTE,
    defaultValue: "",
    description: "Send this email [15] minutes before the webinar",
    timeStr: "15 minutes before",
    hasSendEmail: true,
    hasSendSMS: true,
    limit: 1,
    disabledEditTime: true,
    timeUnit: "minutes",
    addonTimeInput: "minutes before the webinar",
    defaultTime: 15,
  },
  {
    title: "Post Event",
    type: NotificationType.POST_EVENT,
    defaultValue: "",
    description: "Send this email [hours] after the webinar",
    timeStr: "24 hours later",
    hasSendEmail: true,
    timeUnit: "hours",
    addonTimeInput: "hours after the webinar",
    defaultTime: 24,
  },
];
