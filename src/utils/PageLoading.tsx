import React from "react";
import { Spinner } from "react-bootstrap";
import styled from "styled-components";

const Wrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export interface PageLoadingProps {}

const PageLoading: React.FC<PageLoadingProps> = () => {
  return (
    <Wrapper>
      <div className="d-flex">
        <h3 className="mr-1">Loading...</h3>
        <Spinner animation="border"></Spinner>
      </div>
    </Wrapper>
  );
};

export default PageLoading;
