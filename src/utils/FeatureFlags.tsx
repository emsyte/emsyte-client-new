import { FEATURES } from "@emsyte/common/features";
import React, { useContext } from "react";

export const FeatureFlagsContext = React.createContext({
  isFeatureEnabled(feature: string) {
    return true;
  },
});

export interface FeatureFlagsProviderProps {}

const FeatureFlagsProvider: React.FC<FeatureFlagsProviderProps> = ({
  children,
}) => {
  return (
    <FeatureFlagsContext.Provider
      value={{
        isFeatureEnabled(feature: string) {
          return FEATURES[feature] ?? true;
        },
      }}
    >
      {children}
    </FeatureFlagsContext.Provider>
  );
};

export function useFeatureFlag(feature: string) {
  const { isFeatureEnabled } = useContext(FeatureFlagsContext);

  if (feature === "live_chat") {
    return true;
  }

  return isFeatureEnabled(feature);
}

export default FeatureFlagsProvider;
