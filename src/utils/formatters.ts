import { Duration } from "./duration";

export function formatCurrency(value: number): string {
  return (value / 100).toLocaleString("en-us", {
    style: "currency",
    currency: "USD",
  });
}

export function formatPercentage(value: number): string {
  return (value * 100).toFixed(2) + "%";
}

export function formatDuration(duration: Duration): string {
  const parts = [
    duration.hours.toString().padStart(2, "0"),
    duration.minutes.toString().padStart(2, "0"),
    duration.seconds.toString().padStart(2, "0"),
  ];
  return parts.join(":");
}
