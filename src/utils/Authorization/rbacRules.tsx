const limits = {
    team: {
        members: 3,
    },
    business: {
        members: 3,
    },
}
const rules = {
    user: {
        static: [
            "posts:list", 
            "home-page:visit", 
            "dashboard:visit"
        ]
    },
    startup: {
        static: [
            "posts:list", 
            "home-page:visit", 
            "dashboard:visit"
        ]
    },
    team: {
        static: [
            "team:list", 
            "team:create", 
            "users:getSelf", 
            "home-page:visit", 
            "dashboard:visit"
        ],
        dynamic: {
            "team:add":({
                uid,
                ownerId,
                size
            }) => {
                if (!uid || !ownerId) return false;
                return (uid === ownerId && limits.team.members > size);
            },
            "team:edit":({
                uid,
                ownerId,
            }) => {
                if (!uid || !ownerId) return false;
                return (uid === ownerId);
            },
            "posts:edit": ({
                uid,
                postOwnerId
            }) => {
                if (!uid || !postOwnerId) return false;
                return uid === postOwnerId;
            }
        }
    },
    business: {
        static: [
            "team:list", 
            "team:create", 
            "users:getSelf", 
            "home-page:visit", 
            "dashboard:visit"
        ],
        dynamic: {
            "team:add":({
                uid,
                ownerId,
                size
            }) => {
                if (!uid || !ownerId) return false;
                return (uid === ownerId && limits.business.members > size);
            },
            "team:edit":({
                uid,
                ownerId
            }) => {
                if (!uid || !ownerId) return false;
                return uid === ownerId;
            },
            "posts:edit": ({
                uid,
                postOwnerId
            }) => {
                if (!uid || !postOwnerId) return false;
                return uid === postOwnerId;
            }
        }
    },
    admin: {
        static: [
            "team:list", 
            "team:create",
            "posts:list", 
            "posts:create", 
            "posts:edit", 
            "posts:delete", 
            "users:get", 
            "users:getSelf", 
            "home-page:visit", 
            "dashboard:visit"
        ]
    }
};
export default rules;