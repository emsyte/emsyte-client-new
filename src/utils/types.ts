export type ThenArg<T> = T extends PromiseLike<infer U> ? U : T;

export type APIResult<T extends (...args: any) => any> = ThenArg<ReturnType<T>>;
