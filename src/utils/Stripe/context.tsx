// External
import React, { createContext } from "react";

import axios from "axios";
import moment from "moment";

import { API } from "../constants";
import { useAuth } from "../Authentication";
import Cookies from "js-cookie";

const { REACT_APP_API } = process.env;

moment.updateLocale("en", {
  relativeTime: {
    future: "in %s",
    past: "%s",
    s: "1s",
    ss: "%ss",
    m: "1m",
    mm: "%dm",
    h: "1h",
    hh: "%dh",
    d: "1d",
    dd: "%dd",
    M: "1M",
    MM: "%dM",
    y: "1y",
    yy: "%dY",
  },
});

export const StripeServiceContext = createContext<{
  stripe: ReturnType<typeof useStripe>;
}>(null as any);

function useStripe() {
  const { getCurrentTeam, logout, setUser, getToken } = useAuth();
  const api = axios.create({
    baseURL: REACT_APP_API,
  });

  api.interceptors.request.use((config) => {
    const currentTeam = getCurrentTeam();
    console.log(currentTeam);
    if (config.url && currentTeam) {
      config.url = config.url.replace(/:team/g, currentTeam ?? "");
    }

    return config;
  });

  api.interceptors.request.use(function (config) {
    // Query param will always override the cookie
    const token = getToken();
    config.headers.common["Authorization"] = token ? `Bearer ${token}` : null;
    return config;
  });

  // Refresh Token Configuration
  api.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      // This function is part of a promise chain.
      // It needs to return a value or another promise so the caller knows when it
      // has completed.

      // Pass all non 401s back to the caller.
      if (error.response?.status !== 401 && error.response?.status !== 403) {
        return Promise.reject(error);
      }

      const tokens = {
        token: Cookies.get("tl-token"),
        refreshToken: Cookies.get("tl-refresh"),
      };

      // reject removes a previously added interceptor with an id.
      // I don't think you want this, it isn't doing anything.
      // axios.interceptors.response.reject(/* id */);

      // As the refresh request was not being returned, it wasn't part of the
      // promise chain. The callers .then / .catch would be called before the
      // token refresh had completed and you would still have the old token in
      // localStorage.
      console.log(tokens);
      // return;
      return api
        .post(`${API.USERS}/token/refresh`, tokens)
        .then((response) => {
          setUser(response.data.token, response.data.refreshToken);

          // This will update future request headers
          axios.defaults.headers.common[
            "Authorization"
          ] = `Bearer ${response.data.token}`;

          axios.defaults.headers.common["Content-Type"] = "application/json";
          axios.defaults.headers.common["Allow"] = "application/json";

          // Still need to return the an error because the original request failed
          // but added a property to show it can be tried again.
          error.hasRefreshedToken = true;

          return Promise.reject(error);
        })
        .catch((error) => {
          const tokenError: any = new Error("Cannot refresh token");
          tokenError.originalError = error;
          logout();
          // history.push('/login');
          return Promise.reject(tokenError);
        });
    }
  );

  /*
   * Stripe Payment Methods
   * ----------------------
   */
  const getPayments = async (customer_id: string) => {
    const response = await api.get(`${API.PAYMENTS}/${customer_id}`);
    return response.data;
  };

  /*
   * Stripe Plans Methods
   * ----------------------
   */
  const getPlans = async () => {
    const response = await api.get(API.PLANS);
    return response.data;
  };

  const validateCoupon = async (coupon: string) => {
    const response = await api.get(API.COUPON, {
      params: {
        coupon,
      },
    });
    return response.data;
  };

  /*
   * Stripe Customer Methods
   * ----------------------
   */
  const createCustomer = async (token: string) => {
    const postData = { token };
    const response = await api.post(API.CUSTOMERS, postData);
    return await response.data;
  };

  /*
   * Stripe Subscription Methods
   * ----------------------
   */
  const createSubscription = async (price: string, coupon: string) => {
    const postData = { price, coupon };
    const response = await api.post(API.SUBSCRIPTIONS, postData);
    return await response.data;
  };

  const deactivateSubscription = async () => {
    const response = await api.delete(`${API.SUBSCRIPTIONS}`);
    return await response.data;
  };

  const getSubscription = async () => {
    const response = await api.get(`${API.SUBSCRIPTIONS}`);
    return await response.data;
  };

  /*
   * Stripe Payments Methods
   * ----------------------
   */
  const doGetPaymentMethod = async () => {
    const response = await api.get(`${API.PAYMENT_METHODS}`);
    return await response.data;
  };

  const doUpdatePaymentMethod = async (token: string) => {
    const response = await api.put(`${API.PAYMENT_METHODS}`, { source: token });
    return await response.data;
  };

  const doDeletePaymentMethod = (customer: string) => {};

  const doCreatePaymentMethod = (customer: string) => {};

  /*
   * Stripe Transaction History
   * ----------------------
   */
  const doGetTransactionHistory = async () => {
    const response = await api.get(`${API.TRANSACTIONS}`);
    return await response.data;
  };

  return {
    getPayments,
    getPlans,
    validateCoupon,
    createCustomer,
    createSubscription,
    deactivateSubscription,
    getSubscription,
    doGetPaymentMethod,
    doUpdatePaymentMethod,
    doDeletePaymentMethod,
    doCreatePaymentMethod,
    doGetTransactionHistory,
  };
}

const StripeServiceProvider = ({ children }) => {
  const stripe = useStripe();
  return (
    <StripeServiceContext.Provider
      value={{
        stripe,
      }}
    >
      {children}
    </StripeServiceContext.Provider>
  );
};

export default StripeServiceProvider;
