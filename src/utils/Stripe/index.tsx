import React, { useContext } from "react";
import StripeServiceProvider, { StripeServiceContext } from "./context";

export const withStripe = (Component: any) => (props: any) => (
  <StripeServiceContext.Consumer>
    {({ stripe }) => {
      return <Component stripe={stripe} {...props} />;
    }}
  </StripeServiceContext.Consumer>
);

export function useStripeService() {
  const { stripe } = useContext(StripeServiceContext);
  return stripe;
}

export default withStripe;
export { StripeServiceContext, StripeServiceProvider };
