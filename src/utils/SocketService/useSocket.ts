import { useCallback, useEffect, useState } from "react";

export const useSocket = (
  onMessage: (data: any) => void,
  onConnected: (e: WebSocket | null) => void
) => {
  const [socket, setSocket] = useState<WebSocket | null>(null);
  const [isConnected, setIsConnected] = useState(false);

  if (socket) {
    socket.onmessage = function (event) {
      const data = JSON.parse(event.data);
      onMessage(data);
    };
  }

  const send = (payload: object) => {
    if (isConnected) {
      socket?.send(JSON.stringify(payload));
    }
  };

  const openConnection = useCallback(() => {
    let ws: WebSocket | null = new WebSocket(
      process.env.REACT_APP_WEB_SOCKET || ""
    );
    ws.onopen = (e) => {
      console.log("Connected");
      onConnected(ws);
      if (e) setIsConnected(true);
    };
    ws.onclose = () => {
      console.log("Lost connection");
      ws = null;
      setIsConnected(false);
      setTimeout(() => openConnection(), 1000);
    };
    setSocket(ws);
    return ws;
  }, [onConnected]);

  useEffect(() => {
    const ws = openConnection();
    return () => {
      ws.close();
    };
  }, [openConnection]);

  return {
    isConnected,
    send,
  };
};
