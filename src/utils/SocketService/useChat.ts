import { useCallback, useEffect, useRef, useState } from "react";
import { useAuth } from "../Authentication/index";
import {
  ChatEvent,
  ChatUser,
  LabelType,
  MessageEventType,
  MessageType,
} from "@emsyte/common/message";
import { useSocket } from "./useSocket";
import { EventObject } from "@emsyte/common/webinar";
import { unknownValue } from "../conditions";
import {
  AuthAttendeeObject,
  AuthType,
  AuthUserObject,
} from "@emsyte/common/auth";
import { decodeToken } from "../Authentication/context";

function uuidv4() {
  return "00-0-4-1-000".replace(/[^-]/g, (s: any) =>
    (((Math.random() + ~~s) * 0x10000) >> s).toString(16).padStart(4, "0")
  );
}

/**
 * De-duplicate a list of sorted items
 *
 * @param items Items to dedup
 * @param key Function to get the key for each item
 */
function dedupSorted<T, K extends string | number>(
  items: Array<T>,
  key: (item: T) => K
): Array<T> {
  let lastKey: K | null = null;
  const result: Array<T> = [];

  for (const item of items) {
    const itemKey = key(item);
    if (itemKey !== lastKey) {
      result.push(item);
    }
    lastKey = itemKey;
  }

  return result;
}

export type ChannelChats = Record<
  string,
  {
    firstKey: string;
    lastKey: string;
    messages: MessageType[];
  }
>;

export function getHostDMRoom(attendeeId: number) {
  return "attendee-" + attendeeId;
}

export const useChat = (event: EventObject) => {
  const { getToken } = useAuth();
  const [user, setUser] = useState<AuthAttendeeObject | AuthUserObject | null>(
    null
  );

  useEffect(() => {
    setUser(decodeToken(getToken()));
  }, [getToken, setUser]);

  const [isLoadingHistory, setIsLoadingHistory] = useState(false);
  const [isAtBeginning, setIsAtBeginning] = useState(false);
  const [isAtEnd, setIsAtEnd] = useState(true);

  const KEY_MAX = "99999999999999-ffff";
  const KEY_MIN = "00000000000000-0000";

  const [attendees, setAttendees] = useState<ChatUser[]>([]);
  const [chats, setChats] = useState<ChannelChats>({
    general: { firstKey: KEY_MAX, messages: [], lastKey: KEY_MIN },
  });

  const [cachedMessages, setCachedMessages] = useState<
    Array<{
      token: string;
      messageId: string;
      callback: Function;
    }>
  >([]);

  const keys = useRef({
    general: { firstKey: KEY_MAX, messages: [], lastKey: KEY_MIN },
  });

  const [barsData, setBarsData] = useState<
    Array<{ count: number; question: boolean }>
  >([]);

  const onConnected = useCallback(
    (ws) => {
      const sendHistories = Object.keys(keys.current).map((roomId) => ({
        roomId,
        lastKey: keys.current[roomId]?.lastKey ?? KEY_MIN,
      }));

      const sub = {
        action: "joinChannel",
        token: getToken(),
        channelId: `event-${event.id}`,
        sendHistories,
      };
      ws?.send(JSON.stringify(sub));
    },
    [event.id, getToken]
  );

  const { isConnected, send } = useSocket(onMessage, onConnected);

  const sendMessage = ({
    content,
    replyTo,
    sendTo,
    question,
  }: {
    content: string;
    replyTo?: string;
    sendTo: "general" | string;
    question?: boolean;
  }) => {
    const idempotenceToken = uuidv4();

    const messageId = Date.now() + "-local";

    const localMessage: MessageType = {
      userId: "local",
      messageId,
      content,
      name: [user?.firstName, user?.lastName].join(" "),
      label:
        user?.type === AuthType.USER ? LabelType.PRESENTER : LabelType.ATTENDEE,
      question,
      ownMessage: true,
      // replyTo?: ReplyType;
    };

    setChats((chats) => ({
      ...chats,
      [sendTo]: {
        ...chats[sendTo],
        messages: [localMessage].concat(chats[sendTo].messages),
      },
    }));

    return new Promise((resolve, reject) => {
      setCachedMessages((cm) =>
        cm.concat({
          token: idempotenceToken,
          messageId,
          callback: resolve,
        })
      );

      send({
        action: "sendMessage",
        channelId: `event-${event.id}`,
        token: idempotenceToken,
        content,
        replyTo,
        sendTo,
        question,
      });
    });
  };

  const removeMessage = (messageId: string) => {
    send({
      action: "removeMessage",
      channelId: `event-${event.id}`,
      messageId: `${messageId}`,
    });
  };

  const requestHistory = (roomId: string) => {
    setIsLoadingHistory(true);
    send({
      action: "historyChannel",
      channelId: `event-${event.id}`,
      roomId,
      to: chats[roomId]?.firstKey ?? KEY_MAX,
    });
  };

  const addMessagesToRoom = (
    roomId: string,
    messages: MessageType[] | { message: MessageType; localMessageId: string },
    reSort: boolean = false
  ) => {
    const room = chats[roomId];
    const prevMessages = room?.messages || [];
    const allMessages = Array.isArray(messages)
      ? messages.concat(prevMessages)
      : prevMessages.map((msg) =>
          msg.messageId === messages.localMessageId ? messages.message : msg
        );

    const sortedMessages = reSort
      ? dedupSorted(
          allMessages.sort((a, b) => b.messageId.localeCompare(a.messageId)),
          (message) => message.messageId
        )
      : allMessages;

    const lastKey = sortedMessages[0]?.messageId ?? KEY_MAX;
    const firstKey = sortedMessages?.slice(-1)[0]?.messageId ?? KEY_MIN;
    keys.current[roomId] = { firstKey: firstKey, lastKey: lastKey };

    setChats((prev) => ({
      ...prev,
      [roomId]: {
        firstKey,
        messages: sortedMessages,
        lastKey,
      },
    }));
  };

  function onMessage(data: ChatEvent) {
    switch (data.event) {
      case MessageEventType.ATTENDEES_LIST: {
        setAttendees(data.attendees);
        break;
      }
      case MessageEventType.JOINED: {
        setAttendees((prev) => [...prev, data.user]);
        break;
      }
      case MessageEventType.LEFT: {
        setAttendees((prev) =>
          prev.filter((user) => user.userId !== data.user.userId)
        );
        break;
      }
      case MessageEventType.BARS_CHANNEL: {
        setBarsData(data.bars);
        break;
      }
      case MessageEventType.MESSAGE:
        const msg = cachedMessages.find((cm) => cm.token === data.token);
        if (msg) {
          msg.callback(data.message);

          setCachedMessages(
            cachedMessages.filter((cm) => cm.token !== data.token)
          );
        }

        addMessagesToRoom(
          data.roomId,
          msg?.messageId
            ? {
                message: data.message,
                localMessageId: msg.messageId,
              }
            : [data.message]
        );
        break;
      case MessageEventType.HISTORY:
        setIsLoadingHistory(false);
        setIsAtBeginning(data.isAtBeginning);

        addMessagesToRoom(data.roomId, data.messages, true);
        break;
      case MessageEventType.REMOVE:
        // setMessages((prev) =>
        //   prev.map((msg) =>
        //     msg.messageId === data.messageId
        //       ? { ...msg, content: "", removed: true, ownMessage: false }
        //       : msg
        //   )
        // );
        break;
      case MessageEventType.ERROR:
        console.log(data);
        break;
      default:
        unknownValue(data);
        console.log(data);
        break;
    }
  }

  const getBarsData = () => {
    send({
      action: "barsChannel",
      channelId: `event-${event.id}`,
    });
  };

  return {
    isConnected,
    sendMessage,
    removeMessage,
    requestHistory,
    chats,
    attendees,
    getBarsData,
    barsData,
    isLoadingHistory,
    isAtBeginning,
    isAtEnd,
  };
};
