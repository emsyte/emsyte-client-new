import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const MySwal = withReactContent(Swal);

export async function apiWrapper(
  fn: () => Promise<void>,
  setLoading: (loading: boolean) => void
) {
  setLoading(true);
  try {
    await fn();
  } catch (error) {
    console.error(error);
    MySwal.fire({
      title: "Error",
      text: error?.response?.data?.message ?? error.message,
      icon: "error",
      confirmButtonText: "Ok",
    });
  } finally {
    setLoading(false);
  }
}
