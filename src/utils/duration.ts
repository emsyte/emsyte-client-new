export interface Duration {
  hours: number;
  minutes: number;
  seconds: number;
}

export function durationToSeconds(duration: Duration): number {
  return 3600 * duration.hours + 60 * duration.minutes + duration.seconds;
}

export function secondsToDuration(seconds: number): Duration {
  if(!seconds)
    return { hours: 0, minutes: 0, seconds: 0 }
  const hours = Math.floor(seconds / 3600);
  seconds -= hours * 3600;
  const minutes = Math.floor(seconds / 60);
  seconds -= minutes * 60;

  return {
    hours,
    minutes,
    seconds,
  };
}
