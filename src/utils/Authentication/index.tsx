import React, { useContext } from "react";
import { AuthContext, AuthProvider } from "./context";

export const withAuth = (Component: any) => (props: any) => (
  <AuthContext.Consumer>
    {({ authUser, setUser, login, logout, register }) => {
      return (
        <Component
          auth={authUser}
          setUser={setUser}
          login={login}
          logout={logout}
          register={register}
          {...props}
        />
      );
    }}
  </AuthContext.Consumer>
);

export function useAuth() {
  return useContext(AuthContext);
}

export default withAuth;
export { AuthContext, AuthProvider };
