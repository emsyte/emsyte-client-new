import { AuthAttendeeObject, AuthUserObject } from "@emsyte/common/auth";
import { UserTeamObject } from "@emsyte/common/team";
import { APIAuthRegisterData } from "@emsyte/common/user";
import axios from "axios";
import Cookies from "js-cookie";
import jwt from "jsonwebtoken";
import * as _ from "lodash";
import React, { useCallback, useEffect, useState } from "react";
import SecureLS from "secure-ls";
import { API } from "../constants";

const secureLS = new SecureLS();
const { REACT_APP_API } = process.env;

/**
 * Function used to silence warnings about unused variables.
 *
 * Any time this function is called should be considered a code smell
 */
function silence(_: any) {}

const getAuthHeaders = () => {
  let headers = {
    Accept: "application/json",
    "Content-Type": "application/json",
  };
  if (Cookies.get("token"))
    headers["Authorization"] = `Bearer ${Cookies.get("authUser")}`;

  return headers;
};
const api = axios.create({
  baseURL: REACT_APP_API,
  headers: getAuthHeaders(),
});

interface AuthContextValue {
  authUser: AuthUserObject | null;
  currentTeam: string | null;
  teams: UserTeamObject[];
  getToken: () => string;
  setCurrentTeam: (team: string) => void;
  getCurrentTeam: () => string | null;
  setUser: (token: string, refresh: string) => void;
  login: (email: string, password: string, rememberMe: boolean) => Promise<any>;
  logout: () => Promise<void>;
  register: (data: APIAuthRegisterData) => Promise<any>;
  setTeams: (teams: UserTeamObject[]) => void;
}

export const AuthContext = React.createContext<AuthContextValue>(null as any);

export const decodeToken = (
  token: string
): AuthUserObject | AuthAttendeeObject | null => {
  if (token) {
    try {
      const obj = jwt.decode(token);
      return obj as AuthUserObject | AuthAttendeeObject;
    } catch (error) {
      // ignore
    }
  }

  return null;
};

const getUserFromToken = (token: string): AuthUserObject | null => {
  if (token) {
    try {
      const obj = jwt.decode(token);
      // Cookies.remove('tltoken');
      // Cookies.set('tltoken', token, { expires: new Date(obj.exp*1000)})
      return obj as AuthUserObject;
    } catch (error) {
      // ignore
    }
  }

  return null;
};

export const AuthProvider: React.FC = ({ children }) => {
  const token = Cookies.get("em-token");

  const setToken = (token: string | null) => {
    Cookies.set("em-token", token);
    window.location.reload();
  };

  // TODO: Is this needed?
  const [refreshToken, setRefreshToken] = useState<string | null>(
    Cookies.get("em-refresh")
  );

  silence(refreshToken);

  const [authUser, setAuthUser] = useState<AuthUserObject | null>(
    token && getUserFromToken(token)
  );

  const [teams, setTeams] = useState<UserTeamObject[]>([]);

  const [currentTeam, _setCurrentTeam] = useState<string | null>(
    secureLS.get("currentTeam")
  );

  // At this point, do we even need the state variable for the current team?
  const setCurrentTeam = useCallback(async (currentTeam: string | null) => {
    _setCurrentTeam(currentTeam);
    if (currentTeam) {
      secureLS.set("currentTeam", currentTeam);
    } else {
      secureLS.remove("currentTeam");
    }

    // TODO: Very dirty, we need a way to invalidate cached data when the team changes
    window.location.reload();
  }, []);

  const getCurrentTeam = () => {
    return secureLS.get("currentTeam");
  };

  const login = async (
    email: string,
    password: string,
    rememberMe: boolean
  ) => {
    const postData = { email, password };
    const response = await api.post(API.LOGIN, postData);
    if (response.status === 200) {
      const expires = rememberMe ? 7 : undefined;
      Cookies.set("em-token", response.data.token, { expires });
      Cookies.set("em-refresh", response.data.refreshToken, { expires });
      setToken(response.data.token);
      setRefreshToken(response.data.refreshToken);
      setCurrentTeam(response.data.defaultTeam);
    }
    return response;
  };

  const logout = useCallback(async () => {
    Cookies.remove("em-token");
    Cookies.remove("em-refresh");
    setAuthUser(null);
    setToken(null);
    setRefreshToken(null);
    setCurrentTeam(null);
  }, [setCurrentTeam]);

  const register = async (data: APIAuthRegisterData) => {
    const response = await api.post(API.REGISTER, data);

    Cookies.set("em-token", response.data.token);
    Cookies.set("em-refresh", response.data.refresh);
    Cookies.set("em-refresh", response.data.refresh);
    setToken(response.data.token);
    setRefreshToken(response.data.refreshToken);
    setCurrentTeam(response.data.defaultTeam);

    return response.data;
  };

  const setUser = async (token: string, refresh: string) => {
    Cookies.set("em-token", token);
    Cookies.set("em-refresh", refresh);
    setToken(token);
    setRefreshToken(refresh);
  };

  useEffect(() => {
    const _token = token || Cookies.get("em-token");
    if (!_token || !authUser) {
      return;
    }

    api
      .get(API.USER_TEAMS, {
        headers: { Authorization: "Bearer " + _token },
      })
      .then((response) => {
        if (response.data.data.length === 0) {
          Cookies.set("em-token", undefined);
          window.location.reload();
        }
        setTeams(response.data.data);
      })
      .catch((error) => {
        if (error.response?.status === 401 || error.response?.status === 403) {
          logout();
        }
        console.log(error);
        setTeams([]);
      });
  }, [authUser, token, logout]);

  useEffect(() => {
    if (!_.isEmpty(teams) && !currentTeam) {
      setCurrentTeam(teams[0].team.slug);
    }
  }, [currentTeam, teams, setCurrentTeam]);

  const getToken = () => {
    const queryParams = new URLSearchParams(window.location.search);
    return queryParams.get("token") || Cookies.get("em-token");
  };

  return (
    <AuthContext.Provider
      value={{
        authUser,
        setUser,
        login,
        logout,
        register,
        getToken,
        teams,
        getCurrentTeam,
        currentTeam,
        setCurrentTeam,
        setTeams,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
