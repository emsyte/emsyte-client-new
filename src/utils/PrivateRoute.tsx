import * as _ from "lodash";
import React, { useContext } from "react";
import { Redirect, Route } from "react-router-dom";
import { AuthContext } from "./Authentication";
import PageLoading from "./PageLoading";

const PrivateRoute = ({ component: RouteComponent, ...rest }: any) => {
  const { authUser, teams, currentTeam } = useContext(AuthContext);
  return (
    <Route
      {...rest}
      render={(routeProps: any) => {
        if (_.isEmpty(authUser)) return <Redirect to={"/login"} />;

        if (_.isEmpty(teams) || _.isEmpty(currentTeam)) {
          return <PageLoading></PageLoading>;
        }

        return <RouteComponent {...routeProps} />;
      }}
    />
  );
};

export default PrivateRoute;
