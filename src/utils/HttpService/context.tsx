import { APIResponse } from "@emsyte/common/api";
import { AttendeeObject } from "@emsyte/common/attendee";
import {
  IntegrationObjectFull,
  IntegrationProvider,
} from "@emsyte/common/integration";
import {
  APINotificationCreateData,
  APINotificationUpdateData,
  NotificationObject,
} from "@emsyte/common/notification";
import {
  APIEditTeamData,
  APIEditTeamMemberData,
  APIInviteTeamMemberData,
  TeamObject,
  TeamUserObject,
} from "@emsyte/common/team";
import {
  APITemplateCreateData,
  APITemplateInstanceCreateData,
  APITemplateInstanceUpdateData,
  APITemplateUpdateData,
  TemplateInstanceObject,
  TemplateInstanceObjectFull,
  TemplateObject,
  TemplateObjectFull,
  TemplateType,
} from "@emsyte/common/template";
import {
  APIChangePasswordData,
  APIUserForgotPasswordConfirmData,
  APIUserForgotPasswordData,
  APIUserResponse,
  APIUserUpdateData,
  UserObject,
} from "@emsyte/common/user";
import {
  APINotificationGatewaySetData,
  APIWebinarAddEventVideoUrl,
  APIWebinarAddRecurrenceData,
  APIWebinarAttendeeCreateData,
  APIWebinarChatCreateData,
  APIWebinarChatUpdateData,
  APIWebinarCreateData,
  APIWebinarOfferCreateData,
  APIWebinarUpdateData,
  CalendarEventObject,
  EventObject,
  PublicWebinarObject,
  RecurrenceObject,
  ViewerEventObject,
  WebinarChatObject,
  WebinarObject,
} from "@emsyte/common/webinar";
import axios from "axios";
import Cookies from "js-cookie";
import moment from "moment";
import React from "react";
import { useAuth } from "../Authentication";
import { API } from "../constants";

const { REACT_APP_API } = process.env;

moment.updateLocale("en", {
  relativeTime: {
    future: "in %s",
    past: "%s",
    s: "1s",
    ss: "%ss",
    m: "1m",
    mm: "%dm",
    h: "1h",
    hh: "%dh",
    d: "1d",
    dd: "%dd",
    M: "1M",
    MM: "%dM",
    y: "1y",
    yy: "%dY",
  },
});

export const HttpServiceContext = React.createContext<{
  api: ReturnType<typeof useAPI>;
}>(null as any);

function useAPI() {
  const { currentTeam, getToken, logout, setUser } = useAuth();
  const api = axios.create({
    baseURL: REACT_APP_API,
  });

  api.interceptors.request.use((config) => {
    if (config.url) {
      config.url = config.url.replace(/:team/g, currentTeam ?? "");
    }

    return config;
  });

  api.interceptors.request.use(function (config) {
    // Query param will always override the cookie
    const token = getToken();
    config.headers.common["Authorization"] = token ? `Bearer ${token}` : null;
    return config;
  });

  // Refresh Token Configuration
  api.interceptors.response.use(
    (response) => {
      return response;
    },
    (error) => {
      // This function is part of a promise chain.
      // It needs to return a value or another promise so the caller knows when it
      // has completed.

      // Pass all non 401s back to the caller.
      if (error.response?.status !== 401 && error.response?.status !== 403) {
        return Promise.reject(error);
      }

      const tokens = {
        token: Cookies.get("tl-token"),
        refreshToken: Cookies.get("tl-refresh"),
      };

      // reject removes a previously added interceptor with an id.
      // I don't think you want this, it isn't doing anything.
      // axios.interceptors.response.reject(/* id */);

      // As the refresh request was not being returned, it wasn't part of the
      // promise chain. The callers .then / .catch would be called before the
      // token refresh had completed and you would still have the old token in
      // localStorage.
      console.log(tokens);
      // return;
      return api
        .post(`${API.USERS}/token/refresh`, tokens)
        .then((response) => {
          setUser(response.data.token, response.data.refreshToken);

          // This will update future request headers
          axios.defaults.headers.common[
            "Authorization"
          ] = `Bearer ${response.data.token}`;

          axios.defaults.headers.common["Content-Type"] = "application/json";
          axios.defaults.headers.common["Allow"] = "application/json";

          // Still need to return the an error because the original request failed
          // but added a property to show it can be tried again.
          error.hasRefreshedToken = true;

          return Promise.reject(error);
        })
        .catch((error) => {
          const tokenError: any = new Error("Cannot refresh token");
          tokenError.originalError = error;
          logout();
          // history.push('/login');
          return Promise.reject(tokenError);
        });
    }
  );

  /*
   * User Methods
   * ----------------------
   */
  const users = {
    doGetUser: async (data: APIUserUpdateData) => {
      const response = await api.get<APIResponse<UserObject>>(
        `${API.ACCOUNT}/user`
      );
      return response.data;
    },

    doUpdateUser: async (data: APIUserUpdateData) => {
      const response = await api.put<APIUserResponse>(
        `${API.ACCOUNT}/user`,
        data
      );
      setUser(response.data.token, response.data.refreshToken);

      return response.data;
    },

    doChangePassword: async (data: APIChangePasswordData) => {
      const response = await api.post<{ message: string }>(
        `${API.ACCOUNT}/change-password`,
        data
      );
      return response.data;
    },

    doDeactivateAccount: async () => {
      const response = await api.delete<{ message: string }>(API.ACCOUNT);
      return response.data;
    },

    doForgotPassword: async (data: APIUserForgotPasswordData) => {
      const response = await api.post<{ message: string }>(
        `${API.USERS}/forgot-password`,
        data
      );
      return response.data;
    },

    doForgotPasswordConfirm: async (data: APIUserForgotPasswordConfirmData) => {
      const response = await api.post<{ message: string }>(
        `${API.USERS}/forgot-password-confirm`,
        data
      );
      return response.data;
    },
  };

  /*
   * Transaction Emails
   * ----------------------
   */
  const emails = {
    doSendInviteEmail: async (
      email: string,
      temp: string = "",
      inviter: string
    ) => {
      const newAccount = !!temp.length;
      const postData = { email, temp, type: "invite", newAccount, inviter };
      const response = await api.post(API.EMAILS, postData);
      return response.data;
    },
    doSendWelcomeEmail: async (
      email: string,
      firstName: string,
      lastName: string
    ) => {
      const postData = { email, firstName, lastName, type: "welcome" };
      const response = await api.post(API.EMAILS, postData);
      return response.data;
    },
    doSendDeactivationEmail: async (email: string) => {
      const postData = { email, type: "deactivate" };
      const response = await api.post(API.EMAILS, postData);
      return response.data;
    },
  };

  /*
   * Integrations
   * ----------------------
   */
  const integrations = {
    doGetIntegrations: async () => {
      const response = await api.get<APIResponse<IntegrationObjectFull[]>>(
        `${API.INTEGRATIONS}/get`
      );
      return await response.data;
    },
    doEditIntegration: async (postData: any) => {
      const response = await api.post<APIResponse<IntegrationObjectFull>>(
        `${API.INTEGRATIONS}/set`,
        postData
      );
      return await response.data;
    },
  };
  /*
   * Templates
   * ----------------------
   */
  const templates = {
    doGetAllTemplates: async () => {
      const response = await api.get<APIResponse<TemplateObject[]>>(
        `${API.TEMPLATES}`
      );
      return await response.data;
    },
    doGetTemplateInstances: async (webinarId: number) => {
      const response = await api.get<APIResponse<TemplateInstanceObject[]>>(
        `${API.TEMPLATES}/webinar/${webinarId}`
      );
      return response.data;
    },

    doCreateTemplateInstance: async (
      webinarId: number,
      data: APITemplateInstanceCreateData
    ) => {
      const response = await api.post<APIResponse<TemplateInstanceObject>>(
        `${API.TEMPLATES}/webinar/${webinarId}`,
        data
      );
      return response.data;
    },

    doUpdateTemplateInstance: async (
      instanceId: number,
      data: APITemplateInstanceUpdateData
    ) => {
      const response = await api.put<APIResponse<TemplateInstanceObject>>(
        `${API.TEMPLATES}/instance/${instanceId}`,
        data
      );
      return response.data;
    },

    doGetTemplateInstance: async (instanceId: number) => {
      const response = await api.get<APIResponse<TemplateInstanceObjectFull>>(
        `${API.TEMPLATES}/instance/${instanceId}`
      );
      return response.data;
    },

    doDeleteTemplateInstance: async (instanceId: number) => {
      const response = await api.delete<APIResponse<TemplateInstanceObject>>(
        `${API.TEMPLATES}/instance/${instanceId}`
      );
      return response.data;
    },

    doResetTemplateInstance: async (instanceId: number) => {
      const response = await api.post<APIResponse<TemplateInstanceObject>>(
        `${API.TEMPLATES}/instance/${instanceId}/reset`
      );
      return response.data;
    },

    doCreateTemplateFromInstance: async (data: APITemplateCreateData) => {
      const response = await api.post<APIResponse<TemplateInstanceObject>>(
        `${API.TEMPLATES}`,
        data
      );
      return response.data;
    },

    doUpdateTemplateFromInstance: async (
      templateId: number,
      data: APITemplateUpdateData
    ) => {
      const response = await api.put<APIResponse<TemplateInstanceObject>>(
        `${API.TEMPLATES}/${templateId}`,
        data
      );
      return response.data;
    },
    doDeleteTemplate: async (templateId: number) => {
      const response = await api.delete<APIResponse<TemplateInstanceObject>>(
        `${API.TEMPLATES}/${templateId}`
      );
      return response.data;
    },

    doGetGlobalTemplate: async (templateId: number) => {
      const response = await api.get<APIResponse<TemplateObjectFull>>(
        `${API.TEMPLATES}/${templateId}`
      );
      return response.data;
    },
  };

  /*
   * Webinar
   * ----------------------
   */
  const webinars = {
    doCreateWebinar: async (data: APIWebinarCreateData) => {
      const response = await api.post<APIResponse<WebinarObject>>(
        `${API.WEBINARS}`,
        data
      );

      return response.data;
    },

    // TODO: Why is this is string?
    doUpdateWebinar: async (
      id: string | number,
      data: APIWebinarUpdateData
    ) => {
      const response = await api.put<APIResponse<WebinarObject>>(
        `${API.WEBINARS}/${id}`,
        data
      );

      return response.data;
    },

    doDeleteWebinar: async (id: number) => {
      const response = await api.delete<APIResponse<WebinarObject>>(
        `${API.WEBINARS}/${id}`
      );

      return response.data;
    },

    doActivateWebinar: async (id: number) => {
      const response = await api.post<APIResponse<WebinarObject>>(
        `${API.WEBINARS}/${id}/activate`
      );

      return response.data;
    },

    doArchiveWebinar: async (id: number) => {
      const response = await api.post<APIResponse<WebinarObject>>(
        `${API.WEBINARS}/${id}/archive`
      );

      return response.data;
    },

    doGetRecurrences: async (id: number) => {
      const response = await api.get<APIResponse<RecurrenceObject[]>>(
        `${API.WEBINARS}/${id}/recurrences`
      );
      return response.data;
    },

    doCreateRecurrence: async (
      id: number,
      data: APIWebinarAddRecurrenceData
    ) => {
      const response = await api.post<APIResponse<RecurrenceObject>>(
        `${API.WEBINARS}/${id}/recurrences`,
        data
      );
      return response.data;
    },

    doDeleteRecurrence: async (id: number, recurrenceId: number) => {
      const response = await api.delete<APIResponse<RecurrenceObject>>(
        `${API.WEBINARS}/${id}/recurrences/${recurrenceId}`
      );
      return response.data;
    },

    doSetEventUrl: async (
      webinarId: number,
      eventId: number,
      data: APIWebinarAddEventVideoUrl
    ) => {
      const response = await api.put<APIResponse<ViewerEventObject>>(
        `${API.WEBINARS}/${webinarId}/events/${eventId}`,
        data
      );
      return response.data;
    },

    doGetSchedule: async () => {
      const response = await api.get<
        APIResponse<{
          events: CalendarEventObject[];
          recurrences: RecurrenceObject[];
        }>
      >(`${API.WEBINARS}/schedule`);

      return response.data;
    },

    doGetWebinar: async (webinar: number) => {
      const response = await api.get<APIResponse<WebinarObject>>(
        `${API.WEBINARS}/${webinar}`
      );
      return response.data;
    },

    doGetWebinarEvent: async (webinarId: number, eventId: number) => {
      const response = await api.get<APIResponse<ViewerEventObject>>(
        `${API.WEBINARS}/${webinarId}/events/${eventId}`
      );
      return response.data;
    },

    doGetAllWebinarsForUser: async () => {
      const response = await api.get<APIResponse<WebinarObject[]>>(
        `${API.WEBINARS}`
      );
      return await response.data;
    },
    doGetAllEventsForUser: async (
      pagination?:
        | {
            startDate: Date;
            endDate: Date;
            includeTime?: boolean;
          }
        | number,
      order?: "ASC" | "DESC"
    ) => {
      function format(
        time: moment.Moment,
        includeTime: boolean = false
      ): string {
        if (includeTime) {
          return time.toISOString();
        }
        return time.format("YYYY-MM-DD");
      }

      let queryParams =
        typeof pagination === "number"
          ? `limit=${pagination}`
          : pagination
          ? `from=${format(
              moment(pagination.startDate),
              pagination.includeTime
            )}&to=${format(moment(pagination.endDate), pagination.includeTime)}`
          : "";
      if (queryParams && order) queryParams = queryParams + "&";
      if (order) queryParams = queryParams + `order=${order}`;

      const response = await api.get<APIResponse<CalendarEventObject[]>>(
        `${API.EVENTS}?${queryParams}`
      );
      return response.data;
    },

    doRenderPageTemplate: async (
      id: number,
      type: TemplateType,
      extra: string
    ) => {
      const PAGE_VIEW_KEY = "em_pvk";
      const pageViewKey = localStorage.getItem(PAGE_VIEW_KEY);
      const params = new URLSearchParams(extra);

      if (pageViewKey) {
        params.append("pageViewKey", pageViewKey);
      }

      const response = await api.get<
        APIResponse<{ content: string; pageViewKey: string }>
      >(`${API.WEBINAR_PUBLIC}/${id}/render/${type}?${params.toString()}`);

      localStorage.setItem(PAGE_VIEW_KEY, response.data.data.pageViewKey);

      return response.data.data.content;
    },

    doGetPublicWebinar: async (id: number) => {
      const response = await api.get<APIResponse<PublicWebinarObject>>(
        `${API.WEBINAR_PUBLIC}/${id}`
      );
      return response.data.data;
    },

    doGetAllPublicWebinarEvents: async (id: number) => {
      const response = await api.get<APIResponse<EventObject[]>>(
        `${API.WEBINAR_PUBLIC}/${id}/events`
      );
      return response.data.data;
    },

    doGetPublicWebinarEvent: async (webinarId: number, eventId: number) => {
      const response = await api.get<APIResponse<EventObject>>(
        `${API.WEBINAR_PUBLIC}/${webinarId}/events/${eventId}`
      );
      return response.data.data;
    },

    doCreateAttendeeForEvent: async (
      webinarId: number,
      eventId: number,
      data: APIWebinarAttendeeCreateData
    ) => {
      const response = await api.post(
        `${API.WEBINAR_PUBLIC}/${webinarId}/events/${eventId}/register`,
        data
      );
      return response.data;
    },
  };
  /*
   * Offers
   * ----------------------
   */
  const offers = {
    doGetWebinarOffers: async (webinarId: number) => {
      const response = await api.get(`${API.WEBINARS}/${webinarId}/offers`);
      return await response.data;
    },
    doCreateWebinarOffer: async (
      webinar: string,
      data: APIWebinarOfferCreateData & { id: number }
    ) => {
      const response = await api.post(
        `${API.WEBINARS}/${webinar}/offers`,
        data
      );
      return await response.data;
    },
  };

  const notifications = {
    doCreateNotification: async (
      webinarId: number,
      data: APINotificationCreateData
    ) => {
      const response = await api.post<APIResponse<NotificationObject>>(
        `${API.WEBINARS}/${webinarId}/notifications`,
        data
      );
      return response.data;
    },

    doGetNotifications: async (webinarId: number) => {
      const response = await api.get<APIResponse<NotificationObject[]>>(
        `${API.WEBINARS}/${webinarId}/notifications`
      );
      return response.data;
    },

    doUpdateNotification: async (
      webinarId: number,
      notificationId: number,
      data: APINotificationUpdateData
    ) => {
      const response = await api.put<APIResponse<NotificationObject>>(
        `${API.WEBINARS}/${webinarId}/notifications/${notificationId}`,
        data
      );
      return response.data;
    },

    doDeleteNotification: async (webinarId: number, notificationId: number) => {
      const response = await api.delete<{}>(
        `${API.WEBINARS}/${webinarId}/notifications/${notificationId}`
      );
      return response.data;
    },
  };

  const notificationGateway = {
    async doSetNotificationGateway(
      webinarId: number,
      type: "email" | "sms",
      data: APINotificationGatewaySetData
    ) {
      const response = await api.post<APIResponse<WebinarObject>>(
        `${API.WEBINARS}/${webinarId}/gateway?type=${type}`,
        data
      );
      return response.data;
    },

    async doRemoveNotificationGateway(
      webinarId: number,
      type: "email" | "sms",
      data: APINotificationGatewaySetData
    ) {
      const response = await api.delete<APIResponse<WebinarObject>>(
        `${API.WEBINARS}/${webinarId}/gateway?type=${type}`
      );

      return response.data;
    },

    async doGetNotificationGatewayFields(provider: IntegrationProvider) {
      const response = await api.get<
        APIResponse<
          {
            label: string;
            value: string;
          }[]
        >
      >(`${API.WEBINARS}/gateway/fields?provider=${provider}`);

      return response.data;
    },
  };

  const fileUpload = {
    doUploadFile: async (
      group: string,
      file: File,
      onUploadProgress?: (progressEvent: any) => void
    ) => {
      const response = await api.get<{ url: string; uploadTo: string }>(
        `${API.MEDIA}/upload-url?group=${group}&filename=${file.name}&contentType=${file.type}`
      );
      const { url, uploadTo } = response.data;
      const instance = axios.create();

      const getData = (file: File) => {
        if (process.env.NODE_ENV === "production") {
          return file;
        }
        const data = new FormData();
        data.append("file", file);
        return data;
      };

      await instance.put(uploadTo, getData(file), {
        headers:
          process.env.NODE_ENV === "production"
            ? {
                "Content-Type": file.type,
              }
            : {},
        onUploadProgress,
      });

      return url;
    },
  };

  const chat = {
    doGetAllWebinarChatMessages: async (webinarId: number) => {
      const response = await api.get<APIResponse<WebinarChatObject[]>>(
        `${API.WEBINARS}/${webinarId}/chats`
      );
      return response.data;
    },
    doCreateWebinarChatMessage: async (
      webinarId: number,
      data: APIWebinarChatCreateData
    ) => {
      const response = await api.post<APIResponse<WebinarChatObject>>(
        `${API.WEBINARS}/${webinarId}/chats`,
        data
      );
      return response.data;
    },

    doUpdateWebinarChatMessage: async (
      webinarId: number,
      chatId: number,
      data: APIWebinarChatUpdateData
    ) => {
      const response = await api.put<APIResponse<WebinarChatObject>>(
        `${API.WEBINARS}/${webinarId}/chats/${chatId}`,
        data
      );
      return response.data;
    },

    doGetChatMessage: async (webinarId: number, chatId: number) => {
      const response = await api.put<APIResponse<WebinarChatObject>>(
        `${API.WEBINARS}/${webinarId}/chats/${chatId}`
      );
      return response.data;
    },

    doDeleteChatMessage: async (webinarId: number, chatId: number) => {
      const response = await api.delete<{}>(
        `${API.WEBINARS}/${webinarId}/chats/${chatId}`
      );
      return response.data;
    },

    doUploadWebinarChatMessage: async (webinarId: number, file: any) => {
      const response = await api.post<APIResponse<any>>(
        `${API.WEBINARS}/${webinarId}/chats/upload`,
        file
      );
      return response.data;
    },
  };

  /*
   * Reactions
   * ----------------------
   */
  const reactions = {
    doGetUploadUrl: async (eventId: number, start: number) => {
      const response = await api.get(
        `${API.WEBINAR_VIEWER}/${eventId}/get-upload-url?start=${start}`
      );
      return response.data.upload_to as string;
    },
  };

  /*
   * Teams
   * ----------------------
   */

  const teamManage = {
    // doDeleteTeam: async () => {
    //   const response = await api.delete<APIResponse<TeamObject>>(
    //     API.TEAM_MANAGE
    //   );
    //   return response.data;
    // },

    /**
     * Edit currentTeam
     */
    doEditTeam: async (data: APIEditTeamData) => {
      const response = await api.put<APIResponse<TeamObject>>(
        API.TEAM_MANAGE,
        data
      );
      return response.data;
    },

    /**
     * Get members for the currentTeam
     */
    doGetTeamMembers: async () => {
      const response = await api.get<APIResponse<TeamUserObject[]>>(
        `${API.TEAM_MANAGE}/members`
      );
      return response.data;
    },

    /**
     * Invite a team member to the currentTeam
     */
    doInviteTeamMember: async (data: APIInviteTeamMemberData) => {
      const response = await api.post<APIResponse<TeamUserObject>>(
        `${API.TEAM_MANAGE}/members`,
        data
      );
      return response.data;
    },

    /**
     * Remove a member from the current team
     */
    doRemoveTeamMember: async (userId: number) => {
      const response = await api.delete<{}>(
        `${API.TEAM_MANAGE}/members/${userId}`
      );

      return response.data;
    },

    /**
     * Edit the role of the current team member
     */
    doEditTeamMember: async (userId: number, data: APIEditTeamMemberData) => {
      const response = await api.put<APIResponse<TeamUserObject>>(
        `${API.TEAM_MANAGE}/members/${userId}`,
        data
      );

      return response.data;
    },
  };

  const analytics = {
    doGetAudience: async () => {
      const response = await api.get<APIResponse<AttendeeObject[]>>(
        `${API.ANALYTICS}/audience`
      );

      return response.data;
    },
    doGetAudienceDetails: async (email: string) => {
      const emailId = btoa(email);

      const response = await api.get<APIResponse<any>>(
        `${API.ANALYTICS}/audience/${emailId}`
      );

      return response.data;
    },

    doGetEventAnalytics: async (eventId: number) => {
      const response = await api.get<any>(`${API.ANALYTICS}/event/${eventId}`);

      return response.data.data;
    },

    doGetWebinarAnalytics: async (
      webinarId: number,
      {
        startDate,
        endDate,
      }: {
        startDate: Date;
        endDate: Date;
      }
    ) => {
      const response = await api.get<any>(
        `${API.ANALYTICS}/webinar/${webinarId}?from=${moment(startDate).format(
          "YYYY-MM-DD"
        )}&to=${moment(endDate).format("YYYY-MM-DD")}`
      );

      return response.data.data;
    },

    doGetDashboardAnalytics: async ({
      startDate,
      endDate,
    }: {
      startDate: Date;
      endDate: Date;
    }) => {
      const response = await api.get<any>(
        `${API.ANALYTICS}/dashboard?from=${moment(startDate).format(
          "YYYY-MM-DD"
        )}&to=${moment(endDate).format("YYYY-MM-DD")}`
      );

      return response.data.data;
    },
  };

  return {
    ...fileUpload,
    ...chat,
    ...users,
    ...emails,
    ...integrations,
    ...notificationGateway,
    ...templates,
    ...webinars,
    ...notifications,
    ...reactions,
    ...offers,
    ...teamManage,
    ...analytics,
  };
}
const HttpServiceProvider = ({ children }) => {
  const api = useAPI();
  return (
    <HttpServiceContext.Provider
      value={{
        api,
      }}
    >
      {children}
    </HttpServiceContext.Provider>
  );
};

export default HttpServiceProvider;
