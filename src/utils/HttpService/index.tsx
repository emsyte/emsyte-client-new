import React, { useContext } from "react";
import HttpServiceProvider, { HttpServiceContext } from "./context";

export const withHttpService = (Component: any) => (props: any) => (
  <HttpServiceContext.Consumer>
    {({ api }) => {
      return <Component api={api} {...props} />;
    }}
  </HttpServiceContext.Consumer>
);

export function useHttpService() {
  const { api } = useContext(HttpServiceContext);
  return api;
}

export default withHttpService;
export { HttpServiceContext, HttpServiceProvider };
