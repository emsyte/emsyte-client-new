import { APIResponse } from "@emsyte/common/api";
import { TemplateType } from "@emsyte/common/template";
import axios from "axios";
import { API } from "../constants";

const { REACT_APP_API } = process.env;

export function getPublicHttpService() {
  const api = axios.create({
    baseURL: REACT_APP_API,
  });

  return {
    doRenderPageTemplate: async (
      id: number,
      type: TemplateType,
      extra: string
    ) => {
      const PAGE_VIEW_KEY = "em_pvk";
      const pageViewKey = localStorage.getItem(PAGE_VIEW_KEY);
      const params = new URLSearchParams(extra);

      if (pageViewKey) {
        params.append("pageViewKey", pageViewKey);
      }

      const response = await api.get<
        APIResponse<{ content: string; pageViewKey: string }>
      >(`${API.WEBINAR_PUBLIC}/${id}/render/${type}?${params.toString()}`);

      localStorage.setItem(PAGE_VIEW_KEY, response.data.data.pageViewKey);

      return response.data.data.content;
    },
  };
}
