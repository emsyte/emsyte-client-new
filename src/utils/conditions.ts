/**
 * Function to help the typescript compiler know to fail if some possible values aren't covered in a switch statement
 *
 * For example:
 *
 * ```
 * enum Color {
 *   Red,
 *   Blue,
 *   Green
 * }
 *
 * function getColorName(color: Color) {
 *   switch (color) {
 *     case Color.Red:
 *       return "Red";
 *     case Color.Blue:
 *       return "Blue";
 *     default:
 *       // This won't compile because `color` could be Green, and unknownValue only accepts never
 *       return `Unknown color: ${unknownValue(color)}`;
 *   }
 * }
 *```
 */
export function unknownValue(value: never) {
  return value;
}

type Falsy = false | null | undefined | 0;

/**
 * Return true if the value is a truthy value
 *
 * @param value The value to determine if it is truthy
 */
export function isTruthy<T>(value: T | Falsy): value is T {
  return !!value;
}

/**
 * Filter out the falsy values in an array
 */
export function filterFalsy<T>(items: Array<T | false | undefined | null>) {
  return items.filter(isTruthy);
}
