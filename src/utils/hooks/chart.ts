import { useEffect, useRef, useState } from "react";
import Chart from "../chart";

export function useChart(config: any) {
  const canvasRef = useRef<HTMLCanvasElement>(null);
  const [chart, setChart] = useState<any>();

  useEffect(() => {
    if (canvasRef.current) {
      const chart = new (Chart as any)(canvasRef.current, config);
      setChart(chart);
    }
    // XXX: It's the responsibility of the calling component to call chart.update() at the appropriate time to update the chart
    // eslint-disable-next-line
  }, [canvasRef]);

  return { canvasRef, chart };
}
