import { ErrorOption } from "react-hook-form";

export function setRemoteFormErrors<T extends string>(
  errors: Record<string, string[]> | undefined,
  setError: (name: T, error: ErrorOption) => void
) {
  if (!errors) {
    return;
  }

  for (const [field, message] of Object.entries(errors)) {
    setError(field as T, {
      type: "remote",
      message: message[0],
      shouldFocus: true,
    });
  }
}
