import { APIResponse } from "@emsyte/common/api";
import { ViewerEventObject, WebinarOfferObject } from "@emsyte/common/webinar";
import axios from "axios";
import { useMemo } from "react";
import { useAuth } from "./Authentication";
import { API } from "./constants";

const { REACT_APP_API } = process.env;

function getAPI(token: string) {
  const api = axios.create({
    baseURL: REACT_APP_API,
  });

  api.interceptors.request.use(function (config) {
    // Query param will always override the cookie
    config.headers.common["Authorization"] = token ? `Bearer ${token}` : null;
    return config;
  });

  return {
    doGetViewerEvent: async (eventId: number) => {
      const response = await api.get<APIResponse<ViewerEventObject>>(
        `${API.WEBINAR_VIEWER}/${eventId}/event`
      );
      return response.data.data;
    },
    doGetViewerOffers: async (eventId: number) => {
      const response = await api.get<APIResponse<WebinarOfferObject[]>>(
        `${API.WEBINAR_VIEWER}/${eventId}/offers`
      );
      return response.data.data;
    },
    doTrackOfferClick: async (eventId: number, offerId: number) => {
      const response = await api.post(
        `${API.WEBINAR_VIEWER}/${eventId}/offers/${offerId}/track`,
        {},
        { withCredentials: true }
      );

      return response.data;
    },

    doTrackHeartbeat: async (eventId: number, time_offset: number) => {
      const response = await api.post(
        `${API.WEBINAR_VIEWER}/${eventId}/heartbeat`,
        { time_offset }
      );

      return response.data;
    },
  };
}

export function useViewerAPI() {
  const { getToken } = useAuth();
  const token = getToken();
  const api = useMemo(() => getAPI(token), [token]);
  return api;
}
