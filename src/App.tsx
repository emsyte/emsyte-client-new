import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import routes from "./routes";
import withTracker from "./withTracker";

import { AuthProvider } from "./utils/Authentication";
import { HttpServiceProvider } from "./utils/HttpService";
import { StripeServiceProvider } from "./utils/Stripe";

import PrivateRoute from "./utils/PrivateRoute";

import "bootstrap/dist/css/bootstrap.min.css";
import "./assets/main.scss";

import "react-date-range/dist/styles.css"; // main css file
import "react-date-range/dist/theme/default.css"; // theme css file
import { NotificationProvider } from "./components/common/Notification";
import { QueryClient, QueryClientProvider } from "react-query";
import FeatureFlagsProvider from "./utils/FeatureFlags";

const client = new QueryClient();

const Providers: React.FC = ({ children }) => {
  return (
    <NotificationProvider>
      <AuthProvider>
        <HttpServiceProvider>
          <StripeServiceProvider>
            <QueryClientProvider client={client}>
              <FeatureFlagsProvider>{children}</FeatureFlagsProvider>
            </QueryClientProvider>
          </StripeServiceProvider>
        </HttpServiceProvider>
      </AuthProvider>
    </NotificationProvider>
  );
};

export default () => (
  <Providers>
    <Router basename={process.env.REACT_APP_BASENAME || ""}>
      <div>
        {routes.map((route, index) => {
          if (route.protected) {
            return (
              <PrivateRoute
                key={index}
                path={route.path}
                exact={route.exact}
                component={withTracker((props) => {
                  return (
                    <route.layout {...props} title={route.title}>
                      <route.component {...props} />
                    </route.layout>
                  );
                })}
              />
            );
          } else {
            return (
              <Route
                key={index}
                path={route.path}
                exact={route.exact}
                component={withTracker((props) => {
                  return (
                    <route.layout {...props} title={route.title}>
                      <route.component {...props} />
                    </route.layout>
                  );
                })}
              />
            );
          }
        })}
      </div>
    </Router>
  </Providers>
);
