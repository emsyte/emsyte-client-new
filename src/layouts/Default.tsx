import React, { useState } from "react";
import { LiveStreamNotifications } from "../components/layout/BannerNotifications";
import MainFooter from "../components/layout/MainFooter";
import AppBar from "../components/layout/MainNavbar/AppBar";
import WebinarNavbar from "../components/layout/MainNavbar/WebinarNavbar";
import MainSidebar from "../components/layout/MainSidebar/MainSidebar";
import styled from "styled-components";

const Wrapper = styled.div`
  position: fixed;
  width: 100%;
  height: 100%;
`;
interface DefaultLayoutProps {
  noFooter?: boolean;
  noWebinar?: boolean;
  noSideBar?: boolean;
  noMainNavBar?: boolean;
  title: React.ReactNode | (() => React.ReactNode);
}

const DefaultLayout: React.FC<DefaultLayoutProps> = ({
  children,
  noFooter = false,
  noSideBar = false,
  noMainNavBar = false,
  noWebinar = true,
  title,
}) => {
  const [sidebarOpen, setSidebarOpen] = useState(false);

  return (
    <Wrapper>
      {!noSideBar && (
        <MainSidebar open={sidebarOpen} setOpen={setSidebarOpen} />
      )}
      <main className="main-content">
        {!noMainNavBar && (
          <AppBar
            title={typeof title === "function" ? title() : title}
            onOpen={() => setSidebarOpen(true)}
          ></AppBar>
        )}
        {/* {!noMainNavBar && <MainNavbar onOpen={() => setSidebarOpen(true)} />} */}
        {!noWebinar && <WebinarNavbar />}
        {!noMainNavBar && <LiveStreamNotifications></LiveStreamNotifications>}
        <div className="inner">
          {children}
          {!noFooter && <MainFooter />}
        </div>
        <div className="sidebar-backdrop"></div>
      </main>
    </Wrapper>
  );
};

export default DefaultLayout;
