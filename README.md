[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/emsyte/emsyte-client-new)

Emsyte Client
======================
Just Webinar frontend client

Getting Started
---------------
Make sure the API server is running (from the emsyte-api repo), then run these commands to get started.

1. `yarn install`
2. `yarn start`
