import { APIResponse } from "@emsyte/common/api";
import {
  TeamObject,
  TeamPlan,
  TeamStatus,
  TeamUserRole,
  TeamUserStatus,
  UserTeamObject,
} from "@emsyte/common/team";

export function getTeam(): TeamObject {
  return {
    billingEmail: "",
    name: "My Team",
    plan: TeamPlan.PREMIUM,
    slug: "team",
    status: TeamStatus.ENABLED,
  };
}

export function getTeams(): APIResponse<UserTeamObject[]> {
  return {
    data: [
      {
        role: TeamUserRole.ADMIN,
        status: TeamUserStatus.ACTIVATED,
        team: getTeam(),
      },
    ],
  };
}
