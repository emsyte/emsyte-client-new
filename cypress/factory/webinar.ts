import {
  WebinarObject,
  WebinarSettingsObject,
  WebinarStatus,
  WebinarType,
} from "@emsyte/common/webinar";

export function getWebinar(webinar?: Partial<WebinarObject>): WebinarObject {
  return {
    id: 1,
    name: "",
    checkoutLink: "",
    description: "",
    days: [],
    times: [],
    duration: 3616,
    isPaid: false,
    registrationFee: 0,
    redirectLink: "",
    replayExpiration: 0,
    status: WebinarStatus.ACTIVE,
    type: WebinarType.LIVE,
    videoUrl: "https://www.youtube.com/watch?v=2uMc3rNnTo4",
    timezone: "America/Chicago",
    ...webinar,
    settings: getWebinarSettings(webinar?.settings),
  };
}

export function getWebinarSettings(
  settings?: Partial<WebinarSettingsObject>
): WebinarSettingsObject {
  return {
    postRegistrationUrl: "",
    postViewUrl: "",
    registrationPageScript: "",
    thankyouPageScript: "",
    registrationFormScript: "",
    liveRoomScript: "",
    replayRoomScript: "",
    ...settings,
  };
}
