import { EventObject, ViewerEventObject } from "@emsyte/common/webinar";
import * as moment from "moment";
import { getWebinar } from "./webinar";

export function getEvent(event?: Partial<EventObject>): EventObject {
  return {
    canceled: false,
    end: moment().add(1, "hour").toISOString(),
    start: moment().toISOString(),
    id: 4,
    ...event,
  };
}

export function getViewerEvent(
  event?: Partial<ViewerEventObject>
): ViewerEventObject {
  return {
    ...getEvent(event),
    webinar: getWebinar(event?.webinar),
    ...event,
  };
}
