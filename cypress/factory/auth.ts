import * as jwt from "jsonwebtoken";
import { _ } from "../utils";
import { AuthType, AuthUserObject } from "@emsyte/common/auth";
import { APIAuthResponse, UserStatus } from "@emsyte/common/user";

// TODO: Move this to an environment variable
const APP_SECRET = "EYVZR6bc&e7MpkH##^48KCkd3";

export function getAuthResponse(
  user?: Partial<AuthUserObject>
): APIAuthResponse {
  return {
    token: jwt.sign(
      _<AuthUserObject>({
        email: "bill@example.com",
        firstName: "Bill",
        lastName: "Bobby",
        id: 1,
        status: UserStatus.ACTIVE,
        type: AuthType.USER,
        ...user,
      }),
      APP_SECRET
    ),
    refreshToken: "",
    defaultTeam: "team",
  };
}
