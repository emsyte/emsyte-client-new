/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    endpoint<T = any>(
      method: string,
      endpoint: string,
      response: T | ((request: any) => T),
      statusCode?: number
    ): Chainable;
  }
}
