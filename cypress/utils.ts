export function _<T>(value: T): T {
  return value;
}

export function api(suffix: string) {
  return "http://localhost:4000/development" + suffix;
}

export const WEB_SOCKET = "ws://localhost:3001";
