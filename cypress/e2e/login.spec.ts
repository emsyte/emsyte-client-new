context("Login", () => {
  it("should login with credentials", () => {
    cy.server();

    cy.stub(cy.window);

    cy.visit("/login")
      .get("[name=email]")
      .type("johndoe1@example.com")
      .get("[name=password]")
      .type("access")
      .get("[type=submit]")
      .click();

    cy.location("pathname").should("eq", "/dashboard");
  });
});
