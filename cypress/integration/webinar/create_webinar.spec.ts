import { APIResponse } from "@emsyte/common/api";
import { UserTeamObject } from "@emsyte/common/team";
import {
  APIWebinarScheduleData,
  EventObject,
  WebinarObject,
} from "@emsyte/common/webinar";
import * as moment from "moment";
import { getAuthResponse } from "../../factory/auth";
import { getEvent } from "../../factory/event";
import { getTeams } from "../../factory/teams";
import { getWebinar } from "../../factory/webinar";

context("Create Webinar", () => {
  it("create webinar", () => {
    const { token } = getAuthResponse();
    cy.setCookie("em-token", token);

    cy.endpoint<APIResponse<UserTeamObject[]>>(
      "GET",
      "/account/teams",
      getTeams()
    );

    cy.endpoint<APIResponse<EventObject[]>>(
      "POST",
      "/team/webinars/1/events",
      (request: APIWebinarScheduleData) => {
        const expected: APIWebinarScheduleData = {
          timezone: "America/Los_Angeles",
          days: [
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
          ],
          start: moment().format("YYYY-MM-DD"),
          end: moment().format("YYYY-MM-DD"),
          times: ["13:00:00"],
        };
        expect(request).to.eql(expected);
        return { data: [getEvent()] };
      }
    );

    cy.endpoint<APIResponse<WebinarObject>>(
      "POST",
      "/team/webinars",
      (request) => {
        expect(request).to.eql({
          duration: 3616,
          name: "Test Webinar",
          replayExpiration: "0",
          type: "recorded",
          videoUrl: "https://www.youtube.com/watch?v=2uMc3rNnTo4",
        });
        return { data: getWebinar() };
      }
    );

    cy.visit("/webinars/eyJpZCI6bnVsbCwidHlwZSI6InJlY29yZGVkIn0=/basic");
    cy.get("[name=name]").type("Test Webinar");
    cy.get("[name=videoUrl]")
      .clear()
      .invoke("val", "https://www.youtube.com/watch?v=2uMc3rNnTo4")
      .trigger("input");
    cy.get("[name=duration_hours]").should("have.value", 1);
    cy.get("[name=duration_minutes]").should("have.value", 0);
    cy.get("[name=duration_seconds]").should("have.value", 16);

    cy.get("#timezone").click();
    cy.get("#react-select-2-input").type("America/Los Angeles{enter}");

    cy.get("[name=start_time]").type("13:00");
    cy.get("[data-testid=add-start-time]").click();
    cy.get(".rbc-event")
      .should("have.css", "top", `${(13 / 24) * 960}px`)
      .should("have.text", "1:00 PM – 2:00 PM");

    cy.get("[type=submit]").click();
  });
});
