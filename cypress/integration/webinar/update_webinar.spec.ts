import { APIResponse } from "@emsyte/common/api";
import { UserTeamObject } from "@emsyte/common/team";
import { WebinarObject } from "@emsyte/common/webinar";
import { getAuthResponse } from "../../factory/auth";
import { getTeams } from "../../factory/teams";
import { getWebinar } from "../../factory/webinar";

context("Basic", () => {
  it("Display times correctly", () => {
    const { token } = getAuthResponse();
    cy.setCookie("em-token", token);

    cy.endpoint<APIResponse<UserTeamObject[]>>(
      "GET",
      "/account/teams",
      getTeams()
    );

    cy.endpoint<APIResponse<WebinarObject>>("GET", "/team/webinars/1", {
      data: getWebinar({
        timezone: "America/Chicago",
        times: ["13:00:00"],
      }),
    });

    cy.visit("/webinars/eyJpZCI6MX0=/basic");

    cy.get("#timezone").should("have.text", "America/Chicago");

    cy.get(".rbc-event")
      .should("have.css", "top", `${(13 / 24) * 960}px`)
      .should("have.text", "1:00 PM – 2:00 PM");

    cy.get("[type=submit]").click();
  });
});
