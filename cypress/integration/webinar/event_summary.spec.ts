import { APIResponse } from "@emsyte/common/api";
import { UserTeamObject } from "@emsyte/common/team";
import { ViewerEventObject } from "@emsyte/common/webinar";
import { getAuthResponse } from "../../factory/auth";
import { getViewerEvent } from "../../factory/event";
import { getTeams } from "../../factory/teams";

context("Event Summary", () => {
  const suffix = btoa(JSON.stringify({ webinarId: 1, eventId: 4 }));

  it("should show live view", () => {
    cy.viewport("macbook-16");
    const { token } = getAuthResponse();
    cy.setCookie("em-token", token);

    cy.endpoint<APIResponse<UserTeamObject[]>>(
      "GET",
      "/account/teams",
      getTeams()
    );

    cy.endpoint<APIResponse<ViewerEventObject>>(
      "GET",
      "/team/webinars/1/events/4",
      {
        data: getViewerEvent(),
      }
    );

    cy.endpoint("GET", "/team/analytics/event/4", {
      data: {
        attendance: {
          registered: 1,
          attendees: 1,
          avg_watch_time: 1,
        },
        reactionCount: 1,
      },
    });

    cy.visit("/events/" + suffix);

    cy.get("[data-testid=view-live-event]")
      .should("contain.text", "View Live Event")
      .should("have.attr", "href", "/webinar/watch/" + suffix);
  });
});
