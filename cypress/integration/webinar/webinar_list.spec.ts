import { APIResponse } from "@emsyte/common/api";
import { UserTeamObject } from "@emsyte/common/team";
import { ViewerEventObject, WebinarObject } from "@emsyte/common/webinar";
import * as moment from "moment";
import { getAuthResponse } from "../../factory/auth";
import { getViewerEvent } from "../../factory/event";
import { getTeams } from "../../factory/teams";
import { getWebinar } from "../../factory/webinar";

context("Webinar List", () => {
  it("Should list webinar events in calendar", () => {
    const { token } = getAuthResponse();
    cy.setCookie("em-token", token);

    cy.endpoint<APIResponse<UserTeamObject[]>>(
      "GET",
      "/account/teams",
      getTeams()
    );

    const start = moment();
    start.set("hour", 10);
    start.set("minute", 0);
    start.set("second", 0);

    cy.endpoint<APIResponse<ViewerEventObject[]>>(
      "GET",
      "/team/webinars/events",
      {
        data: [getViewerEvent({ start: start.toISOString() })],
      }
    );

    cy.visit("/webinars");

    cy.get("#view-week").click();

    cy.get(".rbc-event")
      .should("to.have.length", 1)
      .parent()
      .invoke("css", "height")
      .then((height) =>
        cy
          .get(".rbc-event")
          .should(
            "have.css",
            "top",
            `${(parseInt((height as unknown) as string) * 10) / 24}px`
          )
      );
  });

  it("Should list webinars in agenda view", () => {
    const { token } = getAuthResponse();
    cy.setCookie("em-token", token);

    cy.endpoint<APIResponse<UserTeamObject[]>>(
      "GET",
      "/account/teams",
      getTeams()
    );

    cy.endpoint<APIResponse<WebinarObject[]>>("GET", "/team/webinars", {
      data: [
        getWebinar({
          name: "Test Webinar",
        }),
      ],
    });

    cy.visit("/webinars");

    cy.get("#view-agenda").click();
    cy.location("search").should("eq", "?view=agenda");
    cy.get("[data-testid=webinar-1]")
      .should("have.length", 1)
      .get("[data-testid=webinar-name]")
      .should("have.text", "Test Webinar");
  });
});
