import { getAuthResponse } from "../../factory/auth";
import { getTeams } from "../../factory/teams";
import { api } from "../../utils";

context("Login", () => {
  it("show's error when failed to login", () => {
    cy.server();
    cy.route({
      method: "POST",
      url: api(`/users/login`),
      status: 401,
      response: {
        message: "Incorrect Email or Password.",
      },
    });

    cy.visit("/login")
      .get("[name=email]")
      .type("bill@example.com")
      .get("[name=password]")
      .type("password")
      .get("[type=submit]")
      .click();

    cy.get("[data-testid='login-error']").should(
      "contain.text",
      "Incorrect Email or Password."
    );
  });

  it("should login with credentials", () => {
    cy.server();
    cy.route("POST", api("/users/login"), getAuthResponse());
    cy.route(api("/account/teams"), getTeams());

    cy.stub(cy.window);

    cy.visit("/login")
      .get("[name=email]")
      .type("bill@example.com")
      .get("[name=password]")
      .type("password")
      .get("[type=submit]")
      .click();

    cy.location("pathname").should("eq", "/dashboard");
  });
});
