import { APIResponse } from "@emsyte/common/api";
import { ViewerEventObject } from "@emsyte/common/webinar";
import * as moment from "moment";
import { getViewerEvent } from "../../factory/event";
import { getWebinar, getWebinarSettings } from "../../factory/webinar";
import { api, _ } from "../../utils";

context("Live Viewer", () => {
  const suffix = btoa(JSON.stringify({ webinarId: 1, eventId: 4 }));

  it("should include injected script", () => {
    cy.server();
    cy.route(
      "GET",
      api("/viewer/4/event"),
      _<APIResponse<ViewerEventObject>>({
        data: getViewerEvent({
          webinar: getWebinar({
            settings: getWebinarSettings({
              replayRoomScript: "<script>testFn()</script>",
            }),
          }),
        }),
      })
    );
    const testFn = cy.stub();

    cy.visit("/webinar/replay/" + suffix, {
      onBeforeLoad: (win) => {
        (win as any).testFn = testFn;
      },
    });

    cy.wrap(testFn).should("have.been.called");
  });
});
