import { APIResponse } from "@emsyte/common/api";
import { ViewerEventObject } from "@emsyte/common/webinar";
import * as moment from "moment";
import { getViewerEvent } from "../../factory/event";
import { getWebinar, getWebinarSettings } from "../../factory/webinar";
import { api, _ } from "../../utils";

context("Live Viewer", () => {
  const suffix = btoa(JSON.stringify({ webinarId: 1, eventId: 4 }));

  it("should show countdown if early", () => {
    cy.server();
    cy.route(
      "GET",
      api("/viewer/4/event"),
      _<APIResponse<ViewerEventObject>>({
        data: getViewerEvent({
          webinar: getWebinar({
            name: "Test Webinar",
          }),
          start: moment().add(5, "minutes").toISOString(),
        }),
      })
    );

    cy.visit("/webinar/watch/" + suffix);

    cy.get("[data-testid=title").should(
      "contain.text",
      "This event will start soon"
    );
    cy.get("[data-testid=countdown]").should("exist");

    cy.wait(100);

    // Timer should read 5 minutes
    cy.get(".days > .v-row").should("contain.text", "00");
    cy.get(".hours > .v-row").should("contain.text", "00");
    cy.get(".minutes > .v-row").should("contain.text", "04");

    cy.get("[data-testid=open-learn-more]").click();
    cy.get(".modal-title").should(
      "have.text",
      "Why we need access to your camera?"
    );
    cy.get("[data-testid=close-learn-more]").click();
    cy.wait(1000);
    cy.get(".modal-title").should("not.exist");
  });

  it("allows you to enter after it's started", () => {
    cy.viewport("macbook-16");
    cy.server();
    cy.route(
      "GET",
      api("/viewer/4/event"),
      _<APIResponse<ViewerEventObject>>({
        data: getViewerEvent(),
      })
    );
    cy.visit("/webinar/watch/" + suffix);

    cy.get("[data-testid=enter-room]")
      .should("contain.text", "Enter Live Room")
      .click();

    cy.wait(1000);

    cy.get("iframe")
      .its("0.src")
      .should("match", /^https:\/\/www.youtube.com\/embed\/2uMc3rNnTo4/);

    // cy.get("[data-testid=sidebar").should("have.css", "width", "400px");
    // cy.get("[data-testid=btn-show-chat]").click();
    // cy.get("[data-testid=sidebar").should("have.css", "width", "0px");

    // cy.get("[data-testid=btn-show-chat]").click();
    // cy.get("[data-testid=sidebar").should("have.css", "width", "400px");
  });

  it("should redirect after event is over", () => {
    cy.server();
    cy.route(
      "GET",
      api("/viewer/4/event"),
      _<APIResponse<ViewerEventObject>>({
        data: getViewerEvent({
          start: moment()
            .subtract({
              hour: 1,
              minute: 1,
            })
            .toISOString(),
          webinar: getWebinar({
            settings: getWebinarSettings({
              postViewUrl:
                "http://localhost:3000/webinar/thankyou/eyJpZCI6MX0=",
            }),
          }),
        }),
      })
    );

    cy.visit("/webinar/watch/" + suffix);
    cy.get("[data-testid=enter-room]").click();
    cy.get("[data-testid=end-notice]").should(
      "contain.text",
      "The webinar has ended"
    );
    cy.wait(1000);
    cy.location()
      .url()
      .should("eq", "http://localhost:3000/webinar/thankyou/eyJpZCI6MX0=");
  });

  it("should include injected script", () => {
    cy.server();
    cy.route(
      "GET",
      api("/viewer/4/event"),
      _<APIResponse<ViewerEventObject>>({
        data: getViewerEvent({
          webinar: getWebinar({
            settings: getWebinarSettings({
              liveRoomScript: "<script>testFn()</script>",
            }),
          }),
        }),
      })
    );
    const testFn = cy.stub();

    cy.visit("/webinar/watch/" + suffix, {
      onBeforeLoad: (win) => {
        (win as any).testFn = testFn;
      },
    });

    cy.wrap(testFn).should("not.have.been.called");

    cy.get("[data-testid=enter-room]").click();

    cy.wrap(testFn).should("have.been.called");
  });

  it("injected script not trigger event that hasn't happened yet", () => {
    cy.server();
    cy.route(
      "GET",
      api("/viewer/4/event"),
      _<APIResponse<ViewerEventObject>>({
        data: getViewerEvent({
          webinar: getWebinar({
            settings: getWebinarSettings({
              liveRoomScript:
                "<script>justWebinar.addTrigger('watchTime', 0.5, testFn)</script>",
            }),
          }),
        }),
      })
    );
    const testFn = cy.stub();

    cy.visit("/webinar/watch/" + suffix, {
      onBeforeLoad: (win) => {
        (win as any).testFn = testFn;
      },
    });

    cy.wrap(testFn).should("not.have.been.called");

    cy.get("[data-testid=enter-room]").click();

    cy.wrap(testFn).should("not.have.been.called");
  });

  it("injected script should trigger event", () => {
    cy.server();
    cy.route(
      "GET",
      api("/viewer/4/event"),
      _<APIResponse<ViewerEventObject>>({
        data: getViewerEvent({
          start: moment().subtract("30", "minutes").toISOString(),
          end: moment().add("30", "minutes").toISOString(),
          webinar: getWebinar({
            settings: getWebinarSettings({
              liveRoomScript:
                "<script>justWebinar.addTrigger('watchTime', 0.5, testFn)</script>",
            }),
          }),
        }),
      })
    );
    const testFn = cy.stub();

    cy.visit("/webinar/watch/" + suffix, {
      onBeforeLoad: (win) => {
        (win as any).testFn = testFn;
      },
    });

    cy.wrap(testFn).should("not.have.been.called");

    cy.get("[data-testid=enter-room]").click();

    cy.wrap(testFn).should("have.been.called");
  });
});
