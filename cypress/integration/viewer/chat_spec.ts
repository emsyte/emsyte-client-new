import { APIResponse } from "@emsyte/common/api";
import { ViewerEventObject } from "@emsyte/common/webinar";
import { Server, WebSocket } from "mock-socket";
import { getViewerEvent } from "../../factory/event";
import { api, WEB_SOCKET, _ } from "../../utils";
import * as jwt from "jsonwebtoken";
import { AuthType } from "@emsyte/common/auth";
import {
  LabelType,
  MessageEvent,
  MessageEventType,
} from "@emsyte/common/message";

context("Chat", () => {
  const suffix =
    btoa(JSON.stringify({ webinarId: 1, eventId: 4 })) +
    "?token=" +
    jwt.sign({ id: 1, type: AuthType.ATTENDEE }, "not-secret");

  const mockServer = new Server(WEB_SOCKET);

  // Echo chat messages
  mockServer.on("connection", (socket) => {
    socket.on("message", (data) => {
      const request = JSON.parse(data as string);
      console.log(request);

      const { action, content, sendTo } = request;

      if (action === "sendMessage") {
        const event: MessageEvent = {
          event: MessageEventType.MESSAGE,
          roomId: sendTo,
          message: {
            content,
            userId: "attendee-1",
            name: "Bill",
            ownMessage: true,
            question: request.question,
            label: LabelType.ATTENDEE,
            messageId: new Date().valueOf() + "-" + Math.random(),
          },
        };

        socket.send(JSON.stringify(event));
      }
    });
  });

  it("correctly displays received chat messages", () => {
    cy.endpoint<APIResponse<ViewerEventObject>>("GET", "/viewer/4/event", {
      data: getViewerEvent(),
    });

    cy.visit("/webinar/watch/" + suffix, {
      onBeforeLoad: (win) => {
        cy.stub(win, "WebSocket", (url) => {
          return new WebSocket(url);
        });
      },
    });

    cy.get("[data-testid=enter-room]").click();

    cy.wait(10).then(() => {
      const event: MessageEvent = {
        event: MessageEventType.MESSAGE,
        roomId: "general",
        message: {
          content: "Hello World",
          userId: "attendee-2",
          name: "Joe",
          ownMessage: false,
          question: false,
          label: LabelType.ATTENDEE,
          messageId: new Date().valueOf() + "-" + Math.random(),
        },
      };
      mockServer.emit("message", JSON.stringify(event));
    });

    cy.wait(10).then(() => {
      const event: MessageEvent = {
        event: MessageEventType.MESSAGE,
        roomId: "general",
        message: {
          content: "This is a test",
          userId: "attendee-2",
          name: "Mary",
          ownMessage: false,
          question: true,
          label: LabelType.ATTENDEE,
          messageId: new Date().valueOf() + "-" + Math.random(),
        },
      };
      mockServer.emit("message", JSON.stringify(event));
    });

    cy.wait(10).then(() => {
      const event: MessageEvent = {
        event: MessageEventType.MESSAGE,
        roomId: "general",
        message: {
          content: "Hello!",
          userId: "user-1",
          name: "Sue",
          ownMessage: false,
          question: false,
          label: LabelType.PRESENTER,
          messageId: new Date().valueOf() + "-" + Math.random(),
        },
      };
      mockServer.emit("message", JSON.stringify(event));
    });

    cy.wait(10).then(() => {
      const event: MessageEvent = {
        event: MessageEventType.MESSAGE,
        roomId: "general",
        message: {
          content: "Support team here",
          userId: "user-1",
          name: "Edger",
          ownMessage: false,
          question: false,
          label: LabelType.SUPPORT,
          messageId: new Date().valueOf() + "-" + Math.random(),
        },
      };
      mockServer.emit("message", JSON.stringify(event));
    });

    cy.wait(10).then(() => {
      const event: MessageEvent = {
        event: MessageEventType.MESSAGE,
        roomId: "general",
        message: {
          content: "It's me!",
          userId: "attendee-3",
          name: "Bill",
          ownMessage: true,
          question: false,
          label: LabelType.ATTENDEE,
          messageId: new Date().valueOf() + "-" + Math.random(),
        },
      };
      mockServer.emit("message", JSON.stringify(event));
    });

    cy.wait(10);

    cy.get(".message").its("length").should("eq", 5);

    cy.get(".message").eq(4).find(".name").should("contain.text", "Joe");
    cy.get(".message")
      .eq(4)
      .find(".text")
      .should("contain.text", "Hello World");
    cy.get(".message").eq(4).find(".question").should("not.exist");

    cy.get(".message").eq(3).find(".name").should("contain.text", "Mary");
    cy.get(".message")
      .eq(3)
      .find(".text")
      .should("contain.text", "This is a test");
    cy.get(".message").eq(3).find(".question").should("exist");

    cy.get(".message").eq(2).find(".name").should("contain.text", "Sue");
    cy.get(".message").eq(2).find(".label").should("contain.text", "Host");
    cy.get(".message").eq(2).find(".text").should("contain.text", "Hello!");
    cy.get(".message").eq(2).find(".question").should("not.exist");

    cy.get(".message").eq(1).find(".name").should("contain.text", "Edger");
    cy.get(".message").eq(1).find(".label").should("contain.text", "Host");
    cy.get(".message").eq(1).find(".question").should("not.exist");
    cy.get(".message")
      .eq(1)
      .find(".text")
      .should("contain.text", "Support team here");

    cy.get(".message").eq(0).find(".name").should("contain.text", "Bill");
    cy.get(".message").eq(0).find(".text").should("contain.text", "It's me!");
    cy.get(".message").eq(0).find(".question").should("not.exist");
    cy.get(".message").eq(0).parent().should("have.class", "own-message");
  });

  it("can send chat messages", () => {
    cy.endpoint<APIResponse<ViewerEventObject>>("GET", "/viewer/4/event", {
      data: getViewerEvent(),
    });

    cy.visit("/webinar/watch/" + suffix, {
      onBeforeLoad: (win) => {
        cy.stub(win, "WebSocket", (url) => {
          return new WebSocket(url);
        });
      },
    });

    cy.get("[data-testid=enter-room]").click();

    cy.get("form input[name=chat-message]").type("Hello World{enter}");
    cy.get(".message").find(".name").should("contain.text", "Bill");
    cy.get(".message").find(".text").should("contain.text", "Hello World");
    cy.get(".message").parent().should("have.class", "own-message");
  });

  it("displays good on small devices", () => {
    cy.viewport("iphone-x");
    cy.endpoint<APIResponse<ViewerEventObject>>("GET", "/viewer/4/event", {
      data: getViewerEvent(),
    });
    cy.visit("/webinar/watch/" + suffix);

    cy.get("[data-testid=enter-room]").click();
    cy.get("[data-testid=sidebar]").should("have.css", "height", "41px");
    cy.get("[data-testid=btn-show-chat]").should("not.be.visible");
    cy.get("[data-testid=btn-show-chat-mobile]").should("be.visible").click();
    cy.get("[data-testid=sidebar]")
      .should("have.css", "height")
      .and("match", /7[0-9]{2}px/);
  });

  it("displays good on medium devices", () => {
    cy.viewport("ipad-2", "landscape");
    cy.endpoint<APIResponse<ViewerEventObject>>("GET", "/viewer/4/event", {
      data: getViewerEvent(),
    });
    cy.visit("/webinar/watch/" + suffix);

    cy.get("[data-testid=enter-room]").click();
    cy.get("[data-testid=sidebar]").should("have.css", "width", "300px");
    cy.get("[data-testid=btn-show-chat-mobile]").should("not.be.visible");
    cy.get("[data-testid=btn-show-chat]").should("be.visible").click();
    cy.get("[data-testid=sidebar]").should("have.css", "width", "0px");
  });

  it("displays good on large devices", () => {
    cy.viewport("macbook-16");
    cy.endpoint<APIResponse<ViewerEventObject>>("GET", "/viewer/4/event", {
      data: getViewerEvent(),
    });
    cy.visit("/webinar/watch/" + suffix);

    cy.get("[data-testid=enter-room]").click();
    cy.get("[data-testid=sidebar]").should("have.css", "width", "400px");

    cy.get("[data-testid=btn-show-chat-mobile]").should("not.be.visible");
    cy.get("[data-testid=btn-show-chat]").should("be.visible").click();

    cy.get("[data-testid=sidebar]").should("have.css", "width", "0px");
  });

  it("development", () => {
    cy.endpoint<APIResponse<ViewerEventObject>>("GET", "/viewer/4/event", {
      data: getViewerEvent(),
    });

    cy.visit("/webinar/watch/" + suffix, {
      onBeforeLoad: (win) => {
        cy.stub(win, "WebSocket", (url) => {
          return new WebSocket(url);
        });
      },
    });

    cy.get("[data-testid=enter-room]").click();
  });
});
