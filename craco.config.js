const SentryWebpackPlugin = require("@sentry/webpack-plugin");
const { whenProd } = require("@craco/craco");

const commitHash = require("child_process")
  .execSync("git rev-parse HEAD")
  .toString()
  .trim();

module.exports = {
  plugins: whenProd(
    new SentryWebpackPlugin({
      // sentry-cli configuration
      // authToken: process.env.SENTRY_AUTH_TOKEN,

      // TODO: This is bad and you know it
      authToken:
        "aa1dc61c9ebd4e4497c77b15ba64b8723cbc65a3f341493f8a98dfb87d089055",

      org: "emsyte-llc",
      project: "emsyte-client-new",
      release: commitHash,

      // webpack specific configuration
      include: ".",
      ignore: ["node_modules", "craco.config.js"],
    })
  ),
};
